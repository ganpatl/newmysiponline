//
//  WebServices.h
//  MySIPOnline
//
//  Created by Rahul Saini on 20/01/17.
//  Copyright © 2017 Rahul Saini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface WebServices : NSObject

+(void)api_getSettingsWithCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_checkMobileEmail:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getCountryCodeList:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_generateOtpData:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_register:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_loginData:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getLoginUserWithId:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_resetPassword:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_liveMarket:(NSDictionary *)param AndCompletionHandler:(void(^) (id response, bool isError)) completionHandler;
+(void)api_latestUpdate:(NSDictionary *)param AndCompletionHandler:(void(^) (id response, bool isError)) completionHandler;
+(void)api_middleSliders:(NSDictionary *)param AndCompletionHandler:(void(^) (id response, bool isError)) completionHandler;
+(void)api_getFundExplorerScreenDataWithCompletionHandler:(void(^) (id response, bool isError)) completionHandler;
+(void)api_getRecomendFundDivision:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getFundListByExpert:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getCategoryList:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getAMCList:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getFundDetail:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getNavDetail:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;
+(void)api_getInvestmentFormData:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;



+(void)api_getVideoList:(NSDictionary *)param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler;

// TEst
+(void)api_CallWithCompletionHandler :(void(^) (id response, bool isError)) completionHandler;

@end


