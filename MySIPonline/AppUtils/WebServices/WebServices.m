//
//  WebServices.m
//  MySIPOnline
//
//  Created by Rahul Saini on 20/01/17.
//  Copyright © 2017 Rahul Saini. All rights reserved.
//

#import "WebServices.h"
#import "AFNetworking.h"
//#import <Paynimolib/AFNetworkActivityIndicatorManager.h>

@implementation WebServices

+(AFHTTPSessionManager*)getOperationManager {
    AFHTTPSessionManager * manager = [AFHTTPSessionManager manager];
    manager.responseSerializer = [AFJSONResponseSerializer new];
    manager.responseSerializer.acceptableContentTypes = [NSSet setWithObjects:@"application/json", @"text/json", @"text/javascript", @"text/html", @"text/plain", nil];
    return manager;
}

+(void)api_getSettingsWithCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_Settings];
    [manager GET:strURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_checkMobileEmail:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_CheckMobileEmail];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getCountryCodeList:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_CountryCode];
    [manager GET:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_generateOtpData:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_generateOtp];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_register:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_Register];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_loginData:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_LoginData];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getLoginUserWithId:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, LoginUserDataAPI];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_resetPassword:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_ResetPasswordData];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_liveMarket:(NSDictionary *)param AndCompletionHandler:(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_LiveMarket];
    [manager GET:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_latestUpdate:(NSDictionary *)param AndCompletionHandler:(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_LatestUpdate];
    [manager GET:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_middleSliders:(NSDictionary *)param AndCompletionHandler:(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_MiddleSliders];
    [manager GET:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}


+(void)api_getFundExplorerScreenDataWithCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
//    AFHTTPSessionManager *manager = [AFHTTPSessionManager manager];
//    manager.responseSerializer = [AFHTTPResponseSerializer serializer];
//    AFJSONRequestSerializer *requestSerializer = [AFJSONRequestSerializer serializer];
//    [requestSerializer setValue:@"application/json" forHTTPHeaderField:@"Content-Type"];
//    manager.requestSerializer = requestSerializer;
//    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_RecomendFunddivision];
//    NSLog(@"URL: %@", strURL);
//
//    [manager GET:strURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
//        // Success
//        NSString *response = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//        NSLog(@"Success response: %@", response);
//    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
//        //
//        //NSHTTPURLResponse *response = (NSHTTPURLResponse *)operation.response;
//        NSLog(@"Failure response: %@", error.localizedDescription);
//    }];
    
//    [manager GET:strURL
//      parameters:nil
//        progress:nil
//         success:^(NSURLSessionDataTask *operation, id responseObject) {
//             NSString *response = [[NSString alloc] initWithData:responseObject encoding:NSUTF8StringEncoding];
//             NSLog(@"Success response: %@", response);
//         }
//         failure:^(NSURLSessionDataTask *operation, NSError *error) {
//             NSHTTPURLResponse *response = (NSHTTPURLResponse *)operation.response;
//             NSLog(@"Failure response: %@", response);
//         }];
    
    
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_RecomendFunddivision];
    [manager GET:strURL parameters:nil headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getRecomendFundDivision:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@", API_BaseURL, API_RecomendFunddivision];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getFundListByExpert:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@",API_BaseURL, API_GetFundListByExpert];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getCategoryList:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@",API_BaseURL, API_GetCategoryList];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getAMCList:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@",API_BaseURL, API_RecomendFunddivision];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getFundDetail:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@",API_BaseURL, API_FundDetail];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getNavDetail:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@",API_BaseURL, API_NavDetails];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}

+(void)api_getInvestmentFormData:(NSDictionary *) param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"%@%@",API_BaseURL, API_InvestmentForm];
    [manager POST:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}








+(void)api_getVideoList:(NSDictionary *)param AndCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = @"https://www.googleapis.com/youtube/v3/search?key=AIzaSyAH8ysxb8B_v0WmOihv7i6zSVMUmHosvxs&channelId=UCa8MSxsDY8kaSasMB5GW6wQ&part=snippet,id&order=date&maxResults=20";
    [manager GET:strURL parameters:param headers:nil progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        completionHandler(error, YES);
    }];
}


+(void)api_CallWithCompletionHandler :(void(^) (id response, bool isError)) completionHandler {
    AFHTTPSessionManager * manager = [WebServices getOperationManager];
    NSString * strURL = [NSString stringWithFormat:@"http://rishta.epizy.com/api_v1/login.php"];
    NSDictionary *dicHeader = @{@"Cookie":@"__test=ecc30c81c6b3e11a1096bd9e44c9bc52; expires=Friday, 1 January 2038 at 05:25:55; path=/"};
    [manager GET:strURL parameters:nil headers:dicHeader progress:nil success:^(NSURLSessionDataTask * _Nonnull task, id  _Nullable responseObject) {
        NSLog(@"%@", responseObject);
        completionHandler(responseObject, NO);
    } failure:^(NSURLSessionDataTask * _Nullable task, NSError * _Nonnull error) {
        NSLog(@"%@", error);
        completionHandler(error, YES);
    }];
}


@end
