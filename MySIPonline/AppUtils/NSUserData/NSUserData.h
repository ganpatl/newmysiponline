//
//  NSUserData.h
//  MySIPOnline
//
//  Created by Rahul Saini on 20/01/17.
//  Copyright © 2017 Rahul Saini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSUserData : NSObject

+(void)SaveSettingsData:(NSDictionary*)Data;
+(NSDictionary*)GetSettingsData;

+(void)SaveLoginData:(id)Data;
+(id)GetLoginData;

+(NSString *)GetKeyValue;
+(void)SaveKeyValue:(NSString *)key;

+(void)SaveEditProfileDataAPI:(NSMutableDictionary *)Data;
+(NSMutableDictionary *)GetEditProfileDataAPI;
+(void)DeleteEditProfileDataAPI;

+(void)SaveInvestStatus:(NSString *)Data;
+(NSString *)GetInvestStatus;

+(void)SaveProfileStatus:(NSString *)Data;
+(NSString *)GetProfileStatus;

+(void)SaveSortFilterData:(NSDictionary*)Data;
+(NSDictionary*)GetSortFilterData;

+(void)SaveAppliedSortFilterData:(NSDictionary*)Data;
+(NSDictionary*)GetAppliedSortFilterData;

//+(void)SaveLoginAuthCheck :(NSString *) data;
//+(NSString *)GetSaveLoginAuth;
//+(void)saveLoginValue:(NSString *)data;
//+(NSString *)getLoginValue;
//+(void)DeleteLoginValue;
//+(void) DeleteLoginDateAPI;
//+(void) SaveRandomNo : (int) value;
//+(int) GetRandomNo;
//+(void)SaveProfileDetails :(id) data;
//+(NSMutableDictionary *)GetProfileDetails;
//+(void)deleteProfileDetails;
////+(void) SaveScreenNavigation : (int) value;
////+(int) GetScreenNavigation;
////+(void) DeleteScreenNavigation;
//+(void)SaveTotalTransactionDataAPI:(NSMutableArray *) Data;
//+(NSMutableArray *)GetTotalTransactionData;
//+(void)DeletetotalTransactionData;
//+(void) SaveID: (NSString *) data;
//+(NSString *) GetID;
//+(void)DeleteSaveID;
//+(void)SaveWealthData:(NSMutableDictionary *) Data;
//+(NSMutableDictionary *)GetWealthData;
//+(void)DeleteWealthData;
//+(void)SaveWithoutID :(NSString *)Data;
//+(NSString *)GewithoutID;
//+(void) DeleteWithoutLoginDateAPI;
//+(void)SaveFundIDwithoutLogin:(NSString *)data;
//+(NSString *)GetFundIdWithoutLogin;
//+(void)DeleteFundIdWithoutLogin;
//+(void)datasaveforchartwealth:(NSDictionary *)data;
//+(NSDictionary *)getdataforwealth;
//+(void)DeleteSavedataWealth;
//+(void) PushNotificationToken : (NSString *) param;
//+(NSString *) GetPushNotififcationToken;
//+(void)savePaymentDashboardValue:(NSString *)data;
//+(NSString *)getPaymentDashboardValue;
//+(void)DeletePaymentDashboardValue;
//
//+(void)saveredeemswitchstp:(NSString *)data;
//+(NSString *)getredeemswitchstp;
//+(void)Deleteredeemswitchstp;
//
//+(void) saveProfileLockData : (NSDictionary *) parms;
//+(NSDictionary *) GetProfileLockData;
//+(void)saveInvIdValue:(NSString *)data;
//+(NSString *)getInvIdValue;
//
//+(void) Savedebitmandatestatus: (NSString *) data;
//+(NSString *) GetSavedebitmandatestatus;
//+(void)DeleteSaveSavedebitmandatestatus;
//+(void) SaveAmountCodePayment : (NSString *) key;
//+(NSString *) GetAmountCodePayment;
//
//+(void) ProfileslistData : (NSDictionary *) parms;
//+(NSDictionary *) getProfileslistData;
//+(void) DeleteProfileslistData;
//+(void) SaveTouchIdAPI :(id) Data;
//+(id)GetTouchIdAPI;
//+(void) DeleteTouchIdAPI;
//
//+(void) SavePopUpOneTime :(int) Data;
//+(int)GetPopUpOneTime;
//+(void) DeletePopUpOneTime;
//
//+(void) SaveSlideImgData :(id) Data;
//+(id)GetSlideImgData;
//+(void) DeleteSlideImgData;
//
//+(void) SaveActionPopUpOneTime :(int) Data;
//+(int)GetActionPopUpOneTime;
//+(void) DeleteActionPopUpOneTime;
//
//+(void) SaveCartPageOpenBuy : (int) value;
//+(int) GetCartPageOpenBuy;
//+(void) DeleteCartPageOpenBuy;
//
//+(void)SaveRegisterPopUp :(NSString *)Data;
//+(NSString *)GetRegisterPopUp;
//+(void) DeleteRegisterPopUp;
//
//+(void)SaveRedirectToFundList:(NSString*)str;
//+(NSString*)GetRedirectToFundList;
//
//+(void)SaveRedirectionPage:(NSDictionary*)data;
//+(NSDictionary*)GetRedirectionPage;
//+(void)DeleteRedirectionPage;
//
//+(void)SaveisComeFromInsurance:(NSString *)str;
//+(NSString *)GetisComeFromInsurance;
//
//+(void)SaveSipOtpOpen: (int)value;
//+(int)GetSipOtpOpen;
//+(void)DeleteSipOtpOpen;
//
//+(void)SaveSipIncreaseData:(NSDictionary*)data;
//+(NSDictionary*)GetSipIncreaseData;
//
//+(void)SaveMainInvestorPAN:(NSString *)Data;
//+(NSString *)GetMainInvestorPAN;

@end
