//
//  NSUserData.m
//  MySIPOnline
//
//  Created by Rahul Saini on 20/01/17.
//  Copyright © 2017 Rahul Saini. All rights reserved.
//

#import "NSUserData.h"

@implementation NSUserData

+(void)SaveSettingsData:(NSDictionary*)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveSettingsData"];
    [defaults synchronize];
}

+(NSDictionary*)GetSettingsData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *response = [defaults objectForKey:@"SaveSettingsData"];
    return response;
}

+(void)SaveLoginData:(id)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveLoginDataAPI"];
    [defaults synchronize];
}

+(id)GetLoginData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    id response = [defaults valueForKey:@"SaveLoginDataAPI"];
    return response;
}

+(NSString *) GetKeyValue {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *response = [defaults valueForKey:@"keyValueShow"];
    return response;
}
+(void)SaveKeyValue:(NSString *)key {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:key forKey:@"keyValueShow"];
    [defaults synchronize];
}

+(void)SaveEditProfileDataAPI:(NSMutableDictionary *)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveEditProfileDataAPI"];
    [defaults synchronize];
}
+(NSMutableDictionary *)GetEditProfileDataAPI {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSMutableDictionary *response =[defaults valueForKey:@"SaveEditProfileDataAPI"];
    return response;
}
+(void)DeleteEditProfileDataAPI {
    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveEditProfileDataAPI"];
    [[NSUserDefaults standardUserDefaults] synchronize];
}

+(void)SaveInvestStatus:(NSString *)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveInvestStatus"];
    [defaults synchronize];
}
+(NSString *)GetInvestStatus {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *response = [defaults valueForKey:@"SaveInvestStatus"];
    return response;
}

+(void)SaveProfileStatus:(NSString *)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveProfileStatus"];
    [defaults synchronize];
}
+(NSString *)GetProfileStatus {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *response = [defaults valueForKey:@"SaveProfileStatus"];
    return response;
}

+(void)SaveSortFilterData:(NSDictionary*)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveSortFilterData"];
    [defaults synchronize];
}

+(NSDictionary*)GetSortFilterData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *response = [defaults objectForKey:@"SaveSortFilterData"];
    return response;
}

+(void)SaveAppliedSortFilterData:(NSDictionary*)Data {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    [defaults setObject:Data forKey:@"SaveAppliedSortFilterData"];
    [defaults synchronize];
}

+(NSDictionary*)GetAppliedSortFilterData {
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *response = [defaults objectForKey:@"SaveAppliedSortFilterData"];
    return response;
}

//
//+(void)saveLoginValue:(NSString *)data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"saveLoginValue"];
//    [defaults synchronize];
//}
//
//+(NSString *)getLoginValue
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"saveLoginValue"];
//    return response;
//}
//
//+(void)DeleteLoginValue
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveLoginValue"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//
//
//+(void) DeleteLoginDateAPI
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveLoginDataAPI"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) SaveTouchIdAPI :(id) Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:Data forKey:@"SaveTouchIdAPI"];
//    [defaults synchronize];
//}
//
//+(id)GetTouchIdAPI
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"SaveTouchIdAPI"];
//    return response;
//}
//
//+(void) DeleteTouchIdAPI
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveTouchIdAPI"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveLoginAuthCheck :(NSString*) data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"SaveLoginAuthChecked"];
//    [defaults synchronize];
//
//}
//+(NSString*)GetSaveLoginAuth
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *value =[defaults valueForKey:@"SaveLoginAuthChecked"];
//    return value;
//
//}
//
//+(void) SaveRandomNo : (int) value
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@(value) forKey:@"SaveRandomNoValue"];
//    [defaults synchronize];
//}
//
//+(int) GetRandomNo
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int response =[[defaults valueForKey:@"SaveRandomNoValue"] intValue];
//    return response;
//
//}
//
//+(void)SaveProfileDetails :(id) data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *SaveUserDetail = [NSKeyedArchiver archivedDataWithRootObject:data];
//    [defaults setObject:SaveUserDetail forKey:@"SaveProfileDetails"];
//    [defaults synchronize];
//}
//
//+(NSMutableDictionary *)GetProfileDetails
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *SaveUserDetail = [defaults objectForKey:@"SaveProfileDetails"];
//    NSMutableDictionary * response = [NSKeyedUnarchiver unarchiveObjectWithData:SaveUserDetail];
//    return response;
//}
//
//+(void)deleteProfileDetails
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveProfileDetails"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
////+(void) SaveScreenNavigation : (int) value
////{
////    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
////    [defaults setObject:@(value) forKey:@"SaveScreenNavigationValue"];
////    [defaults synchronize];
////}
////
////+(int) GetScreenNavigation
////{
////    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
////    int response =[[defaults valueForKey:@"SaveScreenNavigationValue"] intValue];
////    return response;
////}
////
////+(void) DeleteScreenNavigation
////{
////    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveScreenNavigationValue"];
////    [[NSUserDefaults standardUserDefaults] synchronize];
////}
//
//+(void)SaveTotalTransactionDataAPI:(NSMutableArray *) Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *SaveUserDetail = [NSKeyedArchiver archivedDataWithRootObject:Data];
//    [defaults setObject:SaveUserDetail forKey:@"SaveTotalTransactionDataAPI"];
//    [defaults synchronize];
//}
//+(NSMutableArray *)GetTotalTransactionData
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *SaveUserDetail = [defaults objectForKey:@"SaveTotalTransactionDataAPI"];
//    NSMutableArray * response = [NSKeyedUnarchiver unarchiveObjectWithData:SaveUserDetail];
//    return response;
//}
//
//+(void) SaveID: (NSString *) data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"SaveID"];
//    [defaults synchronize];
//}
//+(NSString *) GetID
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"SaveID"];
//    return response;
//}
//
//
//+(void)DeleteSaveID
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveID"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)DeletetotalTransactionData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveTotalTransactionDataAPI"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveWealthData:(NSMutableArray *) Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *SaveUserDetail = [NSKeyedArchiver archivedDataWithRootObject:Data];
//    [defaults setObject:SaveUserDetail forKey:@"SaveWealthData"];
//    [defaults synchronize];
//}
//
//+(NSMutableArray *)GetWealthData
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSData *SaveUserDetail = [defaults objectForKey:@"SaveWealthData"];
//    NSMutableArray * response = [NSKeyedUnarchiver unarchiveObjectWithData:SaveUserDetail];
//    return response;
//
//}
//
//+(void)DeleteWealthData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveWealthData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveWithoutID :(NSString *)Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:Data forKey:@"SaveWithoutID"];
//    [defaults synchronize];
//}
//+(NSString *)GewithoutID
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"SaveWithoutID"];
//    return response;
//}
//
//+(void) DeleteWithoutLoginDateAPI
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveWithoutID"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveFundIDwithoutLogin:(NSString *)data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"SaveFundIDwithoutLogin"];
//    [defaults synchronize];
//}
//
//+(NSString *)GetFundIdWithoutLogin
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"SaveFundIDwithoutLogin"];
//    return response;
//}
//
//+(void)DeleteFundIdWithoutLogin
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveFundIDwithoutLogin"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) LoginSipSaveID: (NSString *) data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"LoginSipSaveID"];
//    [defaults synchronize];
//}
//
//+(NSString *) GetLoginsipID
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"LoginSipSaveID"];
//    return response;
//}
//
//+(void)DeleteLoignsipSaveID
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"LoginSipSaveID"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//
//+(void)datasaveforchartwealth:(NSDictionary *)data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"datasaveforchartwealth"];
//    [defaults synchronize];
//}
//
//+(NSDictionary *)getdataforwealth
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *response =[defaults valueForKey:@"datasaveforchartwealth"];
//    return response;
//}
//
//+(void)DeleteSavedataWealth
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"datasaveforchartwealth"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) PushNotificationToken : (NSString *) param
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setValue:param forKey:@"PushNotificationTokenID"];
//    [defaults synchronize];
//}
//
//+(NSString *) GetPushNotififcationToken
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *selectValue = [defaults valueForKey:@"PushNotificationTokenID"];
//    return selectValue;
//}
//
//+(void)savePaymentDashboardValue:(NSString *)data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"savePaymentDashboard"];
//    [defaults synchronize];
//}
//
//+(NSString *)getPaymentDashboardValue
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"savePaymentDashboard"];
//    return response;
//}
//
//+(void)DeletePaymentDashboardValue
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"savePaymentDashboard"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)saveredeemswitchstp:(NSString *)data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"saveredeemswitchstp"];
//    [defaults synchronize];
//}
//
//+(NSString *)getredeemswitchstp
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"saveredeemswitchstp"];
//    return response;
//}
//
//+(void)Deleteredeemswitchstp
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"saveredeemswitchstp"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) saveProfileLockData : (NSDictionary *) parms
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:parms forKey:@"savedProfileLockData"];
//    [defaults synchronize];
//}
//
//+(NSDictionary *) GetProfileLockData
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"savedProfileLockData"];
//    return response;
//}
//
//+(void)saveInvIdValue:(NSString *)data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"saveInvIdValueAPI"];
//    [defaults synchronize];
//}
//
//+(NSString *)getInvIdValue
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"saveInvIdValueAPI"];
//    return response;
//}
//
//+(void) Savedebitmandatestatus: (NSString *) data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"Savedebitmandatestatus"];
//    [defaults synchronize];
//}
//+(NSString *) GetSavedebitmandatestatus
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"Savedebitmandatestatus"];
//    return response;
//}
//
//+(void)DeleteSaveSavedebitmandatestatus
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"Savedebitmandatestatus"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) SaveAmountCodePayment : (NSString *) key
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:key forKey:@"SaveAmountCodePayment"];
//    [defaults synchronize];
//}
//
//+(NSString *) GetAmountCodePayment
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"SaveAmountCodePayment"];
//    return response;
//}
//
//+(void) ProfileslistData : (NSDictionary *) parms
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:parms forKey:@"ProfileslistData"];
//    [defaults synchronize];
//}
//
//+(NSDictionary *) getProfileslistData
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSMutableDictionary *response =[defaults valueForKey:@"ProfileslistData"];
//    return response;
//}
//
//+(void) DeleteProfileslistData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"ProfileslistData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) SavePopUpOneTime :(int) Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@(Data) forKey:@"SavePopUpOneTime"];
//    [defaults synchronize];
//}
//
//+(int)GetPopUpOneTime
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int response =[[defaults valueForKey:@"SavePopUpOneTime"] intValue];
//    return response;
//}
//
//+(void) DeletePopUpOneTime
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SavePopUpOneTime"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) SaveSlideImgData :(id) Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:Data forKey:@"SaveSlideImgData"];
//    [defaults synchronize];
//}
//
//+(id)GetSlideImgData
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    id response =[defaults valueForKey:@"SaveSlideImgData"];
//    return response;
//}
//
//+(void) DeleteSlideImgData
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveSlideImgData"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) SaveActionPopUpOneTime :(int) Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@(Data) forKey:@"SaveActionPopUpOneTime"];
//    [defaults synchronize];
//}
//
//+(int)GetActionPopUpOneTime
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int response =[[defaults valueForKey:@"SaveActionPopUpOneTime"] intValue];
//    return response;
//}
//
//+(void) DeleteActionPopUpOneTime
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveActionPopUpOneTime"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void) SaveCartPageOpenBuy : (int) value
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@(value) forKey:@"SaveCartPageOpenBuy"];
//    [defaults synchronize];
//}
//
//+(int) GetCartPageOpenBuy
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int response =[[defaults valueForKey:@"SaveCartPageOpenBuy"] intValue];
//    return response;
//}
//
//+(void) DeleteCartPageOpenBuy
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveCartPageOpenBuy"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveRegisterPopUp :(NSString *)Data
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:Data forKey:@"SaveRegisterPopUp"];
//    [defaults synchronize];
//}
//
//+(NSString *)GetRegisterPopUp
//{
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"SaveRegisterPopUp"];
//    return response;
//}
//
//+(void) DeleteRegisterPopUp
//{
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveRegisterPopUp"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveRedirectToFundList:(NSString*)str {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:str forKey:@"SaveRedirectToFundList"];
//    [defaults synchronize];
//}
//
//+(NSString*)GetRedirectToFundList {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response = [defaults valueForKey:@"SaveRedirectToFundList"];
//    return response;
//}
//
//+(void)SaveRedirectionPage:(NSDictionary*)data {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"SaveRedirectionPage"];
//    [defaults synchronize];
//}
//
//+(NSDictionary*)GetRedirectionPage {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *response = [defaults valueForKey:@"SaveRedirectionPage"];
//    return response;
//}
//
//+(void)DeleteRedirectionPage {
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SaveRedirectionPage"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveisComeFromInsurance:(NSString *)str {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:str forKey:@"isComeFromInsurance"];
//    [defaults synchronize];
//}
//
//+(NSString *)GetisComeFromInsurance {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response = [defaults valueForKey:@"isComeFromInsurance"];
//    return response;
//}
//
//+(void)SaveSipOtpOpen: (int)value {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:@(value) forKey:@"SipOtpOpen"];
//    [defaults synchronize];
//}
//
//+(int)GetSipOtpOpen {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    int response =[[defaults valueForKey:@"SipOtpOpen"] intValue];
//    return response;
//}
//
//+(void)DeleteSipOtpOpen {
//    [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"SipOtpOpen"];
//    [[NSUserDefaults standardUserDefaults] synchronize];
//}
//
//+(void)SaveSipIncreaseData:(NSDictionary*)data {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:data forKey:@"SipIncreaseData"];
//    [defaults synchronize];
//}
//
//+(NSDictionary*)GetSipIncreaseData {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSDictionary *response = [defaults valueForKey:@"SipIncreaseData"];
//    return response;
//}
//
//+(void)SaveMainInvestorPAN:(NSString *)Data {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    [defaults setObject:Data forKey:@"MainInvestorPAN"];
//    [defaults synchronize];
//}
//
//+(NSString *)GetMainInvestorPAN {
//    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
//    NSString *response =[defaults valueForKey:@"MainInvestorPAN"];
//    return response;
//}

@end

