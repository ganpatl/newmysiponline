//
//  LoaderVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LoaderVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgLoader;
@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblStaticMessage;

+(LoaderVC*)showWithStatus:(nullable NSString*)strStatus;
+(void)dismiss;

@end

NS_ASSUME_NONNULL_END
