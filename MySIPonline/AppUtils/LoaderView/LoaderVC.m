//
//  LoaderVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "LoaderVC.h"
#import "UIImage+animatedGIF.h"

static LoaderVC *sharedInstance = nil;

@interface LoaderVC ()

@end

@implementation LoaderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CGRect size = [[UIScreen mainScreen] bounds];
    self.view.frame = size;
    
    // Set Gif Image
    NSString *filePath = [[NSBundle mainBundle] pathForResource: @"loader" ofType: @"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile: filePath];
    self.imgLoader.image = [UIImage animatedImageWithAnimatedGIFData:gifData];
    //self.imgLoader.backgroundColor = UIColor.whiteColor;
    self.imgLoader.layer.cornerRadius = self.imgLoader.frame.size.height / 2;
}

+(LoaderVC*)showWithStatus:(nullable NSString*)strStatus {
    [sharedInstance.view removeFromSuperview];
    if(!sharedInstance) {
        sharedInstance = [[LoaderVC alloc] initWithNibName:@"LoaderVC" bundle:[NSBundle mainBundle]];
    }
    [sharedInstance showWithStatus:strStatus];
    return sharedInstance;
}

+(void)dismiss {
    if (sharedInstance != nil) {
        dispatch_async(dispatch_get_main_queue(), ^{
            [UIView animateWithDuration:0.5 animations:^{
                sharedInstance.view.alpha = 0;
                [sharedInstance.view removeFromSuperview];
            }];
        });
    }
}

-(void)showWithStatus:(NSString*)strStatus {
    UIWindow *topWindow = [UIApplication sharedApplication].keyWindow;
    
    dispatch_async(dispatch_get_main_queue(), ^{
        sharedInstance.view.center = topWindow.center;
        if (strStatus != nil) {
            sharedInstance.lblMessage.text = strStatus;
        }
        if (sharedInstance.view.superview == nil) {
            [topWindow addSubview:sharedInstance.view];
        }
        [UIView animateWithDuration:0.1 animations:^{
            sharedInstance.view.alpha = 1;
        }];
    });
}

@end
