//
//  AppHelper.m
//
//  Created by Rahul on 25/07/16.
//  Copyright © 2016 Rahul Saini. All rights reserved.
//

#import "AppHelper.h"
#import <Google/Analytics.h>

@import UserNotifications;

@implementation AppHelper

+(void)displayLocalNotification:(NSString *)strAlertBody TimeIntervalInSecond:(int)timeInterval Identifier:(NSString*)identifier Repeat:(BOOL)doRepeat {
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
        if (settings.authorizationStatus != UNAuthorizationStatusAuthorized) {
            // Notifications not allowed
            GSLog(@"Local Notifications not allowed");
        } else {
            UNMutableNotificationContent *content = [UNMutableNotificationContent new];
            content.title = @"";
            //content.subtitle = @"sub title";
            content.body = strAlertBody;
            content.sound = [UNNotificationSound defaultSound];
            //content.attachments =
            
            // fire notification
            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:timeInterval repeats:doRepeat];
            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger: trigger];
            [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
                if (error != nil) {
                    GSLog(@"Something went wrong: %@",error);
                }
            }];
        }
    }];
    
#else
    // create a corresponding local notification
    UILocalNotification *notification = [[UILocalNotification alloc] init];
    notification.alertBody = strAlertBody; // text that will be displayed in the notification
    
    //notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:timeInterval];//date (when notification will be fired)
    notification.soundName = UILocalNotificationDefaultSoundName; // play default sound
    notification.alertAction = @"View";
    //notification.userInfo = ["title": item.title, "UUID": item.UUID] // assign a unique identifier to the notification so that we can retrieve it later
    [[UIApplication sharedApplication] scheduleLocalNotification: notification];
#endif
}

/**
 Check the passed object for 'nil' and `NSNull` and returns a boolean value YES if passed object is NOT nil or NOT NSnull. Otherwise returns NO.
 @param obj Any Object for null checking
 @return BOOL
 */
+(BOOL)isNotNull:(id)obj {
    return ((obj != nil) && (obj != (id)[NSNull null]));
}

+(void)sendToScreenTracker:(NSString*)strScreenName {
    id<GAITracker> tracker = [[GAI sharedInstance] defaultTracker];
    [tracker set:kGAIScreenName value: strScreenName];
    [tracker send:[[GAIDictionaryBuilder createScreenView] build]];
    //[FIRAnalytics setScreenName:strScreenName screenClass:strScreenName];
}

+(NSString*)convertDictionaryToString:(NSDictionary*)dic {
    NSError * err;
    NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dic options:0 error:&err];
    if (err == nil) {
        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
        //GSLog(@"%@",myString);
        return myString;
    } else {
        return @"";
    }
}

+(void)ShowAlert:(NSString *)message Title:(NSString*)title FromVC:(UIViewController*)selfVC Completion: (void (^)(UIAlertAction *action)) callbackBlock {
    UIAlertController * alert = [UIAlertController alertControllerWithTitle:title message:message preferredStyle:(UIAlertControllerStyleAlert)];
    UIAlertAction *action = [UIAlertAction actionWithTitle:@"OK" style:(UIAlertActionStyleDefault) handler:callbackBlock];
    [alert addAction:action];
    [selfVC presentViewController:alert animated:true completion:nil];
}

+(void)showToast:(NSString*)strText {
    iToastSettings *theSettings = [iToastSettings getSharedSettings];
    theSettings.cornerRadius = -5;
    theSettings.useShadow = NO;
    [[[[iToast makeText:strText] setGravity:iToastGravityBottom] setDuration:iToastDurationNormal] show];
}

+ (BOOL)validationNameString:(NSString *)name {
    NSString *nameRegex = @"^[A-Za-z.' ]+$";
    NSPredicate *NameTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", nameRegex];
    return [NameTest evaluateWithObject:name];
}
+ (BOOL)validateEmailWithString:(NSString*)emails {
    //NSString *emailRegex = @"[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSString *emailRegex = @"[A-Z0-9a-z._%+-]{3,}+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,4}";
    NSPredicate *emailTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", emailRegex];
    return [emailTest evaluateWithObject:emails];
}
+ (BOOL)validatePhoneNumber:(NSString*)phoneno {
    NSString *phoneRegex = @"[0-9]{10}";
    NSPredicate *phoneTest = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", phoneRegex];
    return [phoneTest evaluateWithObject:phoneno];
}

+(NSString*)convertNSDateToString:(NSDate*)date FormatString:(NSString *)strFormat {
    NSDateFormatter *dateFormatter = [NSDateFormatter new];
    [dateFormatter setDateFormat:strFormat];
    return [dateFormatter stringFromDate:date]; // Here convert date in NSString
}

+(NSString*)maskStringWithXtoDigit:(int)digit String:(NSString*)string {
    NSString *panstring = string;
    if (panstring.length != 0) {
        long a = panstring.length;
        NSString *appends = @"X";
        NSString *trimmedString = [panstring substringFromIndex:MAX((int)[panstring length]-digit, 0)]; //in case string is less than 4 characters long
        
        long b = trimmedString.length;
        long c = a - b;
        for (int i = 0; i < c; i++) {
            trimmedString = [appends stringByAppendingString:trimmedString];
        }
        return [NSString stringWithFormat:@"%@", trimmedString];
    } else {
        return string;
    }
}

+(WebVC*)createWebViewControllerWithTitle:(NSString *)strTitle URLString:(NSString *)strUrl ShowShareButton:(BOOL)showShareButton {
    WebVC *destVC = [WebVC new];
    destVC.strTitle = strTitle;
    destVC.strUrlLink = strUrl;
    destVC.showShareButton = showShareButton;
    return destVC;
}








#pragma mark - Components

+(CGFloat)getSafeAreaBottomMargin {
    if (@available(iOS 11.0, *)) {
        UIWindow *currentwindow = UIApplication.sharedApplication.windows.firstObject;
        return currentwindow.safeAreaLayoutGuide.owningView.frame.size.height - currentwindow.safeAreaLayoutGuide.layoutFrame.size.height - currentwindow.safeAreaLayoutGuide.layoutFrame.origin.y;
    } else {
        return 0;
    }
}

+(CGRect)getSafeFrameWithUpdatedY:(CGRect)rect {
    rect.origin.y = rect.origin.y - [self getSafeAreaBottomMargin];
    return rect;
}

+(CGRect)getSafeFrameWithUpdatedHeight:(CGRect)rect {
    rect.size.height = rect.size.height - [self getSafeAreaBottomMargin];
    return rect;
}

+(void)addShadowOnView:(UIView*)view Color:(UIColor*)color OffetSize:(CGSize)offset Opacity:(float)opacity Radius:(CGFloat)radius {
    view.layer.shadowColor = color.CGColor;
    view.layer.shadowOffset = offset;
    view.layer.shadowOpacity = opacity;
    view.layer.shadowRadius = radius;
}

+(void)addTopShadowOnView:(UIView*)view {
    [AppHelper addShadowOnView:view Color:[UIColor darkGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.3 Radius:2];
}

+(void)addDashedBorderOnView:(UIView*)view withColor:(UIColor*)color {
    CAShapeLayer *dashedBorder = [CAShapeLayer layer];
    dashedBorder.strokeColor = color.CGColor;
    dashedBorder.fillColor = nil;
    dashedBorder.lineDashPattern = @[@3, @5];
    dashedBorder.path = [UIBezierPath bezierPathWithRect:view.bounds].CGPath;
    [view.layer addSublayer:dashedBorder];
}

+(void)setBackgroundGradient:(UIView *)mainView Color1:(UIColor*)color1 Color2:(UIColor*)color2 {    
    [mainView setBackgroundColor:[UIColor clearColor]];
    CAGradientLayer *theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = mainView.bounds;
    
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[color1 CGColor], (id)[color2 CGColor], nil];
    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
    [mainView.layer insertSublayer:theViewGradient atIndex:0];
}

+(void)startAnimationOnView:(UIView*)view Duration:(NSTimeInterval)duration Delay:(NSTimeInterval)delay AnimationType:(CSAnimationType)type {
    Class <CSAnimation> class = [CSAnimation classForAnimationType:type];
    [class performAnimationOnView:view duration:duration delay:delay];
}

+(UIImage *)scaleAndRotateImage:(UIImage *)img {
    int kMaxResolution = 600; // Or whatever
    CGImageRef imgRef = img.CGImage;
    
    CGFloat width = CGImageGetWidth(imgRef);
    CGFloat height = CGImageGetHeight(imgRef);
    
    CGAffineTransform transform = CGAffineTransformIdentity;
    CGRect bounds = CGRectMake(0, 0, width, height);
    if (width > kMaxResolution || height > kMaxResolution) {
        CGFloat ratio = width/height;
        if (ratio > 1) {
            bounds.size.width = kMaxResolution;
            bounds.size.height = roundf(bounds.size.width / ratio);
        } else {
            bounds.size.height = kMaxResolution;
            bounds.size.width = roundf(bounds.size.height * ratio);
        }
    }
    
    CGFloat scaleRatio = bounds.size.width / width;
    CGSize imageSize = CGSizeMake(CGImageGetWidth(imgRef), CGImageGetHeight(imgRef));
    CGFloat boundHeight;
    UIImageOrientation orient = img.imageOrientation;
    switch(orient) {
        case UIImageOrientationUp: //EXIF = 1
            transform = CGAffineTransformIdentity;
            break;
            
        case UIImageOrientationUpMirrored: //EXIF = 2
            transform = CGAffineTransformMakeTranslation(imageSize.width, 0.0);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            break;
            
        case UIImageOrientationDown: //EXIF = 3
            transform = CGAffineTransformMakeTranslation(imageSize.width, imageSize.height);
            transform = CGAffineTransformRotate(transform, M_PI);
            break;
            
        case UIImageOrientationDownMirrored: //EXIF = 4
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.height);
            transform = CGAffineTransformScale(transform, 1.0, -1.0);
            break;
            
        case UIImageOrientationLeftMirrored: //EXIF = 5
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, imageSize.width);
            transform = CGAffineTransformScale(transform, -1.0, 1.0);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationLeft: //EXIF = 6
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(0.0, imageSize.width);
            transform = CGAffineTransformRotate(transform, 3.0 * M_PI / 2.0);
            break;
            
        case UIImageOrientationRightMirrored: //EXIF = 7
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeScale(-1.0, 1.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        case UIImageOrientationRight: //EXIF = 8
            boundHeight = bounds.size.height;
            bounds.size.height = bounds.size.width;
            bounds.size.width = boundHeight;
            transform = CGAffineTransformMakeTranslation(imageSize.height, 0.0);
            transform = CGAffineTransformRotate(transform, M_PI / 2.0);
            break;
            
        default:
            [NSException raise:NSInternalInconsistencyException format:@"Invalid image orientation"];
            
    }
    
    UIGraphicsBeginImageContext(bounds.size);
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    if (orient == UIImageOrientationRight || orient == UIImageOrientationLeft) {
        CGContextScaleCTM(context, -scaleRatio, scaleRatio);
        CGContextTranslateCTM(context, -height, 0);
    } else {
        CGContextScaleCTM(context, scaleRatio, -scaleRatio);
        CGContextTranslateCTM(context, 0, -height);
    }
    
    CGContextConcatCTM(context, transform);
    CGContextDrawImage(UIGraphicsGetCurrentContext(), CGRectMake(0, 0, width, height), imgRef);
    UIImage *imageCopy = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return imageCopy;
}

+(UIImage *)getFirstFrame:(NSURL *)videoURL {
    NSData *data = [NSData dataWithContentsOfURL:videoURL];
    NSString *filePath = [NSTemporaryDirectory() stringByAppendingPathComponent:@"temp.mov"];
    BOOL res = [data writeToFile:filePath atomically:NO];
    if (res == NO) {
        return [UIImage new];
    }
    NSURL *url = [NSURL fileURLWithPath:filePath];
    AVURLAsset *asset = [[AVURLAsset alloc] initWithURL:url options:nil];
    NSParameterAssert(asset);
    AVAssetImageGenerator *assetIG = [[AVAssetImageGenerator alloc] initWithAsset:asset];
    assetIG.appliesPreferredTrackTransform = YES;
    assetIG.apertureMode = AVAssetImageGeneratorApertureModeEncodedPixels;
    
    CGImageRef thumbnailImageRef = NULL;
    CFTimeInterval thumbnailImageTime = 1;
    NSError *igError = nil;
    thumbnailImageRef =
    [assetIG copyCGImageAtTime:CMTimeMake(thumbnailImageTime, 60)
                    actualTime:NULL
                         error:&igError];
    
    if (!thumbnailImageRef)
        NSLog(@"thumbnailImageGenerationError %@", igError );
    
    UIImage *thumbnailImage = thumbnailImageRef
    ? [[UIImage alloc] initWithCGImage:thumbnailImageRef]
    : nil;
    
    return thumbnailImage;
}



///**
// Check the passed object for 'nil' and `NSNull` and returns a boolean value YES if passed object is NOT nil or NOT NSnull. Otherwise returns NO.
// @param obj Any Object for null checking
// @return BOOL
// */
//+(BOOL)isNotNull:(id)obj {
//    return ((obj != nil) && (obj != (id)[NSNull null]));
//}
//
//+(void)displayLocalNotification:(NSString *)strAlertBody TimeIntervalInSecond:(int)timeInterval Identifier:(NSString*)identifier Repeat:(BOOL)doRepeat {
//#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
//    UNUserNotificationCenter *center = [UNUserNotificationCenter currentNotificationCenter];
//    [center getNotificationSettingsWithCompletionHandler:^(UNNotificationSettings * _Nonnull settings) {
//        if (settings.authorizationStatus != UNAuthorizationStatusAuthorized) {
//            // Notifications not allowed
//            GSLog(@"Local Notifications not allowed");
//        } else {
//            UNMutableNotificationContent *content = [UNMutableNotificationContent new];
//            content.title = @"";
//            //content.subtitle = @"sub title";
//            content.body = strAlertBody;
//            content.sound = [UNNotificationSound defaultSound];
//            //content.attachments =
//
//            // fire notification
//            UNTimeIntervalNotificationTrigger *trigger = [UNTimeIntervalNotificationTrigger triggerWithTimeInterval:timeInterval repeats:doRepeat];
//            UNNotificationRequest *request = [UNNotificationRequest requestWithIdentifier:identifier content:content trigger: trigger];
//            [center addNotificationRequest:request withCompletionHandler:^(NSError * _Nullable error) {
//                if (error != nil) {
//                    GSLog(@"Something went wrong: %@",error);
//                }
//            }];
//        }
//    }];
//
//#else
//    // create a corresponding local notification
//    UILocalNotification *notification = [[UILocalNotification alloc] init];
//    notification.alertBody = strAlertBody; // text that will be displayed in the notification
//
//    //notification.alertAction = "open" // text that is displayed after "slide to..." on the lock screen - defaults to "slide to view"
//    notification.fireDate = [NSDate dateWithTimeIntervalSinceNow:timeInterval];//date (when notification will be fired)
//    notification.soundName = UILocalNotificationDefaultSoundName; // play default sound
//    notification.alertAction = @"View";
//    //notification.userInfo = ["title": item.title, "UUID": item.UUID] // assign a unique identifier to the notification so that we can retrieve it later
//    [[UIApplication sharedApplication] scheduleLocalNotification: notification];
//#endif
//}
//
//+(NSAttributedString *)convertFromHTMLString:(NSString *)htmlString {
//    NSAttributedString *final = [[NSAttributedString alloc] initWithData:[htmlString dataUsingEncoding:NSUTF8StringEncoding] options:@{NSDocumentTypeDocumentAttribute: NSHTMLTextDocumentType, NSCharacterEncodingDocumentAttribute: [NSNumber numberWithInt:NSUTF8StringEncoding]} documentAttributes:nil error:nil];
//    return final;
//}
//

@end
