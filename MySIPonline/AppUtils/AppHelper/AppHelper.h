//
//  AppHelper.h

//
//  Created by Rahul on 25/07/16.
//  Copyright © 2016 Rahul Saini. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CSAnimation.h"
#import "iToast.h"
#import "WebVC.h"

@import  UserNotifications;
@import AVFoundation;
//@import AddressBookUI;
//@import ContactsUI;
//@import MobileCoreServices;

@interface AppHelper : NSObject

+(void)displayLocalNotification:(NSString *)strAlertBody TimeIntervalInSecond:(int)timeInterval Identifier:(NSString*)identifier Repeat:(BOOL)doRepeat;
+(BOOL)isNotNull:(id)obj;
+(void)sendToScreenTracker:(NSString*)strScreenName;
+(NSString*)convertDictionaryToString:(NSDictionary*)dic;

+(void)ShowAlert:(NSString *)message Title:(NSString*)title FromVC:(UIViewController*)selfVC Completion: (void (^)(UIAlertAction *action)) callbackBlock;
+(void)showToast:(NSString*)strText;

+(BOOL)validationNameString:(NSString *)name;
+(BOOL)validateEmailWithString:(NSString*)emails;
+(BOOL)validatePhoneNumber:(NSString*)phoneno;

+(NSString*)convertNSDateToString:(NSDate*)date FormatString:(NSString *)strFormat;
+(NSString*)maskStringWithXtoDigit:(int)digit String:(NSString*)string;

+(WebVC*)createWebViewControllerWithTitle:(NSString *)strTitle URLString:(NSString *)strUrl ShowShareButton:(BOOL)showShareButton;

// Components
+(CGFloat)getSafeAreaBottomMargin;
+(CGRect)getSafeFrameWithUpdatedY:(CGRect)rect;
+(CGRect)getSafeFrameWithUpdatedHeight:(CGRect)rect;

+(void)addShadowOnView:(UIView*)view Color:(UIColor*)color OffetSize:(CGSize)offset Opacity:(float)opacity Radius:(CGFloat)radius;
+(void)addTopShadowOnView:(UIView*)view;

+(void)addDashedBorderOnView:(UIView*)view withColor:(UIColor*)color;
+(void)setBackgroundGradient:(UIView *)mainView Color1:(UIColor*)color1 Color2:(UIColor*)color2;

+(void)startAnimationOnView:(UIView*)view Duration:(NSTimeInterval)duration Delay:(NSTimeInterval)delay AnimationType:(CSAnimationType)type;

+(UIImage *)scaleAndRotateImage:(UIImage *)img;
+(UIImage *)getFirstFrame:(NSURL *)videoURL;

//+(void) PopUpAlertController : (id) sender : (NSString *) Message : (NSString *)title;
//+(void) InternetPopUpAlertController : (id) sender : (NSString *) Message : (NSString *)title;
//
//+(BOOL)validationSpecialCharacter : (NSString *) zipCode;
//+(BOOL)validateStringContainsAlphabetsOnly:(NSString*)strng;
//+(BOOL)validatePasswordWithString:(NSString *)password;
//+(BOOL)unitAlphabetsOnly:(NSString*)strng;
//+(BOOL)unitStreetAlphabetsOnly:(NSString*)strng;
//
//+(UIButton *)getBackButtonObject;
//+(UIImageView *)getImageViewWithName:(NSString*)name;
//
//+(BOOL)compareImageFirst:(UIImage *)image1 isEqualTo:(UIImage *)image2;
//
//+(UIButton *)createFinishButton:(UIView *)parentView X:(int)x Y:(int)y;
//+(UIImageView *)createFinishImageView:(UIView *)parentView X:(int)x Y:(int)y;
//+(void)setLeftPadding:(UITextField *)textField;
//+(void)formatTextField:(UITextField *)textField BorderWidth:(CGFloat)bWith BorderColor:(UIColor*)bColor;
//
//+(CGFloat)getSafeAreaBottomMargin;
//+(CGFloat)getSafeAreaTopMargin;
//
//+(CGRect)getSafeFrameWithUpdatedYDecrease:(CGRect)rect;
//+(CGRect)getSafeFrameWithUpdatedYIncrease:(CGRect)rect;
//
//+(CGRect)getSafeFrameWithUpdatedHeightBottom:(CGRect)rect;
//+(CGRect)getSafeFrameWithUpdatedHeightTop:(CGRect)rect;
//
//+(BOOL)isDevice_iPhoneX;
//
//
//+(void)sendWhatsAppMessageTo:(NSString *)whatsAppNo;
//+(void)AddNumber:(NSString *)whatsNo;
//+(BOOL)isWANumberSaved;
//+(void)whatsApp;
//
//+(BOOL)isNotNull:(id)obj;
//
//+(void)displayLocalNotification:(NSString *)strAlertBody TimeIntervalInSecond:(int)timeInterval Identifier:(NSString*)identifier Repeat:(BOOL)doRepeat;
//
//+(NSAttributedString *)convertFromHTMLString:(NSString *)htmlString;
//

@end
