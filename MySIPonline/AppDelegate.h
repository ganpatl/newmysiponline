//
//  AppDelegate.h
//  MySIPonline
//
//  Created by Ganpat on 12/11/18.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property UIInterfaceOrientationMask orientationMask;

+ (AppDelegate*) getAppDelegate;
- (BOOL)checkNetworkReachability;

@end

