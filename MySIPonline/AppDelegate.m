//
//  AppDelegate.m
//  MySIPonline
//
//  Created by Ganpat on 12/11/18.
//

#import "AppDelegate.h"
#import "Reachability.h"
#import <Fabric/Fabric.h>
#import <Crashlytics/Crashlytics.h>
#import <Google/Analytics.h>
#import "IQKeyboardManager.h"
@import GoogleSignIn;
@import UserNotifications;


@interface AppDelegate () <UNUserNotificationCenterDelegate>

@end

@implementation AppDelegate

#pragma mark - App Delegate Methods

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    // Get Settings from Server
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_getSettingsWithCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    NSDictionary *dicSettings = response[@"result"];
                    [NSUserData SaveSettingsData:dicSettings];
                }
            } else {
                //NSError *err = (NSError*)response;
                //[AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    }
    
    _orientationMask = UIInterfaceOrientationMaskPortrait;
    [IQKeyboardManager sharedManager].enable = YES;
    
    [GIDSignIn sharedInstance].clientID = @"275458538038-s4fakm22q10roml6etnj1bah7r4nt2t6.apps.googleusercontent.com";
    // Optional: configure GAI options.
    [[GAI sharedInstance] trackerWithTrackingId:@"UA-83374415-1"];
    GAI *gai = [GAI sharedInstance];
    gai.trackUncaughtExceptions = YES;  // report uncaught exceptions
    //gai.logger.logLevel = kGAILogLevelVerbose;  // remove before app release
    // [END tracker_objc]
    
    // Fabric Crashlytics
    [Fabric with:@[[Crashlytics class]]];
    
    // Register for Remote Notifications
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
        // iOS 10 or later
        // For iOS 10 display notification (sent via APNS)
        [UNUserNotificationCenter currentNotificationCenter].delegate = self;
        UNAuthorizationOptions authOptions = UNAuthorizationOptionAlert |
        UNAuthorizationOptionSound | UNAuthorizationOptionBadge;
        [[UNUserNotificationCenter currentNotificationCenter] requestAuthorizationWithOptions:authOptions completionHandler:^(BOOL granted, NSError * _Nullable error) {
             dispatch_async(dispatch_get_main_queue(), ^{
                 [application registerForRemoteNotifications];
             });
        }];
#else
        // iOS 10 notifications aren't available; fall back to iOS 8-9 notifications.
        UIUserNotificationType allNotificationTypes =
        (UIUserNotificationTypeSound | UIUserNotificationTypeAlert | UIUserNotificationTypeBadge);
        UIUserNotificationSettings *settings = [UIUserNotificationSettings settingsForTypes:allNotificationTypes categories:nil];
        [application registerUserNotificationSettings:settings];
#endif
    
    NSDictionary *userInfo = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
    if (userInfo) {
        NSString *type = userInfo[@"aps"][@"type"];
        [self pushNotificationUserActionWithType:type andUserInfo:userInfo];
        return YES;
    }
    
    //UILocalNotification *locationNotification = [launchOptions objectForKey:UIApplicationLaunchOptionsLocalNotificationKey];
    //if (locationNotification) {
        // Set icon badge number to zero
        // application.applicationIconBadgeNumber = 0;
    //}
    
    
    NSDictionary *dicLoginData = [NSUserData GetLoginData];
    if (dicLoginData != nil) {
        UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
        UIViewController *navHomeVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"navHomeVC"];
        self.window.rootViewController = navHomeVC;
    }
    
    return YES;
}

-(UIInterfaceOrientationMask)application:(UIApplication *)application supportedInterfaceOrientationsForWindow:(UIWindow *)window {
    return _orientationMask;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
}


- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    [AppHelper displayLocalNotification:@"Now you can Invest Anytime & Anywhere. Mutual fund investment platform in now at your fingertips" TimeIntervalInSecond:86400 Identifier:@"localAfter24h" Repeat:YES];
}


- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
}


- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
#if defined(__IPHONE_10_0) && __IPHONE_OS_VERSION_MAX_ALLOWED >= __IPHONE_10_0
    [[UNUserNotificationCenter currentNotificationCenter] removePendingNotificationRequestsWithIdentifiers:@[@"localAfter24h"]];
#else
        [[UIApplication sharedApplication] cancelAllLocalNotifications];
#endif
}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

#pragma mark - Other Methods

+ (AppDelegate*) getAppDelegate {
    return (AppDelegate*)[UIApplication sharedApplication].delegate;
}

-(BOOL) checkNetworkReachability {
    Reachability *reachability = [Reachability reachabilityForInternetConnection];
    [reachability startNotifier];
    
    NetworkStatus status = [reachability currentReachabilityStatus];
    if (status == NotReachable) {
        return NO;
    } else if (status == ReachableViaWiFi) {
        return YES;
    } else if (status == ReachableViaWWAN) {
        return YES;
    }
    return NO;
}

- (void)pushNotificationUserActionWithType:(NSString *)type andUserInfo:(NSDictionary *)userInfo {
    [UIApplication sharedApplication].applicationIconBadgeNumber = [UIApplication sharedApplication].applicationIconBadgeNumber + 1;
//    NSDictionary *params = [NSUserData GetLoginDataAPI];
//    if (params != NULL) {
//        NSString *str = [NSUserData GetSaveLoginAuth];
//        if ([str isEqualToString:@"1"] || [str isEqualToString:@"2"]) {
//            NSString *strRememberedPassword = [NSUserData getLoginValue];
//            if ([strRememberedPassword isEqualToString:@"1"]) {
//                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                         bundle:nil];
//                UITabBarController *tabBarController = (UITabBarController *)[mainStoryboard
//                    instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
//                self.window.rootViewController = tabBarController;
//            } else {
//                [NSUserData DeleteLoginDateAPI];
//                [NSUserData DeleteLoginValue];
//                [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//                UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                         bundle:nil];
//                UINavigationController *svc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NavLoginController"];
//                self.window.rootViewController = svc;
//            }
//        }
//    } else {
//        NSString *str = [NSUserData GetSaveLoginAuth];
//        if ([str isEqualToString:@"1"] || [str isEqualToString:@"2"]) {
//            [NSUserData DeleteLoginDateAPI];
//            [NSUserData DeleteLoginValue];
//            [[UIApplication sharedApplication] setApplicationIconBadgeNumber:0];
//            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main"
//                                                                     bundle:nil];
//            UINavigationController *svc = [mainStoryboard instantiateViewControllerWithIdentifier:@"NavLoginController"];
//            self.window.rootViewController = svc;
//        }
//    }
//    NSString *alertMessage = [[userInfo valueForKey:@"aps"] valueForKey:@"alert"];
//    UIAlertView *PushNotificationAlert = [[UIAlertView alloc] initWithTitle:@"" message:alertMessage delegate:self cancelButtonTitle:@"OK" otherButtonTitles:nil];
//    [PushNotificationAlert show];
}

@end
