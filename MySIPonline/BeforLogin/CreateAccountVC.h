//
//  CreateAccountVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import <UIKit/UIKit.h>
#import "SkyFloatingLabelTextField-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface CreateAccountVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfFullName;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfEmail;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfPassword;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfConfirmPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblErrFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblErrEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblErrPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblErrConfirmPassword;

@end

NS_ASSUME_NONNULL_END
