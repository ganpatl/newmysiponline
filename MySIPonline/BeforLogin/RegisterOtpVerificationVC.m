//
//  RegisterOtpVerificationVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import "RegisterOtpVerificationVC.h"
#import "ResetPasswordVC.h"
#import "HomeVC.h"
#import <Crashlytics/Crashlytics.h>

@interface RegisterOtpVerificationVC () {
    NSString *strOtpCode;
    NSTimer *timer;
    int remainTime;
    BOOL isIos12;
}

@end

@implementation RegisterOtpVerificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.textColor = [UIColor getAppColorTitleText];
    self.tfOtp1.text = @"";
    self.tfOtp2.text = @"";
    self.tfOtp3.text = @"";
    self.tfOtp4.text = @"";
    self.tfOtp5.text = @"";
    self.tfOtp6.text = @"";
    self.btnResendCode.hidden = YES;
    self.lblOtpTime.hidden = YES;
    self.lblDesc.hidden = YES;
    
    isIos12 = (SYSTEM_VERSION_GREATER_THAN_OR_EQUAL_TO(@"12.0"));
    if (isIos12) {
        self.singleOtpView.hidden = NO;
        self.transparentView.hidden = YES;
    }
    [self generateOTP];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
}

-(IBAction)otpTextFieldEditingChange:(UITextField*)textField {
    if (textField == _tfOtp1) {
        [_tfOtp2 becomeFirstResponder];
    } else if (textField == _tfOtp2) {
        [_tfOtp3 becomeFirstResponder];
    } else if (textField == _tfOtp3) {
        [_tfOtp4 becomeFirstResponder];
    } else if (textField == _tfOtp4) {
        [_tfOtp5 becomeFirstResponder];
    } else if (textField == _tfOtp5) {
        [_tfOtp6 becomeFirstResponder];
    } else if (textField == _tfOtp6) {
        if (timer != nil) {
            [timer invalidate];
            timer = nil;
            _lblOtpTime.text = @"";
            if (_tfOtp1.text.length > 0 && _tfOtp2.text.length > 0 && _tfOtp3.text.length > 0 && _tfOtp4.text.length > 0 && _tfOtp5.text.length > 0 && _tfOtp6.text.length > 0) {
                self.transparentView.hidden = YES;
            } else {
                self.transparentView.hidden = NO;
            }
        }
    }
}

#pragma mark - Web API Call

-(void)generateOTP {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"mobile":self.dicRegData[@"mobile"], @"email":self.dicRegData[@"email"], @"reqType":@"registration"};
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_generateOtpData:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                int status = [[response valueForKey:@"status"] intValue];
                if (status == 1) {
                    NSString *strKey = response[@"key"];
                    self->strOtpCode = response[@"otp-val"];
                    NSArray *aryComponent = [strKey componentsSeparatedByString:@"-"];
                    if (aryComponent.count > 1) {
                        if ([aryComponent.firstObject isEqualToString:@"B"]) {
                            // Back
                            self->strOtpCode = [self->strOtpCode substringToIndex: self->strOtpCode.length - [aryComponent[1] intValue]];
                        } else {
                            // Front
                            self->strOtpCode = [self->strOtpCode substringFromIndex: [aryComponent[1] intValue]];
                        }
                    }
                    self->remainTime = 60;
                    self->timer = [NSTimer scheduledTimerWithTimeInterval:1 target:self selector:@selector(timerEvent) userInfo:nil repeats:YES];
                    self.lblOtpTime.text = [NSString stringWithFormat:@"%d", self->remainTime];
                    self.lblOtpTime.hidden = NO;
                    self.btnResendCode.hidden = YES;
                    
                    NSString *main_string = [NSString stringWithFormat:@"Please enter the OTP sent on your phone number - %@", self.dicRegData[@"mobile"]];
                    NSString *string_to_color = self.dicRegData[@"mobile"];
                    NSRange range = [main_string rangeOfString:string_to_color];
                    NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:main_string];
                    [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor getAppColorDarkGrayText] range:range];
                    self.lblDesc.attributedText = attributedString;
                    self.lblDesc.hidden = NO;
                    
                    //[AppHelper ShowAlert:[response[@"message"] capitalizedString] Title:@"" FromVC:self Completion:nil];
                } else {
                    NSString *error = [NSString stringWithFormat:@"%@",[response valueForKey:@"msg"]];
                    [AppHelper ShowAlert:error Title:@"Error" FromVC:self Completion:nil];
                }
            } else {
                // NSError *err = (NSError*)response;
                // NSLog(@"Error %@",err.localizedDescription);
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)registerAPICall {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSString *strDate = [AppHelper convertNSDateToString:[NSDate date] FormatString:@"yyyy-MM-dd HH:mm:ss"];
        NSMutableDictionary *dicParam = [[NSMutableDictionary alloc] initWithDictionary:self.dicRegData];
        dicParam[@"date"] = strDate;
        dicParam[@"formtype"] = @"register-ios";
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_register:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
             if (!isError) {
                 int Status = [response[@"status"] intValue];
                 if (Status == 1) {
                     
                     NSDictionary *dicLogin = response[@"data"];
                     [NSUserData SaveLoginData:dicLogin];
                     
                     NSString *LoginUserId = [NSString stringWithFormat:@"%@", dicLogin[@"id"]];
                     NSString *LoginEmailId = dicLogin[@"email"];
                     NSString *LoginUserName = dicLogin[@"name"];
                     [CrashlyticsKit setUserIdentifier:LoginUserId];
                     [CrashlyticsKit setUserEmail:LoginEmailId];
                     [CrashlyticsKit setUserName:LoginUserName];
                     
                     HomeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                     [self presentViewController:destVC animated:NO completion:nil];
                 } else {
                     NSString *error = [NSString stringWithFormat:@"%@",[response valueForKey:@"message"]];
                     [AppHelper ShowAlert:error Title:@"Error" FromVC:self Completion:nil];
                 }
             } else {
                 NSError *err = (NSError*)response;
                 [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
             }
         }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)getLoggedInUserData:(NSString *)strUserId {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"id":strUserId};
        //MBProgressHUD *hud = [MBProgressHUD showHUDAddedTo:self.navigationController.view animated:YES];
        //hud.label.text = @"Please Wait";
        [WebServices api_getLoginUserWithId:dicParam AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                int Status = [response[@"status"] intValue];
                if (Status == 1) {
                    //[NSUserData DeleteSaveSavedebitmandatestatus];
                    //NSDictionary *login_data = [response valueForKey:@"data"];
                    //[NSUserData SaveLoginDataAPI:login_data];
                    //NSString *WithoutLoginSip = [NSUserData GetFundIdWithoutLogin];
                    NSString *LoginUserId = response[@"Otherclient_id"];
                    GSLog(@"%@", LoginUserId);
                    //NSString *LoginEmailId = [login_data valueForKey:@"email"];
                    //NSString *LoginUserName = [NSString stringWithFormat:@"%@",[login_data valueForKey:@"name"]];
                    
                    //[CrashlyticsKit setUserIdentifier:LoginUserId];
                    //[CrashlyticsKit setUserEmail:LoginEmailId];
                    //[CrashlyticsKit setUserName:LoginUserName];
                    
                    //                     [NSUserData SaveRegisterPopUp:@"1"];
                    //                     if ([WithoutLoginSip length]) {
                    //                         [NSUserData SaveWithoutID:@"Yes"];
                    //                     }
                    HomeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                    [self presentViewController:destVC animated:NO completion:nil];
                } else {
                    NSString *error = [NSString stringWithFormat:@"%@",[response valueForKey:@"message"]];
                    [AppHelper ShowAlert:error Title:@"Error" FromVC:self Completion:nil];
                }
            } else {
                // NSError *err = (NSError*)response;
                // NSLog(@"Error %@",err.localizedDescription);
            }
            //[hud hideAnimated:YES];
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)timerEvent {
    if (remainTime > 0) {
        remainTime -= 1;
        _lblOtpTime.text = [NSString stringWithFormat:@"00:%02d", remainTime];
    } else {
        [timer invalidate];
        timer = nil;
        self.btnResendCode.hidden = NO;
        self.lblOtpTime.hidden = YES;
    }
}

-(BOOL)isValidFields {
    if (isIos12) {
        if (self.tfSingleOtp.text.length != 6) {
            [AppHelper ShowAlert:@"Please enter OTP" Title:@"" FromVC:self Completion:nil];
            return NO;
        }
    } else {
        if (self.tfOtp1.text.length == 0 || self.tfOtp2.text.length == 0 || self.tfOtp3.text.length == 0 || self.tfOtp4.text.length == 0 ||
            self.tfOtp5.text.length == 0 || self.tfOtp6.text.length == 0) {
            [AppHelper ShowAlert:@"Please enter OTP" Title:@"" FromVC:self Completion:nil];
            return NO;
        }
    }
    return YES;
}

- (IBAction)btnVerify_Tapped:(id)sender {
    if ([self isValidFields]) {
        NSString *strEnteredCode;
        if (isIos12) {
            strEnteredCode = [NSString stringWithFormat:@"%@", self.tfSingleOtp.text];
        } else {
            strEnteredCode = [NSString stringWithFormat:@"%@%@%@%@%@%@", _tfOtp1.text, _tfOtp2.text, _tfOtp3.text, _tfOtp4.text, _tfOtp5.text, _tfOtp6.text];
        }
        if ([strEnteredCode isEqualToString:strOtpCode]) {
            if (self.isResetPassword) {
                // Go to Reset Password Screen
                ResetPasswordVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ResetPasswordVC"];
                destVC.strEmail = self.dicRegData[@"email"];
                destVC.strMobile = self.dicRegData[@"mobile"];
                [self.navigationController pushViewController:destVC animated:YES];
            } else {
                // Register and go further
                [self registerAPICall];
            }
        } else {
            [AppHelper ShowAlert:@"Invalid OTP entered." Title:@"" FromVC:self Completion:nil];
        }
    }
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
