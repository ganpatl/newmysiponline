//
//  IntroVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/11/18.
//

#import <UIKit/UIKit.h>
#import "MDCPageControl.h"

NS_ASSUME_NONNULL_BEGIN

@interface IntroVC : UIViewController

@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet MDCPageControl *pageControl;

@end

NS_ASSUME_NONNULL_END
