//
//  SignInVC.m
//  MySIPonline
//
//  Created by Ganpat on 16/11/18.
//

#import "SignInVC.h"
#import "HomeVC.h"
#import "PhoneVerificaitonVC.h"
#import <Crashlytics/Crashlytics.h>

@interface SignInVC () {
    
}

@end

@implementation SignInVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
}

- (IBAction)tfEmail_EditingChanged:(id)sender {
}
- (IBAction)tfPassword_EditingChanged:(id)sender {
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShowPassword_TouchDown:(id)sender {
    self.tfPassword.secureTextEntry = NO;
}
- (IBAction)btnShowPassword_TouchUpInside:(id)sender {
    self.tfPassword.secureTextEntry = YES;
}
- (IBAction)btnShowPassword_TouchUpOutside:(id)sender {
    self.tfPassword.secureTextEntry = YES;
}

- (IBAction)btnForgotPassword_Tapped:(id)sender {
    PhoneVerificaitonVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneVerificaitonVC"];
    destVC.isResetPassword = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnContinue_Tapped:(id)sender {
    if ([self isValidated]) {
        [self callLoginAPI];
    }
}

-(BOOL)isValidated {
    // Email
    if ([self.tfEmail.text.TrimSpace isEqualToString:@""]) {
        self.tfEmail.errorMessage = self.tfEmail.title;
        self.lblErrEmail.text = EnterEmailMessage;
        self.lblErrEmail.hidden = NO;
        return NO;
    }
    if (![AppHelper validateEmailWithString:self.tfEmail.text.TrimSpace]) {
        self.tfEmail.errorMessage = self.tfEmail.title;
        self.lblErrEmail.text = EnterValidEmail;
        self.lblErrEmail.hidden = NO;
        return NO;
    }
    self.lblErrEmail.text = @"";
    self.lblErrEmail.hidden = YES;
    self.tfEmail.errorMessage = @"";
    
    // Password
    if ([self.tfPassword.text.TrimSpace isEqualToString:@""]) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = EnterPasswordMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
//    if (!([self.tfPassword.text length] > 4)) {
//        self.tfPassword.errorMessage = self.tfPassword.title;
//        self.lblErrPassword.text = Password4CharMessage;
//        self.lblErrPassword.hidden = NO;
//        return NO;
//    }
//    if ([self.tfPassword.text length] > 20) {
//        self.tfPassword.errorMessage = self.tfPassword.title;
//        self.lblErrPassword.text = PasswordLimitMessage;
//        self.lblErrPassword.hidden = NO;
//        return NO;
//    }
//    self.lblErrPassword.text = @"";
//    self.lblErrPassword.hidden = YES;
//    self.tfPassword.errorMessage = @"";
    return YES;
}

-(void)callLoginAPI {
    [self.view endEditing:NO];
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"email": self.tfEmail.text.TrimSpace, @"password":self.tfPassword.text};
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_loginData:dicParam AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                int Status = [response[@"status"] intValue];
                if (Status == 1) {
                    NSDictionary *dicProfile = response[@"data"];
                    // Remove <Null> element from Dictionary
                    NSMutableDictionary *mdic = dicProfile.mutableCopy;
                    [mdic removeObjectsForKeys:[mdic allKeysForObject:[NSNull null]]];
                    [NSUserData SaveEditProfileDataAPI:mdic];
                    
                    // Save Login User Data
                    NSString *LoginUserId = dicProfile[@"id"];
                    NSString *LoginEmailId = dicProfile[@"email"];
                    NSString *LoginUserName = [NSString stringWithFormat:@"%@", dicProfile[@"name"]];
                    NSDictionary *dicLogin = @{@"email":LoginEmailId, @"id":LoginUserId, @"image":dicProfile[@"image"], @"mobile":dicProfile[@"mobile"], @"name":dicProfile[@"name"]};
                    [NSUserData SaveLoginData:dicLogin];
                    
                    // Save Invest Status
                    [NSUserData SaveInvestStatus:[NSString stringWithFormat:@"%@", dicProfile[@"invest_status"]]];
                    
                    // Save Profile Status
                    [NSUserData SaveProfileStatus:[NSString stringWithFormat:@"%@", dicProfile[@"profile_status"]]];
                    
                    //NSString *WithoutLoginSip = [NSUserData GetFundIdWithoutLogin];
                    [CrashlyticsKit setUserIdentifier:LoginUserId];
                    [CrashlyticsKit setUserEmail:LoginEmailId];
                    [CrashlyticsKit setUserName:LoginUserName];
                    
                    //
                    //                             if ([WithoutLoginSip length]) {
                    //                                 [NSUserData SaveWithoutID:@"Yes"];
                    //                                 UIStoryboard *stry = self.storyboard;
                    //                                 UITabBarController *svc = [stry instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
                    //                                 [self presentViewController:svc animated:NO completion:nil];
                    //                             } else {
                    //                                 UIStoryboard *stry = self.storyboard;
                    //                                 UITabBarController *svc = [stry instantiateViewControllerWithIdentifier:@"HomeTabBarController"];
                    //                                 [self presentViewController:svc animated:NO completion:nil];
                    //                             }
                    //                         } else {
                    //                             NSString *error = [NSString stringWithFormat:@"%@",[response valueForKey:@"message"]];
                    //                             UIAlertView *alert = [[UIAlertView alloc]initWithTitle:@"Error" message:error delegate:self cancelButtonTitle:OKString otherButtonTitles:nil, nil];
                    //                             [alert show];
                    HomeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                    [self.navigationController pushViewController:destVC animated:YES];
                } else {
                    [AppHelper ShowAlert:response[@"msg"] Title:@"Error" FromVC:self Completion:nil];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
            [LoaderVC dismiss];
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self.navigationController pushViewController:destVC animated:NO];
    }
}

@end
