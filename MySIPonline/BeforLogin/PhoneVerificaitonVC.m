//
//  PhoneVerificaitonVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import "PhoneVerificaitonVC.h"
#import "RegisterOtpVerificationVC.h"

@interface PhoneVerificaitonVC () <UIPickerViewDelegate, UIPickerViewDataSource, UITextFieldDelegate> {
    NSMutableArray *aryCountry;
    UIPickerView *countryPickerView;
    NSString *strSelectedCountry;
}

@end

@implementation PhoneVerificaitonVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblTitle.textColor = [UIColor getAppColorTitleText];
    self.lblDesc.textColor  = [UIColor getAppColorGrayText];
    if (self.isResetPassword) {
        self.lblTitle.text = @"Reset Password";
    } else {
        self.lblTitle.text = @"Phone Verification";
    }
    
    // Get Country from Device
    NSLocale *countryLocale = [NSLocale currentLocale];
    NSString *countryCode = [countryLocale objectForKey:NSLocaleCountryCode];
    strSelectedCountry = [countryLocale displayNameForKey:NSLocaleCountryCode value:countryCode];
    //GSLog(@"Country Code:%@ Name:%@", countryCode, country);
    if (strSelectedCountry == nil || [strSelectedCountry isEqualToString:@""]) {
        self.tfCountryCode.text = @"India";
        strSelectedCountry = self.tfCountryCode.text;
    } else {
        self.tfCountryCode.text = strSelectedCountry;
    }
    
    aryCountry = [NSMutableArray new];
    [self getCountryCodeList];
    
    // Create Picker View
    countryPickerView = [UIPickerView new];
    countryPickerView.delegate = self;
    countryPickerView.dataSource = self;
    
    self.tfMobileNumber.delegate = self;
    self.tfCountryCode.inputView = countryPickerView;    
}


-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
}

// Before Text Change
- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    if (textField == self.tfMobileNumber) {
        int mobileLimit = 14;
        if ([self.tfCountryCode.text isEqualToString:@"IN +91"]) {
            mobileLimit = 10;
        }
        if (self.tfMobileNumber.text.length >= mobileLimit && range.length == 0) {
            return NO; // Change not allowed
        } else {
            if ([self.tfCountryCode.text isEqualToString:@"IN +91"] && self.tfMobileNumber.text.length == 0 && [string intValue] < 5 && range.length == 0) {
                return NO; // Allow only starting digit >= 5
            } else {
                return YES;
            }
        }
    } else {
        return YES;
    }
}

// After Text Change
- (IBAction)textFieldMobileEditingChanged:(UITextField*)sender {
    int mobileLimit = 14;
    if ([self.tfCountryCode.text isEqualToString:@"IN +91"]) {
        mobileLimit = 10;
    }
    if (self.tfCountryCode.text.length > 0 && self.tfMobileNumber.text.length == mobileLimit) {
        //self.transparentView.hidden = YES;
        [self checkMobileRegistered:self.tfMobileNumber.text];
    } else {
        self.transparentView.hidden = NO;
    }
}

- (IBAction)countryTextEditingBegin:(UITextField *)sender {
    int intIdx = [self getIndexOfCounty:strSelectedCountry];
    if (intIdx >= 0 && intIdx < aryCountry.count) {
        [countryPickerView selectRow:intIdx inComponent:0 animated:NO];
    }
}

-(BOOL)isValidated {
    if ([self.tfCountryCode.text.TrimSpace isEqualToString:@""]) {
        [AppHelper ShowAlert:SelectCountryMessage Title:@"Error" FromVC:self Completion:nil];
        return NO;
    }
    if ([self.tfMobileNumber.text.TrimSpace isEqualToString:@""]) {
        [AppHelper ShowAlert:EnterMobileNumberMessage Title:@"Error" FromVC:self Completion:nil];
        return NO;
    }
    if (![AppHelper validatePhoneNumber:self.tfMobileNumber.text]) {
        [AppHelper ShowAlert:EnterValidMobileNumberMessage Title:@"Error" FromVC:self Completion:nil];
        return NO;
    }
    return YES;
}

#pragma mark - Web Api Call

-(void)checkMobileRegistered:(NSString *)strMobile {
    [self.view endEditing:NO];
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"mobile":strMobile, @"email":@""};
        [LoaderVC showWithStatus:@"Please Wait"];
        NSString __weak *mobileNo = strMobile;
        [WebServices api_checkMobileEmail:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                self.transparentView.hidden = NO;
                int status = [response[@"status"] intValue];
                if (self.isResetPassword) {
                    if (status == 1) {
                        NSString *strEmail = response[@"result"][@"Otherclient"][@"email"];
                        if (strEmail != nil) {
                            self.dicRegData = @{@"email":strEmail, @"mobile":mobileNo};
                            //[self goToOtpVerificationScreen];
                            self.transparentView.hidden = YES;
                        }
                    } else {
                        [AppHelper ShowAlert:response[@"result"] Title:@"Error" FromVC:self Completion:nil];
                    }
                } else {
                    if (status == 0) {
                        NSMutableDictionary *mdic = self.dicRegData.mutableCopy;
                        mdic[@"mobile"] = strMobile;
                        self.dicRegData = mdic;
                        self.transparentView.hidden = YES;
                    } else {
                        [AppHelper ShowAlert:@"Mobile number already exist." Title:@"Error" FromVC:self Completion:nil];
                    }
                }
                
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)getCountryCodeList {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [WebServices api_getCountryCodeList:nil AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryCountry = [response[@"result"] mutableCopy];
                    if (self->aryCountry != nil) {
                        int idx = [self getIndexOfCounty:self.tfCountryCode.text];
                        if (idx >= 0) {
                            self.tfCountryCode.tag = idx;
                            self.tfCountryCode.text = [NSString stringWithFormat:@"%@ +%@", self->aryCountry[idx][@"code"], self->aryCountry[idx][@"phone_code"]];
                            self->strSelectedCountry = self->aryCountry[idx][@"name"];
                        } else {
                            self.tfCountryCode.text = @"India";
                            self->strSelectedCountry = self.tfCountryCode.text;
                            int idx2 = [self getIndexOfCounty:self.tfCountryCode.text];
                            if (idx2 >= 0) {
                                self.tfCountryCode.tag = idx2;
                                self.tfCountryCode.text = [NSString stringWithFormat:@"%@ +%@", self->aryCountry[idx2][@"code"], self->aryCountry[idx2][@"phone_code"]];
                                self->strSelectedCountry = self->aryCountry[idx2][@"name"];
                            }
                        }
                        [self->countryPickerView reloadAllComponents];
                    }
                }
            } else {
                //NSError *err = (NSError*)response;
                //[AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(int)getIndexOfCounty:(NSString *)country {
    int idx = 0;
    for (NSDictionary *dic in aryCountry) {
        if ([dic[@"name"] isEqualToString:country]) {
            return idx;
        }
        idx++;
    }
    return -1;
}

-(void)goToOtpVerificationScreen {
    RegisterOtpVerificationVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RegisterOtpVerificationVC"];
    destVC.isResetPassword = self.isResetPassword;
    NSMutableDictionary *dicData = [[NSMutableDictionary alloc] initWithDictionary:self.dicRegData];
    //dicData[@"mobile"] = self.tfMobileNumber.text;
    if (![self.tfCountryCode.text isEqualToString:@"IN +91"]) {
        dicData[@"nri_country"] = strSelectedCountry;
    }
    destVC.dicRegData = dicData;
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnGetOtp_Tapped:(id)sender {
    if ([self isValidated]) {
        [self goToOtpVerificationScreen];
//        if (self.isResetPassword) {
//            [self checkMobileRegistered:self.tfMobileNumber.text];
//        } else {
//            [self goToOtpVerificationScreen];
//        }
    }
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Country Picker View Delegates

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return aryCountry.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (row < aryCountry.count) {
        return aryCountry[row][@"name"];
    } else {
        return @"";
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (aryCountry.count > row) {
        self.tfCountryCode.text = [NSString stringWithFormat:@"%@ +%@", aryCountry[row][@"code"], aryCountry[row][@"phone_code"]];
        strSelectedCountry = aryCountry[row][@"name"];
        self.tfCountryCode.tag = row;
        
        int mobileLimit = 14;
        if ([self.tfCountryCode.text isEqualToString:@"IN +91"]) {
            mobileLimit = 10;
        }
        if (self.tfMobileNumber.text.length == mobileLimit) {
            //self.transparentView.hidden = YES;
            [self checkMobileRegistered:self.tfMobileNumber.text];
        } else {
            self.transparentView.hidden = NO;
        }
    }
}

@end
