//
//  IntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/11/18.
//

#import "IntroVC.h"
#import "HomeVC.h"
#import "IntroSliderView.h"
#import "CreateAccountVC.h"
#import "PhoneVerificaitonVC.h"
#import <Crashlytics/Crashlytics.h>
@import GoogleSignIn;

@interface IntroVC () <UIScrollViewDelegate, GIDSignInDelegate, GIDSignInUIDelegate> {
    
}

@end

@implementation IntroVC

#pragma mark - ViewController LifeCycle Events

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.pageControl.numberOfPages = 3;
    self.pageControl.currentPageIndicatorTintColor = [UIColor getAppColorBlue];
    //pageControl.autoresizingMask = (UIViewAutoresizingFlexibleWidth |  UIViewAutoresizingFlexibleHeight);
    //CGSize pageControlSize = [pageControl sizeThatFits:pageControl.bounds.size];
    //pageControl.frame = CGRectMake(0, self.scrollView.frame.origin.y + self.scrollView.frame.size.height,
//                                   self.view.bounds.size.width, 35);
    [self.pageControl addTarget:self action:@selector(didChangePage:) forControlEvents:UIControlEventValueChanged];
    self.scrollView.showsHorizontalScrollIndicator = NO;
    self.scrollView.delegate = self;
    
    //Advanced_algo
    [self setupSliderImages];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
}

-(void)setupSliderImages {
    NSDictionary *dicItem1 = @{@"title":@"Advanced Algorithmic Platform",
                               @"desc":@"Back-tested Algorithms To Automate Your Investments",
                               @"image":@"Advanced_algo"};
    NSDictionary *dicItem2 = @{@"title":@"Say Goodbye to Forms",
                               @"desc":@"Paperless Investing Solution for Smart People",
                               @"image":@"goodby"};
    
    NSArray *aryImages = @[dicItem1, dicItem2, dicItem1];
    int intStartX = 0;
    int intY = 0;
    for (NSDictionary *dicItem in aryImages) {
        // Load view from xib
        IntroSliderView *view = [[[NSBundle mainBundle] loadNibNamed:@"IntroSliderView" owner:self options:nil] objectAtIndex:0];
        intY = (self.scrollView.frame.size.height - view.frame.size.height) / 2;
        // Set data on it
        view.frame = CGRectMake(intStartX, intY, view.frame.size.width, view.frame.size.height);
        view.imgMain.image = [UIImage imageNamed:dicItem[@"image"]];
        //view.imgMain.image = [UIImage animatedImageWithAnimatedGIFURL:[NSURL URLWithString:@"https://upload.wikimedia.org/wikipedia/commons/d/d3/Newtons_cradle_animation_book_2.gif"]];
        view.lblTitle.text = dicItem[@"title"];
        view.lblDesc.text = dicItem[@"desc"];
        [self.scrollView addSubview:view];
        intStartX += self.scrollView.frame.size.width;
    }
    self.scrollView.contentSize = CGSizeMake(self.scrollView.frame.size.width * aryImages.count,
                                             self.scrollView.frame.size.height);
}

#pragma mark - Page Control page Change event

-(void)didChangePage:(MDCPageControl*)sender {
    CGPoint offset = self.scrollView.contentOffset;
    offset.x = (CGFloat)sender.currentPage * self.scrollView.bounds.size.width;
    [self.scrollView setContentOffset:offset animated:YES];
}

#pragma mark - ScrollView Delegates

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    [self.pageControl scrollViewDidScroll:scrollView];
}

-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
    [self.pageControl scrollViewDidEndDecelerating:scrollView];
}

-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
    [self.pageControl scrollViewDidEndScrollingAnimation:scrollView];
}

#pragma mark - Button Tap Events

- (IBAction)btnOtherEmail_Tapped:(id)sender {
    CreateAccountVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateAccountVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnGoogle_Tapped:(id)sender {
    [GIDSignIn sharedInstance].delegate   = self;
    [GIDSignIn sharedInstance].uiDelegate = self;
    [LoaderVC showWithStatus:@"Please Wait"];
    [[GIDSignIn sharedInstance] signIn];
}

#pragma mark - Google Sign In

- (void)signInWillDispatch:(GIDSignIn *)signIn error:(NSError *)error {
    [LoaderVC dismiss];
}

// Present a view that prompts the user to sign in with Google
- (void)signIn:(GIDSignIn *)signIn presentViewController:(UIViewController *)viewController {
    [LoaderVC dismiss];
    [self presentViewController:viewController animated:YES completion:nil];
}

// Dismiss the "Sign in with Google" view
- (void)signIn:(GIDSignIn *)signIn dismissViewController:(UIViewController *)viewController {
//    [SVProgressHUD showWithStatus:@"Please Wait" maskType:SVProgressHUDMaskTypeBlack];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)signIn:(GIDSignIn *)signIn didSignInForUser:(GIDGoogleUser *)user withError:(NSError *)error {
    // Perform any operations on signed in user here.
//    [SVProgressHUD dismiss];
    if (user != nil) {
        NSString *email = user.profile.email;
        if (email != nil) {
            //NSString *strDate = [AppHelper convertNSDateToString:[NSDate date] FormatString:@"yyyy-MM-dd HH:mm:ss"];
            NSDictionary *dicParams = @{@"email":email, @"mobile":@"0"};
            GSLog(@"%@", dicParams);
            
            [self loginAPICallWithEmail:email GoogleUser:user];
            
//            typeof(user) __weak weakObj = user;
            
//            if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//                [LoaderVC showWithStatus:@"Please Wait"];
//                [WebServices api_checkMobileEmail:dicParams AndCompletionHandler:^(id response, bool isError) {
//                     if (!isError) {
//                         NSString *userId = weakObj.userID; // For client-side use only!
//                         int Status = [response[@"status"] intValue];
//                         if (Status == 1) {
//                             // Already registered - call login api
//                             [self loginAPICallWithEmail:email GoogleId:userId];
//
//                         } else if (Status == 0) {
//                             // Not Registered - redirect to Mobile Verification
//                             PhoneVerificaitonVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneVerificaitonVC"];
//                             //    NSString *idToken = user.authentication.idToken; // Safe to send to the server
//                             NSString *fullName = weakObj.profile.name;
//                             //    NSString *givenName = user.profile.givenName;
//                             //    NSString *familyName = user.profile.familyName;
//
//                             NSMutableDictionary *mdic = dicParams.mutableCopy;
//                             mdic[@"password"] = @"";
//                             mdic[@"google_id"] = userId;
//                             mdic[@"name"] = fullName;
//                             destVC.dicRegData = mdic;
//                             [self.navigationController pushViewController:destVC animated:YES];
//
//                         }
//                     } else {
//                        //NSError *err = (NSError*)response;
//                        //NSLog(@"Error %@",err.localizedDescription);
//                     }
//                     [LoaderVC dismiss];
//                 }];
//            } else {
//                NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
//                [self.navigationController pushViewController:destVC animated:NO];
//            }
        }
    }
}

-(void)loginAPICallWithEmail:(NSString *)strEmail GoogleUser:(GIDGoogleUser*)user {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSString *strGoogleId = user.userID; // For client-side use only!
        NSDictionary *dicParam = @{@"email":strEmail, @"google_id":strGoogleId};
        [LoaderVC showWithStatus:@"Please Wait"];
        typeof(user) __weak weakObj = user;
        [WebServices api_loginData:dicParam AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                int Status = [response[@"status"] intValue];
                if (Status == 1) {
                    // Gmail Id Already Registerd
                    
                    NSDictionary *dicProfile = response[@"data"];
                    // Remove <Null> element from Dictionary
                    NSMutableDictionary *mdic = dicProfile.mutableCopy;
                    [mdic removeObjectsForKeys:[mdic allKeysForObject:[NSNull null]]];
                    [NSUserData SaveEditProfileDataAPI:mdic];
                    
                    // Save Login User Data
                    NSString *LoginUserId = dicProfile[@"id"];
                    NSString *LoginEmailId = dicProfile[@"email"];
                    NSString *LoginUserName = [NSString stringWithFormat:@"%@", dicProfile[@"name"]];
                    NSDictionary *dicLogin = @{@"email":LoginEmailId, @"id":LoginUserId, @"image":dicProfile[@"image"], @"mobile":dicProfile[@"mobile"], @"name":dicProfile[@"name"]};
                    [NSUserData SaveLoginData:dicLogin];
                    
                    // Save Invest Status
                    [NSUserData SaveInvestStatus:[NSString stringWithFormat:@"%@", dicProfile[@"invest_status"]]];
                    
                    // Save Profile Status
                    [NSUserData SaveProfileStatus:[NSString stringWithFormat:@"%@", dicProfile[@"profile_status"]]];
                    
                    // Set Crashlytics user infor
                    [CrashlyticsKit setUserIdentifier:LoginUserId];
                    [CrashlyticsKit setUserEmail:LoginEmailId];
                    [CrashlyticsKit setUserName:LoginUserName];

                    // Redirect to Home Screen
                    HomeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HomeVC"];
                    [self.navigationController pushViewController:destVC animated:YES];
                } else if (Status == 2) {
                    // Not Registered - redirect to Mobile Verification
                    PhoneVerificaitonVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneVerificaitonVC"];
                    NSMutableDictionary *mdic = dicParam.mutableCopy;
                    mdic[@"google_id"] = weakObj.userID;
                    mdic[@"name"] = weakObj.profile.name;
                    destVC.dicRegData = mdic;
                    [self.navigationController pushViewController:destVC animated:YES];
                }
            } else {
                //                      NSError *err = (NSError*)response;
                //                      NSLog(@"Error %@",err.localizedDescription);
            }
            [LoaderVC dismiss];
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self.navigationController pushViewController:destVC animated:NO];
    }
}

@end
