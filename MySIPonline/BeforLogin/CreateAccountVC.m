//
//  CreateAccountVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import "CreateAccountVC.h"
#import "PhoneVerificaitonVC.h"
#import "SignInVC.h"

@interface CreateAccountVC () {
    BOOL isEmailVerified;
}

@end

@implementation CreateAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblTitle.textColor = [UIColor getAppColorTitleText];
    self.lblDesc.textColor  = [UIColor getAppColorGrayText];
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
}

#pragma mark - TextField Editing

- (IBAction)tfFullName_EditingChanged:(id)sender {
    self.tfFullName.errorMessage = @"";
    self.lblErrFullName.text = @"";
    self.lblErrFullName.hidden = YES;
}
- (IBAction)tfEmail_EditingChanged:(id)sender {
    self.tfEmail.errorMessage = @"";
    self.lblErrEmail.text = @"";
    self.lblErrEmail.hidden = YES;
}
- (IBAction)tfPassword_EditingChanged:(id)sender {
    self.tfPassword.errorMessage = @"";
    self.lblErrPassword.text = @"";
    self.lblErrPassword.hidden = YES;
}
- (IBAction)tfConfirmPassword_EditingChanged:(id)sender {
    self.tfConfirmPassword.errorMessage = @"";
    self.lblErrConfirmPassword.text = @"";
    self.lblErrConfirmPassword.hidden = YES;
}

- (IBAction)tfEmail_EditingDidEnd:(id)sender {
    if ([AppHelper validateEmailWithString:self.tfEmail.text.TrimSpace]) {
        if ([AppDelegate getAppDelegate].checkNetworkReachability) {
            NSDictionary *dicParam = @{@"email": self.tfEmail.text.TrimSpace, @"mobile":@""};
            [WebServices api_checkMobileEmail:dicParam AndCompletionHandler:^(id response, bool isError) {
                if (!isError) {
                    int Status = [response[@"status"] intValue];
                    if (Status == 1) {
                        [AppHelper ShowAlert:@"This email already exist." Title:@"Error" FromVC:self Completion:^(UIAlertAction *action) {
                            self.tfEmail.text = @"";
                            [self.tfEmail becomeFirstResponder];
                        }];
                    }
                    self->isEmailVerified = Status != 1;
                } else {
                    NSError *err = (NSError*)response;
                    [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
                }
            }];
        } else {
            NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
            [self.navigationController pushViewController:destVC animated:NO];
        }
    }
}

-(BOOL)isValidated {
    // Full Name
    if ([self.tfFullName.text.TrimSpace isEqualToString:@""]) {
        self.tfFullName.errorMessage = self.tfFullName.title;
        self.lblErrFullName.text = EnterFullNameMessage;
        self.lblErrFullName.hidden = NO;
        return NO;
    }
    if (![AppHelper validationNameString:self.tfFullName.text.TrimSpace]) {
        self.tfFullName.errorMessage = self.tfFullName.title;
        self.lblErrFullName.text = SpecialCharacterMessage;
        self.lblErrFullName.hidden = NO;
        return NO;
    }
    self.lblErrFullName.text = @"";
    self.lblErrFullName.hidden = YES;
    self.tfFullName.errorMessage = @"";
    
    // Email
    if ([self.tfEmail.text.TrimSpace isEqualToString:@""]) {
        self.tfEmail.errorMessage = self.tfEmail.title;
        self.lblErrEmail.text = EnterEmailMessage;
        self.lblErrEmail.hidden = NO;
        return NO;
    }
    if (![AppHelper validateEmailWithString:self.tfEmail.text.TrimSpace]) {
        self.tfEmail.errorMessage = self.tfEmail.title;
        self.lblErrEmail.text = EnterValidEmail;
        self.lblErrEmail.hidden = NO;
        return NO;
    }
    self.lblErrEmail.text = @"";
    self.lblErrEmail.hidden = YES;
    self.tfEmail.errorMessage = @"";
    
    // Password
    if ([self.tfPassword.text.TrimSpace isEqualToString:@""]) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = EnterPasswordMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
    if (!([self.tfPassword.text length] >= 4)) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = Password4CharMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
    if ([self.tfPassword.text length] > 20) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = PasswordLimitMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
    self.lblErrPassword.text = @"";
    self.lblErrPassword.hidden = YES;
    self.tfPassword.errorMessage = @"";
    
    // Confirm Password
    if ([self.tfConfirmPassword.text.TrimSpace isEqualToString:@""]) {
        self.tfConfirmPassword.errorMessage = self.tfConfirmPassword.title;
        self.lblErrConfirmPassword.text = confirmPasswordMessage;
        self.lblErrConfirmPassword.hidden = NO;
        return NO;
    }
    if (!([self.tfPassword.text isEqualToString:self.tfConfirmPassword.text])) {
        self.tfConfirmPassword.errorMessage = self.tfConfirmPassword.title;
        self.lblErrConfirmPassword.text = PasswordAndRetypePasswordMessage;
        self.lblErrConfirmPassword.hidden = NO;
        return NO;
    }
    if (isEmailVerified == NO) {
        [AppHelper ShowAlert:@"Email not verified." Title:@"" FromVC:self Completion:nil];
        return NO;
    }
    self.lblErrConfirmPassword.text = @"";
    self.lblErrConfirmPassword.hidden = YES;
    self.tfConfirmPassword.errorMessage = @"";
    
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnContinue_Tapped:(id)sender {
    if ([self isValidated]) {
        PhoneVerificaitonVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PhoneVerificaitonVC"];
        destVC.dicRegData = @{@"name":self.tfFullName.text.TrimSpace, @"email":self.tfEmail.text.TrimSpace, @"password":self.tfPassword.text};
        [self.navigationController pushViewController:destVC animated:YES];
    }
}

- (IBAction)btnSignIn_Tapped:(id)sender {
    SignInVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SignInVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
