//
//  SignInVC.h
//  MySIPonline
//
//  Created by Ganpat on 16/11/18.
//

#import <UIKit/UIKit.h>
#import "SkyFloatingLabelTextField-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface SignInVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfEmail;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblErrEmail;
@property (weak, nonatomic) IBOutlet UILabel *lblErrPassword;

@end

NS_ASSUME_NONNULL_END
