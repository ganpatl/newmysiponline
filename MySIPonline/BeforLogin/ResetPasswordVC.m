//
//  ResetPasswordVC.m
//  MySIPonline
//
//  Created by Ganpat on 16/11/18.
//

#import "ResetPasswordVC.h"

@interface ResetPasswordVC () {
    
}

@end

@implementation ResetPasswordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
}


-(BOOL)isValidated {
    // Password
    if ([self.tfPassword.text.TrimSpace isEqualToString:@""]) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = EnterPasswordMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
    if (!([self.tfPassword.text length] > 4)) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = Password4CharMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
    if ([self.tfPassword.text length] > 20) {
        self.tfPassword.errorMessage = self.tfPassword.title;
        self.lblErrPassword.text = PasswordLimitMessage;
        self.lblErrPassword.hidden = NO;
        return NO;
    }
    self.lblErrPassword.text = @"";
    self.lblErrPassword.hidden = YES;
    self.tfPassword.errorMessage = @"";
    
    // Confirm Password
    if ([self.tfConfirmPassword.text.TrimSpace isEqualToString:@""]) {
        self.tfConfirmPassword.errorMessage = self.tfConfirmPassword.title;
        self.lblErrConfirmPassword.text = confirmPasswordMessage;
        self.lblErrConfirmPassword.hidden = NO;
        return NO;
    }
    if (!([self.tfPassword.text isEqualToString:self.tfConfirmPassword.text])) {
        self.tfConfirmPassword.errorMessage = self.tfConfirmPassword.title;
        self.lblErrConfirmPassword.text = PasswordAndRetypePasswordMessage;
        self.lblErrConfirmPassword.hidden = NO;
        return NO;
    }
    self.lblErrConfirmPassword.text = @"";
    self.lblErrConfirmPassword.hidden = YES;
    self.tfConfirmPassword.errorMessage = @"";
    
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)btnUpdate_Tapped:(id)sender {
    if ([self isValidated]) {
        [self callResetPasswordAPI];
    }
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Web api call

-(void)callResetPasswordAPI {
    [self.view endEditing:NO];
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"mobile":self.strMobile, @"email":self.strEmail, @"password":self.tfPassword.text};
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_resetPassword:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                int Status = [response[@"status"] intValue];
                if (Status == 1) {
                    [AppHelper ShowAlert:response[@"msg"] Title:@"" FromVC:self Completion:^(UIAlertAction *action) {
                        [self.navigationController popToRootViewControllerAnimated:YES];
                    }];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self.navigationController pushViewController:destVC animated:NO];
    }
}

@end
