//
//  PhoneVerificaitonVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PhoneVerificaitonVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UITextField *tfCountryCode;
@property (weak, nonatomic) IBOutlet UITextField *tfMobileNumber;
@property (weak, nonatomic) IBOutlet UIButton *btnGetOtp;
@property (weak, nonatomic) IBOutlet UIView *transparentView;

@property NSDictionary *dicRegData;
@property BOOL isResetPassword;

@end

NS_ASSUME_NONNULL_END
