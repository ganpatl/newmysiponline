//
//  RegisterOtpVerificationVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RegisterOtpVerificationVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UITextField *tfOtp1;
@property (weak, nonatomic) IBOutlet UITextField *tfOtp2;
@property (weak, nonatomic) IBOutlet UITextField *tfOtp3;
@property (weak, nonatomic) IBOutlet UITextField *tfOtp4;
@property (weak, nonatomic) IBOutlet UITextField *tfOtp5;
@property (weak, nonatomic) IBOutlet UITextField *tfOtp6;
@property (weak, nonatomic) IBOutlet UIView *transparentView;
@property (weak, nonatomic) IBOutlet UILabel *lblOtpTime;
@property (weak, nonatomic) IBOutlet UIButton *btnResendCode;
@property (weak, nonatomic) IBOutlet UIView *singleOtpView;
@property (weak, nonatomic) IBOutlet UITextField *tfSingleOtp;

@property NSDictionary *dicRegData;
@property BOOL isResetPassword;

@end

NS_ASSUME_NONNULL_END
