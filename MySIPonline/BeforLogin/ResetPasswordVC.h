//
//  ResetPasswordVC.h
//  MySIPonline
//
//  Created by Ganpat on 16/11/18.
//

#import <UIKit/UIKit.h>
#import "SkyFloatingLabelTextField-Swift.h"

NS_ASSUME_NONNULL_BEGIN

@interface ResetPasswordVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfPassword;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfConfirmPassword;

@property (weak, nonatomic) IBOutlet UILabel *lblErrPassword;
@property (weak, nonatomic) IBOutlet UILabel *lblErrConfirmPassword;

@property NSString *strMobile;
@property NSString *strEmail;

@end

NS_ASSUME_NONNULL_END
