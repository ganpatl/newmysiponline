//
//  AppUpdateVC.h
//  MySIPonline
//
//  Created by Ganpat on 07/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AppUpdateVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdateNow;

@end

NS_ASSUME_NONNULL_END
