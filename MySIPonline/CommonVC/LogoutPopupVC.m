//
//  LogoutPopupVC.m
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import "LogoutPopupVC.h"

@interface LogoutPopupVC ()

@end

@implementation LogoutPopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.popupView.layer.cornerRadius = 4;
    [AppHelper addShadowOnView:self.bottomShadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, -2) Opacity:0.5 Radius:2.0];
}

#pragma mark - Button Tap Events

- (IBAction)btnCancel_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnYes_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
    [NSUserData SaveLoginData:nil];
    UIViewController *navIntroVC = [self.storyboard instantiateViewControllerWithIdentifier:@"navIntroVC"];
    [AppDelegate getAppDelegate].window.rootViewController = navIntroVC;
}


@end
