//
//  AppUpdateVC.m
//  MySIPonline
//
//  Created by Ganpat on 07/03/19.
//

#import "AppUpdateVC.h"

@interface AppUpdateVC ()

@end

@implementation AppUpdateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.popupView.layer.cornerRadius = 3;
    self.btnUpdateNow.layer.cornerRadius = 3;
}

- (IBAction)btnUpdateNow_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
