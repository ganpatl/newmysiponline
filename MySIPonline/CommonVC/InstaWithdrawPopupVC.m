//
//  InstaWithdrawPopupVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/03/19.
//

#import "InstaWithdrawPopupVC.h"

@interface InstaWithdrawPopupVC ()

@end

@implementation InstaWithdrawPopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    _btnGotIt.layer.cornerRadius = _btnGotIt.frame.size.height / 2;
    _btnGotIt.layer.masksToBounds = YES;
}

#pragma mark - Button Tap Event

- (IBAction)btnGotIt_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
