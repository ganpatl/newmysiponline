//
//  webVC.h
//  MySIPonline
//
//  Created by Ganpat on 18/08/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
@import WebKit;

@interface WebVC : UIViewController

@property (strong, nonatomic) NSString *strTitle;
@property (strong, nonatomic) NSString *strUrlLink;
@property BOOL showShareButton;

@end
