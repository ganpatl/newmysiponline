//
//  ContactUsVC.h
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import <UIKit/UIKit.h>
@import MapKit;

NS_ASSUME_NONNULL_BEGIN

@interface ContactUsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btnHeadOffice;
@property (weak, nonatomic) IBOutlet UIButton *btnBranchOffice;
@property (weak, nonatomic) IBOutlet UILabel *lblAddress;
@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@end

NS_ASSUME_NONNULL_END
