//
//  webVC.m
//  MySIPonline
//
//  Created by Ganpat on 18/08/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "WebVC.h"
//#import "CancelOrderVC.h"
#import "BankMandateInvestorListVC.h"
//#import "InProcessTransactionView.h"

@interface WebVC () <WKNavigationDelegate, WKScriptMessageHandler> {
    WKWebView *webView;
    BOOL isPaymentSuccess;
    NSTimer *timer;
}

@end

@implementation WebVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    isPaymentSuccess = NO;
    self.title = _strTitle;
    
    CGRect wFrame = self.view.frame;
    
    self.view.backgroundColor = UIColor.whiteColor;
    
    // Back Button
    UIButton *btnBack = [UIButton new];
    btnBack.translatesAutoresizingMaskIntoConstraints = NO;
    [btnBack setBackgroundImage:[UIImage imageNamed:@"Back Arrow"] forState:UIControlStateNormal];
    [btnBack addTarget:self action:@selector(btnBack_Tapped) forControlEvents:UIControlEventTouchUpInside];
    btnBack.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.view addSubview:btnBack];
    NSArray *horizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-16-[btnBack]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnBack)];
    [self.view addConstraints:horizontal];
    
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    [configuration.userContentController addScriptMessageHandler:self name:@"MySipOnline"];
    webView = [[WKWebView alloc] initWithFrame:wFrame configuration:configuration];
    [self.view addSubview:webView];
    
    //webView.backgroundColor = [UIColor redColor];
    webView.translatesAutoresizingMaskIntoConstraints = NO;
    if (@available(iOS 11, *)) {
        UILayoutGuide * guide = self.view.safeAreaLayoutGuide;
        [webView.leadingAnchor constraintEqualToAnchor:guide.leadingAnchor].active = YES;
        [webView.trailingAnchor constraintEqualToAnchor:guide.trailingAnchor].active = YES;
        [btnBack.topAnchor constraintEqualToAnchor:guide.topAnchor].active = YES;
        [webView.bottomAnchor constraintEqualToAnchor:guide.bottomAnchor].active = YES;
    } else {
        UILayoutGuide *margins = self.view.layoutMarginsGuide;
        [webView.leadingAnchor constraintEqualToAnchor:margins.leadingAnchor].active = YES;
        [webView.trailingAnchor constraintEqualToAnchor:margins.trailingAnchor].active = YES;
        [btnBack.topAnchor constraintEqualToAnchor:self.topLayoutGuide.bottomAnchor].active = YES;
        [webView.bottomAnchor constraintEqualToAnchor:self.bottomLayoutGuide.topAnchor].active = YES;
    }
    NSArray *vertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[btnBack]-8-[webView]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnBack, webView)];
    [self.view addConstraints:vertical];
    
    if (self.showShareButton) {
        UIButton *btnShare = [UIButton new];
        btnShare.translatesAutoresizingMaskIntoConstraints = NO;
        [btnShare setBackgroundImage:[UIImage imageNamed:@"blogShare"] forState:UIControlStateNormal];
        [btnShare addTarget:self action:@selector(btnBack_Tapped) forControlEvents:UIControlEventTouchUpInside];
        btnShare.imageView.contentMode = UIViewContentModeScaleAspectFit;
        [self.view addSubview:btnShare];
        NSArray *horizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:[btnShare(45)]-16-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnShare)];
        [self.view addConstraints:horizontal];
        NSArray *vertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:[btnShare(45)]" options:0 metrics:nil views:NSDictionaryOfVariableBindings(btnShare)];
        [self.view addConstraints:vertical];
        [self.view addConstraint:[NSLayoutConstraint constraintWithItem:btnShare attribute:NSLayoutAttributeBottom relatedBy:NSLayoutRelationEqual toItem:webView attribute:NSLayoutAttributeBottom multiplier:1.0 constant:-16]];
    }
    
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
    //[self.myView layoutIfNeeded];
    
    
    // Removes cache of WKWebView
    NSSet *websiteDataTypes
    = [NSSet setWithArray:@[
                            WKWebsiteDataTypeDiskCache,
                            //WKWebsiteDataTypeOfflineWebApplicationCache,
                            WKWebsiteDataTypeMemoryCache,
                            //WKWebsiteDataTypeLocalStorage,
                            //WKWebsiteDataTypeCookies,
                            //WKWebsiteDataTypeSessionStorage,
                            //WKWebsiteDataTypeIndexedDBDatabases,
                            //WKWebsiteDataTypeWebSQLDatabases
                            ]];
    //// All kinds of data
    //NSSet *websiteDataTypes = [WKWebsiteDataStore allWebsiteDataTypes];
    //// Date from
    NSDate *dateFrom = [NSDate dateWithTimeIntervalSince1970:0];
    //// Execute
    [[WKWebsiteDataStore defaultDataStore] removeDataOfTypes:websiteDataTypes modifiedSince:dateFrom completionHandler:^{
        // Done
    }];
    
    webView.navigationDelegate = self;
    NSURL *url = [NSURL URLWithString:_strUrlLink];
    if (url != nil) {
        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
        [webView loadRequest:requestObj];
    } else {
        //NSString *link = @"https://www.youtube.com/embed/7IuHk13LUb4?rel=0";
        [webView loadHTMLString:@"<head> <meta name=viewport content='width=device-width, initial-scale=1'><style type='text/css'> body { margin: 0;} </style></head><iframe width=100% height=100% src=https://www.youtube.com/embed/7IuHk13LUb4?rel=0 frameborder=0 allowfullscreen></iframe>" baseURL:nil];
    }
    [LoaderVC showWithStatus:@"Please Wait"];
    // Do any additional setup after loading the view.
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    if ([self.title isEqualToString:@"Payment"]) {
        self.tabBarController.tabBar.hidden = YES;
    }
}

-(void)viewWillDisappear:(BOOL)animated {
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


#pragma mark - WKWebView Navigation Delegate

-(void)webView:(WKWebView *)webView didStartProvisionalNavigation:(WKNavigation *)navigation {
    //[self.activityIndicator startAnimating];
}

-(void)webView:(WKWebView *)webView didFinishNavigation:(WKNavigation *)navigation {
    if ([self.title isEqualToString:@"Payment"]) {
        timer = [NSTimer scheduledTimerWithTimeInterval:8.0 repeats:NO block:^(NSTimer * _Nonnull timer) {
            [LoaderVC dismiss];
            [timer invalidate];
            timer = nil;
        }];
    } else {
        [LoaderVC dismiss];
    }
    //[webView evaluateJavaScript:@"window.location.reload(true)" completionHandler:nil];
}

#pragma mark - WKScriptMessageHandler

-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSDictionary *sentData = (NSDictionary *)message.body;
    // window.webkit.messageHandlers.MySipOnline.postMessage({"message":"Button Tapped"});
    NSLog(@"\n\nMessage received: %@", sentData);
    
    if ([AppHelper isNotNull:sentData[@"openScreen"]]) {
        if ([sentData[@"openScreen"] isEqualToString:@"inProcessTransaction"]) {
            //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            //InProcessTransactionView *destVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"InProcessTransactionView"];
            //UINavigationController *nav = self.navigationController;
            //[self.navigationController popViewControllerAnimated:NO];
            //[nav pushViewController:destVC animated:YES];
            
        } else if ([sentData[@"openScreen"] isEqualToString:@"BankMandate"]) {
            UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
            BankMandateInvestorListVC *destVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"BankMandateInvestorListVC"];
            UINavigationController *nav = self.navigationController;
            [self.navigationController popToRootViewControllerAnimated:NO];
            [nav pushViewController:destVC animated:YES];
            
        } else if ([sentData[@"openScreen"] isEqualToString:@"cart"]) {
            NSString *strId = sentData[@"surveyKey"];
            [self addSurveyWithId:strId];
        }
    } else if (sentData[@"status"] != nil) {
        //skipSurvey = YES;
        isPaymentSuccess = [sentData[@"status"] isEqualToString:@"success"];
        if (isPaymentSuccess == NO) {
            //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            //CancelOrderVC *destVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"CancelOrderVC"];
            //CATransition* transition = [CATransition animation];
            //transition.duration = 0.3;
            //transition.type = kCATransitionFade;
            //transition.subtype = kCATransitionFromTop;
            
            //[self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            //[self.navigationController pushViewController:destVC animated:NO];
        }
    }
    //[self dismissMyView];
}


-(void)btnBack_Tapped {
    if ([self.strTitle.lowercaseString isEqualToString:@"payment"]) {
        if (isPaymentSuccess) {
            [self.navigationController popToRootViewControllerAnimated:YES];
        } else {
            //UIStoryboard *mainStoryboard = [UIStoryboard storyboardWithName:@"Main" bundle: nil];
            //CancelOrderVC *destVC = [mainStoryboard instantiateViewControllerWithIdentifier:@"CancelOrderVC"];
            //CATransition* transition = [CATransition animation];
            //transition.duration = 0.3;
            //transition.type = kCATransitionFade;
            //transition.subtype = kCATransitionFromTop;
            
            //destVC.modalPresentationStyle = UIModalPresentationPopover;
            //[self.navigationController.view.layer addAnimation:transition forKey:kCATransition];
            //[self.navigationController pushViewController:destVC animated:NO];
        }
    } else {
        [self.navigationController popViewControllerAnimated:YES];
    }
}

#pragma mark - Web Api Call

-(void)addSurveyWithId:(NSString *)reasonId {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *dicLogin = [NSUserData GetLoginDataAPI];
//        NSString *UserID = [NSString stringWithFormat:@"%@", [dicLogin valueForKey:@"id"]];
//        NSArray *aryOrderId = [NSUserData GetOrderIdArray];
//        NSString *strAmount  = [NSUserData GetOrderAmount];
//        strAmount = [NSUserData GetOrderAmount];
//        NSDictionary *dicParams = @{@"client_id":UserID, @"orderno":aryOrderId, @"amount":strAmount, @"reason":reasonId};
//        [LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_AddSurveyData:dicParams AndCompletionHandler:^(id response, bool isError) {
//            if (!isError) {
//                int status = [[response valueForKey:@"status"] intValue];
//                if (status) {
//                    [self.navigationController popViewControllerAnimated:YES];
//                } else {
//                    //NSLog(@"Not Equal");
//                }
//            } else {
//                // NSError *err = (NSError*)response;
//                // NSLog(@"Error %@",err.localizedDescription);
//            }
//            [LoaderVC dismiss];
//        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

@end
