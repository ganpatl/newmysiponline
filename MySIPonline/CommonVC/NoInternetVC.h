//
//  NoInternetVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NoInternetVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end

NS_ASSUME_NONNULL_END
