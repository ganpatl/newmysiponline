//
//  NoInternetVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/11/18.
//

#import "NoInternetVC.h"

@interface NoInternetVC ()

@end

@implementation NoInternetVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.lblTitle.textColor = [UIColor getAppColorTitleText];
    self.lblDesc.textColor  = [UIColor getAppColorGrayText];
}

- (IBAction)btnRetry_Tapped:(id)sender {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [self dismissViewControllerAnimated:NO completion:nil];
    }
}

@end
