//
//  ContactUsVC.m
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import "ContactUsVC.h"

@interface ContactUsVC ()

@end

@implementation ContactUsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    MKCoordinateRegion region;
    region.center.latitude = 24.5993673;
    region.center.longitude = 73.6873341;
    region.span.latitudeDelta = 0.01;
    region.span.longitudeDelta = 0.01;
    region = [self.mapView regionThatFits:region];
    [self.mapView setRegion:region animated:TRUE];
    
    MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
    annotation.title = @"Cognus Technology";
    annotation.coordinate = region.center;
//    [self.mapView addSubview:view];
    [self.mapView addAnnotation:annotation];
    
}


#pragma mark - Button Tap Events


- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
