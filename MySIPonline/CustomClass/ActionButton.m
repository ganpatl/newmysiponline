//
//  ActionButton.m
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import "ActionButton.h"

@implementation ActionButton

- (void)setupDefaults {
    //[self.titleLabel setFont:[UIFont fontWithName:@"Roboto-Medium" size:14]];
    self.layer.cornerRadius = 3;
    self.layer.masksToBounds = YES;
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

@end
