//
//  BodyDarkLabel.m
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import "BodyDarkLabel.h"

@implementation BodyDarkLabel

- (void)setupDefaults {
    [self setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.textColor = [UIColor getAppColorDarkGrayText];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

@end
