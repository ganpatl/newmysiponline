//
//  HeadlineLabel.m
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import "HeadlineLabel.h"

@implementation HeadlineLabel

- (void)setupDefaults {
    [self setFont:[UIFont fontWithName:@"Roboto-Regular" size:24]];
    self.textColor = [UIColor getAppColorTitleText];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

@end
