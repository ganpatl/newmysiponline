//
//  SubHeaderDarkLabel.m
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import "SubHeaderDarkLabel.h"

@implementation SubHeaderDarkLabel

- (void)setupDefaults {
    [self setFont:[UIFont fontWithName:@"Roboto-Medium" size:16]];
    self.textColor = [UIColor getAppColorDarkGrayText];
}

- (instancetype)initWithFrame:(CGRect)frame {
    self = [super initWithFrame:frame];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

- (instancetype)initWithCoder:(NSCoder *)coder {
    self = [super initWithCoder:coder];
    if (self) {
        [self setupDefaults];
    }
    return self;
}

@end
