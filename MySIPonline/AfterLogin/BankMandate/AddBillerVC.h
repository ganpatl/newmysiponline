//
//  AddBillerVC.h
//  MySIPonline
//
//  Created by Ganpat on 26/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AddBillerVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblAddBiller;
@property (weak, nonatomic) IBOutlet UIButton *btnAddBiller;
@property NSString *strInvestorId;
@property NSString *strMandateId;
@end

NS_ASSUME_NONNULL_END
