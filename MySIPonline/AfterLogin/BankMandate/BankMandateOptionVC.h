//
//  BankMandateOptionVC.h
//  MySIPonline
//
//  Created by Ganpat on 04/02/19.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankMandateOptionVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;

@property NSDictionary *dicInvestor;
@property NSString *strMandateId;
@end

NS_ASSUME_NONNULL_END
