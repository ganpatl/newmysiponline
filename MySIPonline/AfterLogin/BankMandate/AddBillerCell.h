//
//  AddBillerCell.h
//  MySIPonline
//
//  Created by Ganpat on 27/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddBillerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnCopyURN;
@property (weak, nonatomic) IBOutlet UIButton *btnKnowMore;

@end

NS_ASSUME_NONNULL_END
