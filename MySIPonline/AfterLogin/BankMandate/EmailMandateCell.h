//
//  EmailMandateCell.h
//  MySIPonline
//
//  Created by Ganpat on 27/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface EmailMandateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnEmailForm;
@property (weak, nonatomic) IBOutlet UIButton *btnUploadForm;

@end

NS_ASSUME_NONNULL_END
