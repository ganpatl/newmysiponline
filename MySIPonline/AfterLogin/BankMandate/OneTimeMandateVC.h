//
//  OneTimeMandateVC.h
//  MySIPonline
//
//  Created by Ganpat on 04/02/19.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface OneTimeMandateVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblEmailMandate;

@property NSString *strInvestorId;
@property NSString *strMandateId;
@property NSString *strBankId;

@end

NS_ASSUME_NONNULL_END
