//
//  AddBillerVC.m
//  MySIPonline
//
//  Created by Ganpat on 26/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "AddBillerVC.h"
#import "AddBillerCell.h"
//#import "StartSipPopUpVC.h"
//#import "WebViewInvestVC.h"

@interface AddBillerVC () <RKTagsViewDelegate, UITableViewDataSource, UITableViewDelegate> {
    AddBillerCell *addBillerCell;
    CGFloat tagviewLastHeight;
    NSString *strBankUrl;
}

@end

@implementation AddBillerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tblAddBiller registerNib:[UINib nibWithNibName:@"AddBillerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AddBillerCell"];
    self.tblAddBiller.dataSource = self;
    self.tblAddBiller.delegate = self;
    
    [self getBillerUrn_apiCall];
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

#pragma mark - Web Api Call

- (void)getBillerUrn_apiCall {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *params = @{@"client_id":self.strInvestorId};
//        [LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_getBillerUrn:params AndCompletionHandler:^(id response, bool isError) {
//            if (!isError) {
//                NSString *status = [NSString stringWithFormat:@"%@", [response valueForKey:@"status"]];
//                NSInteger StatusId = [status integerValue];
//                if (StatusId == 1) {
//                    self->strBankUrl = response[@"bankurl"];
//                    [self->addBillerCell.btnCopyURN setTitle:response[@"biller_id"] forState:UIControlStateNormal];
//                    self->addBillerCell.btnCopyURN.hidden = NO;
//                } else {
//                    [Helper ShowAlert:response[@"msg"] FromVC:self Completion:^(UIAlertAction *action) {
//                        [self.navigationController popViewControllerAnimated:YES];
//                    }];
//                }
//            } else {
//                // NSError *err = (NSError*)response;
//                // NSLog(@"Error %@",err.localizedDescription);
//            }
//            [LoaderVC dismiss];
//        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}


#pragma mark - TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (addBillerCell == nil) {
        addBillerCell = [tableView dequeueReusableCellWithIdentifier:@"AddBillerCell"];
        [addBillerCell.btnKnowMore addTarget:self action:@selector(btnKnowMore_Tapped) forControlEvents:UIControlEventTouchUpInside];
        addBillerCell.btnCopyURN.hidden = YES;
        [addBillerCell.btnCopyURN addTarget:self action:@selector(btnCopyURN_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return addBillerCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 440;
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCopyURN_Tapped:(UIButton*)sender {
    UIPasteboard *pasteboard = [UIPasteboard generalPasteboard];
    pasteboard.string = sender.titleLabel.text;
    [AppHelper ShowAlert:@"URN number copied." Title:@"" FromVC:self Completion:nil];
}

-(void)btnKnowMore_Tapped {
//    WebViewInvestVC *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"WebViewInvestVC"];
//    NSString *strBlogId = @"793";
//    svc.urlLinkBlog = [NSString stringWithFormat:@"%@blog.php?id=%@", API_BLOGURL, strBlogId];
//    [self.navigationController pushViewController:svc animated:YES];
}

- (IBAction)btnAddBiller_Tapped:(id)sender {
//    if (self.strMandateId != nil && ![self.strMandateId isEqualToString:@"0"]) {
//        UIViewController *webVC = [Helper createWebViewControllerWithTitle:@"Add Biller" URLString:strBankUrl];
//        [self.navigationController pushViewController:webVC animated:YES];
//    } else {
//        // display popup to start SIP
//        StartSipPopUpVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StartSipPopUpVC"];
//        [self presentViewController:destVC animated:YES completion:nil];
//    }
}

@end
