//
//  BankMandateOptionVC.m
//  MySIPonline
//
//  Created by Ganpat on 04/02/19.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "BankMandateOptionVC.h"
#import "AddBillerVC.h"
#import "OneTimeMandateVC.h"
//#import "InProcessTransactionView.h"

@interface BankMandateOptionVC () {
    BOOL doneDebitMandate;
}

@end

@implementation BankMandateOptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [AppHelper addShadowOnView:self.shadowView1 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
    [AppHelper addShadowOnView:self.shadowView2 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
    
//    NSDictionary *dicProfile = [NSUserData GetProfileDetails];
//    if ([Helper isNotNull:dicProfile[@"personal"][@"debit_mandate_status"]]) {
//        NSString *strDebitMandateStatus = dicProfile[@"personal"][@"debit_mandate_status"];
//        doneDebitMandate = [strDebitMandateStatus isEqualToString:@"1"];
//    }
}

#pragma mark - Button Tap Events

-(IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddBiller_Tapped:(id)sender {
    AddBillerVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddBillerVC"];
    destVC.strInvestorId = self.dicInvestor[@"id"];
    NSArray *aryBanks = self.dicInvestor[@"bank"];
    NSDictionary *dicBank;
    for (NSDictionary *dic in aryBanks) {
        if ([dic[@"preferred_bank"] isEqualToString:@"1"]) {
            dicBank = dic;
        }
    }
    if (dicBank == nil && aryBanks.count > 0) {
        dicBank = aryBanks.firstObject;
    }
    destVC.strMandateId = dicBank[@"mandate_id"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnOneTimeMandate_Tapped:(id)sender {
    OneTimeMandateVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OneTimeMandateVC"];
    destVC.strInvestorId = self.dicInvestor[@"id"];
    NSArray *aryBanks = self.dicInvestor[@"bank"];
    NSDictionary *dicBank;
    for (NSDictionary *dic in aryBanks) {
        if ([dic[@"preferred_bank"] isEqualToString:@"1"]) {
            dicBank = dic;
        }
    }
    if (dicBank == nil && aryBanks.count > 0) {
        dicBank = aryBanks.firstObject;
    }
    destVC.strMandateId = dicBank[@"mandate_id"];
    destVC.strBankId = dicBank[@"id"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
