//
//  BMInstructionCell.h
//  MySIPonline
//
//  Created by Ganpat on 30/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BMInstructionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnKnowMore;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblKnowMore;

@end

NS_ASSUME_NONNULL_END
