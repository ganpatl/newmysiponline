//
//  MandateDetailsVC.m
//  MySIPonline
//
//  Created by Ganpat on 30/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "MandateDetailsVC.h"

@interface MandateDetailsVC ()

@end

@implementation MandateDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(1, 2) Opacity:0.6 Radius:3];
    
    _viewUnderProcess.layer.cornerRadius = 5;
    _viewUnderProcess.layer.borderWidth = 1;
    _viewUnderProcess.layer.borderColor = [UIColor colorWithHexString:@"efa62a"].CGColor;
    
    if (self.isInProcess) {
        self.lblTitle.text = @"Mandate Status";
        self.imgMandateStatus.image = nil;
        self.lblMandateStatus.text = @"In Process";
        self.containerView.hidden = YES;
    } else {
        self.lblTitle.text = @"Mandate Details";
        NSArray *aryBanks = self.dicInvestor[@"bank"];
        NSDictionary *dicBank;
        for (NSDictionary *dic in aryBanks) {
            if ([dic[@"preferred_bank"] isEqualToString:@"1"]) {
                dicBank = dic;
            }
        }
        if (dicBank == nil && aryBanks.count > 0) {
            dicBank = aryBanks.firstObject;
        }
        self.lblStartDate.text = dicBank[@"mandate_start_date"];
        self.lblEndDate.text = dicBank[@"mandate_end_date"];
        self.lblMandateLimit.text = dicBank[@"debit_mndt_amt_limit"];
    }
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
