//
//  BankMandateInvestorListVC.m
//  MySIPonline
//
//  Created by Ganpat on 30/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "BankMandateInvestorListVC.h"
#import "BMInvestorListCell.h"
#import "BMInstructionCell.h"
#import "BankMandateOptionVC.h"
#import "MandateDetailsVC.h"

@interface BankMandateInvestorListVC () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *aryInvestor;
}

@end

@implementation BankMandateInvestorListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    aryInvestor = [@[@{@"name":@"Sonu Kumar Sharma", @"bank":@[@{@"preferred_bank":@"1", @"acc_num":@"123456789", @"bank_name":@"ICICI Bank Ltd"}]}] mutableCopy];
    
    [self.tblInvestorList registerNib:[UINib nibWithNibName:@"BMInvestorListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BMInvestorListCell"];
    [self.tblInvestorList registerNib:[UINib nibWithNibName:@"BMInstructionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BMInstructionCell"];
    self.tblInvestorList.estimatedRowHeight = 88;
    self.tblInvestorList.dataSource = self;
    self.tblInvestorList.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [self InvestorListData];
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryInvestor.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (aryInvestor.count > indexPath.row) {
        if ([aryInvestor[indexPath.row] isKindOfClass:[NSNumber class]]) {
            BMInstructionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BMInstructionCell"];
            [cell.btnKnowMore removeTarget:self action:@selector(btnKnowMore_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnKnowMore addTarget:self action:@selector(btnKnowMore_Tapped) forControlEvents:UIControlEventTouchUpInside];
            return cell;
        } else {
            BMInvestorListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BMInvestorListCell"];
            NSDictionary *dicInvestor = aryInvestor[indexPath.row];
            cell.btnInvestor.tag = indexPath.row;
            [cell.btnInvestor removeTarget:self action:@selector(btnInvestor_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            [cell.btnInvestor addTarget:self action:@selector(btnInvestor_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            
            NSArray *aryBanks = dicInvestor[@"bank"];
            NSDictionary *dicBank;
            for (NSDictionary *dic in aryBanks) {
                if ([dic[@"preferred_bank"] isEqualToString:@"1"]) {
                    dicBank = dic;
                }
            }
            if (dicBank == nil && aryBanks.count > 0) {
                dicBank = aryBanks.firstObject;
            }
            BOOL isRegistered = [dicBank[@"debit_mandate_register_status"] intValue] == 1;
            BOOL isInProcess = [dicBank[@"debit_mandate_status"] intValue] == 1;
            if (isRegistered) {
                cell.lblInvestorName.text = dicInvestor[@"name"];
                NSString *strAc = [AppHelper maskStringWithXtoDigit:4 String:dicBank[@"acc_num"]];
                cell.lblBankAndAc.text = [NSString stringWithFormat:@"%@ : %@", dicBank[@"bank_name"], strAc];
                cell.lblStatus.text = @"Registered";
                cell.lblStatus.textColor = [UIColor colorWithHexString:@"34A853"];
                cell.imgStatus.image = [UIImage imageNamed:@"radioChecked.png"];
            } else {
                if (isInProcess) {
                    cell.lblInvestorName.text = dicInvestor[@"name"];
                    NSString *strAc = [AppHelper maskStringWithXtoDigit:4 String:dicBank[@"acc_num"]];
                    cell.lblBankAndAc.text = [NSString stringWithFormat:@"%@ : %@", dicBank[@"bank_name"], strAc];
                    cell.lblStatus.text = @"In Process";
                    cell.lblStatus.textColor = [UIColor colorWithRed:237.0/255.0 green:221.0/255.0 blue:40.0/255.0 alpha:1.0];
                    cell.imgStatus.image = [UIImage imageNamed:@"Group 862.png"];
                } else {
                    cell.lblInvestorName.text = dicInvestor[@"name"];
                    NSString *strAc = [AppHelper maskStringWithXtoDigit:4 String:dicBank[@"acc_num"]];
                    cell.lblBankAndAc.text = [NSString stringWithFormat:@"%@ : %@", dicBank[@"bank_name"], strAc];
                    cell.lblStatus.text = @"Not Registered";
                    cell.lblStatus.textColor = [UIColor colorWithHexString:@"EB4335"];
                    cell.imgStatus.image = [UIImage imageNamed:@"red-error.png"];
                }
            }
            return cell;
        }
    }
    return [UITableViewCell new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

- (void)InvestorListData {
//    NSDictionary *params = [NSUserData GetLoginData];
//    NSString *UserID = [NSString stringWithFormat:@"%@", [params valueForKey:@"id"]];
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *params = @{ @"user_id" : UserID};
//        [LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_InvestorsListData:params AndCompletionHandler:^(id response, bool isError) {
//            if (!isError) {
//                NSString *status = [NSString stringWithFormat:@"%@", [response valueForKey:@"status"]];
//                NSInteger StatusId = [status integerValue];
//                if (StatusId == 1) {
//                    self->aryInvestor = [[response valueForKey:@"clients"] mutableCopy];
//                    [self->aryInvestor addObject:@YES];
//                    [self.tblInvestorList reloadData];
//                } else {
//                    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"" message:NODataMessage delegate:self cancelButtonTitle:nil otherButtonTitles:OKString, nil];
//                    [alert show];
//                }
//            } else {
//                NSError *err = (NSError*)response;
//                [Helper ShowAlert:err.localizedDescription FromVC:self Completion:nil];
//            }
//            [LoaderVC dismiss];
//        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

- (void)btnInvestor_Tapped:(UIButton*)sender {
//    NSDictionary *dicInvestor = aryInvestor[sender.tag];
//    NSArray *aryBanks = dicInvestor[@"bank"];
//    NSDictionary *dicBank;
//    for (NSDictionary *dic in aryBanks) {
//        if ([dic[@"preferred_bank"] isEqualToString:@"1"]) {
//            dicBank = dic;
//        }
//    }
//    if (dicBank == nil && aryBanks.count > 0) {
//        dicBank = aryBanks.firstObject;
//    }
//    BOOL isRegistered = [dicBank[@"debit_mandate_register_status"] intValue] == 1;
//    BOOL isInProcess = [dicBank[@"debit_mandate_status"] intValue] == 1;
//    if (isRegistered) {
//        MandateDetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MandateDetailsVC"];
//        destVC.isInProcess = NO;
//        destVC.dicInvestor = dicInvestor;
//        [self.navigationController pushViewController:destVC animated:YES];
//    } else {
//        if (isInProcess) {
//            MandateDetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MandateDetailsVC"];
//            destVC.isInProcess = YES;
//            [self.navigationController pushViewController:destVC animated:YES];
//        } else { // Not Registered
//            BankMandateOptionVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BankMandateOptionVC"];
//            destVC.dicInvestor = dicInvestor;
//            [self.navigationController pushViewController:destVC animated:YES];
//        }
//    }
    
    BankMandateOptionVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BankMandateOptionVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (void)btnKnowMore_Tapped {
    
}

@end
