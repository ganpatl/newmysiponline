//
//  MandateDetailsVC.h
//  MySIPonline
//
//  Created by Ganpat on 30/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MandateDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblMandateStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgMandateStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStartDate;
@property (weak, nonatomic) IBOutlet UILabel *lblEndDate;
@property (weak, nonatomic) IBOutlet UILabel *lblMandateLimit;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *containerView;
@property (weak, nonatomic) IBOutlet UILabel *lblInProcessMessage;
@property (weak, nonatomic) IBOutlet UIView *viewUnderProcess;

@property NSDictionary *dicInvestor;
@property BOOL isInProcess;

@end

NS_ASSUME_NONNULL_END
