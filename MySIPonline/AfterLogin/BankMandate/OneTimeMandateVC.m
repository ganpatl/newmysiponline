//
//  OneTimeMandateVC.m
//  MySIPonline
//
//  Created by Ganpat on 04/02/19.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "OneTimeMandateVC.h"
#import "EmailMandateCell.h"
#import "PECropViewController.h"
//#import "InProcessTransactionView.h"
//#import "StartSipPopUpVC.h"

@interface OneTimeMandateVC () <RKTagsViewDelegate, UITableViewDataSource, UITableViewDelegate, UINavigationControllerDelegate, UIImagePickerControllerDelegate, PECropViewControllerDelegate> {
    EmailMandateCell *emailMandateCell;
    CGFloat tagviewLastHeight;
}

@end

@implementation OneTimeMandateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [self.tblEmailMandate registerNib:[UINib nibWithNibName:@"EmailMandateCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"EmailMandateCell"];
    self.tblEmailMandate.dataSource = self;
    self.tblEmailMandate.delegate = self;
    
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    self.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.tabBarController.tabBar.hidden = NO;
}

-(void)callEmailFormAPI {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *params = @{@"user_id":self.strInvestorId, @"doc_type":@"mandate"};
//        [LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_EmailDocumentData:params AndCompletionHandler:^(id response, bool isError) {
//             if (!isError) {
//                 NSString *status = [response valueForKey:@"status"];
//                 NSInteger statusID = [status integerValue];
//                 NSString *Message = [ response valueForKey:@"msg"];
//                 if (statusID == 1) {
//                     [Helper ShowAlert:Message FromVC:self Completion:^(UIAlertAction *action) {
//                         [self.navigationController popViewControllerAnimated:YES];
//                     }];
//                     //UIAlertView *successAlert = [[UIAlertView alloc]initWithTitle:@"Success" message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//                     //[successAlert show];
//                 } else {
//                     UIAlertView *ErrorAlert = [[UIAlertView alloc]initWithTitle:@"Error" message:Message delegate:self cancelButtonTitle:nil otherButtonTitles:@"OK", nil];
//                     [ErrorAlert show];
//                 }
//             } else {
//                 // NSError *err = (NSError*)response;
//                 // NSLog(@"Error %@",err.localizedDescription);
//             }
//             [LoaderVC dismiss];
//         }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (emailMandateCell == nil) {
        emailMandateCell = [tableView dequeueReusableCellWithIdentifier:@"EmailMandateCell"];
        [emailMandateCell.btnEmailForm addTarget:self action:@selector(btnDownloadForm_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [emailMandateCell.btnUploadForm addTarget:self action:@selector(btnUploadForm_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        if (self.strMandateId == nil || [self.strMandateId isEqualToString:@"0"]) {
            emailMandateCell.btnUploadForm.hidden = YES;
        }
    }
    return emailMandateCell;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 440;
}


#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnDownloadForm_Tapped:(id)sender {
    if (self.strMandateId != nil && ![self.strMandateId isEqualToString:@"0"]) {
        [self callEmailFormAPI];
    } else {
        // display popup to start SIP
//        StartSipPopUpVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"StartSipPopUpVC"];
//        [self presentViewController:destVC animated:YES completion:nil];
    }
}

- (IBAction)btnUploadForm_Tapped:(id)sender {
    UIAlertController *alertController_Browser = [UIAlertController alertControllerWithTitle:@"" message:@"Choose Mandate Form Image" preferredStyle:UIAlertControllerStyleActionSheet];
    [alertController_Browser addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        
        [self presentViewController:picker animated:YES completion:NULL];
        
    }]];
    
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
        [alertController_Browser addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            [self presentViewController:picker animated:YES completion:NULL];
        }]];
    }
    
    [alertController_Browser addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
        //
    }]];
    [self presentViewController:alertController_Browser animated:TRUE completion:nil];
}

#pragma mark - Image Picker Controller Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    //ImageUpload = YES;
    [picker dismissViewControllerAnimated:YES completion:^{
        [self openEditor:info[UIImagePickerControllerOriginalImage]];
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

#pragma mark CropView

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    [controller dismissViewControllerAnimated:YES completion:NULL];
    UIImage *chosenCustomerImage = croppedImage;
    chosenCustomerImage = [AppHelper scaleAndRotateImage:chosenCustomerImage];
    CGFloat compression = 1.0f;
    NSData *imageData = UIImageJPEGRepresentation(chosenCustomerImage, compression);
    //NSLog(@"%lu",(unsigned long)[imageData length]);
    [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent animated:YES];
    [self callUploadDocumentAPI:[UIImage imageWithData:imageData]];
}

-(void)openEditor:(UIImage*)temImage; {
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = temImage;
    UIImage *image = temImage;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2, (height - length) / 2, length, length);
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:YES completion:NULL];
}

-(void)callUploadDocumentAPI:(UIImage*)selectedImg {
    NSString *base64StringImage;
    base64StringImage = [UIImagePNGRepresentation(selectedImg)
                             base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *dictionary =  @{@"user_id":self.strInvestorId, @"document_type":@"mandateform", @"bank_id":self.strBankId, @"document":base64StringImage};
//        [LoaderVC showWithStatus:@"Please wait as this may take a few minutes..."];
//        [WebServices api_uploadDocumentsData:dictionary AndCompletionHandler:^(id response, bool isError) {
//            if (!isError) {
//                int statusVa = [[response valueForKey:@"status"] intValue];
//                NSString *msg = [response valueForKey:@"msg"];
//                if (statusVa == 1) {
//                    NSMutableDictionary *dicProfile = [[NSUserData GetProfileDetails] mutableCopy];
//                    NSMutableDictionary *dicPersonal = [dicProfile[@"personal"] mutableCopy];
//                    dicPersonal[@"debit_mandate_status"] = @"1";
//                    dicProfile[@"personal"] = dicPersonal;
//                    [NSUserData SaveProfileDetails:dicProfile];
//
//                    [Helper ShowAlert:msg FromVC:self Completion:^(UIAlertAction *action) {
//                        UINavigationController *nav = self.navigationController;
//                        InProcessTransactionView *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InProcessTransactionView"];
//                        destVC.scrollToEnd = YES;
//                        [self.navigationController popToViewController:self.navigationController.viewControllers[self.navigationController.viewControllers.count - 3] animated:NO];
//                        //[self.navigationController popViewControllerAnimated:NO];
//                        [nav pushViewController:destVC animated:YES];
//                    }];
//                }
//            } else {
//                // NSError *err = (NSError*)response;
//                // NSLog(@"Error %@",err.localizedDescription);
//            }
//            [LoaderVC dismiss];
//        }];
    } else {
        //   [Helper PopUpAlertController:sender :InternetConnectionMessage :@""];
    }
}

@end
