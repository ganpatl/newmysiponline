//
//  BankMandateInvestorListVC.h
//  MySIPonline
//
//  Created by Ganpat on 30/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankMandateInvestorListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblInvestorList;

@end

NS_ASSUME_NONNULL_END
