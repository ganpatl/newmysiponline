//
//  BMInvestorListCell.h
//  MySIPonline
//
//  Created by Ganpat on 30/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BMInvestorListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestorName;
@property (weak, nonatomic) IBOutlet UILabel *lblBankAndAc;
@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UIButton *btnInvestor;

@end

NS_ASSUME_NONNULL_END
