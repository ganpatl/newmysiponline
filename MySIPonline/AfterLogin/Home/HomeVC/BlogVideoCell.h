//
//  BlogVideoCell.h
//  MySIPonline
//
//  Created by Ganpat on 19/11/18.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BlogVideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgBlog;
@property (weak, nonatomic) IBOutlet UILabel *lblBlogTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnBlog;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoTitle;
@property (weak, nonatomic) IBOutlet UIButton *btnVideo;
@property (weak, nonatomic) IBOutlet UIButton *btnViewAllBlogs;
@property (weak, nonatomic) IBOutlet UIButton *btnViewAllVideo;
@property (weak, nonatomic) IBOutlet YTPlayerView *ytPlayer;

@end

NS_ASSUME_NONNULL_END
