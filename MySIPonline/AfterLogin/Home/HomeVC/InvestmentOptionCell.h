//
//  InvestmentOptionCell.h
//  MySIPonline
//
//  Created by Ganpat on 19/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InvestmentOptionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblTitle;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblDesc;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
// Constraints
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shadowViewTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shadowViewBottomConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *titleTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *descBottomConstraint;


@end

NS_ASSUME_NONNULL_END
