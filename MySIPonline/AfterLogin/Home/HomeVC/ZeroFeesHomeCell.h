//
//  ZeroFeesHomeCell.h
//  MySIPonline
//
//  Created by Ganpat on 28/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ZeroFeesHomeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

NS_ASSUME_NONNULL_END
