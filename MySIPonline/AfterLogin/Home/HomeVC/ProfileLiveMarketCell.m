//
//  ProfileLiveMarketCell.m
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import "ProfileLiveMarketCell.h"

@implementation ProfileLiveMarketCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.imgSensexArrow.layer.cornerRadius = self.imgSensexArrow.frame.size.height / 2;
    self.imgSensexArrow.layer.masksToBounds = YES;
    self.imgNiftyArrow.layer.cornerRadius = self.imgNiftyArrow.frame.size.height / 2;
    self.imgNiftyArrow.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
