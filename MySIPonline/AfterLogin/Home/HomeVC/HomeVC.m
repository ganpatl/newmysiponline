//
//  HomeVC.m
//  MySIPonline
//
//  Created by Ganpat on 16/11/18.
//

#import "HomeVC.h"
#import "FundExploreVC.h"
#import "NFOListVC.h"
#import "DashboardVC.h"
#import "SmartSavingVC.h"
#import "SmartSavingIntroVC.h"
#import "GoldSavingVC.h"
#import "GoldSavingIntroVC.h"
#import "SipInsuranceVC.h"
#import "SipInsuranceIntroVC.h"
#import "LogoutPopupVC.h"
#import "BlogsVideoVC.h"
//#import "AppUpdateVC.h"

// Menu
#import "MyAccountVC.h"
#import "TransactionListVC.h"
#import "ManageSipVC.h"

// Cells
#import "LeftMenuItemCell.h"
#import "ProfileLiveMarketCell.h"
#import "CompleteYourProfileCell.h"
#import "ZeroFeesHomeCell.h"
#import "GetStartedTitleCell.h"
#import "InvestmentOptionCell.h"
#import "MiddleSliderCell.h"
#import "MiddleSliderCollectionCell.h"
#import "BlogVideoCell.h"
#import "NotificationVC.h"

#import "HelpVC.h"

@interface HomeVC () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    LeftMenuItemCell *leftMenuCell;
    ProfileLiveMarketCell *profileLiveMarketCell;
    CompleteYourProfileCell *completeYourProfileCell;
    ZeroFeesHomeCell *zeroFeesHomeCell;
    GetStartedTitleCell *getStartedTitleCell;
    //InvestmentOptionCell *investmentOptionCell;
    MiddleSliderCell *middleSliderCell;
    GetStartedTitleCell *latestUpdateCell;
    BlogVideoCell *blogVideoCell;
    
    //CGPoint menuOriginCenter;
    //CGPoint tranperentViewOrgCenter;
    
    NSArray *aryInvestmentOption;
    NSMutableArray *aryMiddleSlider;
    
    NSDictionary *dicLoginData;
    
    BOOL isProfileComplete;
}

@end

@implementation HomeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [AppHelper addTopShadowOnView:self.topTitleView];
    
    aryInvestmentOption = @[@{@"image":@"investments", @"title":@"Investments", @"desc":@"Start early to Grow early"},
                            @{@"image":@"smartSavings", @"title":@"Smart Savings", @"desc":@"Let your Idle Money earn for you"},
                            @{@"image":@"sipinsure", @"title":@"SIP Insurance", @"desc":@"Free Life Insurance with Mutual Funds"},
                            @{@"image":@"goldInvestment", @"title":@"Gold Investment", @"desc":@"The Modern Way of accumulating gold"},
                            @{@"image":@"NFOs", @"title":@"New Fund Offers", @"desc":@"View upcoming Mutual Fund schemes"}];
    
    [self.tblLeftMenu registerNib:[UINib nibWithNibName:@"LeftMenuItemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LeftMenuItemCell"];
    self.tblLeftMenu.dataSource = self;
    self.tblLeftMenu.delegate = self;
    
    [self.tblHome registerNib:[UINib nibWithNibName:@"ProfileLiveMarketCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ProfileLiveMarketCell"];
    [self.tblHome registerNib:[UINib nibWithNibName:@"CompleteYourProfileCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CompleteYourProfileCell"];
    [self.tblHome registerNib:[UINib nibWithNibName:@"ZeroFeesHomeCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ZeroFeesHomeCell"];
    [self.tblHome registerNib:[UINib nibWithNibName:@"GetStartedTitleCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GetStartedTitleCell"];
    [self.tblHome registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    [self.tblHome registerNib:[UINib nibWithNibName:@"MiddleSliderCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"MiddleSliderCell"];
    [self.tblHome registerNib:[UINib nibWithNibName:@"BlogVideoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BlogVideoCell"];
    
    self.tblHome.dataSource = self;
    self.tblHome.delegate = self;
    self.tblHome.estimatedRowHeight = 85;
    self.tblHome.rowHeight = UITableViewAutomaticDimension;
    
    //menuOriginCenter = self.leftMenuView.center;
    //self.leftMenuView.center = CGPointMake(self.leftMenuView.center.x - self.view.frame.size.width, self.leftMenuView.center.y);
    self.menuLeadingConstraint.constant = -self.view.frame.size.width;
    self.btnTransClose.hidden = YES;
    
    blogVideoCell = [self.tblHome dequeueReusableCellWithIdentifier:@"BlogVideoCell"];
    blogVideoCell.selectionStyle = UITableViewCellSelectionStyleNone;
    [blogVideoCell.btnBlog addTarget:self action:@selector(btnBlogs_Tapped) forControlEvents:UIControlEventTouchUpInside];
    [blogVideoCell.btnVideo addTarget:self action:@selector(btnVideos_Tapped) forControlEvents:UIControlEventTouchUpInside];
    [blogVideoCell.btnViewAllBlogs addTarget:self action:@selector(btnAllBlogs_Tapped) forControlEvents:UIControlEventTouchUpInside];
    [blogVideoCell.btnViewAllVideo addTarget:self action:@selector(btnAllVideos_Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    dicLoginData = [NSUserData GetLoginData];
    
    [self getMiddleSliderData];
    [self getLatestUpdateData];
}

-(void)viewWillAppear:(BOOL)animated {
    // Required for IQKeyboard Manager
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
    
    [self getLiveMarketData];
    
    //AppUpdateVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AppUpdateVC"];
    //[self presentViewController:destVC animated:YES completion:nil];
}

-(void)openLeftMenu {
    self.menuLeadingConstraint.constant = 0;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
        //self.leftMenuView.center = self->menuOriginCenter;
    }];
    [UIView transitionWithView:self.btnTransClose duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.btnTransClose.hidden = NO;
    } completion:NULL];
}

-(void)closeLeftMenu {
    self.menuLeadingConstraint.constant = -self.view.frame.size.width;
    [UIView animateWithDuration:0.5 animations:^{
        [self.view layoutIfNeeded];
        //self.leftMenuView.center = CGPointMake(self.leftMenuView.center.x - self.view.frame.size.width, self.leftMenuView.center.y);
    }];
    [UIView transitionWithView:self.btnTransClose duration:0.5 options:UIViewAnimationOptionTransitionCrossDissolve animations:^{
        self.btnTransClose.hidden = YES;
    } completion:NULL];
}

#pragma mark - Web API Call

-(void)getMiddleSliderData {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_middleSliders:nil AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryMiddleSlider = [NSMutableArray new];
                    NSString *strImagePath = response[@"imagepath"];
                    NSArray *aryTemp = response[@"result"];
                    for (NSDictionary *dic in aryTemp) {
                        NSMutableDictionary *mutDic = [dic mutableCopy];
                        NSString *strCombine = dic[@"discription"];
                        NSArray *arySplit = [strCombine componentsSeparatedByString:@"&@"];
                        if (arySplit.count == 2) {
                            mutDic[@"title"] = arySplit[0];
                            mutDic[@"discription"] = arySplit[1];
                            mutDic[@"image"] = [NSString stringWithFormat:@"%@%@", strImagePath, dic[@"name"]];
                            NSString *strColors = dic[@"color"];
                            NSArray *aryColor = [strColors.TrimSpace componentsSeparatedByString:@","];
                            if (aryColor.count > 0) {
                                NSString *hex1 = [((NSString*)aryColor.firstObject) stringByReplacingOccurrencesOfString:@"0xFF" withString:@""];
                                NSString *hex2 = [((NSString*)aryColor.lastObject)  stringByReplacingOccurrencesOfString:@"0xFF" withString:@""];
                                mutDic[@"color1"] = [UIColor colorWithHexString:hex1.TrimSpace];
                                mutDic[@"color2"] = [UIColor colorWithHexString:hex2.TrimSpace];
                            }
                        }
                        [self->aryMiddleSlider addObject:mutDic];
                    }
                    [self.tblHome reloadData];
                    if (self->middleSliderCell.cvMiddleSlider != nil) {
                        [self->middleSliderCell.cvMiddleSlider reloadData];
                    }
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)getLatestUpdateData {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [WebServices api_latestUpdate:nil AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    // Blog
                    NSDictionary *dicBlog = response[@"result"];
                    NSURL *imageUrl = [NSURL URLWithString:dicBlog[@"thumb"]];
                    if (imageUrl != nil) {
                        [self->blogVideoCell.imgBlog sd_setImageWithURL:imageUrl placeholderImage:nil];
                    }
                    self->blogVideoCell.lblBlogTitle.text = dicBlog[@"txttitle"];
                    
                    // Video
                    self->blogVideoCell.lblVideoTitle.text = response[@"videotitle"];
                    [self->blogVideoCell.ytPlayer loadWithVideoId:response[@"videoId"]];
                    [self.tblHome reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)getLiveMarketData {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [WebServices api_liveMarket:nil AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    NSDictionary *dicSensex = response[@"result"][@"sensex"];
                    NSDictionary *dicNifty  = response[@"result"][@"nifty"];
                    self->profileLiveMarketCell.lblSensex.text = [NSString stringWithFormat:@"%@", dicSensex[@"val"]];
                    double dblChangeSensex = [dicSensex[@"change"] doubleValue];
                    self->profileLiveMarketCell.lblSensexUpDown.text = [NSString stringWithFormat:@"(%.2f)", dblChangeSensex];
                    NSString *sensexPercent = [NSString stringWithFormat:@"%@", dicSensex[@"perc"]];
                    if ([sensexPercent containsString:@"-"]) {
                        self->profileLiveMarketCell.imgSensexArrow.image = [UIImage imageNamed:@"downArrow-Red"];
                    } else {
                        self->profileLiveMarketCell.imgSensexArrow.image = [UIImage imageNamed:@"upArrow-Green"];
                    }
                    
                    self->profileLiveMarketCell.lblNifty.text = [NSString stringWithFormat:@"%@", dicNifty[@"val"]];
                    double dblChangeNifty = [dicNifty[@"change"] doubleValue];
                    self->profileLiveMarketCell.lblNiftyUpDown.text = [NSString stringWithFormat:@"(%.2f)", dblChangeNifty];
                    NSString *niftyPercent = [NSString stringWithFormat:@"%@", dicNifty[@"perc"]];
                    if ([niftyPercent containsString:@"-"]) {
                        self->profileLiveMarketCell.imgNiftyArrow.image = [UIImage imageNamed:@"downArrow-Red"];
                    } else {
                        self->profileLiveMarketCell.imgNiftyArrow.image = [UIImage imageNamed:@"upArrow-Green"];
                    }
                    
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}


#pragma mark - TableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    if (tableView == self.tblLeftMenu) {
        return 1;
    } else {
        return 7;
    }
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tblLeftMenu) {
        return 1;
    } else {
        if (section == 0) {
            return 1;
        } else if (section == 1) {
            return 1;
        } else if (section == 2) {
            return 1;
        } else if (section == 3) {
            return aryInvestmentOption.count;
        } else if (section == 4) {
            return 1;
        } else if (section == 5) {
            return 1;
        } else {
            return 1;
        }
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tblLeftMenu) {
        if (leftMenuCell == nil) {
            leftMenuCell = [tableView dequeueReusableCellWithIdentifier:@"LeftMenuItemCell"];
            
            [leftMenuCell.btnMyAccount addTarget:self action:@selector(btnMyAccount_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnAllTransactions addTarget:self action:@selector(btnAllTransactions_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnManageSIPs addTarget:self action:@selector(btnManageSips_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnSIPCalculator addTarget:self action:@selector(btnSipCalculator_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnFundExplorer addTarget:self action:@selector(btnFundExplorer_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnNewsBlogs addTarget:self action:@selector(btnNewsAndBlogs_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnHelp addTarget:self action:@selector(btnHelp_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnAboutUs addTarget:self action:@selector(btnAboutUs_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [leftMenuCell.btnShare addTarget:self action:@selector(btnShare_Tapped) forControlEvents:UIControlEventTouchUpInside];            
            [leftMenuCell.btnLogout addTarget:self action:@selector(btnLogout_Tapped) forControlEvents:UIControlEventTouchUpInside];
            leftMenuCell.imgUser.layer.cornerRadius = leftMenuCell.imgUser.frame.size.height / 2;
            leftMenuCell.imgUser.layer.masksToBounds = YES;
        }
        leftMenuCell.lblUserName.text = dicLoginData[@"name"];
        NSURL *imageUrl = [NSURL URLWithString:dicLoginData[@"image"]];
        if (imageUrl != nil) {
            [leftMenuCell.imgUser sd_setImageWithURL:imageUrl placeholderImage:nil];
        }
        return leftMenuCell;
    } else {
        
        // HOME TABLE
        if (indexPath.section == 0) {
            if (profileLiveMarketCell == nil) {
                profileLiveMarketCell = [tableView dequeueReusableCellWithIdentifier:@"ProfileLiveMarketCell"];
                profileLiveMarketCell.selectionStyle = UITableViewCellSelectionStyleNone;
                profileLiveMarketCell.lblSensex.text = @"";
                profileLiveMarketCell.lblSensexUpDown.text = @"";
                profileLiveMarketCell.lblNifty.text = @"";
                profileLiveMarketCell.lblNiftyUpDown.text = @"";
                if (dicLoginData != nil) {
                    profileLiveMarketCell.lblWelcomeUser.text = [NSString stringWithFormat:@"Welcome %@", dicLoginData[@"name"]];
                }
            }
            return profileLiveMarketCell;
            
        } else if (indexPath.section == 1) {
            if (isProfileComplete) {
                if (zeroFeesHomeCell == nil) {
                    zeroFeesHomeCell = [tableView dequeueReusableCellWithIdentifier:@"ZeroFeesHomeCell"];
                    zeroFeesHomeCell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                return zeroFeesHomeCell;
            
            } else {
                if (completeYourProfileCell == nil) {
                    completeYourProfileCell = [tableView dequeueReusableCellWithIdentifier:@"CompleteYourProfileCell"];
                    completeYourProfileCell.selectionStyle = UITableViewCellSelectionStyleNone;
                }
                return completeYourProfileCell;
            }
        } else if (indexPath.section == 2) {
            if (getStartedTitleCell == nil) {
                getStartedTitleCell = [tableView dequeueReusableCellWithIdentifier:@"GetStartedTitleCell"];
                getStartedTitleCell.selectionStyle = UITableViewCellSelectionStyleNone;
            }
            return getStartedTitleCell;
            
        } else if (indexPath.section == 3) {
            InvestmentOptionCell *investmentOptionCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
            //if (investmentOptionCell == nil) {
            //}
            if (aryInvestmentOption.count > indexPath.row) {
                investmentOptionCell.imgIcon.image = [UIImage imageNamed:aryInvestmentOption[indexPath.row][@"image"]];
                investmentOptionCell.lblTitle.text = aryInvestmentOption[indexPath.row][@"title"];
                investmentOptionCell.lblDesc.text = aryInvestmentOption[indexPath.row][@"desc"];
            }
            return investmentOptionCell;
            
        } else if (indexPath.section == 4) {
            if (middleSliderCell == nil) {
                middleSliderCell = [tableView dequeueReusableCellWithIdentifier:@"MiddleSliderCell"];
                middleSliderCell.selectionStyle = UITableViewCellSelectionStyleNone;
                [middleSliderCell.cvMiddleSlider registerNib:[UINib nibWithNibName:@"MiddleSliderCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"MiddleSliderCollectionCell"];
                middleSliderCell.cvMiddleSlider.dataSource = self;
                middleSliderCell.cvMiddleSlider.delegate = self;
            }
            return middleSliderCell;
            
        } else if (indexPath.section == 5) {
            if (latestUpdateCell == nil) {
                latestUpdateCell = [tableView dequeueReusableCellWithIdentifier:@"GetStartedTitleCell"];
                latestUpdateCell.selectionStyle = UITableViewCellSelectionStyleNone;
                latestUpdateCell.lblTitle.text = @"Latest Update";
            }
            return latestUpdateCell;
            
        } else {
            return blogVideoCell;
        }
    }
}

#pragma mark - TableView Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (tableView == self.tblLeftMenu) {
        return 620;
    } else {
        if (indexPath.section == 0) {
            return UITableViewAutomaticDimension;
        } else if (indexPath.section == 1) {
            if (isProfileComplete) {
                return 100;
            } else {
                return 65;
            }
        } else if (indexPath.section == 2) {
            return 36;
        } else if (indexPath.section == 3) {
            return 81;
        } else if (indexPath.section == 4) {
            return 120;
        } else if (indexPath.section == 5) {
            return 36;
        } else {
            return UITableViewAutomaticDimension; //216;
        }
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        if (!isProfileComplete) {
            UINavigationController *navProfile = [self.storyboard instantiateViewControllerWithIdentifier:@"navProfileDocs"];
            [self presentViewController:navProfile animated:YES completion:nil];
            CFRunLoopWakeUp(CFRunLoopGetCurrent());
        }
    
    } else if (indexPath.section == 3) {
        if (indexPath.row == 0) {
            BOOL investmentStatus = NO;
            if (investmentStatus) {
                // Dashboard
                DashboardVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DashboardVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            } else {
                // Fund Explore
                FundExploreVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundExploreVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            }
            
        
        } else if (indexPath.row == 1) {
            BOOL investmentStatus = NO;
            if (investmentStatus) {
                SmartSavingVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SmartSavingVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            } else {
                // Static Smart Saving - Intro
                SmartSavingIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SmartSavingIntroVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            }
            
            
        } else if (indexPath.row == 2) {
            // SIP Insurance
            BOOL investmentStatus = NO;
            if (investmentStatus) {
                SipInsuranceVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SipInsuranceVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            } else {
                // Static Sip Insurance
                SIPInsuranceIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SIPInsuranceIntroVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            }
            
        } else if (indexPath.row == 3) {
            // Gold Saving
            BOOL investmentStatus = NO;
            if (investmentStatus) {
                GoldSavingVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GoldSavingVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            } else {
                // Static Gold Saving
                GoldSavingIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GoldSavingIntroVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            }
            
        } else if (indexPath.row == 4) {
            // NFO
            NFOListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NFOListVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
}

#pragma mark - CollectionView DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return aryMiddleSlider.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    MiddleSliderCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"MiddleSliderCollectionCell" forIndexPath:indexPath];
    if (aryMiddleSlider.count > indexPath.item) {
        NSDictionary *dicItem = aryMiddleSlider[indexPath.item];
        [cell.imgIcon sd_setImageWithURL:[NSURL URLWithString:dicItem[@"image"]] placeholderImage:nil];
        cell.lblTitle.text = dicItem[@"title"];
        cell.lblDesc.text = dicItem[@"discription"];
        if (dicItem[@"color1"] != nil && dicItem[@"color2"] != nil) {
            [AppHelper setBackgroundGradient:cell.gradientView Color1:dicItem[@"color1"] Color2:dicItem[@"color2"]];
        } else {
            [AppHelper setBackgroundGradient:cell.gradientView Color1:[UIColor colorWithHexString:@"FCB350"] Color2:[UIColor colorWithHexString:@"FC7E44"]];
        }
    }
    //ImageAvatar = [NSString stringWithFormat:@"%@",[DictData objectAtIndex:indexPath.row]];
    //[cell.ImageView sd_setImageWithURL:[NSURL URLWithString:ImageAvatar] placeholderImage:nil];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(269, 93);
}

#pragma mark - Button Tap Events

- (IBAction)btnNotification_Tapped:(id)sender {
    NotificationVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NotificationVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnSerach_Tapped:(id)sender {
    
}

- (IBAction)btnMoreOption_Tapped:(id)sender {
    
}


- (IBAction)btnLeftMenu_Tapped:(id)sender {
    [self openLeftMenu];
}

- (IBAction)btnTransparetnClose_Tapped:(id)sender {
    [self closeLeftMenu];
}

-(void)btnBlogs_Tapped {
    UIViewController *destVC = [AppHelper createWebViewControllerWithTitle:@"Test" URLString:@"https://blog.mysiponline.com/why-should-you-invest-in-mutual-funds-before-the-age-of-25" ShowShareButton:YES];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnVideos_Tapped {
    
}

-(void)btnAllBlogs_Tapped {
    BlogsVideoVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BlogsVideoVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnAllVideos_Tapped {
    BlogsVideoVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BlogsVideoVC"];
    destVC.showVideo = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Menu Button Tap Events

-(void)btnMyAccount_Tapped {
    MyAccountVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"MyAccountVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    [self closeLeftMenu];
}

-(void)btnAllTransactions_Tapped {
    TransactionListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionListVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    [self closeLeftMenu];
}

-(void)btnManageSips_Tapped {
    ManageSipVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ManageSipVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    [self closeLeftMenu];
}

-(void)btnSipCalculator_Tapped {
    
}

-(void)btnFundExplorer_Tapped {    
    FundExploreVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundExploreVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    [self closeLeftMenu];
}

-(void)btnNewsAndBlogs_Tapped {
    [self btnAllBlogs_Tapped];
    [self closeLeftMenu];
}

-(void)btnHelp_Tapped {
    HelpVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    [self closeLeftMenu];
}

-(void)btnAboutUs_Tapped {
    
}

-(void)btnShare_Tapped {
    
}

-(void)btnLogout_Tapped {
    LogoutPopupVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LogoutPopupVC"];
    [self presentViewController:destVC animated:YES completion:nil];
    [self closeLeftMenu];    
}


@end
