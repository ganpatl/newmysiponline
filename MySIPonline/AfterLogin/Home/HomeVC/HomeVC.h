//
//  HomeVC.h
//  MySIPonline
//
//  Created by Ganpat on 16/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HomeVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblLeftMenu;
@property (weak, nonatomic) IBOutlet UIView *leftMenuView;
@property (weak, nonatomic) IBOutlet UIButton *btnTransClose;
@property (weak, nonatomic) IBOutlet UITableView *tblHome;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *menuLeadingConstraint;

@end

NS_ASSUME_NONNULL_END
