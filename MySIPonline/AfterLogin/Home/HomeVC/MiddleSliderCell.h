//
//  MiddleSliderCell.h
//  MySIPonline
//
//  Created by Ganpat on 19/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MiddleSliderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *cvMiddleSlider;

@end

NS_ASSUME_NONNULL_END
