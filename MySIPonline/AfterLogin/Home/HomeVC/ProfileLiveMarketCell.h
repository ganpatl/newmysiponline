//
//  ProfileLiveMarketCell.h
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileLiveMarketCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblWelcomeUser;
@property (weak, nonatomic) IBOutlet UILabel *lblSensex;
@property (weak, nonatomic) IBOutlet UILabel *lblSensexUpDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgSensexArrow;
@property (weak, nonatomic) IBOutlet UILabel *lblNifty;
@property (weak, nonatomic) IBOutlet UILabel *lblNiftyUpDown;
@property (weak, nonatomic) IBOutlet UIImageView *imgNiftyArrow;

@end

NS_ASSUME_NONNULL_END
