//
//  LeftMenuItemCell.h
//  MySIPonline
//
//  Created by Ganpat on 17/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LeftMenuItemCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet TitleLabel *lblUserName;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblProfileStatus;
@property (weak, nonatomic) IBOutlet UIImageView *imgProfileStatus;

@property (weak, nonatomic) IBOutlet UIButton *btnMyAccount;
@property (weak, nonatomic) IBOutlet UIButton *btnAllTransactions;
@property (weak, nonatomic) IBOutlet UIButton *btnManageSIPs;
@property (weak, nonatomic) IBOutlet UIButton *btnSIPCalculator;
@property (weak, nonatomic) IBOutlet UIButton *btnFundExplorer;
@property (weak, nonatomic) IBOutlet UIButton *btnNewsBlogs;
@property (weak, nonatomic) IBOutlet UIButton *btnHelp;
@property (weak, nonatomic) IBOutlet UIButton *btnAboutUs;
@property (weak, nonatomic) IBOutlet UIButton *btnShare;
@property (weak, nonatomic) IBOutlet UIButton *btnLogout;

@end

NS_ASSUME_NONNULL_END
