//
//  MiddleSliderCollectionCell.m
//  MySIPonline
//
//  Created by Ganpat on 19/11/18.
//

#import "MiddleSliderCollectionCell.h"

@implementation MiddleSliderCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    //[AppHelper setBackgroundGradient:self.gradientView Color1:[UIColor colorWithHexString:@"FCB350"] Color2:[UIColor colorWithHexString:@"FC7E44"]];
    self.contentView.layer.cornerRadius = 4;
    self.contentView.layer.masksToBounds = YES;
    [AppHelper addShadowOnView:self.contentView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:3];
}

@end
