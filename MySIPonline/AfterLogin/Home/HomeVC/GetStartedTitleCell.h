//
//  GetStartedTitleCell.h
//  MySIPonline
//
//  Created by Ganpat on 19/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GetStartedTitleCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;

@end

NS_ASSUME_NONNULL_END
