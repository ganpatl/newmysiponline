//
//  NotificationVC.h
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblNotification;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *noNotificationView;
@property (weak, nonatomic) IBOutlet UIButton *btnClearAll;

@end

NS_ASSUME_NONNULL_END
