//
//  NotificationCell.h
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NotificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblDateTime;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblTitle;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblMessage;

@end

NS_ASSUME_NONNULL_END
