//
//  NotificationVC.m
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import "NotificationVC.h"
#import "NotificationCell.h"

@interface NotificationVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryNotification;
}

@end

@implementation NotificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    
    [self.tblNotification registerNib:[UINib nibWithNibName:@"NotificationCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NotificationCell"];
    self.tblNotification.dataSource = self;
    self.tblNotification.delegate = self;
    
    aryNotification = @[@{@"date":@"12 May 2018, 4 hours Ago", @"title":@"New NFO Launched standard dummy text ever since the 1500s", @"msg":@"Dear sir, your KYC update within 3-7 days in. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. View Details"},
                        @{@"date":@"12 May 2018, 4 hours Ago", @"title":@"New NFO Launched", @"msg":@"Dear sir, your KYC update within 3-7 days in. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book."},
                        @{@"date":@"12 May 2018, 4 hours Ago", @"title":@"Lorem Ipsum has been the industry's New NFO Launched", @"msg":@"Dear sir, your KYC update within 3-7 days in. Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the."}];
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryNotification.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    NotificationCell *cell = [tableView dequeueReusableCellWithIdentifier:@"NotificationCell"];
    if (aryNotification.count > indexPath.row) {
        NSDictionary *dicNotification = aryNotification[indexPath.row];
        // Date Time
        cell.lblDateTime.text = dicNotification[@"date"];
        
        // Title
        cell.lblTitle.text = dicNotification[@"title"];
        
        // Message
        cell.lblMessage.text = dicNotification[@"msg"];
    }
    return cell;
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnClearAll_Tapped:(id)sender {
    aryNotification = [NSArray new];
    [self.tblNotification reloadData];
    self.noNotificationView.hidden = NO;
    self.btnClearAll.hidden = YES;
}


@end
