//
//  BestFundListCell.h
//  MySIPonline
//
//  Created by Ganpat on 26/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BestFundListCell : UITableViewCell
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblFundName;
@property (weak, nonatomic) IBOutlet UILabel *lblReturn;
@property (weak, nonatomic) IBOutlet UILabel *lblRisk;
@property (weak, nonatomic) IBOutlet SubHeaderLabel *lblFundSize;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreOption;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

NS_ASSUME_NONNULL_END
