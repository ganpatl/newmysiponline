//
//  BestFundsListVC.h
//  MySIPonline
//
//  Created by Ganpat on 22/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BestFundsListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblMainTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn1Month;
@property (weak, nonatomic) IBOutlet UIButton *btn3Month;
@property (weak, nonatomic) IBOutlet UIButton *btn6Month;
@property (weak, nonatomic) IBOutlet UIButton *btn1Year;
@property (weak, nonatomic) IBOutlet UIButton *btn3Year;
@property (weak, nonatomic) IBOutlet UIButton *btn5Year;
@property (weak, nonatomic) IBOutlet UITableView *tblBestFundList;
@property (weak, nonatomic) IBOutlet UILabel *lblFilterCount;
@property (weak, nonatomic) IBOutlet UIButton *btnCartBadge;

@property NSString *strCategoryType;
@property NSString *strTitle;
@property NSString *strAmcCode;

@end

NS_ASSUME_NONNULL_END
