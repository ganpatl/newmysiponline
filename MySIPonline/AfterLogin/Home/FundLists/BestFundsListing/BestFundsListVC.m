//
//  BestFundsListVC.m
//  MySIPonline
//
//  Created by Ganpat on 22/11/18.
//

#import "BestFundsListVC.h"
#import "BestFundListCell.h"
#import "SortVC.h"
#import "FilterVC.h"
#import "FundDetailsVC.h"
#import "NIDropDown.h"
#import "CartVC.h"

@interface BestFundsListVC () <UITableViewDataSource, UITableViewDelegate, NIDropDownDelegate, SortingDelegate> {
    BestFundListCell *bestFundListCell;
    NSMutableDictionary *mdictParam;
    
    NSMutableArray *aryFunds;
    NSString *selectedData;
    
    NSArray *aryOptions;    
    NIDropDown *dropDownOptions;
    
    int intOffset;
    BOOL isEndOfData;
    BOOL isPageRefreshing;
}

@end

@implementation BestFundsListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    self.btnCartBadge.layer.cornerRadius = self.btnCartBadge.frame.size.height / 2;
    aryOptions = @[@"Buy Now", @"Add to Cart"];
    aryFunds = [NSMutableArray new];
    
    self.lblMainTitle.text = self.strTitle;
    
    [self.tblBestFundList registerNib:[UINib nibWithNibName:@"BestFundListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BestFundListCell"];
    self.tblBestFundList.dataSource = self;
    self.tblBestFundList.delegate = self;
    
    intOffset = 0;
    isEndOfData = NO;
    isPageRefreshing = NO;
    selectedData = @"oney";
    
    NSDictionary *dicApplied = @{@"sort":@{@"Return":@"desc"}};
    [NSUserData SaveAppliedSortFilterData:dicApplied];
    
    [self btn1Year_Tapped:nil];
    [self getFundListByKey];
}

-(void)setButtonBackgroundColor:(UIColor *)color {
    self.btn1Month.backgroundColor = color;
    self.btn3Month.backgroundColor = color;
    self.btn6Month.backgroundColor = color;
    self.btn1Year.backgroundColor = color;
    self.btn3Year.backgroundColor = color;
    self.btn5Year.backgroundColor = color;
    [self.btn1Month setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn3Month setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn6Month setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn1Year setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn3Year setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn5Year setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
}

- (void)animateCells {
    for (BestFundListCell *cell in self.tblBestFundList.visibleCells) {
//        [UIView animateWithDuration:0.25 animations:^{
//            cell.lblReturn.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1.09, 1.09);
//        } completion:^(BOOL finished) {
//            [UIView animateWithDuration:0.25 animations:^{
//                cell.lblReturn.transform = CGAffineTransformScale(CGAffineTransformIdentity, 1, 1);
//            }];
//        }];
        [AppHelper startAnimationOnView:cell.lblReturn Duration:0.5 Delay:0 AnimationType:CSAnimationTypePopAlpha];
    }
}

#pragma mark - Web Api Call

-(void)getFundListByKey {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSMutableDictionary *mdicParam = [NSMutableDictionary new];
        mdicParam[@"cat_type"] = self.strCategoryType;
        mdicParam[@"offset"] = [NSString stringWithFormat:@"%d", intOffset];
        if ([self.strCategoryType.lowercaseString isEqualToString:@"amc fund"]) {
            mdicParam[@"amc_code"] = self.strAmcCode;
        }
        NSDictionary *dicApplied = [NSUserData GetAppliedSortFilterData];
        if (dicApplied != nil) {
            NSDictionary *dicSort = dicApplied[@"sort"];
            if (dicSort != nil) {
                if ([dicSort.allKeys.firstObject isEqualToString:@"Return"]) {
                    mdicParam[@"sort"] = @{[NSString stringWithFormat:@"return_%@", selectedData]:dicSort.allValues[0]};
                } else {
                    mdicParam[@"sort"] = dicSort;
                }
            }
            
        }
        GSLog(@"%@", mdicParam);
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_getFundListByExpert:mdicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    NSArray *aryTemp = response[@"result"];
                    self->isEndOfData = aryTemp.count < 10;
                    if (self->intOffset == 0) {
                        self->aryFunds = aryTemp.mutableCopy;
                    } else {
                        [self->aryFunds addObjectsFromArray: aryTemp];
                    }
                    
                    NSMutableDictionary *mdic = [NSMutableDictionary new];
                    mdic[@"investment_plan"] = response[@"investment_plan"];
                    mdic[@"category"] = response[@"category"];
                    mdic[@"amc"] = response[@"amc"];
                    mdic[@"sort"] = response[@"sort"];
                    [NSUserData SaveSortFilterData:mdic];
                    
//                    NSMutableArray *aryTemp = [NSMutableArray new];
//                    for (NSDictionary *dic in self->aryFunds) {
//                        NSMutableDictionary *mutDic = [dic mutableCopy];
//                        NSString *strName = dic[@"fund_manager"];
//                        NSArray *aryWords = [strName componentsSeparatedByString:@" "];
//                        if (aryWords.count > 1) {
//                            NSString *strShort = [NSString stringWithFormat:@"%@%@", [aryWords[0] substringToIndex:1], [aryWords[1] substringToIndex:1]];
//                            mutDic[@"short"] = strShort;
//                        }
//                        [aryTemp addObject:mutDic];
//                    }
//                    self->aryFunds = aryTemp;
                    [self.tblBestFundList reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
            self->isPageRefreshing = NO;
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryFunds.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    BestFundListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BestFundListCell"];
    if (aryFunds.count > indexPath.row) {
        NSDictionary *dicFund = aryFunds[indexPath.row];
        // Scheme Name
        cell.lblFundName.text = dicFund[@"scheme_name"];
        // Risk
        NSString *strRisk = dicFund[@"scheme_risk"];
        cell.lblRisk.text = strRisk;
        if ([strRisk.lowercaseString isEqualToString:@"high"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskHighColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"low"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskLowColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"moderate low"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskModerateLowColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"moderate"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskModerateColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"moderate high"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskModerateHighColor];
        }
        // Fund Size
        NSString *str = [NSString stringWithFormat:@"%@", aryFunds[indexPath.row][@"asset_size_cr"]]; //is your str
        NSArray *items = [str componentsSeparatedByString:@"("];
        NSString *str1 = [items objectAtIndex:0];   //shows Description
        cell.lblFundSize.text = [NSString stringWithFormat:@"₹ %@ Cr.", str1.TrimSpace];
        // Return
        NSString *strReturn = @"";
        if ([selectedData isEqualToString:@"onem"]) {
            strReturn = dicFund[@"return_onem_abs"];
        } else if ([selectedData isEqualToString:@"threem"]) {
            strReturn = dicFund[@"return_threem_abs"];
        } else if ([selectedData isEqualToString:@"sixm"]) {
            strReturn = dicFund[@"return_sixm_abs"];
        } else if ([selectedData isEqualToString:@"oney"]) {
            strReturn = dicFund[@"return_oney_abs"];
        } else if ([selectedData isEqualToString:@"threey"]) {
            strReturn = dicFund[@"return_threey"];
        } else if ([selectedData isEqualToString:@"fivey"]) {
            strReturn = dicFund[@"return_fivey"];
        }
        float fltReturn = [strReturn floatValue];
        cell.lblReturn.text = [NSString stringWithFormat:@"%.2f", fltReturn];
        if (fltReturn > 0) {
            cell.lblReturn.textColor = [UIColor getAppAlertSuccessColor];
        } else {
            cell.lblReturn.textColor = [UIColor getAppAlertDangerColor];
        }
        
        [cell.btnMoreOption removeTarget:self action:@selector(btnMoreOption_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMoreOption addTarget:self action:@selector(btnMoreOption_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return cell;
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 108;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];    
    NSDictionary *dic = aryFunds[indexPath.row];
    FundDetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundDetailsVC"];
    destVC.strSchemeCode = dic[@"scheme_code"];
    destVC.strAMCCode = dic[@"amc_code"];
    destVC.strSchemeId = dic[@"id"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCart_Tapped:(id)sender {
    CartVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CartVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btn1Month_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn1Month.backgroundColor = [UIColor getAppColorBlue];
    [self.btn1Month setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"onem";
    [self.tblBestFundList reloadData];
    [self animateCells];
}

- (IBAction)btn3Month_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn3Month.backgroundColor = [UIColor getAppColorBlue];
    [self.btn3Month setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"threem";
    [self.tblBestFundList reloadData];
    [self animateCells];
}

- (IBAction)btn6Month_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn6Month.backgroundColor = [UIColor getAppColorBlue];
    [self.btn6Month setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"sixm";
    [self.tblBestFundList reloadData];
    [self animateCells];
}

- (IBAction)btn1Year_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn1Year.backgroundColor = [UIColor getAppColorBlue];
    [self.btn1Year setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"oney";
    [self.tblBestFundList reloadData];
    [self animateCells];
}

- (IBAction)btn3Year_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn3Year.backgroundColor = [UIColor getAppColorBlue];
    [self.btn3Year setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"threey";
    [self.tblBestFundList reloadData];
    [self animateCells];
}

- (IBAction)btn5Year_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn5Year.backgroundColor = [UIColor getAppColorBlue];
    [self.btn5Year setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"fivey";
    [self.tblBestFundList reloadData];
    [self animateCells];
}

-(void)btnMoreOption_Tapped:(UIButton*)sender {
    if (dropDownOptions == nil) {
        CGFloat height = 60;
        [self hideAllDropDowns];
        CGRect frame = [sender convertRect:sender.bounds toView:self.view];
        UIButton *btn = [[UIButton alloc] initWithFrame:frame];
        dropDownOptions = [[NIDropDown alloc] showDropDown:btn Label:[UILabel new] Height:&height Array:aryOptions ImageArray:nil Direction:@"down" InView:self.view];
        dropDownOptions.delegate = self;
        dropDownOptions.tag = sender.tag;
        self.tblBestFundList.scrollEnabled = NO;
    } else {
        [dropDownOptions hideDropDown:sender InView:self.view];
        dropDownOptions = nil;
        self.tblBestFundList.scrollEnabled = YES;
    }
}

#pragma mark - Drop Down Methods

-(void)hideAllDropDowns {
    if (dropDownOptions) {
        [dropDownOptions hideDropDown];
        dropDownOptions = nil;
    }
}

-(void)niDropDownDelegateMethod:(NIDropDown *)sender {
    if (sender == dropDownOptions) {
        NSString *strSelection = [NSString stringWithFormat:@"%@", sender.selectedDictionary];
        NSLog(@"%@", strSelection);
        if ([strSelection isEqualToString:@"Add to Cart"]) {
            [AppHelper showToast:@"Fund added to cart successfully"];
        } else if ([strSelection isEqualToString:@"Buy Now"]) {
            
        }
        dropDownOptions = nil;
    }
    self.tblBestFundList.scrollEnabled = YES;
}

#pragma mark - Sort Fileter Button

- (IBAction)btnSort_Tapped:(id)sender {
    SortVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SortVC"];
    destVC.delegate = self;
    [self presentViewController:destVC animated:YES completion:nil];
}

- (IBAction)btnFilter_Tapped:(id)sender {
    FilterVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FilterVC"];
    [self presentViewController:destVC animated:YES completion:nil];
}

#pragma mark - Sorting Delegate Method

-(void)SortSelected {
    intOffset = 0;
    [self getFundListByKey];
}

#pragma mark - ScrollView Method

-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
    if (self.tblBestFundList.contentOffset.y >= (self.tblBestFundList.contentSize.height - self.tblBestFundList.bounds.size.height)) {
        if (!isEndOfData && !isPageRefreshing) {
            //self.customView.hidden = NO;
            //[self.activityInd startAnimating];
            isPageRefreshing = YES;
            intOffset += 1;
            [self getFundListByKey];
        }
    }    
}

@end
