//
//  AllocationDetailsCell.m
//  MySIPonline
//
//  Created by Ganpat on 22/01/19.
//

#import "AllocationDetailsCell.h"

@implementation AllocationDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 0) Opacity:1.0 Radius:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
