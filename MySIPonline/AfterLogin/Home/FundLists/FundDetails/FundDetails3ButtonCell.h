//
//  FundDetails3ButtonCell.h
//  MySIPonline
//
//  Created by Ganpat on 17/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundDetails3ButtonCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnOverview;
@property (weak, nonatomic) IBOutlet UIButton *btnPerformance;
@property (weak, nonatomic) IBOutlet UIButton *btnAllocation;

@property NSString *selectedButton;

- (IBAction)btnOverview_Tapped:(UIButton*)sender;

@end

NS_ASSUME_NONNULL_END
