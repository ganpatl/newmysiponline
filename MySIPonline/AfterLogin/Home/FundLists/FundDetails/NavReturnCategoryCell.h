//
//  NavReturnCategoryCell.h
//  MySIPonline
//
//  Created by Ganpat on 14/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NavReturnCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblNavDate;
@property (weak, nonatomic) IBOutlet UILabel *lblNav;
@property (weak, nonatomic) IBOutlet UILabel *lbl1DayReturn;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory;


@end

NS_ASSUME_NONNULL_END
