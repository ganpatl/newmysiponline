//
//  FundDetails3ButtonCell.m
//  MySIPonline
//
//  Created by Ganpat on 17/12/18.
//

#import "FundDetails3ButtonCell.h"

@implementation FundDetails3ButtonCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    _selectedButton = _btnOverview.titleLabel.text;
    _btnOverview.layer.cornerRadius = _btnOverview.frame.size.height / 2;
    _btnPerformance.layer.cornerRadius = _btnPerformance.frame.size.height / 2;
    _btnAllocation.layer.cornerRadius = _btnAllocation.frame.size.height / 2;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

#pragma mark - Button Tap Events

- (IBAction)btnOverview_Tapped:(UIButton*)sender {
    _selectedButton = sender.titleLabel.text;
    _btnOverview.selected = !sender.selected;
    _btnPerformance.selected = NO;
    _btnAllocation.selected = NO;
    
    _btnOverview.backgroundColor = [UIColor getAppColorBlue];
    _btnPerformance.backgroundColor = [UIColor getAppColorBorder];
    _btnAllocation.backgroundColor = [UIColor getAppColorBorder];
    
    if ([self.superview isKindOfClass:[UITableView class]]) {
        UITableView *tbl = (UITableView*)self.superview;
        [tbl reloadData];
    }
}

- (IBAction)btnPerformance_Tapped:(UIButton*)sender {
    _selectedButton = sender.titleLabel.text;
    _btnOverview.selected = NO;
    _btnPerformance.selected = !sender.selected;
    _btnAllocation.selected = NO;
    
    _btnOverview.backgroundColor = [UIColor getAppColorBorder];
    _btnPerformance.backgroundColor = [UIColor getAppColorBlue];
    _btnAllocation.backgroundColor = [UIColor getAppColorBorder];
    
    if ([self.superview isKindOfClass:[UITableView class]]) {
        UITableView *tbl = (UITableView*)self.superview;
        [tbl reloadData];
    }
}

- (IBAction)btnAllocation_Tapped:(UIButton*)sender {
    _selectedButton = sender.titleLabel.text;
    _btnOverview.selected = NO;
    _btnPerformance.selected = NO;
    _btnAllocation.selected = !sender.selected;
    
    _btnOverview.backgroundColor = [UIColor getAppColorBorder];
    _btnPerformance.backgroundColor = [UIColor getAppColorBorder];
    _btnAllocation.backgroundColor = [UIColor getAppColorBlue];
    
    if ([self.superview isKindOfClass:[UITableView class]]) {
        UITableView *tbl = (UITableView*)self.superview;
        [tbl reloadData];
    }
}

@end
