//
//  ReturnCompareCell.h
//  MySIPonline
//
//  Created by Ganpat on 19/01/19.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface ReturnCompareCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UITextField *tfInvestmentAmount;
@property (weak, nonatomic) IBOutlet UIButton *btn1Y;
@property (weak, nonatomic) IBOutlet UIButton *btn3Y;
@property (weak, nonatomic) IBOutlet UIButton *btn5Y;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblBankAccountAmt;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblCorporateBondAmt;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblFixedDepositAmt;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblMutualFundsAmt;

@end

NS_ASSUME_NONNULL_END
