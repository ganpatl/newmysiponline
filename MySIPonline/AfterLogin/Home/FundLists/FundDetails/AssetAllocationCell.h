//
//  AssetAllocationCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/01/19.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface AssetAllocationCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIButton *btnYoutube;
@property (weak, nonatomic) IBOutlet PieChartView *chartView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

NS_ASSUME_NONNULL_END
