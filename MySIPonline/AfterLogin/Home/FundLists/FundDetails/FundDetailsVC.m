//
//  FundDetailsVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/12/18.
//

#import "FundDetailsVC.h"
#import "MySIPonline-Swift.h"
#import "NIDropDown.h"
#import "InvestmentVC.h"

// Cells
#import "NavReturnCategoryCell.h"
#import "LineChartCell.h"
#import "HowMuchEarnedCell.h"
#import "FundDetails3ButtonCell.h"
#import "BasicDetailsCell.h"
#import "RiskometerCell.h"
#import "CheckHowMuchEarnedCell.h"
#import "InvestmentReturnCell.h"
#import "ReturnCompareCell.h"
#import "AssetAllocationCell.h"
#import "AllocationDetailsCell.h"
#import "AllocationItemCell.h"

@import Charts;

@interface FundDetailsVC () <UITableViewDataSource, UITableViewDelegate, NIDropDownDelegate> {
    NavReturnCategoryCell *navReturnCategoryCell;
    LineChartCell *lineChartCell;
    HowMuchEarnedCell *howMuchEarnedCell;
    FundDetails3ButtonCell *fundDetails3ButtonCell;
    BasicDetailsCell *basicDetailsCell;
    RiskometerCell *riskometerCell;
    CheckHowMuchEarnedCell *checkHowMuchEarnedCell;
    InvestmentReturnCell *investmentReturnCell;
    ReturnCompareCell *returnCompareCell;
    AssetAllocationCell *assetAllocationCell;
    AllocationDetailsCell *allocationDetailsCell;
    
    NSMutableArray *aryAllocationDetails;
    NSMutableArray *aryAmountSlider;
    NSArray *aryYearSlider;
    NIDropDown *AllocationTypeDropDown;
    
    NSString *strSelectedSubCategory;
    
    NSDictionary *dicFundDetail;
    NSArray *aryNavGraphData;
    NSString *strSelectedNav;
}

@end

@implementation FundDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    //[AppHelper addTopShadowOnView:self.topTitleView];
    strSelectedSubCategory = @"market_cap";
    strSelectedNav = @"3m";
    
    int minAmt = 1000;
    int maxAmt = 100000;
    aryAmountSlider = [NSMutableArray new];
    for (int value = minAmt; value <= maxAmt; value+=500) {
        [aryAmountSlider addObject:[NSNumber numberWithInteger:value]];
    }
    aryYearSlider = @[@1, @2, @3, @4, @5];
    
    aryAllocationDetails = [NSMutableArray new];
    NSDictionary *dicEquity = @{@"market_cap":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                            ],
                                @"sector":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ],
                                @"company":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ]
                                };
    
    NSDictionary *dicDebt = @{@"market_cap":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ],
                                @"sector":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ],
                                @"company":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ]
                                };
    
    NSDictionary *dicHybrid = @{@"market_cap":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ],
                                @"sector":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ],
                                @"company":@[
                                        @{@"name":@"Technology", @"per":@"45.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"},
                                        @{@"name":@"Engineering", @"per":@"8.05"},
                                        @{@"name":@"Service", @"per":@"20.05"},
                                        @{@"name":@"Cash Holding", @"per":@"10.05"}
                                        ]
                                };
    [aryAllocationDetails addObjectsFromArray:@[dicEquity, dicDebt, dicHybrid]];
    
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"NavReturnCategoryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NavReturnCategoryCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"LineChartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LineChartCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"HowMuchEarnedCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"HowMuchEarnedCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"FundDetails3ButtonCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FundDetails3ButtonCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"BasicDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BasicDetailsCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"RiskometerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"RiskometerCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"CheckHowMuchEarnedCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"CheckHowMuchEarnedCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"InvestmentReturnCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentReturnCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"ReturnCompareCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ReturnCompareCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"AssetAllocationCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AssetAllocationCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"AllocationDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AllocationDetailsCell"];
    [self.tblFundDetails registerNib:[UINib nibWithNibName:@"AllocationItemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AllocationItemCell"];
    
    //self.tblFundDetails.estimatedRowHeight = 2.0; // any number but 2.0 is the smallest one that works
    self.tblFundDetails.dataSource = self;
    self.tblFundDetails.delegate = self;
    
    fundDetails3ButtonCell = [self.tblFundDetails dequeueReusableCellWithIdentifier:@"FundDetails3ButtonCell"];
    [fundDetails3ButtonCell btnOverview_Tapped:fundDetails3ButtonCell.btnOverview];
    
    [self getFundDetail];
    [self getNavDetailWithRange:strSelectedNav];
}

- (void)setDataUsingArray:(NSArray*)aryNavData ChartView:(ChartViewBase *)chartView {
    if (aryNavData == nil || aryNavData.count == 0) return;
    NSMutableArray *values = [[NSMutableArray alloc] init];
    double minValue = 9999999;
    double maxValue = 0;
    NSInteger navCount = aryNavData.count;
    for (int i = 0; i < navCount; i++) {
        double val = [aryNavData[i][@"nav"] doubleValue];
        if (val < minValue) {
            minValue = val;
        }
        if (val > maxValue) {
            maxValue = val;
        }
        [values addObject:[[ChartDataEntry alloc] initWithX:i y:val icon:[UIImage imageNamed:@"icon"] data: aryNavData[i]]];
    }

    lineChartCell.chartView.leftAxis.axisMinimum = minValue;
    lineChartCell.chartView.leftAxis.axisMaximum = maxValue;
    
    LineChartDataSet *set1 = nil;
    if (chartView.data.dataSetCount > 0) {
        set1 = (LineChartDataSet *)chartView.data.dataSets[0];
        set1.values = values;
        [chartView.data notifyDataChanged];
        [chartView notifyDataSetChanged];
    } else {
        set1 = [[LineChartDataSet alloc] initWithValues:values label:@""]; // DataSet 1
        set1.drawIconsEnabled = NO;
        [set1 setColor:[UIColor colorWithHexString:@"49D981"]];
        set1.lineWidth = 1.0;
        //[set1 setCircleColor:UIColor.blackColor];
        //set1.circleRadius = 3.0;
        set1.drawCirclesEnabled = NO;
        //set1.drawCircleHoleEnabled = YES;
        set1.valueFont = [UIFont systemFontOfSize:9.f];
        set1.formLineDashLengths = @[ @5.f, @2.5f ];
        set1.formSize = 15.0;
        
        
        //[set1 setHighlightEnabled:YES]; // handle cross hair
        // Hide Top Values
        set1.drawValuesEnabled = NO;
        
        // Set gradient fill
        NSArray *gradientColors = @[
            //(id)[ChartColorTemplates colorFromString:@"#00ff0000"].CGColor,
            //(id)[ChartColorTemplates colorFromString:@"#ffff0000"].CGColor
            (id)[UIColor whiteColor].CGColor,
            (id)[UIColor colorWithHexString:@"21CE99"].CGColor
        ];
        CGGradientRef gradient = CGGradientCreateWithColors (nil, (CFArrayRef)gradientColors, nil);
        set1.fillAlpha = 1.f;
        set1.fill = [ChartFill fillWithLinearGradient:gradient angle:90.f];
        set1.drawFilledEnabled = YES;
        CGGradientRelease (gradient);

        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];

        LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];

        chartView.data = data;
    }
}

#pragma mark - WEB API Call

-(void)getFundDetail {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"scheme_code":self.strSchemeCode};
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_getFundDetail:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->dicFundDetail = response[@"result"];
                    NSLog(@"%@", self->dicFundDetail);
                    self.lblTitle.text = [NSString stringWithFormat:@"%@", self->dicFundDetail[@"scheme_name"]];
                    [self.tblFundDetails reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(void)getNavDetailWithRange:(NSString *)strRange {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"scheme_code":@"128SCGP", @"amc_code":@"128", @"range":@"3m"};
        //NSDictionary *dicParam = @{@"scheme_code":self.strSchemeCode, @"amc_code":self.strAMCCode, @"range":strRange};
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_getNavDetail:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryNavGraphData = response[@"graph_data"];
                    NSLog(@"%@", self->aryNavGraphData);
                    [self.tblFundDetails reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 7;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if ([fundDetails3ButtonCell.selectedButton isEqualToString:@"Overview"]) {
        // OVERVIEW
        return 1;
        
    } else if ([fundDetails3ButtonCell.selectedButton isEqualToString:@"Performance"]) {
        // PERFORMANCE
        return 1;
    } else {
        // ALLOCATION
        if (section == 5) {
            return ((NSArray*)aryAllocationDetails[0][strSelectedSubCategory]).count + 1;
        } else {
            return 1;
        }
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (navReturnCategoryCell == nil) {
            navReturnCategoryCell = [tableView dequeueReusableCellWithIdentifier:@"NavReturnCategoryCell"];
        }
        NSString *strReturn = [NSString stringWithFormat:@"%@", dicFundDetail[@"navChange"]];
        navReturnCategoryCell.lblNavDate.text = [NSString stringWithFormat:@"%@", dicFundDetail[@"latest_nav_date"]];
        navReturnCategoryCell.lblNav.text = [NSString stringWithFormat:@"%@", dicFundDetail[@"latest_nav"]];
        navReturnCategoryCell.lbl1DayReturn.text = strReturn;
        navReturnCategoryCell.lblCategory.text = [NSString stringWithFormat:@"%@", dicFundDetail[@"scheme_category"]];
        
        // Change font color
        float fltReturn = [strReturn floatValue];
        navReturnCategoryCell.lbl1DayReturn.text = [NSString stringWithFormat:@"%.2f", fltReturn];
        if (fltReturn > 0) {
            navReturnCategoryCell.lbl1DayReturn.textColor = [UIColor getAppAlertSuccessColor];
        } else {
            navReturnCategoryCell.lbl1DayReturn.textColor = [UIColor getAppAlertDangerColor];
        }
        return navReturnCategoryCell;
        
    } else if (indexPath.section == 1) {
        if (lineChartCell == nil) {
            lineChartCell = [tableView dequeueReusableCellWithIdentifier:@"LineChartCell"];
            lineChartCell.chartView.dragEnabled = YES;
            [lineChartCell.chartView setScaleEnabled:YES];
            lineChartCell.chartView.pinchZoomEnabled = YES;
            
            // Hide all axis data & horizontal grid line
            lineChartCell.chartView.rightAxis.enabled = NO;
            lineChartCell.chartView.leftAxis.enabled = NO;
            lineChartCell.chartView.xAxis.enabled = NO;
            
            //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
            //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
            
            // Hide legend
            lineChartCell.chartView.legend.form = ChartLegendFormNone;
            
            lineChartCell.chartView.highlightPerTapEnabled = YES;
            lineChartCell.chartView.drawMarkers = YES;
            
            ChartYAxis *leftAxis = lineChartCell.chartView.leftAxis;
            leftAxis.axisMaximum = 1000.0;
            leftAxis.axisMinimum = 0;
        }
//        BalloonMarker *marker = [[BalloonMarker alloc]
//                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
//                                 font: [UIFont systemFontOfSize:12.0]
//                                 textColor: UIColor.whiteColor
//                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
//        marker.chartView = _chartView;
//        marker.minimumSize = CGSizeMake(80.f, 40.f);
//        lineChartCell.chartView.marker = marker;

        BalloonMarker *marker = [[BalloonMarker alloc]
                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                                 font: [UIFont systemFontOfSize:12.0]
                                 textColor: UIColor.whiteColor
                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
        marker.chartView = lineChartCell.chartView;;
        marker.minimumSize = CGSizeMake(80.f, 40.f);
        lineChartCell.chartView.marker = marker;
        
        //_sliderX.value = 45.0;
        //_sliderY.value = 100.0;
        //[self slidersValueChanged:nil];
        [self setDataUsingArray:aryNavGraphData ChartView:lineChartCell.chartView];
        
        [lineChartCell.chartView animateWithXAxisDuration:2.5];
        
        
        return lineChartCell;
        
    } else if (indexPath.section == 2) {
        if (howMuchEarnedCell == nil) {
            howMuchEarnedCell = [tableView dequeueReusableCellWithIdentifier:@"HowMuchEarnedCell"];
            [howMuchEarnedCell.btnHowMuchEarnedLink addTarget:self action:@selector(btnHowMuchYouEarned_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return howMuchEarnedCell;
    
    } else if (indexPath.section == 3) {
        if (fundDetails3ButtonCell == nil) {
            fundDetails3ButtonCell = [tableView dequeueReusableCellWithIdentifier:@"FundDetails3ButtonCell"];
            [fundDetails3ButtonCell btnOverview_Tapped:fundDetails3ButtonCell.btnOverview];
        }
        return fundDetails3ButtonCell;
        
    } else if (indexPath.section == 4) {
        if ([fundDetails3ButtonCell.selectedButton isEqualToString:@"Overview"]) {
            if (basicDetailsCell == nil) {
                basicDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"BasicDetailsCell"];
                [basicDetailsCell.btnYoutube addTarget:self action:@selector(btnYoutube_Tapped) forControlEvents:UIControlEventTouchUpInside];
            }
            NSString *strYoutube = [NSString stringWithFormat:@"%@", dicFundDetail[@"youtube_video"]];
            [basicDetailsCell.btnYoutube setHidden:[strYoutube isEqualToString:@""]];
            basicDetailsCell.lblCategory.text = [NSString stringWithFormat:@"%@", dicFundDetail[@"scheme_category"]];
            NSDictionary *dicOverview = dicFundDetail[@"overview"];
            basicDetailsCell.lblFundType.text = [NSString stringWithFormat:@"%@", dicOverview[@"fund_type"]];
            basicDetailsCell.lblLaunchDate.text = [NSString stringWithFormat:@"%@", dicOverview[@"launch_date"]];
            basicDetailsCell.lblBenchmark.text = [NSString stringWithFormat:@"%@", dicOverview[@"benchmark"]];
            basicDetailsCell.lblMinInvestment.text = [NSString stringWithFormat:@"%@", dicOverview[@"minimum_investment"]];
            basicDetailsCell.lblLockIn.text = [NSString stringWithFormat:@"%@", dicOverview[@"lockin"]];
            basicDetailsCell.lblExitLoad.text = [NSString stringWithFormat:@"%@", dicOverview[@"exit_load"]];
            basicDetailsCell.lblFundManager.text = [NSString stringWithFormat:@"%@", dicOverview[@"fund_manager"]];
            return basicDetailsCell;
            
        } else if ([fundDetails3ButtonCell.selectedButton isEqualToString:@"Performance"]) {
            if (investmentReturnCell == nil) {
                investmentReturnCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentReturnCell"];
                [investmentReturnCell.btnYoutube addTarget:self action:@selector(btnYoutube_Tapped) forControlEvents:UIControlEventTouchUpInside];
                
                investmentReturnCell.chartView.dragEnabled = NO;
                [investmentReturnCell.chartView setScaleEnabled:NO];
                investmentReturnCell.chartView.pinchZoomEnabled = NO;
                investmentReturnCell.chartView.highlightPerTapEnabled = NO;
                
                // Hide all axis data & horizontal grid line
                investmentReturnCell.chartView.rightAxis.enabled = YES; // Bottom
                investmentReturnCell.chartView.leftAxis.enabled = NO;   // Top
                investmentReturnCell.chartView.xAxis.enabled = NO;      // Left-Right
                
                //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
                //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
                
                // Hide legend
                investmentReturnCell.chartView.legend.form = ChartLegendFormNone;
                investmentReturnCell.chartView.drawMarkers = YES;
                
                ChartYAxis *leftAxis = investmentReturnCell.chartView.leftAxis;
                leftAxis.axisMaximum = 100.0;
                leftAxis.axisMinimum = 0;
                
                investmentReturnCell.chartView.rightAxis.axisMaximum = 25.0;
                investmentReturnCell.chartView.rightAxis.axisMinimum = -25.0;
                investmentReturnCell.chartView.rightAxis.drawGridLinesEnabled = NO;
                investmentReturnCell.chartView.rightAxis.drawZeroLineEnabled = YES;
                investmentReturnCell.chartView.rightAxis.zeroLineColor = [UIColor colorWithRed:204.0/255.0 green:204.0/255.0 blue:204.0/255.0 alpha:1.0];
                investmentReturnCell.chartView.rightAxis.zeroLineDashLengths = @[@2,@3];
                investmentReturnCell.chartView.rightAxis.labelCount = 0;
                
                ChartXAxis *xAxis = investmentReturnCell.chartView.xAxis;
                xAxis.labelPosition = XAxisLabelPositionBothSided;
                xAxis.drawGridLinesEnabled = NO;
                xAxis.drawAxisLineEnabled = NO;
                xAxis.axisMinimum = 0.0;
                xAxis.axisMaximum = 110.0;
                xAxis.centerAxisLabelsEnabled = YES;
                xAxis.labelCount = 12;
                xAxis.granularity = 10.0;
            }
            
            NSString *strYoutube = [NSString stringWithFormat:@"%@", dicFundDetail[@"youtube_video"]];
            [investmentReturnCell.btnYoutube setHidden:[strYoutube isEqualToString:@""]];
            
            //BalloonMarker *marker = [[BalloonMarker alloc]
            //                         initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
            //                         font: [UIFont systemFontOfSize:12.0]
            //                         textColor: UIColor.whiteColor
            //                         insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
            //marker.chartView = investmentReturnCell.chartView;;
            //marker.minimumSize = CGSizeMake(80.f, 40.f);
            //investmentReturnCell.chartView.marker = marker;
            
            //[self setChartData]; -- Fill Data
            NSDictionary *dicPerformance = dicFundDetail[@"performance"];
            
            NSMutableArray *yValues = [NSMutableArray array];
            // 3 Month
            float fltThreeMonth = [dicPerformance[@"return_threem_abs"] floatValue];
            if (fltThreeMonth > 0) {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:90 yValues:@[ @0, [NSNumber numberWithFloat:fltThreeMonth] ]]];
                investmentReturnCell.lbl3Month.text = [NSString stringWithFormat:@"+%.2f%%", fltThreeMonth];
                investmentReturnCell.lbl3Month.textColor = [UIColor getAppAlertSuccessColor];
            } else {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:90 yValues:@[ [NSNumber numberWithFloat:fltThreeMonth], @0 ]]];
                investmentReturnCell.lbl3Month.text = [NSString stringWithFormat:@"%.2f%%", fltThreeMonth];
                investmentReturnCell.lbl3Month.textColor = [UIColor getAppAlertDangerColor];
            }
            
            // 6 Month
            float fltSixMonth = [dicPerformance[@"return_sixm_abs"] floatValue];
            if (fltSixMonth > 0) {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:70 yValues:@[ @0, [NSNumber numberWithFloat:fltSixMonth] ]]];
                investmentReturnCell.lbl6Month.text = [NSString stringWithFormat:@"+%.2f%%", fltSixMonth];
                investmentReturnCell.lbl6Month.textColor = [UIColor getAppAlertSuccessColor];
            } else {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:70 yValues:@[ [NSNumber numberWithFloat:fltSixMonth], @0 ]]];
                investmentReturnCell.lbl6Month.text = [NSString stringWithFormat:@"%.2f%%", fltSixMonth];
                investmentReturnCell.lbl6Month.textColor = [UIColor getAppAlertDangerColor];
            }
            
            // 1 Year
            float fltOneYear = [dicPerformance[@"return_oney"] floatValue];
            if (fltOneYear > 0) {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:50 yValues:@[ @0, [NSNumber numberWithFloat:fltOneYear] ]]];
                investmentReturnCell.lbl1Year.text = [NSString stringWithFormat:@"+%.2f%%", fltOneYear];
                investmentReturnCell.lbl1Year.textColor = [UIColor getAppAlertSuccessColor];
            } else {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:50 yValues:@[ [NSNumber numberWithFloat:fltOneYear], @0 ]]];
                investmentReturnCell.lbl1Year.text = [NSString stringWithFormat:@"%.2f%%", fltOneYear];
                investmentReturnCell.lbl1Year.textColor = [UIColor getAppAlertDangerColor];
            }
            
            // 3 Year
            float fltThreeYear = [dicPerformance[@"return_threey"] floatValue];
            if (fltThreeYear > 0) {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:30 yValues:@[ @0, [NSNumber numberWithFloat:fltThreeYear] ]]];
                investmentReturnCell.lbl3Year.text = [NSString stringWithFormat:@"+%.2f%%", fltThreeYear];
                investmentReturnCell.lbl3Year.textColor = [UIColor getAppAlertSuccessColor];
            } else {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:30 yValues:@[ [NSNumber numberWithFloat:fltThreeYear], @0 ]]];
                investmentReturnCell.lbl3Year.text = [NSString stringWithFormat:@"%.2f%%", fltThreeYear];
                investmentReturnCell.lbl3Year.textColor = [UIColor getAppAlertDangerColor];
            }
            
            // 5 Year
            float fltFiveYear = [dicPerformance[@"return_fivey"] floatValue];
            if (fltFiveYear > 0) {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:10 yValues:@[ @0, [NSNumber numberWithFloat:fltFiveYear] ]]];
                investmentReturnCell.lbl5Year.text = [NSString stringWithFormat:@"+%.2f%%", fltFiveYear];
                investmentReturnCell.lbl5Year.textColor = [UIColor getAppAlertSuccessColor];
            } else {
                [yValues addObject:[[BarChartDataEntry alloc] initWithX:10 yValues:@[ [NSNumber numberWithFloat:fltFiveYear], @0 ]]];
                investmentReturnCell.lbl5Year.text = [NSString stringWithFormat:@"%.2f%%", fltFiveYear];
                investmentReturnCell.lbl5Year.textColor = [UIColor getAppAlertDangerColor];
            }
            
            BarChartDataSet *set = nil;
            if (investmentReturnCell.chartView.data.dataSetCount > 0) {
                set = (BarChartDataSet *)investmentReturnCell.chartView.data.dataSets[0];
                set.values = yValues;
                [investmentReturnCell.chartView.data notifyDataChanged];
                [investmentReturnCell.chartView notifyDataSetChanged];
            } else {
                NSNumberFormatter *customFormatter = [[NSNumberFormatter alloc] init];
                customFormatter.negativePrefix = @"";
                customFormatter.positiveSuffix = @"m";
                customFormatter.negativeSuffix = @"m";
                customFormatter.minimumSignificantDigits = 1;
                customFormatter.minimumFractionDigits = 1;
                
                set = [[BarChartDataSet alloc] initWithValues:yValues label:@""];
                set.drawIconsEnabled = NO;
                set.valueFormatter = [[ChartDefaultValueFormatter alloc] initWithFormatter:customFormatter];
                set.valueFont = [UIFont systemFontOfSize:7.f];
                set.axisDependency = AxisDependencyRight;
                set.colors = @[
                               [UIColor colorWithHexString:@"EB4335"],
                               [UIColor colorWithHexString:@"6FB414"]
                               ];
                //set.stackLabels = @[@"Men", @"Women"];
                
                BarChartData *data = [[BarChartData alloc] initWithDataSet:set];
                data.barWidth = 8.5;
                
                investmentReturnCell.chartView.data = data;
                [investmentReturnCell.chartView setNeedsDisplay];
            }
            
            [investmentReturnCell.chartView animateWithXAxisDuration:1.0];
            return investmentReturnCell;
        
        } else {
            if (assetAllocationCell == nil) {
                assetAllocationCell = [tableView dequeueReusableCellWithIdentifier:@"AssetAllocationCell"];
                [assetAllocationCell.btnYoutube addTarget:self action:@selector(btnYoutube_Tapped) forControlEvents:UIControlEventTouchUpInside];
                
                assetAllocationCell.chartView.usePercentValuesEnabled = YES;
                assetAllocationCell.chartView.drawSlicesUnderHoleEnabled = YES;
                assetAllocationCell.chartView.holeRadiusPercent = 0.58;
                assetAllocationCell.chartView.transparentCircleRadiusPercent = 0.61;
                assetAllocationCell.chartView.chartDescription.enabled = NO;
                //[assetAllocationCell.chartView setExtraOffsetsWithLeft:5.f top:10.f right:5.f bottom:5.f];
                
                assetAllocationCell.chartView.drawCenterTextEnabled = YES;
                NSMutableParagraphStyle *paragraphStyle = [[NSParagraphStyle defaultParagraphStyle] mutableCopy];
                paragraphStyle.lineBreakMode = NSLineBreakByTruncatingTail;
                paragraphStyle.alignment = NSTextAlignmentCenter;
                
                ///NSMutableAttributedString *centerText = [[NSMutableAttributedString alloc] initWithString:@"Asset\n Allocation"];
                //[centerText setAttributes:@{
                //                            NSFontAttributeName: [UIFont fontWithName:@"Roboto-Regular" size:12.f],
                //                            NSParagraphStyleAttributeName: paragraphStyle
                //                            } range:NSMakeRange(0, centerText.length)];
                //assetAllocationCell.chartView.centerAttributedText = centerText;
                assetAllocationCell.chartView.centerText = @"Asset\n Allocation";
                
                assetAllocationCell.chartView.drawHoleEnabled = YES;
                assetAllocationCell.chartView.rotationAngle = 0.0;
                assetAllocationCell.chartView.rotationEnabled = NO;
                assetAllocationCell.chartView.highlightPerTapEnabled = NO;
                
                ChartLegend *l = assetAllocationCell.chartView.legend;
                l.horizontalAlignment = ChartLegendHorizontalAlignmentRight;
                l.verticalAlignment = ChartLegendVerticalAlignmentCenter;
                l.orientation = ChartLegendOrientationVertical;
                l.drawInside = NO;
                l.xEntrySpace = 7.0;
                l.yEntrySpace = 0.0;
                l.yOffset = 0.0;
                
                // entry label styling
                //assetAllocationCell.chartView.entryLabelColor = UIColor.whiteColor;
                //assetAllocationCell.chartView.entryLabelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f];
                assetAllocationCell.chartView.drawEntryLabelsEnabled = NO;
            }
            NSString *strYoutube = [NSString stringWithFormat:@"%@", dicFundDetail[@"youtube_video"]];
            [assetAllocationCell.btnYoutube setHidden:[strYoutube isEqualToString:@""]];
            
            // Set Data
            NSMutableArray *values = [[NSMutableArray alloc] init];
            NSDictionary *dicAllocationPer = dicFundDetail[@"percentage"];
            if (dicAllocationPer != nil) {
                NSArray *aryKeys = dicAllocationPer.allKeys;
                if (aryKeys.count > 0) {
                    [values addObject:[[PieChartDataEntry alloc] initWithValue:[dicAllocationPer[aryKeys[0]] doubleValue] label:[NSString stringWithFormat:@"%@ : %@%%", [aryKeys[0] capitalizedString], dicAllocationPer[aryKeys[0]]]]];
                }
                if (aryKeys.count > 1) {
                    [values addObject:[[PieChartDataEntry alloc] initWithValue:[dicAllocationPer[aryKeys[1]] doubleValue] label:[NSString stringWithFormat:@"%@ : %@%%", [aryKeys[1] capitalizedString], dicAllocationPer[aryKeys[1]]]]];
                }
                if (aryKeys.count > 2) {
                    [values addObject:[[PieChartDataEntry alloc] initWithValue:[dicAllocationPer[aryKeys[2]] doubleValue] label:[NSString stringWithFormat:@"%@ : %@%%", [aryKeys[2] capitalizedString], dicAllocationPer[aryKeys[2]]]]];
                }
            }
            
            PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:values label:@""];
            dataSet.drawIconsEnabled = NO;
            //dataSet.sliceSpace = 2.0;
            
            // add colors
            NSMutableArray *colors = [[NSMutableArray alloc] init];
            [colors addObject:[UIColor colorWithHexString:@"FB8005"]];
            [colors addObject:[UIColor colorWithHexString:@"49D981"]];
            [colors addObject:[UIColor colorWithHexString:@"F8D673"]];
            dataSet.colors = colors;
            
            dataSet.drawValuesEnabled = NO;
            
            PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
            assetAllocationCell.chartView.data = data;
            //[assetAllocationCell.chartView highlightValues:nil];
            
            [assetAllocationCell.chartView animateWithXAxisDuration:1.4 easingOption:ChartEasingOptionEaseOutBack];
            
            return assetAllocationCell;
        }        
        
    } else if (indexPath.section == 5) {
        if ([fundDetails3ButtonCell.selectedButton isEqualToString:@"Overview"]) {
            // OVERVIEW
            if (riskometerCell == nil) {
                riskometerCell = [tableView dequeueReusableCellWithIdentifier:@"RiskometerCell"];
            }
            return riskometerCell;
            
        } else if ([fundDetails3ButtonCell.selectedButton isEqualToString:@"Performance"]) {
            // PERFORMANCE
            if (returnCompareCell == nil) {
                returnCompareCell = [tableView dequeueReusableCellWithIdentifier:@"ReturnCompareCell"];
                returnCompareCell.chartView.chartDescription.enabled = NO;
                
                returnCompareCell.chartView.maxVisibleCount = 40;
                returnCompareCell.chartView.pinchZoomEnabled = NO;
                returnCompareCell.chartView.dragEnabled = NO;
                [returnCompareCell.chartView setScaleEnabled:NO];
                returnCompareCell.chartView.drawGridBackgroundEnabled = NO;
                returnCompareCell.chartView.drawBarShadowEnabled = NO;
                returnCompareCell.chartView.drawValueAboveBarEnabled = NO;
                returnCompareCell.chartView.highlightFullBarEnabled = NO;
                returnCompareCell.chartView.highlightPerTapEnabled = NO;
                
                NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
                leftAxisFormatter.maximumFractionDigits = 1;
                leftAxisFormatter.negativeSuffix = @"";
                leftAxisFormatter.positiveSuffix = @"";
                
                returnCompareCell.chartView.leftAxis.enabled  = NO;
                returnCompareCell.chartView.rightAxis.enabled = NO;
                returnCompareCell.chartView.xAxis.enabled = NO;//Top
                returnCompareCell.chartView.legend.enabled = NO;
            }
            
            // Setup Date
            int count = 4;
            int range = 100;
            NSMutableArray *yVals = [[NSMutableArray alloc] init];
            for (int i = 0; i < count; i++) {
                double val1 = (double)arc4random_uniform(range);
                double val2 = 100-val1;
                [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@(val1), @(val2)]]];
            }
            
            BarChartDataSet *set1 = nil;
            if (returnCompareCell.chartView.data.dataSetCount > 0) {
                set1 = (BarChartDataSet *)returnCompareCell.chartView.data.dataSets[0];
                set1.values = yVals;
                [returnCompareCell.chartView.data notifyDataChanged];
                [returnCompareCell.chartView notifyDataSetChanged];
            } else {
                set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@""];
                set1.drawIconsEnabled = NO;
                set1.colors = @[[UIColor colorWithHexString:@"6FB414"], [UIColor colorWithHexString:@"E3E3E3"]];
                set1.drawValuesEnabled = NO;
                NSMutableArray *dataSets = [[NSMutableArray alloc] init];
                [dataSets addObject:set1];
                
                BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
                [data setValueTextColor:UIColor.whiteColor];
                data.barWidth = 0.5;
                
                //returnCompareCell.chartView.fitBars = YES;
                returnCompareCell.chartView.data = data;
            }
            [returnCompareCell.chartView animateWithYAxisDuration:1];
            
            return returnCompareCell;
        
        } else {
            // ALLOCATION
            if (indexPath.row == 0) {
                if (allocationDetailsCell == nil) {
                    allocationDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"AllocationDetailsCell"];
                    [allocationDetailsCell.btnAllocationDropDown addTarget:self action:@selector(btnAllocationDropDown_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    [allocationDetailsCell.btnMarketCap addTarget:self action:@selector(btnMarketCap_Tapped) forControlEvents:UIControlEventTouchUpInside];
                    [allocationDetailsCell.btnSector addTarget:self action:@selector(btnSector_Tapped) forControlEvents:UIControlEventTouchUpInside];
                    [allocationDetailsCell.btnCompany addTarget:self action:@selector(btnCompany_Tapped) forControlEvents:UIControlEventTouchUpInside];
                    
                }
                return allocationDetailsCell;
            } else {
                // Allocation per % cells
                AllocationItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AllocationItemCell"];
                NSArray* aryCat = aryAllocationDetails[0][strSelectedSubCategory];
                if (indexPath.row-1 < aryCat.count) {
                    // @{@"name":@"Technology", @"per":@"45.05"}
                    NSDictionary *dicItem = aryCat[indexPath.row-1];
                    cell.lblName.text = dicItem[@"name"];
                    NSString *strPer = dicItem[@"per"];
                    cell.lblPercent.text = [NSString stringWithFormat:@"%@ %%", strPer];
                    cell.progressView.progress = [strPer floatValue] / 100;
                }
                return cell;
            }
        }
        
    } else if (indexPath.section == 6) {
        if (checkHowMuchEarnedCell == nil) {
            checkHowMuchEarnedCell = [tableView dequeueReusableCellWithIdentifier:@"CheckHowMuchEarnedCell"];
            // Amount Slider
            //[checkHowMuchEarnedCell.sliderAmount setThumbImage:[UIImage imageNamed:@"fundWithRegularIncome"] forState:UIControlStateNormal];
            checkHowMuchEarnedCell.sliderAmount.thumbTintColor = UIColor.getAppColorBlue;
            checkHowMuchEarnedCell.sliderAmount.minimumValue = 0;
            checkHowMuchEarnedCell.sliderAmount.maximumValue = ((float)[aryAmountSlider count] - 1);
            checkHowMuchEarnedCell.lblAmount.text = [NSString stringWithFormat:@"₹ %@", aryAmountSlider.firstObject];
            [checkHowMuchEarnedCell.sliderAmount addTarget:self action:@selector(amountSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
            // Year Slider
            checkHowMuchEarnedCell.sliderYear.value = 0;
            checkHowMuchEarnedCell.sliderYear.minimumValue = 0;
            checkHowMuchEarnedCell.sliderYear.maximumValue = ((float)[aryYearSlider count] - 1);
            checkHowMuchEarnedCell.lblTimeYear.text = [NSString stringWithFormat:@"%@ Year", aryYearSlider.firstObject];
            [checkHowMuchEarnedCell.sliderYear addTarget:self action:@selector(yearSliderValueChanged:) forControlEvents:UIControlEventValueChanged];
        }
        return checkHowMuchEarnedCell;
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 50;
    } else if (indexPath.section == 1) {
        return 225;
    } else if (indexPath.section == 2) {
        return UITableViewAutomaticDimension;
    } else if (indexPath.section == 3) {
        return UITableViewAutomaticDimension;
    } else if (indexPath.section == 4) {
        return UITableViewAutomaticDimension;
    } else if (indexPath.section == 5) {
        return UITableViewAutomaticDimension;
    } else if (indexPath.section == 6) {
        return UITableViewAutomaticDimension;
    } else {
        return 0;
    }
}

#pragma mark - Slider Value Changed

-(void)amountSliderValueChanged:(UISlider*)slider {
    NSUInteger index = (NSUInteger)(slider.value + 0.5);
    [slider setValue:index animated:NO];
    NSNumber *number = aryAmountSlider[index];
    checkHowMuchEarnedCell.lblAmount.text = [NSString stringWithFormat:@"₹ %@", number];
}

-(void)yearSliderValueChanged:(UISlider*)slider {
    NSUInteger index = (NSUInteger)(slider.value + 0.5);
    [slider setValue:index animated:NO];
    NSNumber *number = aryYearSlider[index];
    checkHowMuchEarnedCell.lblTimeYear.text = [NSString stringWithFormat:@"%@ Year", number];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShare_Tapped:(id)sender {
    
}

- (IBAction)btnAddToCart_Tapped:(id)sender {
    // Add fund to cart
    
}

- (IBAction)btnInvestNow_Tapped:(id)sender {
    InvestmentVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InvestmentVC"];
    destVC.strSchemeName = dicFundDetail[@"scheme_name"];
    destVC.strSchemeId = self.strSchemeId;
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnHowMuchYouEarned_Tapped {
    [self.tblFundDetails scrollToRowAtIndexPath:[NSIndexPath indexPathForRow:0 inSection:6] atScrollPosition:UITableViewScrollPositionTop animated:YES];
}

-(void)btnYoutube_Tapped {
    NSString *strYoutube = [NSString stringWithFormat:@"%@", dicFundDetail[@"youtube_video"]];
    if (![strYoutube isEqualToString:@""]) {
        // Show video
    }
}

#pragma mark - Allocation Type Button Tap Events

-(void)btnAllocationDropDown_Tapped:(UIButton*)sender {
    if (AllocationTypeDropDown == nil) {
        CGFloat height = 90;
        [self hideAllDropDowns];
        NSArray *aryType = @[@"Equity", @"Debt", @"Hybrid"];
        AllocationTypeDropDown = [[NIDropDown alloc] showDropDown:sender Label:allocationDetailsCell.lblAllocationDropDown Height:&height Array:aryType ImageArray:nil Direction:@"down" InView:self.view];
        AllocationTypeDropDown.delegate = self;
    } else {
        [AllocationTypeDropDown hideDropDown:sender InView:self.view];
        AllocationTypeDropDown = nil;
    }
}

-(void)btnMarketCap_Tapped {
    allocationDetailsCell.indicatorLeadingConstraint.constant = 0;
    
}

-(void)btnSector_Tapped {
    allocationDetailsCell.indicatorLeadingConstraint.constant = allocationDetailsCell.btnMarketCap.frame.size.width + 8;
    
}

-(void)btnCompany_Tapped {
    allocationDetailsCell.indicatorLeadingConstraint.constant = (allocationDetailsCell.btnMarketCap.frame.size.width * 2) + 16;
    
}

#pragma mark - Drop Down Methods

-(void)hideAllDropDowns {
    if (AllocationTypeDropDown) {
        [AllocationTypeDropDown hideDropDown];
        AllocationTypeDropDown = nil;
    }
}

-(void)niDropDownDelegateMethod:(NIDropDown *)sender {
    if (sender == AllocationTypeDropDown) {
        //SipdateString = [NSString stringWithFormat:@"%@",sender.selectedDictionary];
        AllocationTypeDropDown = nil;
    }
}

@end
