//
//  BasicDetailsCell.h
//  MySIPonline
//
//  Created by Ganpat on 17/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BasicDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnYoutube;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblCategory;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblFundType;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblLaunchDate;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblBenchmark;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblMinInvestment;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblLockIn;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblExitLoad;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblFundManager;

@end

NS_ASSUME_NONNULL_END
