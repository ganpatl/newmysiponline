//
//  LineChartCell.h
//  MySIPonline
//
//  Created by Ganpat on 14/12/18.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface LineChartCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet LineChartView *chartView;
@property (weak, nonatomic) IBOutlet UILabel *lblShowingPerformanceSince;
@property (weak, nonatomic) IBOutlet UIView *marginView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leftMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *rightMarginConstraint;

@end

NS_ASSUME_NONNULL_END
