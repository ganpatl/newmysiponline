//
//  AllocationItemCell.m
//  MySIPonline
//
//  Created by Ganpat on 28/01/19.
//

#import "AllocationItemCell.h"

@implementation AllocationItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 0) Opacity:1.0 Radius:0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
