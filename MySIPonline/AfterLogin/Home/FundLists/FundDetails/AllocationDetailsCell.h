//
//  AllocationDetailsCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AllocationDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnAllocationDropDown;
@property (weak, nonatomic) IBOutlet UILabel *lblAllocationDropDown;
@property (weak, nonatomic) IBOutlet UIButton *btnMarketCap;
@property (weak, nonatomic) IBOutlet UIButton *btnSector;
@property (weak, nonatomic) IBOutlet UIButton *btnCompany;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *indicatorLeadingConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topHeadingConstraint;
@end

NS_ASSUME_NONNULL_END
