//
//  CheckHowMuchEarnedCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CheckHowMuchEarnedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnSip;
@property (weak, nonatomic) IBOutlet UIButton *btnLumpsum;
@property (weak, nonatomic) IBOutlet Body1Label *lblAmount;
@property (weak, nonatomic) IBOutlet Body1Label *lblTimeYear;
@property (weak, nonatomic) IBOutlet UISlider *sliderAmount;
@property (weak, nonatomic) IBOutlet UISlider *sliderYear;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestmentAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblFutureValue;

@end

NS_ASSUME_NONNULL_END
