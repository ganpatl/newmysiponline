//
//  RiskometerCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RiskometerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;

@end

NS_ASSUME_NONNULL_END
