//
//  InvestmentReturnCell.h
//  MySIPonline
//
//  Created by Ganpat on 18/01/19.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface InvestmentReturnCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet HorizontalBarChartView *chartView;
@property (weak, nonatomic) IBOutlet UIButton *btnYoutube;

@property (weak, nonatomic) IBOutlet UILabel *lbl3Month;
@property (weak, nonatomic) IBOutlet UILabel *lbl6Month;
@property (weak, nonatomic) IBOutlet UILabel *lbl1Year;
@property (weak, nonatomic) IBOutlet UILabel *lbl3Year;
@property (weak, nonatomic) IBOutlet UILabel *lbl5Year;

@end

NS_ASSUME_NONNULL_END
