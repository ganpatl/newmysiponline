//
//  HowMuchEarnedCell.h
//  MySIPonline
//
//  Created by Ganpat on 17/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HowMuchEarnedCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnHowMuchEarnedLink;


@end

NS_ASSUME_NONNULL_END
