//
//  FundDetailsVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblFundDetails;
@property (weak, nonatomic) IBOutlet TitleLabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;

@property NSString *strSchemeCode;
@property NSString *strSchemeId;
@property NSString *strAMCCode;

@end

NS_ASSUME_NONNULL_END
