//
//  ExpertPortfolioSliderCell.h
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ExpertPortfolioSliderCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *cvExpertPortfolioSlider;


@end

NS_ASSUME_NONNULL_END
