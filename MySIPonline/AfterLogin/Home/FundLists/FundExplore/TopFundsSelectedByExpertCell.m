//
//  TopFundsSelectedByExpertCell.m
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import "TopFundsSelectedByExpertCell.h"

@implementation TopFundsSelectedByExpertCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.cvFundsByExpert Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
