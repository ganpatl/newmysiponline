//
//  DisclaimerCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DisclaimerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet CaptionLabel *lblText;

@end

NS_ASSUME_NONNULL_END
