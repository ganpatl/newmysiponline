//
//  ViewFundsByAmcCell.h
//  MySIPonline
//
//  Created by Ganpat on 21/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ViewFundsByAmcCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnViewFundByAmc;
@property (weak, nonatomic) IBOutlet UICollectionView *cvFundsByAmc;

@end

NS_ASSUME_NONNULL_END
