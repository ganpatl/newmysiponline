//
//  TopFundsSelectedByExpertCell.h
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TopFundsSelectedByExpertCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *cvFundsByExpert;

@end

NS_ASSUME_NONNULL_END
