//
//  FundExploreVC.m
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import "FundExploreVC.h"
#import "BestFundsListVC.h"
#import "PartnerAMCsVC.h"
#import "FundManagerListVC.h"
#import "FundManagersFundListVC.h"
#import "CategoryVC.h"
#import "TaxSaverIntroVC.h"
#import "AggressivePortfolioIntroVC.h"
#import "BalancedPortfolioIntroVC.h"
#import "ConservativePortfolioIntroVC.h"
#import "SpeculativePortfolioIntroVC.h"

// Cells
#import "GetStartedTitleCell.h"
#import "ExpertPortfolioSliderCell.h"
#import "ExpertPortfolioSliderCollectionCell.h"
#import "TopFundsSelectedByExpertCell.h"
#import "BestFundsCategoryCell.h"
#import "ViewFundsByAmcCell.h"
#import "TopFundsByExpertCollectionCell.h"
#import "FundsByAmcCollectionCell.h"
#import "ThemeBasedFundCell.h"
#import "FundsByFundManagerCell.h"
#import "FundsByFundManagerCollectionCell.h"
#import "DisclaimerCell.h"

@interface FundExploreVC () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, RKTagsViewDelegate> {
    GetStartedTitleCell *investingWithExportPortFolio;
    ExpertPortfolioSliderCell *expertPortfolioSliderCell;
    GetStartedTitleCell *topFundsByExpert;
    TopFundsSelectedByExpertCell *topFundsSelectedByExpertCell;
    BestFundsCategoryCell *bestFundsCategoryCell;
    ViewFundsByAmcCell *viewFundsByAmcCell;
    ThemeBasedFundCell *themeBasedFundCell;
    FundsByFundManagerCell *fundsByFundManagerCell;
    DisclaimerCell *disclaimerCell;
    
    NSMutableArray *aryExpertPortfolioSlider;
    NSArray *aryFundsByExpert;
    NSArray *aryBestFundsCategory;
    NSArray *aryFundsByAmc;
    NSArray *aryThemeBasedFunds;
    NSArray *aryFundManagers;
}

@end

@implementation FundExploreVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.shadowView];
    
    aryExpertPortfolioSlider = [NSMutableArray new];
    
    self.tblFundExplore.dataSource = self;
    self.tblFundExplore.delegate = self;
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"GetStartedTitleCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GetStartedTitleCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"ExpertPortfolioSliderCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ExpertPortfolioSliderCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"TopFundsSelectedByExpertCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TopFundsSelectedByExpertCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"BestFundsCategoryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BestFundsCategoryCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"ViewFundsByAmcCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ViewFundsByAmcCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"ThemeBasedFundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ThemeBasedFundCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"FundsByFundManagerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FundsByFundManagerCell"];
    [self.tblFundExplore registerNib:[UINib nibWithNibName:@"DisclaimerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DisclaimerCell"];
    
    [self getFundExplorerScreenData];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMic_Tapped:(id)sender {
    
}

-(void)btnFundsByExpert_Tapped:(UIButton*)sender {
    NSDictionary *dic = aryFundsByExpert[sender.tag];
    // Open Fund List Screen
    BestFundsListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestFundsListVC"];
    destVC.strCategoryType = dic[@"key"];
    destVC.strTitle = dic[@"title"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnViewAllCategory_Tapped {
    CategoryVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CategoryVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnViewAllFundsByAMC_Tapped {
    PartnerAMCsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PartnerAMCsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnViewAllFundManager_Tapped {
    FundManagerListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundManagerListVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Web API Call

-(void)getFundExplorerScreenData {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [WebServices api_getFundExplorerScreenDataWithCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    
                    // expert_portfolio - Investing with Expert Portfolio
                    NSMutableArray *aryTempEP = response[@"expert_portfolio"];
                    self->aryExpertPortfolioSlider = [NSMutableArray new];
                    NSString *strEPimagePath = response[@"expert_icon_url"];
                    for (NSDictionary *dicEP in aryTempEP) {
                        NSArray *aryColors = [dicEP[@"color_code"] componentsSeparatedByString:@","];
                        [self->aryExpertPortfolioSlider addObject:@{@"image":[NSString stringWithFormat:@"%@%@", strEPimagePath, dicEP[@"image"]],
                                                                    @"title":dicEP[@"title"],
                                                                    @"discription":dicEP[@"tag"],
                                                                    @"color1":[UIColor colorWithHexString:aryColors[0]],
                                                                    @"color2":[UIColor colorWithHexString:aryColors[1]]
                                                                    }
                        ];
                    }
                    
                    // fund_list - Top Funds Handpicked by Experts
                    self->aryFundsByExpert = response[@"fund_list"];
                    
                    // Best Category
                    self->aryBestFundsCategory = response[@"category_list"];
                    
                    // AMC
                    self->aryFundsByAmc = response[@"amc_list"];
                    NSMutableArray *aryTemp = [NSMutableArray new];
                    NSString *strPath = response[@"amc_img_url"];
                    for (NSDictionary *dic in self->aryFundsByAmc) {
                        NSMutableDictionary *mutDic = [dic mutableCopy];
                        mutDic[@"logo"] = [NSString stringWithFormat:@"%@%@", strPath, dic[@"logo"]];
                        if ([AppHelper isNotNull:dic[@"logo"]]) {
                            [aryTemp addObject:mutDic];
                        }
                    }
                    self->aryFundsByAmc = aryTemp;
                    [self->viewFundsByAmcCell.cvFundsByAmc reloadData];
                    
                    // fund_manager_list
                    self->aryFundManagers = response[@"fund_manager_list"];
                    aryTemp = [NSMutableArray new];
                    for (NSDictionary *dic in self->aryFundManagers) {
                        NSMutableDictionary *mutDic = [dic mutableCopy];
                        NSString *strName = dic[@"fund_manager"];
                        NSArray *aryWords = [strName componentsSeparatedByString:@" "];
                        if (aryWords.count > 1) {
                            NSString *strShort = [NSString stringWithFormat:@"%@%@", [aryWords[0] substringToIndex:1], [aryWords[1] substringToIndex:1]];
                            mutDic[@"short"] = strShort;
                        }
                        [aryTemp addObject:mutDic];
                    }
                    self->aryFundManagers = aryTemp;
                    [self->fundsByFundManagerCell.cvFundManagers reloadData];
                    
                    [self.tblFundExplore reloadData];
                    [self->expertPortfolioSliderCell.cvExpertPortfolioSlider reloadData];
                    [self->topFundsSelectedByExpertCell.cvFundsByExpert reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView DataSource Methods

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 8;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (expertPortfolioSliderCell == nil) {
            expertPortfolioSliderCell = [tableView dequeueReusableCellWithIdentifier:@"ExpertPortfolioSliderCell"];
            [expertPortfolioSliderCell.cvExpertPortfolioSlider registerNib:[UINib nibWithNibName:@"ExpertPortfolioSliderCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"ExpertPortfolioSliderCollectionCell"];
            expertPortfolioSliderCell.cvExpertPortfolioSlider.dataSource = self;
            expertPortfolioSliderCell.cvExpertPortfolioSlider.delegate = self;
        }
        return expertPortfolioSliderCell;
        
    } else if (indexPath.section == 1) {
        if (topFundsByExpert == nil) {
            topFundsByExpert = [tableView dequeueReusableCellWithIdentifier:@"GetStartedTitleCell"];
            topFundsByExpert.lblTitle.text = @"Top Funds Selected by Experts";
            topFundsByExpert.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return topFundsByExpert;
        
    } else if (indexPath.section == 2) {
        if (topFundsSelectedByExpertCell == nil) {
            topFundsSelectedByExpertCell = [tableView dequeueReusableCellWithIdentifier:@"TopFundsSelectedByExpertCell"];
            topFundsSelectedByExpertCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [topFundsSelectedByExpertCell.cvFundsByExpert registerNib:[UINib nibWithNibName:@"TopFundsByExpertCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"TopFundsByExpertCollectionCell"];
            topFundsSelectedByExpertCell.cvFundsByExpert.dataSource = self;
            topFundsSelectedByExpertCell.cvFundsByExpert.delegate = self;
        }
        return topFundsSelectedByExpertCell;
        
    } else if (indexPath.section == 3) {
        if (bestFundsCategoryCell == nil) {
            bestFundsCategoryCell = [tableView dequeueReusableCellWithIdentifier:@"BestFundsCategoryCell"];
            [bestFundsCategoryCell.tagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            bestFundsCategoryCell.tagsView.tagButtonHeight = 28;
            bestFundsCategoryCell.tagsView.tagBackgroundColor = [UIColor getAppColorBlue];
            bestFundsCategoryCell.tagsView.tintColor = [UIColor whiteColor];
            bestFundsCategoryCell.tagsView.editable = NO;
            bestFundsCategoryCell.tagsView.lineSpacing = 12;
            bestFundsCategoryCell.tagsView.interitemSpacing = 8;
            bestFundsCategoryCell.tagsView.buttonCornerRadius = 14;
            bestFundsCategoryCell.tagsView.delegate = self;
            bestFundsCategoryCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [bestFundsCategoryCell.btnViewAllFundByCategory addTarget:self action:@selector(btnViewAllCategory_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        if (aryBestFundsCategory.count > indexPath.section) {
            if (bestFundsCategoryCell.tagsView.tags.count != aryBestFundsCategory.count) {
                [bestFundsCategoryCell.tagsView removeAllTags];
                for (NSString *strCategory in self->aryBestFundsCategory) {
                    //if ([AppHelper isNotNull:dic[@"category"]]) {
                        [bestFundsCategoryCell.tagsView addTag:strCategory];
                    //}
                }
            }
        }
        return bestFundsCategoryCell;
    
    } else if (indexPath.section == 4) {
        if (viewFundsByAmcCell == nil) {
            viewFundsByAmcCell = [tableView dequeueReusableCellWithIdentifier:@"ViewFundsByAmcCell"];
            [viewFundsByAmcCell.cvFundsByAmc registerNib:[UINib nibWithNibName:@"FundsByAmcCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"FundsByAmcCollectionCell"];
            viewFundsByAmcCell.cvFundsByAmc.dataSource = self;
            viewFundsByAmcCell.cvFundsByAmc.delegate = self;
            viewFundsByAmcCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [viewFundsByAmcCell.btnViewFundByAmc addTarget:self action:@selector(btnViewAllFundsByAMC_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return viewFundsByAmcCell;
    
    } else if (indexPath.section == 5) {
        if (themeBasedFundCell == nil) {
            themeBasedFundCell = [tableView dequeueReusableCellWithIdentifier:@"ThemeBasedFundCell"];
            themeBasedFundCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return themeBasedFundCell;
    
    } else if (indexPath.section == 6) {
        if (fundsByFundManagerCell == nil) {
            fundsByFundManagerCell = [tableView dequeueReusableCellWithIdentifier:@"FundsByFundManagerCell"];
            [fundsByFundManagerCell.cvFundManagers registerNib:[UINib nibWithNibName:@"FundsByFundManagerCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"FundsByFundManagerCollectionCell"];
            fundsByFundManagerCell.cvFundManagers.dataSource = self;
            fundsByFundManagerCell.cvFundManagers.delegate = self;
            fundsByFundManagerCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [fundsByFundManagerCell.btnViewAll addTarget:self action:@selector(btnViewAllFundManager_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return fundsByFundManagerCell;
    
    } else {
        if (disclaimerCell == nil) {
            disclaimerCell = [tableView dequeueReusableCellWithIdentifier:@"DisclaimerCell"];
            disclaimerCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return disclaimerCell;
    }
}

#pragma mark - TableView Delegate Method

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 180;
    } else if (indexPath.section == 1) {
        return 36;
    } else if (indexPath.section == 2) {
        return 340;
    } else if (indexPath.section == 3) {
        return 170;
    } else if (indexPath.section == 4) {
        return 187;
    } else if (indexPath.section == 5) {
        return 140;
    } else if (indexPath.section == 6) {
        return  155;
    } else {
        return 82;
    }
}

#pragma mark - TagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    NSString *strCategory = aryBestFundsCategory[index];
    BestFundsListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestFundsListVC"];
    destVC.strCategoryType = strCategory;
    destVC.strTitle = strCategory;
    [self.navigationController pushViewController:destVC animated:YES];
    return NO;
}

#pragma mark - CollectionView DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    if (collectionView == expertPortfolioSliderCell.cvExpertPortfolioSlider) {
        return aryExpertPortfolioSlider.count;
    } else if (collectionView == topFundsSelectedByExpertCell.cvFundsByExpert) {
        return aryFundsByExpert.count;
    } else if (collectionView == viewFundsByAmcCell.cvFundsByAmc) {
        return aryFundsByAmc.count;
    } else {
        return aryFundManagers.count;
    }
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == expertPortfolioSliderCell.cvExpertPortfolioSlider) {
        ExpertPortfolioSliderCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"ExpertPortfolioSliderCollectionCell" forIndexPath:indexPath];
        if (aryExpertPortfolioSlider.count > indexPath.item) {
            NSDictionary *dicItem = aryExpertPortfolioSlider[indexPath.item];
            cell.imgIcon.image = [UIImage imageNamed:dicItem[@"image"]];
            cell.lblTitle.text = dicItem[@"title"];
            cell.lblDesc.text = dicItem[@"discription"];
            if (dicItem[@"color1"] != nil && dicItem[@"color2"] != nil) {
                [AppHelper setBackgroundGradient:cell.gradientView Color1:dicItem[@"color1"] Color2:dicItem[@"color2"]];
            } else {
                [AppHelper setBackgroundGradient:cell.gradientView Color1:[UIColor colorWithHexString:@"FCB350"] Color2:[UIColor colorWithHexString:@"FC7E44"]];
            }
            [AppHelper addShadowOnView:cell.gradientView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:3];
        }
        //ImageAvatar = [NSString stringWithFormat:@"%@",[DictData objectAtIndex:indexPath.row]];
        //[cell.ImageView sd_setImageWithURL:[NSURL URLWithString:ImageAvatar] placeholderImage:nil];
        
        return cell;
    
    } else if (collectionView == topFundsSelectedByExpertCell.cvFundsByExpert) {
        // Funds By Expert
        TopFundsByExpertCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"TopFundsByExpertCollectionCell" forIndexPath:indexPath];
        if (aryFundsByExpert.count > indexPath.item) {
            NSDictionary *dic= aryFundsByExpert[indexPath.item];
            NSURL *urlImage = [NSURL URLWithString:dic[@"img"]];
            if (urlImage != nil) {
                [cell.imgTop sd_setImageWithURL:urlImage placeholderImage:nil];
            }
            [cell.btnFund addTarget:self action:@selector(btnFundsByExpert_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            cell.btnFund.tag = indexPath.item;
            cell.lblTitle.text = dic[@"title"];
        }
        return cell;
        
    } else if (collectionView == viewFundsByAmcCell.cvFundsByAmc) {
        // Funds By AMC
        FundsByAmcCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FundsByAmcCollectionCell" forIndexPath:indexPath];
        if (aryFundsByAmc.count > indexPath.item) {
            NSURL *urlImage = [NSURL URLWithString:aryFundsByAmc[indexPath.item][@"logo"]];
            if (urlImage != nil) {
                [cell.imgAMC sd_setImageWithURL:urlImage placeholderImage:nil];
            }
        }
        return cell;
    
    } else {
        // Funds By Fund Managers
        FundsByFundManagerCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"FundsByFundManagerCollectionCell" forIndexPath:indexPath];
        if (aryFundManagers.count > indexPath.item) {
            NSDictionary *dicManager = aryFundManagers[indexPath.item];
            cell.lblName.text = dicManager[@"fund_manager"];
            cell.lblShortName.text = dicManager[@"short"];
        }
        return cell;
    }
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == expertPortfolioSliderCell.cvExpertPortfolioSlider) {
        return CGSizeMake(147, 120);
    } else if (collectionView == topFundsSelectedByExpertCell.cvFundsByExpert) {
        int width = (self.view.frame.size.width - 24 - 20) / 3;
        return CGSizeMake(width, 130);
    } else if (collectionView == viewFundsByAmcCell.cvFundsByAmc) {
        int width = (self.view.frame.size.width - 24 - 4) / 5;
        return CGSizeMake(width, 32);
    } else {
        int width = (self.view.frame.size.width - 24 - 4) / 4;
        return CGSizeMake(width, 80);
    }
}

-(void)collectionView:(UICollectionView *)collectionView didSelectItemAtIndexPath:(NSIndexPath *)indexPath {
    if (collectionView == expertPortfolioSliderCell.cvExpertPortfolioSlider) {
        NSDictionary *dict = aryExpertPortfolioSlider[indexPath.item];
        if ([[dict[@"title"] lowercaseString] isEqualToString:@"tax saver"]) {
            TaxSaverIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TaxSaverIntroVC"];
            [self.navigationController pushViewController:destVC animated:YES];
            
        } else if ([[dict[@"title"] lowercaseString] isEqualToString:@"aggressive"]) {
            AggressivePortfolioIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AggressivePortfolioIntroVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        
        } else if ([[dict[@"title"] lowercaseString] isEqualToString:@"balanced"]) {
            BalancedPortfolioIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BalancedPortfolioIntroVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        
        } else if ([[dict[@"title"] lowercaseString] isEqualToString:@"conservative"]) {
            ConservativePortfolioIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ConservativePortfolioIntroVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        
        } else if ([[dict[@"title"] lowercaseString] isEqualToString:@"speculative"]) {
            SpeculativePortfolioIntroVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SpeculativePortfolioIntroVC"];
            [self.navigationController pushViewController:destVC animated:YES];
            
        }
        
        
    } else if (collectionView == viewFundsByAmcCell.cvFundsByAmc) {
        // Funds By AMC
        NSDictionary *dic = aryFundsByAmc[indexPath.row];
        BestFundsListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestFundsListVC"];
        destVC.strCategoryType = @"Amc fund";
        destVC.strTitle = dic[@"amc"];
        destVC.strAmcCode = dic[@"amc_code"];
        [self.navigationController pushViewController:destVC animated:YES];
        
    } else {
        // Funds By Fund Managers
        if (aryFundManagers.count > indexPath.item) {
            NSDictionary *dicManager = aryFundManagers[indexPath.item];
            FundManagersFundListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundManagersFundListVC"];
            destVC.strTitle = dicManager[@"fund_manager"];
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
}

@end
