//
//  ExpertPortfolioSliderCollectionCell.m
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import "ExpertPortfolioSliderCollectionCell.h"

@implementation ExpertPortfolioSliderCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.contentView.layer.cornerRadius = 4;
    self.contentView.layer.masksToBounds = YES;
    [AppHelper addShadowOnView:self.contentView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:3];
    
}

@end
