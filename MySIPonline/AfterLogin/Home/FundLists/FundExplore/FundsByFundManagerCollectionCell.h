//
//  FundsByFundManagerCollectionCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundsByFundManagerCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgFundManager;
@property (weak, nonatomic) IBOutlet UILabel *lblShortName;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblName;

@end

NS_ASSUME_NONNULL_END
