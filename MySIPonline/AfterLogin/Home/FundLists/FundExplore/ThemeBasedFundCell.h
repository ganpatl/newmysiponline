//
//  ThemeBasedFundCell.h
//  MySIPonline
//
//  Created by Ganpat on 21/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ThemeBasedFundCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *cvThemeBasedFunds;

@end

NS_ASSUME_NONNULL_END
