//
//  FundExploreVC.h
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundExploreVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblFundExplore;
@property (weak, nonatomic) IBOutlet UIView *shadowView;


@end

NS_ASSUME_NONNULL_END
