//
//  BestFundsCategoryCell.h
//  MySIPonline
//
//  Created by Ganpat on 20/11/18.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface BestFundsCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnViewAllFundByCategory;
@property (weak, nonatomic) IBOutlet RKTagsView *tagsView;

@end

NS_ASSUME_NONNULL_END
