//
//  FundsByFundManagerCollectionCell.m
//  MySIPonline
//
//  Created by Ganpat on 22/11/18.
//

#import "FundsByFundManagerCollectionCell.h"

@implementation FundsByFundManagerCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    
    self.lblShortName.layer.cornerRadius = self.lblShortName.frame.size.height / 2;
    self.lblShortName.layer.masksToBounds = YES;
    self.imgFundManager.layer.cornerRadius = self.imgFundManager.frame.size.height / 2;
    self.imgFundManager.layer.masksToBounds = YES;
    
}

@end
