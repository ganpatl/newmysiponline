//
//  FundsByFundManagerCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundsByFundManagerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnViewAll;
@property (weak, nonatomic) IBOutlet UICollectionView *cvFundManagers;

@end

NS_ASSUME_NONNULL_END
