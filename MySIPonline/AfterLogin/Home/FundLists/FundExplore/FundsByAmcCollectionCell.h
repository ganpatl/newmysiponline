//
//  FundsByAmcCollectionCell.h
//  MySIPonline
//
//  Created by Ganpat on 21/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundsByAmcCollectionCell : UICollectionViewCell
@property (weak, nonatomic) IBOutlet UIImageView *imgAMC;

@end

NS_ASSUME_NONNULL_END
