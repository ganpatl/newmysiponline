//
//  CategoryVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CategoryVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UITableView *tblCategory;
@property (weak, nonatomic) IBOutlet RKTagsView *categoryTag;

@end

NS_ASSUME_NONNULL_END
