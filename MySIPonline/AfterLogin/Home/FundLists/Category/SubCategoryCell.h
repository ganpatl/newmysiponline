//
//  SubCategoryCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SubCategoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet SubHeaderLabel *lblTitle;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblDesc;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *topMarginConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bottomMarginConstraint;

@end

NS_ASSUME_NONNULL_END
