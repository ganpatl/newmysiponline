//
//  CategoryVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import "CategoryVC.h"
#import "SubCategoryCell.h"
#import "BestFundsListVC.h"

@interface CategoryVC () <UITableViewDataSource, UITableViewDelegate, RKTagsViewDelegate> {
    NSDictionary *dicCategories;
    NSArray *arySubCategory;
}

@end

@implementation CategoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    
    [self.tblCategory registerNib:[UINib nibWithNibName:@"SubCategoryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SubCategoryCell"];
    
    self.tblCategory.dataSource = self;
    self.tblCategory.delegate = self;
    self.tblCategory.tableFooterView = [UIView new];
    
    [self getAllCategory];
    
    arySubCategory = [NSArray new];
}


#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
//    if (aryCategory != nil && aryCategory.count > 0 && [aryCategory.firstObject isKindOfClass:[NSDictionary class]]) {
//        ((NSArray*)((NSDictionary*)aryCategory.firstObject).allValues.firstObject).count;
//    }
    return arySubCategory.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (arySubCategory.count > indexPath.row) {
        SubCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubCategoryCell"];
        cell.lblTitle.text = arySubCategory[indexPath.row][@"title"];
        cell.lblDesc.text = arySubCategory[indexPath.row][@"desc"];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    NSDictionary *dicCategory = arySubCategory[indexPath.row];
    NSString *strCategory = dicCategory[@"title"];
    BestFundsListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestFundsListVC"];
    destVC.strCategoryType = strCategory;
    destVC.strTitle = strCategory;
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Web API Call

-(void)getAllCategory {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [WebServices api_getCategoryList:nil AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    
                    self->dicCategories = response[@"result"];
                    self->arySubCategory = (NSArray*)((NSDictionary*)self->dicCategories.allValues.firstObject);
                    
                    // Setup RKTagsView
                    [self.categoryTag setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
                    self.categoryTag.tagButtonHeight = 30;
                    self.categoryTag.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
                    self.categoryTag.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
                    self.categoryTag.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
                    self.categoryTag.selectedTagTintColor = [UIColor whiteColor];
                    self.categoryTag.editable = NO;
                    self.categoryTag.allowsMultipleSelection = NO;
                    self.categoryTag.lineSpacing = 8;
                    self.categoryTag.interitemSpacing = 8;
                    self.categoryTag.buttonCornerRadius = 14;
                    self.categoryTag.delegate = self;
                    self.categoryTag.scrollView.showsHorizontalScrollIndicator = NO;
                    
                    if (self.categoryTag.tags.count != self->dicCategories.allKeys.count) {
                        [self.categoryTag removeAllTags];
                        for (NSString *strKey in self->dicCategories) {
                            [self.categoryTag addTag:strKey];
                        }
                        // Set First item selected
                        [self.categoryTag selectTagAtIndex:0];
                    }
                    
                    [self.tblCategory reloadData];
                }
            } else {
                //NSError *err = (NSError*)response;
                //[AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - RKTags View select method

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    self->arySubCategory = (NSArray*)((NSDictionary*)dicCategories.allValues[index]);
    [self.tblCategory reloadData];
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCart_Tapped:(id)sender {
    
}

@end
