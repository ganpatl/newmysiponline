//
//  FundManagersFundListVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundManagersFundListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblMainTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn1Month;
@property (weak, nonatomic) IBOutlet UIButton *btn3Month;
@property (weak, nonatomic) IBOutlet UIButton *btn6Month;
@property (weak, nonatomic) IBOutlet UIButton *btn1Year;
@property (weak, nonatomic) IBOutlet UIButton *btn3Year;
@property (weak, nonatomic) IBOutlet UIButton *btn5Year;
@property (weak, nonatomic) IBOutlet UITableView *tblFundList;
@property (weak, nonatomic) IBOutlet UIView *sortFilterView;

@property NSString *strTitle;

@end

NS_ASSUME_NONNULL_END
