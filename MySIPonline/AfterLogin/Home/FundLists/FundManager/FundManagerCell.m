//
//  FundManagerCell.m
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import "FundManagerCell.h"

@implementation FundManagerCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
