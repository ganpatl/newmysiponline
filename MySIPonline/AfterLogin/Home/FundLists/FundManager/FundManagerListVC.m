//
//  FundManagerListVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import "FundManagerListVC.h"
#import "FundManagersFundListVC.h"
#import "FundManagerCell.h"

@interface FundManagerListVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryFundManager;
}

@end

@implementation FundManagerListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    [AppHelper addShadowOnView:self.sortFilterView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, -3) Opacity:0.4 Radius:3];
    
    [self.tblFundManager registerNib:[UINib nibWithNibName:@"FundManagerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FundManagerCell"];
    
    self.tblFundManager.dataSource = self;
    self.tblFundManager.delegate = self;
    self.tblFundManager.tableFooterView = [UIView new];
    
    [self getAllFundManager];
}

#pragma mark - Web API Call

-(void)getAllFundManager {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"offset":@"0", @"division_type":@"fund_manager"};
        [WebServices api_getAMCList:dicParam AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryFundManager = response[@"result"];
                    //self->strImagePath = response[@"amc_img_url"];
                    [self.tblFundManager reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryFundManager.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryFundManager.count > indexPath.row) {
        FundManagerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FundManagerCell"];
        cell.lblManagerName.text = aryFundManager[indexPath.row][@"name"];
        cell.lblAMCname.text = aryFundManager[indexPath.row][@"amc"];
        cell.lblHighestReturn.text = aryFundManager[indexPath.row][@"return"];
        cell.lblTotalFunds.text = aryFundManager[indexPath.row][@"funds"];
        cell.lblFundSize.text = aryFundManager[indexPath.row][@"fund_size"];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 111;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    FundManagersFundListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundManagersFundListVC"];
    destVC.strTitle = aryFundManager[indexPath.row][@"name"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCart_Tapped:(id)sender {
    
}

- (IBAction)btnSort_Tapped:(id)sender {
    
}

- (IBAction)btnFilter_Tapped:(id)sender {
    
}

@end
