//
//  FundManagerCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundManagerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgFundManager;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblManagerName;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblAMCname;
@property (weak, nonatomic) IBOutlet UILabel *lblHighestReturn;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalFunds;
@property (weak, nonatomic) IBOutlet SubHeaderLabel *lblFundSize;

@end

NS_ASSUME_NONNULL_END
