//
//  FundManagerListVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundManagerListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UITableView *tblFundManager;
@property (weak, nonatomic) IBOutlet UIView *sortFilterView;
@property (weak, nonatomic) IBOutlet UILabel *lblFilterCount;

@end

NS_ASSUME_NONNULL_END
