//
//  FundManagersFundListVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/12/18.
//

#import "FundManagersFundListVC.h"
#import "BestFundListCell.h"

@interface FundManagersFundListVC () <UITableViewDataSource, UITableViewDelegate> {
    BestFundListCell *fundManagerCell;
    
    NSArray *aryFunds;
    NSString *selectedData;
}

@end

@implementation FundManagersFundListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [AppHelper addShadowOnView:self.sortFilterView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, -3) Opacity:0.4 Radius:3];
    
    self.lblMainTitle.text = self.strTitle;
    
    [self.tblFundList registerNib:[UINib nibWithNibName:@"BestFundListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BestFundListCell"];
    self.tblFundList.dataSource = self;
    self.tblFundList.delegate = self;
    
    selectedData = @"1Y";
    [self btn1Year_Tapped:nil];
    
    [self getFundListByKey:@"top performing"];
    
}

-(void)setButtonBackgroundColor:(UIColor *)color {
    self.btn1Month.backgroundColor = color;
    self.btn3Month.backgroundColor = color;
    self.btn6Month.backgroundColor = color;
    self.btn1Year.backgroundColor = color;
    self.btn3Year.backgroundColor = color;
    self.btn5Year.backgroundColor = color;
    [self.btn1Month setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn3Month setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn6Month setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn1Year  setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn3Year  setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.btn5Year  setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
}

- (void)animateCells {
    for (BestFundListCell *cell in self.tblFundList.visibleCells) {
        [AppHelper startAnimationOnView:cell.lblReturn Duration:0.5 Delay:0 AnimationType:CSAnimationTypePopAlpha];
    }
}

#pragma mark - Web Api Call

-(void)getFundListByKey:(NSString *)strKey {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"cat_type":strKey, @"offset":@"0"};
        [WebServices api_getFundListByExpert:dicParam AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryFunds = response[@"result"];
                    //                    NSMutableArray *aryTemp = [NSMutableArray new];
                    //                    for (NSDictionary *dic in self->aryFunds) {
                    //                        NSMutableDictionary *mutDic = [dic mutableCopy];
                    //                        NSString *strName = dic[@"fund_manager"];
                    //                        NSArray *aryWords = [strName componentsSeparatedByString:@" "];
                    //                        if (aryWords.count > 1) {
                    //                            NSString *strShort = [NSString stringWithFormat:@"%@%@", [aryWords[0] substringToIndex:1], [aryWords[1] substringToIndex:1]];
                    //                            mutDic[@"short"] = strShort;
                    //                        }
                    //                        [aryTemp addObject:mutDic];
                    //                    }
                    //                    self->aryFunds = aryTemp;
                    [self.tblFundList reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryFunds.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    BestFundListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BestFundListCell"];
    if (aryFunds.count > indexPath.row) {
        NSDictionary *dicFund = aryFunds[indexPath.row];
        // Scheme Name
        cell.lblFundName.text = dicFund[@"scheme_name"];
        // Risk
        NSString *strRisk = dicFund[@"scheme_risk"];
        cell.lblRisk.text = strRisk;
        if ([strRisk.lowercaseString isEqualToString:@"high"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskHighColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"low"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskLowColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"moderate low"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskModerateLowColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"moderate"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskModerateColor];
        } else if ([strRisk.lowercaseString isEqualToString:@"moderate high"]) {
            cell.lblRisk.textColor = [UIColor getAppRiskModerateHighColor];
        }
        // Fund Size
        NSString *str = [NSString stringWithFormat:@"%@", aryFunds[indexPath.row][@"asset_size_cr"]]; //is your str
        NSArray *items = [str componentsSeparatedByString:@"("];
        NSString *str1 = [items objectAtIndex:0];   //shows Description
        cell.lblFundSize.text = [NSString stringWithFormat:@"₹ %@ Cr.", str1.TrimSpace];
        // Return
        NSString *strReturn = @"";
        if ([selectedData isEqualToString:@"1M"]) {
            strReturn = dicFund[@"return_onem_abs"];
        } else if ([selectedData isEqualToString:@"3M"]) {
            strReturn = dicFund[@"return_threem_abs"];
        } else if ([selectedData isEqualToString:@"6M"]) {
            strReturn = dicFund[@"return_sixm_abs"];
        } else if ([selectedData isEqualToString:@"1Y"]) {
            strReturn = dicFund[@"return_oney_abs"];
        } else if ([selectedData isEqualToString:@"3Y"]) {
            strReturn = dicFund[@"return_threey"];
        } else if ([selectedData isEqualToString:@"5Y"]) {
            strReturn = dicFund[@"return_fivey"];
        }
        float fltReturn = [strReturn floatValue];
        cell.lblReturn.text = [NSString stringWithFormat:@"%.2f", fltReturn];
        if (fltReturn > 0) {
            cell.lblReturn.textColor = [UIColor getAppAlertSuccessColor];
        } else {
            cell.lblReturn.textColor = [UIColor getAppAlertDangerColor];
        }
    }
    return cell;
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 108;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCart_Tapped:(id)sender {
    
}

- (IBAction)btn1Month_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn1Month.backgroundColor = [UIColor getAppColorBlue];
    [self.btn1Month setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"1M";
    [self.tblFundList reloadData];
    [self animateCells];
}

- (IBAction)btn3Month_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn3Month.backgroundColor = [UIColor getAppColorBlue];
    [self.btn3Month setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"3M";
    [self.tblFundList reloadData];
    [self animateCells];
}

- (IBAction)btn6Month_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn6Month.backgroundColor = [UIColor getAppColorBlue];
    [self.btn6Month setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"6M";
    [self.tblFundList reloadData];
    [self animateCells];
}

- (IBAction)btn1Year_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn1Year.backgroundColor = [UIColor getAppColorBlue];
    [self.btn1Year setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"1Y";
    [self.tblFundList reloadData];
    [self animateCells];
}

- (IBAction)btn3Year_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn3Year.backgroundColor = [UIColor getAppColorBlue];
    [self.btn3Year setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"3Y";
    [self.tblFundList reloadData];
    [self animateCells];
}

- (IBAction)btn5Year_Tapped:(id)sender {
    [self setButtonBackgroundColor:[UIColor whiteColor]];
    self.btn5Year.backgroundColor = [UIColor getAppColorBlue];
    [self.btn5Year setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    selectedData = @"5Y";
    [self.tblFundList reloadData];
    [self animateCells];
}

@end
