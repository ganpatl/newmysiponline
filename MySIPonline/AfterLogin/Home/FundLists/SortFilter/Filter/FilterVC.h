//
//  FilterVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol FilterDelegate<NSObject>
//@optional
- (void)FilterApplied;
@end

@interface FilterVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topShadowView;
@property (weak, nonatomic) IBOutlet UIView *bottomShadowView;

@property (weak, nonatomic) IBOutlet UIView *viewFundOption;
@property (weak, nonatomic) IBOutlet UIView *viewCategory;
@property (weak, nonatomic) IBOutlet UIView *viewAMC;

@property (weak, nonatomic) IBOutlet UIView *containerFundOption;
@property (weak, nonatomic) IBOutlet UIView *containerCategory;
@property (weak, nonatomic) IBOutlet UIView *containerAMC;


@property (weak, nonatomic) IBOutlet UIImageView *imgFundOptionCheck;
@property (weak, nonatomic) IBOutlet UILabel *lblCategoryCount;
@property (weak, nonatomic) IBOutlet UILabel *lblAMCcount;

@property (weak, nonatomic) IBOutlet UIScrollView *menuScrollView;
@property (weak, nonatomic) IBOutlet UITableView *tblCategoryOption;
@property (weak, nonatomic) IBOutlet UITableView *tblAmcOption;

@property (assign, nonatomic) id <FilterDelegate>delegate;

@end

NS_ASSUME_NONNULL_END
