//
//  FilterTickCell.h
//  MySIPonline
//
//  Created by Ganpat on 14/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FilterTickCell : UITableViewCell

@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblOption;
@property (weak, nonatomic) IBOutlet UIImageView *imgTick;

@end

NS_ASSUME_NONNULL_END
