//
//  FilterVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/12/18.
//

#import "FilterVC.h"
#import "FilterTickCell.h"

#define SCREEN_WIDTH [[UIScreen mainScreen] bounds].size.width
#define kDefaultEdgeInsets UIEdgeInsetsMake(5, 10, 5, 10)

#define tickImage [UIImage imageNamed:@"check_tick"]
#define unTickImage [UIImage imageNamed:@"check_untick"]


@interface FilterVC () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *aryMainCategory;
    //NSMutableArray *aryCategoryOption;
    NSMutableArray *aryAmcOption;
    
    NSInteger selectedCategoryIndex;
}

@end

@implementation FilterVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topShadowView];
    [AppHelper addShadowOnView:self.bottomShadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, -3) Opacity:0.4 Radius:3];

    selectedCategoryIndex = 0;
    NSDictionary *dicSortFilterData = [NSUserData GetSortFilterData];
    // setup Category
    aryMainCategory = [dicSortFilterData[@"category"] mutableCopy];
    [self addButtonsInScrollMenu];
    
    // setup AMC
    aryAmcOption = [NSMutableArray new];
    NSArray *aryAmc = dicSortFilterData[@"amc"];
    for (NSDictionary *dic in aryAmc) {
        NSMutableDictionary *mdic = [dic mutableCopy];
        mdic[@"checked"] = @"0";
        [aryAmcOption addObject:mdic];        
    }
    
    [self.tblCategoryOption registerNib:[UINib nibWithNibName:@"FilterTickCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FilterTickCell"];
    [self.tblAmcOption registerNib:[UINib nibWithNibName:@"FilterTickCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"FilterTickCell"];
    
    self.tblCategoryOption.dataSource = self;
    self.tblCategoryOption.delegate = self;
    
    self.tblAmcOption.dataSource = self;
    self.tblAmcOption.delegate = self;
    
    
//    aryCategoryOption = [@[
//                          @{@"option":@"Aggressive", @"checked":@"0"},
//                          @{@"option":@"Balanced", @"checked":@"0"},
//                          @{@"option":@"Conservative Hybrid", @"checked":@"0"},
//                          @{@"option":@"Arbitrage", @"checked":@"0"},
//                          @{@"option":@"Equity Savings", @"checked":@"0"},
//                          @{@"option":@"Dynamic Asset Allocation", @"checked":@"0"},
//                          @{@"option":@"Multi Asset Allocation", @"checked":@"0"}
//                        ] mutableCopy];
//    aryAmcOption = [@[
//                      @{@"option":@"Tata", @"checked":@"0"},
//                      @{@"option":@"Reliance", @"checked":@"0"},
//                      @{@"option":@"ICICI", @"checked":@"0"},
//                      @{@"option":@"Kotak", @"checked":@"0"},
//                      @{@"option":@"SBI", @"checked":@"0"},
//                      @{@"option":@"UTI", @"checked":@"0"},
//                      @{@"option":@"AXIS", @"checked":@"0"},
//                      @{@"option":@"IDBI", @"checked":@"0"},
//                      @{@"option":@"DSP", @"checked":@"0"},
//                      @{@"option":@"Tata", @"checked":@"0"},
//                      @{@"option":@"Reliance", @"checked":@"0"},
//                      @{@"option":@"ICICI", @"checked":@"0"},
//                      @{@"option":@"Kotak", @"checked":@"0"},
//                      @{@"option":@"SBI", @"checked":@"0"},
//                      @{@"option":@"UTI", @"checked":@"0"},
//                      @{@"option":@"AXIS", @"checked":@"0"},
//                      @{@"option":@"IDBI", @"checked":@"0"},
//                      @{@"option":@"DSP", @"checked":@"0"}
//                      ] mutableCopy];
    
    [self btnFundOption_Tapped:nil];
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (tableView == self.tblCategoryOption) {
        NSDictionary *dicTemp = aryMainCategory[selectedCategoryIndex];
        return ((NSArray*)dicTemp.allValues.firstObject).count;
    } else {
        return aryAmcOption.count;
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (tableView == self.tblCategoryOption) {
        NSDictionary *dicTemp = aryMainCategory[selectedCategoryIndex];
        NSArray *aryCategoryOption = dicTemp.allValues.firstObject;
        if (aryCategoryOption.count > indexPath.row) {
            FilterTickCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterTickCell"];
            cell.lblOption.text = aryCategoryOption[indexPath.row][@"option"];
            if ([aryCategoryOption[indexPath.row][@"checked"] isEqualToString:@"1"]) {
                cell.imgTick.image = tickImage;
            } else {
                cell.imgTick.image = unTickImage;
            }
            return cell;
        }
        
    } else {
        if (aryAmcOption.count > indexPath.row) {
            FilterTickCell *cell = [tableView dequeueReusableCellWithIdentifier:@"FilterTickCell"];
            //cell.selectionStyle = UITableViewCellSelectionStyleNone;
            cell.lblOption.text = aryAmcOption[indexPath.row][@"amc"];
            if ([aryAmcOption[indexPath.row][@"checked"] isEqualToString:@"1"]) {
                cell.imgTick.image = tickImage;
            } else {
                cell.imgTick.image = unTickImage;
            }
            return cell;
        }
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 50;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:NO];
    if (tableView == self.tblCategoryOption) {
        NSMutableDictionary *mdicTemp = [aryMainCategory[selectedCategoryIndex] mutableCopy];
        NSString *strKey = mdicTemp.allKeys.firstObject;
        NSMutableArray *aryCategoryOption = [mdicTemp[strKey] mutableCopy];
        if (aryCategoryOption.count > indexPath.row) {
            NSMutableDictionary *mutDic = [aryCategoryOption[indexPath.row] mutableCopy];
            if ([mutDic[@"checked"] isEqualToString:@"1"]) {
                mutDic[@"checked"] = @"0";
            } else {
                mutDic[@"checked"] = @"1";
            }
            [aryCategoryOption replaceObjectAtIndex:indexPath.row withObject:mutDic];
            mdicTemp[strKey] = aryCategoryOption;
            [aryMainCategory replaceObjectAtIndex:selectedCategoryIndex withObject:mdicTemp];
            [tableView reloadData];
        }
        
    } else {
        if (aryAmcOption.count > indexPath.row) {
            NSMutableDictionary *mutDic = [aryAmcOption[indexPath.row] mutableCopy];
            if ([mutDic[@"checked"] isEqualToString:@"1"]) {
                mutDic[@"checked"] = @"0";
            } else {
                mutDic[@"checked"] = @"1";
            }
            [aryAmcOption replaceObjectAtIndex:indexPath.row withObject:mutDic];
            [tableView reloadData];
        }
    }
}

#pragma mark - Add Menu Buttons in Menu Scroll View
- (void)addButtonsInScrollMenu {
    CGFloat buttonHeight = self.menuScrollView.frame.size.height;
    CGFloat cWidth = 0.0f;

    for (int i = 0; i < aryMainCategory.count; i++) {
        NSMutableDictionary *mdicCat = [aryMainCategory[i] mutableCopy];
        NSString *tagTitle = mdicCat.allKeys.firstObject;
        mdicCat[tagTitle] = [self prepareSubCategoryArrayFrom:mdicCat[tagTitle]];
        [aryMainCategory replaceObjectAtIndex:i withObject:mdicCat];
        CGFloat buttonWidth = [self widthForMenuTitle:tagTitle buttonEdgeInsets:kDefaultEdgeInsets];
        UIButton *button = [UIButton buttonWithType:UIButtonTypeCustom];
        button.frame = CGRectMake (cWidth, 0.0f, buttonWidth, buttonHeight);
        [button setTitle:tagTitle forState:UIControlStateNormal];
        button.titleLabel.font = [UIFont fontWithName:@"Roboto-Regular" size:14.0];

        [button setTitleColor:[UIColor blackColor] forState:UIControlStateNormal];
        [button setTitleColor:[UIColor blackColor] forState:UIControlStateSelected];
        [button addTarget:self action:@selector (buttonPressed:) forControlEvents:UIControlEventTouchUpInside];
        button.tag = i;
        [self.menuScrollView addSubview:button];

        UIView *bottomView = [[UIView alloc] initWithFrame:CGRectMake (0, button.frame.size.height - 5, button.frame.size.width, 4)];
        bottomView.backgroundColor = [UIColor colorWithHexString:@"007FEB"];
        bottomView.tag = 1001;
        [button addSubview:bottomView];
        if (i == 0) {
            button.selected = YES;
            [bottomView setHidden:NO];
        } else {
            [bottomView setHidden:YES];
        }
        cWidth += buttonWidth;
    }
    self.menuScrollView.contentSize = CGSizeMake (cWidth, self.menuScrollView.frame.size.height);
}
- (CGFloat)widthForMenuTitle:(NSString *)title buttonEdgeInsets:(UIEdgeInsets)buttonEdgeInsets {
    NSDictionary *attributes = @{NSFontAttributeName:[UIFont fontWithName:@"Roboto-Regular" size:14.0]};
    CGSize size = [title sizeWithAttributes:attributes];
    return CGSizeMake(size.width + buttonEdgeInsets.left + buttonEdgeInsets.right, size.height + buttonEdgeInsets.top + buttonEdgeInsets.bottom).width;
}

-(NSMutableArray*)prepareSubCategoryArrayFrom:(NSArray*)aryOrg {
    NSMutableArray *aryResult = [NSMutableArray new];
    for (NSString *strSubCat in aryOrg) {
        [aryResult addObject:@{@"option":strSubCat, @"checked":@"0"}];
    }
    return aryResult;
}

#pragma mark - Menu Button press action
- (void)buttonPressed:(UIButton *)sender {
    selectedCategoryIndex = sender.tag;
    float buttonWidth = 0.0f;
    for (UIView *subView in self.menuScrollView.subviews) {
        if ([subView isKindOfClass:[UIButton class]]) {
            UIButton *btn = (UIButton *)subView;
            UIView *bottomView = [btn viewWithTag:1001];
            if (btn.tag == sender.tag) {
                btn.selected = YES;
                [bottomView setHidden:NO];
                buttonWidth = btn.frame.origin.x;
            } else {
                btn.selected = NO;
                [bottomView setHidden:YES];
            }
        }
    }
    [self.tblCategoryOption reloadData];
    //[self.containerScrollView setContentOffset:CGPointMake(SCREEN_WIDTH * senderbtn.tag, 0) animated:YES];
    //float xx = SCREEN_WIDTH * (senderbtn.tag) * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
    //[self.menuScrollView scrollRectToVisible:CGRectMake (xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
    //[self.menuScrollView scrollRectToVisible:CGRectMake (buttonWidth, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
}

//#pragma mark - Scroll view delegate methods
//- (void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    int page = (scrollView.contentOffset.x / SCREEN_WIDTH);
//
//    UIButton *btn;
//    float buttonWidth = 0.0;
//    for (UIView *subView in self.menuScrollView.subviews) {
//        btn = (UIButton *)subView;
//        UIView *bottomView = [btn viewWithTag:1001];
//
//        if (btn.tag == page) {
//            btn.selected = YES;
//            buttonWidth = btn.frame.size.width;
//            [bottomView setHidden:NO];
//        } else {
//            btn.selected = NO;
//            [bottomView setHidden:YES];
//        }
//    }
//
//    float xx = scrollView.contentOffset.x * (buttonWidth / SCREEN_WIDTH) - buttonWidth;
//    [self.menuScrollView scrollRectToVisible:CGRectMake (xx, 0, SCREEN_WIDTH, self.menuScrollView.frame.size.height) animated:YES];
//}


#pragma mark - Common Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnClearAll_Tapped:(id)sender {
    
}

- (IBAction)btnApply_Tapped:(id)sender {
    NSMutableDictionary *mdict = [NSUserData GetAppliedSortFilterData].mutableCopy;
    // Growth - Dividend
    
    // Category
    NSMutableArray *arySelectedCategory = [NSMutableArray new];
    for (NSDictionary *dic in aryMainCategory) {
        NSMutableDictionary *mdicTemp = [dic mutableCopy];
        NSString *strKey = mdicTemp.allKeys.firstObject;
        NSMutableArray *aryCategoryOption = [mdicTemp[strKey] mutableCopy];
        for (NSDictionary *dicOption in aryCategoryOption) {
            if ([dicOption[@"checked"] isEqualToString:@"1"]) {
                NSLog(@"%@", dicOption);
                [arySelectedCategory addObject:dicOption[@"option"]];
                
                NSDictionary *dicApplied = @{dicSort[@"scheme_category"]:@"asc"};
                
                mdict[@"sort"] = dicApplied;
                [NSUserData SaveAppliedSortFilterData:mdict];
            }
        }
        
    }
//    else {
//        if (aryAmcOption.count > indexPath.row) {
//            NSMutableDictionary *mutDic = [aryAmcOption[indexPath.row] mutableCopy];
//            if ([mutDic[@"checked"] isEqualToString:@"1"]) {
//                mutDic[@"checked"] = @"0";
//            } else {
//                mutDic[@"checked"] = @"1";
//            }
//            [aryAmcOption replaceObjectAtIndex:indexPath.row withObject:mutDic];
//            [tableView reloadData];
//        }
//    }
    // AMC
    
//    NSDictionary *dicApplied = @{dicSort[@"sort"]:strOrder};
//    mdict[@"filter"] = dicApplied;
//    [NSUserData SaveAppliedSortFilterData:mdict];
    if (self.delegate && [self.delegate respondsToSelector:@selector(FilterApplied)]) {
        [self.delegate FilterApplied];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Filter By Option Button

- (IBAction)btnFundOption_Tapped:(id)sender {
    self.viewFundOption.backgroundColor = [UIColor getAppColorBackground];
    self.viewCategory.backgroundColor = [UIColor whiteColor];
    self.viewAMC.backgroundColor = [UIColor whiteColor];
    
    self.containerFundOption.hidden = NO;
    self.containerCategory.hidden = YES;
    self.containerAMC.hidden = YES;
}

- (IBAction)btnCategory_Tapped:(id)sender {
    self.viewFundOption.backgroundColor = [UIColor whiteColor];
    self.viewCategory.backgroundColor = [UIColor getAppColorBackground];
    self.viewAMC.backgroundColor = [UIColor whiteColor];
    
    self.containerFundOption.hidden = YES;
    self.containerCategory.hidden = NO;
    self.containerAMC.hidden = YES;
}

- (IBAction)btnAMC_Tapped:(id)sender {
    self.viewFundOption.backgroundColor = [UIColor whiteColor];
    self.viewCategory.backgroundColor = [UIColor whiteColor];
    self.viewAMC.backgroundColor = [UIColor getAppColorBackground];
    
    self.containerFundOption.hidden = YES;
    self.containerCategory.hidden = YES;
    self.containerAMC.hidden = NO;
}




@end
