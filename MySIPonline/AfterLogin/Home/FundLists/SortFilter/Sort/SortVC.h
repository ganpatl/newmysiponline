//
//  SortVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN
@protocol SortingDelegate<NSObject>
//@optional
- (void)SortSelected;
@end



@interface SortVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UITableView *tblSort;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableHeightConstraint;

@property (assign, nonatomic) id <SortingDelegate>delegate;
@property NSString *strSortingKey;

@end

NS_ASSUME_NONNULL_END
