//
//  SortVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/12/18.
//

#import "SortVC.h"
#import "SortOptionCell.h"

@interface SortVC () <UITableViewDataSource, UITableViewDelegate> {
    NSMutableArray *arySort;
    UIImage *imgDownArrow;
    UIImage *imgUpArrow;
}

@end

@implementation SortVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    
    imgDownArrow = [UIImage imageNamed:@"sort_down"];
    imgUpArrow = [UIImage imageNamed:@"sort_up"];
    
    NSDictionary *dicSortFilter = [NSUserData GetSortFilterData];
    NSArray *aryTemp = dicSortFilter[@"sort"];
    arySort = [NSMutableArray new];
    for (NSString *strSort in aryTemp) {
        [arySort addObject:@{@"sort":strSort}];
    }
    
    // set initial selection if any
    NSDictionary *dicApplied = [NSUserData GetAppliedSortFilterData];
    NSDictionary *dicAppliedSort = dicApplied[@"sort"];
    if (dicAppliedSort != nil) {
        NSString *strKey = dicAppliedSort.allKeys.firstObject;
        NSString *strSortOrder = dicAppliedSort[strKey];
        aryTemp = [arySort copy];
        int counter = 0;
        for (NSDictionary *dic in aryTemp) {
            NSMutableDictionary *mdict = dic.mutableCopy;
            if ([dic[@"sort"] isEqualToString:strKey]) {
                mdict[@"sort_order"] = strSortOrder;
            } else {
                mdict[@"sort_order"] = @"asc";
            }
            [arySort replaceObjectAtIndex:counter withObject:mdict];
            counter++;
        }
    }
    [self.tblSort registerNib:[UINib nibWithNibName:@"SortOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SortOptionCell"];
    self.tblSort.dataSource = self;
    self.tblSort.delegate = self;
}
-(void)viewDidLayoutSubviews {
    self.tableHeightConstraint.constant = self.tblSort.contentSize.height;
}

#pragma mark - TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return arySort.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (arySort.count > indexPath.row) {
        SortOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SortOptionCell"];
        NSDictionary *dicSort = arySort[indexPath.row];
        cell.lblSortBy.text = dicSort[@"sort"];
        if ([dicSort[@"sort_order"] isEqualToString:@"asc"]) {
            cell.lblSortOrder.text = @"Low to High";
            cell.lblSortOrder.textColor = [UIColor getAppColorDarkGrayText];
            cell.imgSort.image = imgDownArrow;
        } else {
            cell.lblSortOrder.text = @"High to Low";
            cell.lblSortOrder.textColor = [UIColor getAppColorBlue];
            cell.imgSort.image = imgUpArrow;
        }
        return cell;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    GSLog(@"%@", arySort[indexPath.row]);
    NSDictionary *dicSort = arySort[indexPath.row];
    NSMutableDictionary *mdict = [NSUserData GetAppliedSortFilterData].mutableCopy;
    NSString *strOrder = [dicSort[@"sort_order"] isEqualToString:@"asc"] ? @"desc" : @"asc";
    NSDictionary *dicApplied = @{dicSort[@"sort"]:strOrder};
    mdict[@"sort"] = dicApplied;
    [NSUserData SaveAppliedSortFilterData:mdict];
    if (self.delegate && [self.delegate respondsToSelector:@selector(SortSelected)]) {
        [self.delegate SortSelected];
    }
    [self dismissViewControllerAnimated:YES completion:nil];
}

//-(void)deselectAll {
//    self.lblReturn_HighToLow.textColor = [UIColor getAppColorDarkGrayText];
//    self.lblRisk_LowToHigh.textColor = [UIColor getAppColorDarkGrayText];
//    self.lblFundSize_LowToHigh.textColor = [UIColor getAppColorDarkGrayText];
//    self.imgReturn_HighToLow.image = [UIImage imageNamed:@"sort_down"];
//    self.imgRisk_LowToHigh.image = [UIImage imageNamed:@"sort_down"];
//    self.imgFundSize_LowToHigh.image = [UIImage imageNamed:@"sort_down"];
//}
//
//-(void)selectSortOption:(NSString *)strSortingKey {
//    [self deselectAll];
//    if ([strSortingKey isEqualToString:key_ReturnHighToLow]) {
//        self.lblReturn_HighToLow.textColor = [UIColor getAppColorBlue];
//        self.imgReturn_HighToLow.image = [UIImage imageNamed:@"sort_up"];
//    } else if ([strSortingKey isEqualToString:key_RiskLowToHigh]) {
//        self.lblRisk_LowToHigh.textColor = [UIColor getAppColorBlue];
//        self.imgRisk_LowToHigh.image = [UIImage imageNamed:@"sort_up"];
//    } else if ([strSortingKey isEqualToString:key_FundSizeLowToHigh]) {
//        self.lblFundSize_LowToHigh.textColor = [UIColor getAppColorBlue];
//        self.imgFundSize_LowToHigh.image = [UIImage imageNamed:@"sort_up"];
//    }
//}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

//- (IBAction)btnReturn_HighToLow_Tapped:(id)sender {
//
//    [self selectSortOption:key_ReturnHighToLow];
//    if (self.delegate && [self.delegate respondsToSelector:@selector(SortSelected)]) {
//        [self.delegate SortSelected];
//    }
//}
//
//- (IBAction)btnRisk_LowToHigh:(id)sender {
//    [NSUserData SaveSortingKey:key_RiskLowToHigh];
//    [self selectSortOption:key_RiskLowToHigh];
//    if (self.delegate && [self.delegate respondsToSelector:@selector(SortSelected)]) {
//        [self.delegate SortSelected];
//    }
//}
//
//- (IBAction)btnFundSize_LowToHigh:(id)sender {
//    [NSUserData SaveSortingKey:key_FundSizeLowToHigh];
//    [self selectSortOption:key_FundSizeLowToHigh];
//    if (self.delegate && [self.delegate respondsToSelector:@selector(SortSelected)]) {
//        [self.delegate SortSelected];
//    }
//}

@end
