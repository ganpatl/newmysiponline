//
//  PartnerAMCsVC.m
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import "PartnerAMCsVC.h"
#import "AMCListCell.h"
#import "BestFundsListVC.h"

@interface PartnerAMCsVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryAMC;
    NSString *strImagePath;
}

@end

@implementation PartnerAMCsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    strImagePath = @"";
    [self.tblAmc registerNib:[UINib nibWithNibName:@"AMCListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AMCListCell"];
    self.tblAmc.dataSource = self;
    self.tblAmc.delegate = self;
    self.tblAmc.tableFooterView = [UIView new];
    
    [self getAllAMC];
}

#pragma mark - Web API Call

-(void)getAllAMC {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"division_type":@"amc"};
        [WebServices api_getAMCList:dicParam AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryAMC = response[@"result"];
                    self->strImagePath = response[@"amc_img_url"];
                    [self.tblAmc reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryAMC.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryAMC.count > indexPath.row) {
        AMCListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"AMCListCell"];
        NSDictionary *dicAmc = aryAMC[indexPath.row];
        cell.lblAmcName.text = dicAmc[@"amc"];
        cell.lblTotalFunds.text = dicAmc[@"total_fund"];
        //cell.lblTotalAums.text = dicAmc[@"aum"];        
        NSString *strImage = [NSString stringWithFormat:@"%@%@", strImagePath, dicAmc[@"logo"]];
        [cell.imgLogo sd_setImageWithURL:[NSURL URLWithString:strImage]];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 71;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NSDictionary *dic = aryAMC[indexPath.row];    
    BestFundsListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestFundsListVC"];
    destVC.strCategoryType = @"Amc fund";
    destVC.strTitle = dic[@"amc"];
    destVC.strAmcCode = dic[@"amc_code"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCart_Tapped:(id)sender {
    
}


@end
