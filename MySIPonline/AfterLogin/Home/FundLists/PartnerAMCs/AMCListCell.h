//
//  AMCListCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AMCListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet SubHeaderLabel *lblAmcName;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblTotalFunds;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblTotalAums;

@end

NS_ASSUME_NONNULL_END
