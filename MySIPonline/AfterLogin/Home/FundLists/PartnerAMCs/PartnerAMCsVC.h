//
//  PartnerAMCsVC.h
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PartnerAMCsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblAmc;

@end

NS_ASSUME_NONNULL_END
