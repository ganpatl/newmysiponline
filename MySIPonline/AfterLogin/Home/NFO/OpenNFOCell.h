//
//  OpenNFOCell.h
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OpenNFOCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblMinInvestment;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblRisk;
@property (weak, nonatomic) IBOutlet UILabel *lblDaysLeft;
@property (weak, nonatomic) IBOutlet UIImageView *imgLike;
@property (weak, nonatomic) IBOutlet UIView *separatorView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *imgLikeHeightConstraint;

-(void)setExpertsRecommendation:(BOOL)show;

@end

NS_ASSUME_NONNULL_END
