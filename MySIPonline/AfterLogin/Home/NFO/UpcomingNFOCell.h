//
//  UpcomingNFOCell.h
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UpcomingNFOCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblMinInvestment;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblOpenDate;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblCloseDate;

@end

NS_ASSUME_NONNULL_END
