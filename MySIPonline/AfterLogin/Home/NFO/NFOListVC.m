//
//  NFOListVC.m
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import "NFOListVC.h"
#import "NfoDetails/NFODetailsVC.h"

// Cell
#import "OpenNFOCell.h"
#import "UpcomingNFOCell.h"

@interface NFOListVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryOpenNfo;
    NSArray *aryUpcomingNfo;
    BOOL showUpcomingNFO;
}

@end

@implementation NFOListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    self.btnOpen.layer.cornerRadius = self.btnOpen.frame.size.height / 2;
    self.btnUpcoming.layer.cornerRadius = self.btnUpcoming.frame.size.height / 2;
    showUpcomingNFO = NO;
    
    aryOpenNfo = @[
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"risk":@"Moderately", @"days_left":@"18", @"recommended":@"1"},
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"risk":@"Moderately", @"days_left":@"6", @"recommended":@"1"},
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"500", @"risk":@"Low", @"days_left":@"26", @"recommended":@"0"},
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"risk":@"Moderately High", @"days_left":@"1", @"recommended":@"0"},
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"1000", @"risk":@"Moderately", @"days_left":@"28", @"recommended":@"0"},
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"3000", @"risk":@"Moderately Low", @"days_left":@"8", @"recommended":@"0"},
                   @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"2500", @"risk":@"Moderately", @"days_left":@"17", @"recommended":@"0"}
                 ];
    aryUpcomingNfo = [NSArray new];
    aryUpcomingNfo = @[
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"},
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"},
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"},
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"},
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"},
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"},
                       @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"min_investment":@"5000", @"open_date":@"24/12/2018", @"close_date":@"24/12/2018"}
                     ];
    
    [self.tblNFOList registerNib:[UINib nibWithNibName:@"OpenNFOCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"OpenNFOCell"];
    [self.tblNFOList registerNib:[UINib nibWithNibName:@"UpcomingNFOCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"UpcomingNFOCell"];
    
    self.tblNFOList.dataSource = self;
    self.tblNFOList.delegate = self;
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (showUpcomingNFO) {
        return aryUpcomingNfo.count;
    } else {
        return aryOpenNfo.count;
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (showUpcomingNFO) {
        // UPCOMING NFO
        if (aryUpcomingNfo.count > indexPath.row) {
            UpcomingNFOCell *cell = [tableView dequeueReusableCellWithIdentifier:@"UpcomingNFOCell"];
            NSDictionary *dictOpen = aryUpcomingNfo[indexPath.row];
            if ([dictOpen[@"scheme_name"] isKindOfClass:[NSString class]]) {
                cell.lblSchemeName.text = dictOpen[@"scheme_name"];
            }
            if ([dictOpen[@"min_investment"] isKindOfClass:[NSString class]]) {
                cell.lblMinInvestment.text = [NSString stringWithFormat:@"₹ %@", dictOpen[@"min_investment"]];
            }
            if ([dictOpen[@"open_date"] isKindOfClass:[NSString class]]) {
                cell.lblOpenDate.text = dictOpen[@"open_date"];
            }
            if ([dictOpen[@"close_date"] isKindOfClass:[NSString class]]) {
                cell.lblCloseDate.text = dictOpen[@"close_date"];
            }
            return cell;
        }
    } else {
        // OPEN NFO
        if (aryOpenNfo.count > indexPath.row) {
            OpenNFOCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OpenNFOCell"];
            NSDictionary *dictOpen = aryOpenNfo[indexPath.row];
            if ([dictOpen[@"scheme_name"] isKindOfClass:[NSString class]]) {
                cell.lblSchemeName.text = dictOpen[@"scheme_name"];
            }
            if ([dictOpen[@"min_investment"] isKindOfClass:[NSString class]]) {
                cell.lblMinInvestment.text = [NSString stringWithFormat:@"₹ %@", dictOpen[@"min_investment"]];
            }
            if ([dictOpen[@"risk"] isKindOfClass:[NSString class]]) {
                cell.lblRisk.text = dictOpen[@"risk"];
            }
            if ([dictOpen[@"days_left"] isKindOfClass:[NSString class]]) {
                cell.lblDaysLeft.text = dictOpen[@"days_left"];
            }
            if ([dictOpen[@"recommended"] isKindOfClass:[NSString class]]) {
                [cell setExpertsRecommendation:[dictOpen[@"recommended"] isEqualToString:@"1"]];
            }
            return cell;
        }
    }
    
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    NFODetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NFODetailsVC"];
    destVC.showUpcomingNFO = showUpcomingNFO;
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnOpen_Tapped:(id)sender {
    showUpcomingNFO = NO;
    [self.btnOpen setBackgroundColor:[UIColor getAppColorBlue]];
    [self.btnOpen setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnUpcoming setBackgroundColor:[UIColor getAppColorBorder]];
    [self.btnUpcoming setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.tblNFOList reloadData];
}

- (IBAction)btnUpcoming_Tapped:(id)sender {
    showUpcomingNFO = YES;
    [self.btnUpcoming setBackgroundColor:[UIColor getAppColorBlue]];
    [self.btnUpcoming setTitleColor:[UIColor whiteColor] forState:UIControlStateNormal];
    [self.btnOpen setBackgroundColor:[UIColor getAppColorBorder]];
    [self.btnOpen setTitleColor:[UIColor getAppColorTitleText] forState:UIControlStateNormal];
    [self.tblNFOList reloadData];
}

@end
