//
//  OpenNFOCell.m
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import "OpenNFOCell.h"

@implementation OpenNFOCell

- (void)awakeFromNib {
    [super awakeFromNib];
    self.imgLike.layer.cornerRadius = self.imgLike.frame.size.height / 2;
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setExpertsRecommendation:(BOOL)show {
    if (show) {
        self.separatorView.hidden = NO;
        self.imgLikeHeightConstraint.constant = 23;
    } else {
        self.separatorView.hidden = YES;
        self.imgLikeHeightConstraint.constant = 0;
    }
}

@end
