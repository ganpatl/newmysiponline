//
//  NfoBasicDetailsCell.h
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NfoBasicDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

NS_ASSUME_NONNULL_END
