//
//  NfoRiskometerCell.h
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface NfoRiskometerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property (weak, nonatomic) IBOutlet PieChartView *chartView;

@end

NS_ASSUME_NONNULL_END
