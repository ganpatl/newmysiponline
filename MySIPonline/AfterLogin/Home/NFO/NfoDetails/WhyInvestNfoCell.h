//
//  WhyInvestNfoCell.h
//  MySIPonline
//
//  Created by Ganpat on 01/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WhyInvestNfoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnWhyInvestNfo;

@end

NS_ASSUME_NONNULL_END
