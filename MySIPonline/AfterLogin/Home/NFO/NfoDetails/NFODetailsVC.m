//
//  NFODetailsVC.m
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import "NFODetailsVC.h"

// Cells
#import "NfoDetailsFirstCell.h"
#import "WhyInvestNfoCell.h"
#import "NfoBasicDetailsCell.h"
#import "NfoRiskometerCell.h"
#import "NfoSuitabilityCell.h"
#import "NfoUpcomingFirstCell.h"

@interface NFODetailsVC () <UITableViewDataSource, UITableViewDelegate> {
    NfoDetailsFirstCell *nfoDetailsFirstCell;
    WhyInvestNfoCell *whyInvestNfoCell;
    NfoBasicDetailsCell *nfoBasicDetailsCell;
    NfoRiskometerCell *nfoRiskometerCell;
    NfoSuitabilityCell *nfoSuitabilityCell;
    NfoSuitabilityCell *nfoSuitabilityCell2;
    NfoUpcomingFirstCell *nfoUpcomingFirstCell;
}

@end

@implementation NFODetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tblNfoDetails registerNib:[UINib nibWithNibName:@"NfoDetailsFirstCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NfoDetailsFirstCell"];
    [self.tblNfoDetails registerNib:[UINib nibWithNibName:@"WhyInvestNfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"WhyInvestNfoCell"];
    [self.tblNfoDetails registerNib:[UINib nibWithNibName:@"NfoBasicDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NfoBasicDetailsCell"];
    [self.tblNfoDetails registerNib:[UINib nibWithNibName:@"NfoRiskometerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NfoRiskometerCell"];
    [self.tblNfoDetails registerNib:[UINib nibWithNibName:@"NfoSuitabilityCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NfoSuitabilityCell"];
    [self.tblNfoDetails registerNib:[UINib nibWithNibName:@"NfoUpcomingFirstCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NfoUpcomingFirstCell"];
    
    //self.tblFundDetails.estimatedRowHeight = 2.0; // any number but 2.0 is the smallest one that works
    self.tblNfoDetails.dataSource = self;
    self.tblNfoDetails.delegate = self;
    
    
}

#pragma mark - TableView Datasource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 6;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.showUpcomingNFO) {
            if (nfoUpcomingFirstCell == nil) {
                nfoUpcomingFirstCell = [tableView dequeueReusableCellWithIdentifier:@"NfoUpcomingFirstCell"];
            }
            return nfoUpcomingFirstCell;
        } else {
            if (nfoDetailsFirstCell == nil) {
                nfoDetailsFirstCell = [tableView dequeueReusableCellWithIdentifier:@"NfoDetailsFirstCell"];
            }
            return nfoDetailsFirstCell;
        }
        
    } else if (indexPath.section == 1) {
        if (whyInvestNfoCell == nil) {
            whyInvestNfoCell = [tableView dequeueReusableCellWithIdentifier:@"WhyInvestNfoCell"];
        }
        return whyInvestNfoCell;
        
    } else if (indexPath.section == 2) {
        if (nfoBasicDetailsCell == nil) {
            nfoBasicDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"NfoBasicDetailsCell"];
        }
        return nfoBasicDetailsCell;
        
    } else if (indexPath.section == 3) {
        if (nfoRiskometerCell == nil) {
            nfoRiskometerCell = [tableView dequeueReusableCellWithIdentifier:@"NfoRiskometerCell"];
            
            nfoRiskometerCell.chartView.holeColor = UIColor.whiteColor;
            nfoRiskometerCell.chartView.transparentCircleColor = [UIColor.whiteColor colorWithAlphaComponent:0.43];
            nfoRiskometerCell.chartView.holeRadiusPercent = 0.58;
            nfoRiskometerCell.chartView.rotationEnabled = NO;
            nfoRiskometerCell.chartView.highlightPerTapEnabled = YES;
            
            nfoRiskometerCell.chartView.maxAngle = 180.0; // Half chart
            nfoRiskometerCell.chartView.rotationAngle = 180.0; // Rotate to make the half on the upper side
            nfoRiskometerCell.chartView.centerTextOffset = CGPointMake(0.0, -20.0);
            
            nfoRiskometerCell.chartView.legend.enabled = NO;
            
            // entry label styling
            nfoRiskometerCell.chartView.entryLabelColor = UIColor.blackColor;
            nfoRiskometerCell.chartView.entryLabelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:12.f];
            //nfoRiskometerCell.chartView.drawEntryLabelsEnabled = YES;
            nfoRiskometerCell.chartView.drawCenterTextEnabled = YES;
            
            //[self updateChartData];
            NSMutableArray *values = [[NSMutableArray alloc] init];
            // IMPORTANT: In a PieChart, no values (Entry) should have the same xIndex (even if from different DataSets), since no values can be drawn above each other.
            //for (int i = 0; i < count; i++) {
            //    [values addObject:[[PieChartDataEntry alloc] initWithValue:(arc4random_uniform(mult) + mult / 5) label:parties[i % parties.count]]];
            //}
            
            [values addObject:[[PieChartDataEntry alloc] initWithValue:20.0 label:@""]];
            [values addObject:[[PieChartDataEntry alloc] initWithValue:20.0 label:@""]];
            [values addObject:[[PieChartDataEntry alloc] initWithValue:20.0 label:@""]];
            [values addObject:[[PieChartDataEntry alloc] initWithValue:20.0 label:@""]];
            [values addObject:[[PieChartDataEntry alloc] initWithValue:20.0 label:@""]];
            
            PieChartDataSet *dataSet = [[PieChartDataSet alloc] initWithValues:values label:@""];
            dataSet.sliceSpace = 3.0;
            dataSet.selectionShift = 5.0;
            
            dataSet.colors = ChartColorTemplates.material;
            
            PieChartData *data = [[PieChartData alloc] initWithDataSet:dataSet];
            
            NSNumberFormatter *pFormatter = [[NSNumberFormatter alloc] init];
            pFormatter.numberStyle = NSNumberFormatterPercentStyle;
            pFormatter.maximumFractionDigits = 1;
            pFormatter.multiplier = @1.f;
            pFormatter.percentSymbol = @"";
            [data setValueFormatter:[[ChartDefaultValueFormatter alloc] initWithFormatter:pFormatter]];
            
            [data setValueFont:[UIFont fontWithName:@"HelveticaNeue-Light" size:11.f]];
            [data setValueTextColor:UIColor.whiteColor];
            
            nfoRiskometerCell.chartView.data = data;
            
            [nfoRiskometerCell.chartView setNeedsDisplay];
            
            [nfoRiskometerCell.chartView animateWithXAxisDuration:1.4 easingOption:ChartEasingOptionEaseOutBack];
            
        }

        return nfoRiskometerCell;

    } else if (indexPath.section == 4) {
        if (nfoSuitabilityCell == nil) {
            nfoSuitabilityCell = [tableView dequeueReusableCellWithIdentifier:@"NfoSuitabilityCell"];
        }
        return nfoSuitabilityCell;
    
    } else if (indexPath.section == 5) {
        if (nfoSuitabilityCell2 == nil) {
            nfoSuitabilityCell2 = [tableView dequeueReusableCellWithIdentifier:@"NfoSuitabilityCell"];
            nfoSuitabilityCell2.lblTitle.text = @"Key Highlights";
            nfoSuitabilityCell2.lblTitleLine.text = @"This Product is suitable for investors who are seeking :-";
            nfoSuitabilityCell2.lblFirstPara.text = @"The stock selection process proposed to be adopted is generally a bottom-up approach seeking to identify companies with long-term sustainable competitive advantage (as this is one of the keys factors responsible for withstanding competitive pressures and does not allow rivals to eat up any excess profits earned by a successful business).";
            nfoSuitabilityCell2.lblSecondPara.text = @"The Scheme would also use a top-down discipline for risk control by ensuring representation of companies from the select theme.";
        }
        return nfoSuitabilityCell2;
    }
    
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnShare_Tapped:(id)sender {
    
}

- (IBAction)btnInvestNow_Tapped:(id)sender {
    
}

- (IBAction)btnAddToCart_Tapped:(id)sender {
    
}

@end
