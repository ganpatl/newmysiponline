//
//  NfoUpcomingFirstCell.m
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import "NfoUpcomingFirstCell.h"

@implementation NfoUpcomingFirstCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
