//
//  NfoUpcomingFirstCell.h
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NfoUpcomingFirstCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblOpenDate;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblCloseDate;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblMinInvestment;

@end

NS_ASSUME_NONNULL_END
