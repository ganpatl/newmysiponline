//
//  NfoDetailsFirstCell.h
//  MySIPonline
//
//  Created by Ganpat on 01/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NfoDetailsFirstCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

NS_ASSUME_NONNULL_END
