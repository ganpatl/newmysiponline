//
//  NfoSuitabilityCell.h
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NfoSuitabilityCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblTitleLine;
@property (weak, nonatomic) IBOutlet UILabel *lblFirstPara;
@property (weak, nonatomic) IBOutlet UILabel *lblSecondPara;

@end

NS_ASSUME_NONNULL_END
