//
//  NFODetailsVC.h
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NFODetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblNfoDetails;

@property BOOL showUpcomingNFO;

@end

NS_ASSUME_NONNULL_END
