//
//  NFOListVC.h
//  MySIPonline
//
//  Created by Ganpat on 31/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface NFOListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UIButton *btnOpen;
@property (weak, nonatomic) IBOutlet UIButton *btnUpcoming;
@property (weak, nonatomic) IBOutlet UITableView *tblNFOList;

@end

NS_ASSUME_NONNULL_END
