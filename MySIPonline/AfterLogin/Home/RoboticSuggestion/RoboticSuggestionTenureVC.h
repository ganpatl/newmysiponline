//
//  RoboticSuggestionTenureVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoboticSuggestionTenureVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLongTerm;
@property (weak, nonatomic) IBOutlet UIImageView *imgMidTerm;
@property (weak, nonatomic) IBOutlet UIImageView *imgShortTerm;

@end

NS_ASSUME_NONNULL_END
