//
//  RoboticSuggestionTenureVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/02/19.
//

#import "RoboticSuggestionTenureVC.h"

@interface RoboticSuggestionTenureVC ()

@end

@implementation RoboticSuggestionTenureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnLongTerm_Tapped:(id)sender {
    self.imgLongTerm.image = [UIImage imageNamed:@"checked"];
    self.imgMidTerm.image = [UIImage imageNamed:@"done-Gray"];
    self.imgShortTerm.image = [UIImage imageNamed:@"done-Gray"];
}

- (IBAction)btnMidTerm_Tapped:(id)sender {
    self.imgLongTerm.image = [UIImage imageNamed:@"done-Gray"];
    self.imgMidTerm.image = [UIImage imageNamed:@"checked"];
    self.imgShortTerm.image = [UIImage imageNamed:@"done-Gray"];
}

- (IBAction)btnShortTerm_Tapped:(id)sender {
    self.imgLongTerm.image = [UIImage imageNamed:@"done-Gray"];
    self.imgMidTerm.image = [UIImage imageNamed:@"done-Gray"];
    self.imgShortTerm.image = [UIImage imageNamed:@"checked"];
}

- (IBAction)btnNext_Tapped:(id)sender {
    
}


@end
