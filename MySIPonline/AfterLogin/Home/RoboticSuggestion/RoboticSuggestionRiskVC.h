//
//  RoboticSuggestionRiskVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RoboticSuggestionRiskVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UIImageView *imgHighRisk;
@property (weak, nonatomic) IBOutlet UIImageView *imgMediumRisk;
@property (weak, nonatomic) IBOutlet UIImageView *imgLowRisk;

@end

NS_ASSUME_NONNULL_END
