//
//  RoboticSuggestionRiskVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/02/19.
//

#import "RoboticSuggestionRiskVC.h"
#import "BestFundsListVC.h"

@interface RoboticSuggestionRiskVC ()

@end

@implementation RoboticSuggestionRiskVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnHighRisk_Tapped:(id)sender {
    self.imgHighRisk.image = [UIImage imageNamed:@"checked"];
    self.imgMediumRisk.image = [UIImage imageNamed:@"done-Gray"];
    self.imgLowRisk.image = [UIImage imageNamed:@"done-Gray"];
}

- (IBAction)btnMediumRisk_Tapped:(id)sender {
    self.imgHighRisk.image = [UIImage imageNamed:@"done-Gray"];
    self.imgMediumRisk.image = [UIImage imageNamed:@"checked"];
    self.imgLowRisk.image = [UIImage imageNamed:@"done-Gray"];
}

- (IBAction)btnLowRisk_Tapped:(id)sender {
    self.imgHighRisk.image = [UIImage imageNamed:@"done-Gray"];
    self.imgMediumRisk.image = [UIImage imageNamed:@"done-Gray"];
    self.imgLowRisk.image = [UIImage imageNamed:@"checked"];
}

- (IBAction)btnViewFunds_Tapped:(id)sender {
    BestFundsListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestFundsListVC"];
    //destVC.strTitle
    //destVC.strCategoryType =
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
