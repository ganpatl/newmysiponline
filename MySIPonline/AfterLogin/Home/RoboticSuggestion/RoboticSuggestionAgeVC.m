//
//  RoboticSuggestionAgeVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/02/19.
//

#import "RoboticSuggestionAgeVC.h"

@interface RoboticSuggestionAgeVC () <RKTagsViewDelegate> {
    
}

@end

@implementation RoboticSuggestionAgeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    // Setup RKTagsView
    [self.investmentTypeTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.investmentTypeTagsView.tagButtonHeight = 30;
    self.investmentTypeTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    self.investmentTypeTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
    self.investmentTypeTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
    self.investmentTypeTagsView.selectedTagTintColor = [UIColor whiteColor];
    self.investmentTypeTagsView.editable = NO;
    self.investmentTypeTagsView.allowsMultipleSelection = NO;
    self.investmentTypeTagsView.lineSpacing = 8;
    self.investmentTypeTagsView.interitemSpacing = 8;
    self.investmentTypeTagsView.buttonCornerRadius = 14;
    self.investmentTypeTagsView.delegate = self;
    self.investmentTypeTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    
    NSArray *arySip = @[@{@"name":@"SIP"}, @{@"name":@"Lumpsum"}];
    if (self.investmentTypeTagsView.tags.count != arySip.count) {
        [self.investmentTypeTagsView removeAllTags];
        for (NSDictionary *dic in arySip) {
            if ([AppHelper isNotNull:dic[@"name"]]) {
                [self.investmentTypeTagsView addTag:dic[@"name"]];
            }
        }
        // Set First item selected
        [self.investmentTypeTagsView selectTagAtIndex:0];
    }
    
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (index == 0) {
        // SIP
        
    } else {
        // Lumpsum
        
    }
    return YES;
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNext_Tapped:(id)sender {
    
}

@end
