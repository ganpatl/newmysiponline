//
//  BlogsVideoVC.m
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import "BlogsVideoVC.h"
#import "VideoPreviewVC.h"

// Cell
#import "BlogCell.h"
#import "VideoCell.h"

@interface BlogsVideoVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryVideo;    
}

@end

@implementation BlogsVideoVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    aryVideo = [NSArray new];
    [AppHelper addTopShadowOnView:self.topTitleView];
    self.btnLatestBlog.layer.cornerRadius = self.btnLatestBlog.frame.size.height / 2;
    self.btnVideos.layer.cornerRadius = self.btnVideos.frame.size.height / 2;
    
    [self.tblBlogsVideo registerNib:[UINib nibWithNibName:@"BlogCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BlogCell"];
    [self.tblBlogsVideo registerNib:[UINib nibWithNibName:@"VideoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"VideoCell"];
    self.tblBlogsVideo.dataSource = self;
    self.tblBlogsVideo.delegate = self;
    
    [self getVideoList];
}

#pragma mark - TableView Data Source Methods
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (self.showVideo) {
        return aryVideo.count;
    } else {
        return 6;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.showVideo) {
        VideoCell *cell = [tableView dequeueReusableCellWithIdentifier:@"VideoCell"];
        NSDictionary *dictVideo = [aryVideo objectAtIndex:indexPath.row];
        cell.lblVideoTitle.text = [NSString stringWithFormat:@"%@", dictVideo[@"snippet"][@"title"]];
        
        NSString *dateString = [NSString stringWithFormat:@"%@", dictVideo[@"snippet"][@"publishedAt"]];
        NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
        [dateFormatter setDateFormat:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
        
        NSDate *date = [dateFormatter dateFromString:dateString];
        // Convert date object into desired format
        [dateFormatter setDateFormat:@"dd MMMM, yyyy"];
        NSString *newDateString = [dateFormatter stringFromDate:date];
        cell.lblVideoDate.text = newDateString;
        
        NSString *urlLink = dictVideo[@"snippet"][@"thumbnails"][@"medium"][@"url"];
        [cell.imgVideo sd_setImageWithURL:[NSURL URLWithString:urlLink] placeholderImage:[UIImage imageNamed:@"default_banner_ic.png"]];
        
        return cell;
    } else {
        BlogCell *cell = [tableView dequeueReusableCellWithIdentifier:@"BlogCell"];
        return cell;
    }
}

#pragma mark - TableView Delegates

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (self.showVideo) {
        NSDictionary *dictVideo = [aryVideo objectAtIndex:indexPath.row];
        VideoPreviewVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoPreviewVC"];
        destVC.strVideoTitle = [NSString stringWithFormat:@"%@", dictVideo[@"snippet"][@"title"]];
        destVC.strVideoId = dictVideo[@"id"][@"videoId"];
        [self presentViewController:destVC animated:YES completion:nil];
        
    } else {
        UIViewController *destVC = [AppHelper createWebViewControllerWithTitle:@"Test WebView" URLString:@"https://blog.mysiponline.com/why-should-you-invest-in-mutual-funds-before-the-age-of-25" ShowShareButton:YES];
        [self.navigationController pushViewController:destVC animated:YES];
    }
}

#pragma mark - Web Api Call

-(void)getVideoList {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_getVideoList:nil AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                self.tblBlogsVideo.hidden = NO;
                self->aryVideo = response[@"items"];
                NSMutableArray *aryTemp = [NSMutableArray new];
                for (NSDictionary *dict in self->aryVideo) {
                    if ([dict[@"id"][@"kind"] isEqualToString:@"youtube#video"]) {
                        [aryTemp addObject:dict];
                    }
                }
                self->aryVideo = aryTemp;
                [self.tblBlogsVideo reloadData];
            } else {
                //NSError *err = (NSError*)response;
                //[AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnBlog_Tapped:(id)sender {
    self.showVideo = NO;
    [self.tblBlogsVideo reloadData];
}

- (IBAction)btnVideo_Tapped:(id)sender {
    self.showVideo = YES;
    [self.tblBlogsVideo reloadData];
}

@end
