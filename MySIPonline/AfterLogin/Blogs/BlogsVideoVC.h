//
//  BlogsVideoVC.h
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BlogsVideoVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UIButton *btnLatestBlog;
@property (weak, nonatomic) IBOutlet UIButton *btnVideos;
@property (weak, nonatomic) IBOutlet UITableView *tblBlogsVideo;

@property BOOL showVideo;

@end

NS_ASSUME_NONNULL_END
