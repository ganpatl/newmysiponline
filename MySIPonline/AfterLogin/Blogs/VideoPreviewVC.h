//
//  VideoPreviewVC.h
//  MySIPonline
//
//  Created by Ganpat on 06/03/19.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoPreviewVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet YTPlayerView *player;

@property NSString *strVideoId;
@property NSString *strVideoTitle;

@end

NS_ASSUME_NONNULL_END
