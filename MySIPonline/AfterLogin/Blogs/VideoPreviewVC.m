//
//  VideoPreviewVC.m
//  MySIPonline
//
//  Created by Ganpat on 06/03/19.
//

#import "VideoPreviewVC.h"

@interface VideoPreviewVC () <WKNavigationDelegate> {
    WKWebView *webView;
}

@end

@implementation VideoPreviewVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    CGRect wFrame = self.player.frame;
    
    WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
    webView = [[WKWebView alloc] initWithFrame:wFrame configuration:configuration];
    [self.player addSubview:webView];
    webView.scrollView.scrollEnabled = NO;
    webView.translatesAutoresizingMaskIntoConstraints = NO;
    
    NSArray *horizontal = [NSLayoutConstraint constraintsWithVisualFormat:@"H:|-0-[webView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(webView)];
    [self.view addConstraints:horizontal];
    NSArray *vertical = [NSLayoutConstraint constraintsWithVisualFormat:@"V:|-0-[webView]-0-|" options:0 metrics:nil views:NSDictionaryOfVariableBindings(webView)];
    [self.view addConstraints:vertical];
    
    // Refresh myView and/or main view
    [self.view layoutIfNeeded];
    //[self.myView layoutIfNeeded];
    
    webView.navigationDelegate = self;
    NSString *link = [NSString stringWithFormat:@"https://www.youtube.com/embed/%@?rel=0", self.strVideoId];
    [webView loadHTMLString:[NSString stringWithFormat:@"<head> <meta name=viewport content='width=device-width, initial-scale=1'><style type='text/css'> body { margin: 0;} </style></head><iframe width=100%% height=100%% src=%@ frameborder=0 allowfullscreen></iframe>", link] baseURL:nil];
    
    //[self.player loadWithVideoId:self.strVideoId];
    self.lblTitle.text = self.strVideoTitle;
    
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(videoExitFullScreen:) name:@"UIWindowDidBecomeHiddenNotification" object:nil];

}

//-(BOOL)prefersStatusBarHidden {
//    return NO;
//}

-(void)videoExitFullScreen:(id)sender {
    //Here do WHat You want    
    [UIApplication sharedApplication].statusBarHidden = NO;
}

#pragma mark - Button Tap Event

- (IBAction)btnClose_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
