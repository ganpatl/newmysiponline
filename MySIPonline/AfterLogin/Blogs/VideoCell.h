//
//  VideoCell.h
//  MySIPonline
//
//  Created by Ganpat on 05/03/19.
//

#import <UIKit/UIKit.h>
#import "YTPlayerView.h"

NS_ASSUME_NONNULL_BEGIN

@interface VideoCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgVideo;


@end

NS_ASSUME_NONNULL_END
