//
//  BlogCell.h
//  MySIPonline
//
//  Created by Ganpat on 05/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BlogCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgBlog;
@property (weak, nonatomic) IBOutlet UILabel *lblBlogTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblBlogDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgBookmark;
@property (weak, nonatomic) IBOutlet UIButton *btnBookmark;

@end

NS_ASSUME_NONNULL_END
