//
//  SipInsuranceCoverVC.h
//  MySIPonline
//
//  Created by Ganpat on 17/05/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SipInsuranceCoverVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblSipInsurance;

@end

NS_ASSUME_NONNULL_END
