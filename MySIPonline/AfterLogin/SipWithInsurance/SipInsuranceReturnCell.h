//
//  SipInsuranceReturnCell.h
//  MySIPonline
//
//  Created by Ganpat on 17/05/19.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface SipInsuranceReturnCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet BarChartView *chartView;
@property (weak, nonatomic) IBOutlet UITextField *tfSIPAmount;
@property (weak, nonatomic) IBOutlet UIButton *btn1Y;
@property (weak, nonatomic) IBOutlet UIButton *btn3Y;
@property (weak, nonatomic) IBOutlet UIButton *btn5Y;
@property (weak, nonatomic) IBOutlet UILabel *lblRelianceAmt;
@property (weak, nonatomic) IBOutlet UILabel *lblIciciAmt;
@property (weak, nonatomic) IBOutlet UILabel *lblBirlaAmt;
@property (weak, nonatomic) IBOutlet UIButton *btnHowWeCalculate;
@property (weak, nonatomic) IBOutlet UIButton *btnViewFAQs;

@end

NS_ASSUME_NONNULL_END
