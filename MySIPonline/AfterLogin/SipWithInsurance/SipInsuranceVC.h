//
//  SipInsuranceVC.h
//  MySIPonline
//
//  Created by Ganpat on 07/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SipInsuranceVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblSipInsurance;
@property (weak, nonatomic) IBOutlet UIView *backGradientView;

@end

NS_ASSUME_NONNULL_END
