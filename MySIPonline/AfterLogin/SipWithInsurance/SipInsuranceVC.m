//
//  SipInsuranceVC.m
//  MySIPonline
//
//  Created by Ganpat on 07/03/19.
//

#import "SipInsuranceVC.h"

// Cells
#import "GoldSavingCell.h"
#import "YouWillGetCell.h"
#import "InsuranceFundCell.h"
#import "InsuranceNoteCell.h"
#import "InvestmentOptionCell.h"


@interface SipInsuranceVC () <UITableViewDataSource, UITableViewDelegate> {
    GoldSavingCell *goldSavingCell;
    YouWillGetCell *youWillGetCell;
    InsuranceNoteCell *insuranceNoteCell;
    InvestmentOptionCell *investmentOptionCell;
    NSMutableArray *aryFunds;
    CAGradientLayer *theViewGradient;
}

@end

@implementation SipInsuranceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.backGradientView];
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"47C8DE"] CGColor], (id)[[UIColor colorWithHexString:@"1DBBD6"] CGColor], (id)[[UIColor colorWithHexString:@"0AA7A1"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"GoldSavingCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GoldSavingCell"];
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"YouWillGetCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"YouWillGetCell"];
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"InsuranceFundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InsuranceFundCell"];
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"InsuranceNoteCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InsuranceNoteCell"];
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    aryFunds = @[
                 @{@"image":@"info-error", @"title":@"Reliance Balanced Advantage Fund (G)", @"1year":@"₹ 11 Lakhs", @"2year":@"₹ 21 Lakhs", @"3year":@"₹ 31 Lakhs"}
                 //@{@"image":@"info-error", @"title":@"ICICI Prudential Fund (G)", @"1year":@"₹ 11 Lakhs", @"2year":@"₹ 21 Lakhs", @"3year":@"₹ 31 Lakhs"}
                ].mutableCopy;
    self.tblSipInsurance.dataSource = self;
    self.tblSipInsurance.delegate = self;
}


-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 5;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 2) {
        return aryFunds.count;
    } else {
        return 1;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (goldSavingCell == nil) {
            goldSavingCell = [tableView dequeueReusableCellWithIdentifier:@"GoldSavingCell"];
            goldSavingCell.selectionStyle = UITableViewCellSelectionStyleNone;
            goldSavingCell.lblGms.text = @"";
        }
        return goldSavingCell;
        
    } else if (indexPath.section == 1) {
        if (youWillGetCell == nil) {
            youWillGetCell = [tableView dequeueReusableCellWithIdentifier:@"YouWillGetCell"];
        }
        return youWillGetCell;
        
    } else if (indexPath.section == 2) {
        if (aryFunds.count > indexPath.row) {
            NSDictionary *dictFund = aryFunds[indexPath.row];
            InsuranceFundCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceFundCell"];
            cell.imgLogo.image = [UIImage imageNamed:dictFund[@"image"]];
            cell.lblSchemeName.text = dictFund[@"title"];
            cell.lblIn1Year.text = dictFund[@"1year"];
            cell.lblIn2Year.text = dictFund[@"2year"];
            cell.lblIn3Year.text = dictFund[@"3year"];
            return cell;
        }
    } else if (indexPath.section == 3) {
        if (insuranceNoteCell == nil) {
            insuranceNoteCell = [tableView dequeueReusableCellWithIdentifier:@"InsuranceNoteCell"];
        }
        return insuranceNoteCell;
        
    } else if (indexPath.section == 4) {
        if (investmentOptionCell == nil) {
            investmentOptionCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
            investmentOptionCell.imgIcon.image = [UIImage imageNamed:@"personalizePortfolio"];
            investmentOptionCell.lblTitle.text = @"Explore Other Mutual Funds";
            investmentOptionCell.lblDesc.text = @"Wide range of funds providing SIP with insurance";
        }
        return investmentOptionCell;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.section > 0) {
        
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnBuyMoreGold_Tapped {
    //AddMoneyVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMoneyVC"];
    //[self.navigationController pushViewController:destVC animated:YES];
}

@end
