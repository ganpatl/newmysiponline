//
//  InsuranceFundCell.h
//  MySIPonline
//
//  Created by Ganpat on 07/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InsuranceFundCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UILabel *lblIn1Year;
@property (weak, nonatomic) IBOutlet UILabel *lblIn2Year;
@property (weak, nonatomic) IBOutlet UILabel *lblIn3Year;
@property (weak, nonatomic) IBOutlet UIButton *btnHowToClaim;
@property (weak, nonatomic) IBOutlet UIButton *btnFAQ;


@end

NS_ASSUME_NONNULL_END
