//
//  SipInsuranceCoverVC.m
//  MySIPonline
//
//  Created by Ganpat on 17/05/19.
//

#import "SipInsuranceCoverVC.h"
#import "HowCalculateSipInsuranceVC.h"
#import "SipInsuranceFundListVC.h"
#import "HelpQuestionsVC.h"
// Cell
#import "TrustedByCell.h"
#import "SipInsuranceTaxSavingCell.h"
#import "SipInsuranceReturnCell.h"

@interface SipInsuranceCoverVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryCellContent;
    TrustedByCell *trustedByCell;
    SipInsuranceTaxSavingCell *sipInsuranceTaxSavingCell;
    SipInsuranceReturnCell *sipInsuranceReturnCell;
}

@end

@implementation SipInsuranceCoverVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"TrustedByCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TrustedByCell"];
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"SipInsuranceTaxSavingCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SipInsuranceTaxSavingCell"];
    [self.tblSipInsurance registerNib:[UINib nibWithNibName:@"SipInsuranceReturnCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SipInsuranceReturnCell"];
    self.tblSipInsurance.delegate = self;
    self.tblSipInsurance.dataSource = self;
    
}

-(void)setNormalBorderForAllButtonts {
    sipInsuranceReturnCell.btn1Y.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    sipInsuranceReturnCell.btn3Y.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    sipInsuranceReturnCell.btn5Y.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    sipInsuranceReturnCell.btn1Y.layer.borderWidth = 1.0;
    sipInsuranceReturnCell.btn3Y.layer.borderWidth = 1.0;
    sipInsuranceReturnCell.btn5Y.layer.borderWidth = 1.0;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (trustedByCell == nil) {
            trustedByCell = [tableView dequeueReusableCellWithIdentifier:@"TrustedByCell"];
            trustedByCell.lblRightTop.text = @"50 Lakhs";
            trustedByCell.lblRightBottom.text = @"Insurance Cover";
        }
        return trustedByCell;

    } else if (indexPath.row == 1) {
        if (sipInsuranceTaxSavingCell == nil) {
            sipInsuranceTaxSavingCell = [tableView dequeueReusableCellWithIdentifier:@"SipInsuranceTaxSavingCell"];
            sipInsuranceTaxSavingCell.selectionStyle = UITableViewCellSelectionStyleNone;
            //[sipInsuranceTaxSavingCell.btnKnowMore addTarget:self action:@selector(btnKnowMore_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return sipInsuranceTaxSavingCell;

    } else if (indexPath.row == 2) {
        if (sipInsuranceReturnCell == nil) {
            sipInsuranceReturnCell = [tableView dequeueReusableCellWithIdentifier:@"SipInsuranceReturnCell"];
            [sipInsuranceReturnCell.btnViewFAQs addTarget:self action:@selector(btnViewFAQs_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [sipInsuranceReturnCell.btnHowWeCalculate addTarget:self action:@selector(btnHowWeCalculateThis_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [sipInsuranceReturnCell.btn1Y addTarget:self action:@selector(btn1Y_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            [sipInsuranceReturnCell.btn3Y addTarget:self action:@selector(btn3Y_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            [sipInsuranceReturnCell.btn5Y addTarget:self action:@selector(btn5Y_Tapped:) forControlEvents:UIControlEventTouchUpInside];
            [self btn3Y_Tapped:nil];
            
            sipInsuranceReturnCell.chartView.chartDescription.enabled = NO;
            sipInsuranceReturnCell.chartView.maxVisibleCount = 40;
            sipInsuranceReturnCell.chartView.pinchZoomEnabled = NO;
            sipInsuranceReturnCell.chartView.dragEnabled = NO;
            [sipInsuranceReturnCell.chartView setScaleEnabled:NO];
            sipInsuranceReturnCell.chartView.drawGridBackgroundEnabled = NO;
            sipInsuranceReturnCell.chartView.drawBarShadowEnabled = NO;
            sipInsuranceReturnCell.chartView.drawValueAboveBarEnabled = NO;
            sipInsuranceReturnCell.chartView.highlightFullBarEnabled = NO;
            sipInsuranceReturnCell.chartView.highlightPerTapEnabled = NO;
            
            NSNumberFormatter *leftAxisFormatter = [[NSNumberFormatter alloc] init];
            leftAxisFormatter.maximumFractionDigits = 1;
            leftAxisFormatter.negativeSuffix = @"";
            leftAxisFormatter.positiveSuffix = @"";
            
            sipInsuranceReturnCell.chartView.leftAxis.enabled  = NO;
            sipInsuranceReturnCell.chartView.rightAxis.enabled = NO;
            sipInsuranceReturnCell.chartView.xAxis.enabled = NO;//Top
            sipInsuranceReturnCell.chartView.legend.enabled = NO;
        }
        // Setup Date
        int count = 3;
        int range = 100;
        NSMutableArray *yVals = [[NSMutableArray alloc] init];
        for (int i = 0; i < count; i++) {
            double val1 = (double)arc4random_uniform(range);
            double val2 = 100-val1;
            [yVals addObject:[[BarChartDataEntry alloc] initWithX:i yValues:@[@(val1), @(val2)]]];
        }
        
        BarChartDataSet *set1 = nil;
        if (sipInsuranceReturnCell.chartView.data.dataSetCount > 0) {
            set1 = (BarChartDataSet *)sipInsuranceReturnCell.chartView.data.dataSets[0];
            set1.values = yVals;
            [sipInsuranceReturnCell.chartView.data notifyDataChanged];
            [sipInsuranceReturnCell.chartView notifyDataSetChanged];
        } else {
            set1 = [[BarChartDataSet alloc] initWithValues:yVals label:@""];
            set1.drawIconsEnabled = NO;
            set1.colors = @[[UIColor colorWithHexString:@"6FB414"], [UIColor colorWithHexString:@"E3E3E3"]];
            set1.drawValuesEnabled = NO;
            NSMutableArray *dataSets = [[NSMutableArray alloc] init];
            [dataSets addObject:set1];
            
            BarChartData *data = [[BarChartData alloc] initWithDataSets:dataSets];
            [data setValueTextColor:UIColor.whiteColor];
            data.barWidth = 0.5;
            
            //returnCompareCell.chartView.fitBars = YES;
            sipInsuranceReturnCell.chartView.data = data;
        }
        [sipInsuranceReturnCell.chartView animateWithYAxisDuration:1];
        
        return sipInsuranceReturnCell;
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    
}

#pragma mark - Button Tap Events

- (IBAction)btn1Y_Tapped:(id)sender {
    [self setNormalBorderForAllButtonts];
    sipInsuranceReturnCell.btn1Y.layer.borderColor = [UIColor getAppColorBlue_2].CGColor;
    sipInsuranceReturnCell.btn1Y.layer.borderWidth = 2.0;
}

- (IBAction)btn3Y_Tapped:(id)sender {
    [self setNormalBorderForAllButtonts];
    sipInsuranceReturnCell.btn3Y.layer.borderColor = [UIColor getAppColorBlue_2].CGColor;
    sipInsuranceReturnCell.btn3Y.layer.borderWidth = 2.0;
}

- (IBAction)btn5Y_Tapped:(id)sender {
    [self setNormalBorderForAllButtonts];
    sipInsuranceReturnCell.btn5Y.layer.borderColor = [UIColor getAppColorBlue_2].CGColor;
    sipInsuranceReturnCell.btn5Y.layer.borderWidth = 2.0;
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnViewFunds_Tapped:(id)sender {
    SipInsuranceFundListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SipInsuranceFundListVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnHowWeCalculateThis_Tapped {
    HowCalculateSipInsuranceVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HowCalculateSipInsuranceVC"];
    [self presentViewController:destVC animated:YES completion:nil];
}

-(void)btnViewFAQs_Tapped {
    HelpQuestionsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpQuestionsVC"];
    destVC.isComeFromInsurance = YES;
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
