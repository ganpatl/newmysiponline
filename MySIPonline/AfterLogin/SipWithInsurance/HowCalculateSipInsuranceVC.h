//
//  HowCalculateSipInsuranceVC.h
//  MySIPonline
//
//  Created by Ganpat on 17/05/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HowCalculateSipInsuranceVC : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btn1Y;
@property (weak, nonatomic) IBOutlet UIButton *btn3Y;
@property (weak, nonatomic) IBOutlet UIButton *btn5Y;
@property (weak, nonatomic) IBOutlet UIButton *btnGotIt;
@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UIView *shadowView3;

@end

NS_ASSUME_NONNULL_END
