//
//  HowCalculateSipInsuranceVC.m
//  MySIPonline
//
//  Created by Ganpat on 17/05/19.
//

#import "HowCalculateSipInsuranceVC.h"

@interface HowCalculateSipInsuranceVC ()

@end

@implementation HowCalculateSipInsuranceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.btnGotIt.layer.cornerRadius = self.btnGotIt.frame.size.height / 2;
    [AppHelper addShadowOnView:self.shadowView1 Color:UIColor.lightGrayColor OffetSize:CGSizeZero Opacity:2.0 Radius:2.0];
    [AppHelper addShadowOnView:self.shadowView2 Color:UIColor.lightGrayColor OffetSize:CGSizeZero Opacity:2.0 Radius:2.0];
    [AppHelper addShadowOnView:self.shadowView3 Color:UIColor.lightGrayColor OffetSize:CGSizeZero Opacity:2.0 Radius:2.0];
    [self btn3Y_Tapped:nil];
}

-(void)setNormalBorderForAllButtonts {
    self.btn1Y.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.btn3Y.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.btn5Y.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.btn1Y.layer.borderWidth = 1.0;
    self.btn3Y.layer.borderWidth = 1.0;
    self.btn5Y.layer.borderWidth = 1.0;
}

#pragma mark - Button Tap Event

- (IBAction)btn1Y_Tapped:(id)sender {
    [self setNormalBorderForAllButtonts];
    self.btn1Y.layer.borderColor = [UIColor getAppColorBlue_2].CGColor;
    self.btn1Y.layer.borderWidth = 2.0;
}

- (IBAction)btn3Y_Tapped:(id)sender {
    [self setNormalBorderForAllButtonts];
    self.btn3Y.layer.borderColor = [UIColor getAppColorBlue_2].CGColor;
    self.btn3Y.layer.borderWidth = 2.0;
}

- (IBAction)btn5Y_Tapped:(id)sender {
    [self setNormalBorderForAllButtonts];
    self.btn5Y.layer.borderColor = [UIColor getAppColorBlue_2].CGColor;
    self.btn5Y.layer.borderWidth = 2.0;
}

- (IBAction)bnGotIt_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
