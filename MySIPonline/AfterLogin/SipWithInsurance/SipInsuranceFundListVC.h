//
//  SipInsuranceFundListVC.h
//  MySIPonline
//
//  Created by Ganpat on 17/05/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SipInsuranceFundListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblMainTitle;
@property (weak, nonatomic) IBOutlet UIButton *btn1Month;
@property (weak, nonatomic) IBOutlet UIButton *btn3Month;
@property (weak, nonatomic) IBOutlet UIButton *btn6Month;
@property (weak, nonatomic) IBOutlet UIButton *btn1Year;
@property (weak, nonatomic) IBOutlet UIButton *btn3Year;
@property (weak, nonatomic) IBOutlet UIButton *btn5Year;
@property (weak, nonatomic) IBOutlet UITableView *tblSIPinsureFundList;
@property (weak, nonatomic) IBOutlet UIButton *btnCartBadge;
@property (weak, nonatomic) IBOutlet UIButton *btnReliance;
@property (weak, nonatomic) IBOutlet UIButton *btnIcici;
@property (weak, nonatomic) IBOutlet UIButton *btnAbsl;

@end

NS_ASSUME_NONNULL_END
