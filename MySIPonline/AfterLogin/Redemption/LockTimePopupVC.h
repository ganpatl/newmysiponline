//
//  LockTimePopupVC.h
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface LockTimePopupVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *btnGotIt;
@property (weak, nonatomic) IBOutlet UIView *popupView;

@end

NS_ASSUME_NONNULL_END
