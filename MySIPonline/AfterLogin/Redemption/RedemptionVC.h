//
//  RedemptionVC.h
//  MySIPonline
//
//  Created by Ganpat on 21/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedemptionVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblRedemption;

@end

NS_ASSUME_NONNULL_END
