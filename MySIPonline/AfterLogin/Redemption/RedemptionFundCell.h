//
//  RedemptionFundCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedemptionFundCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet UILabel *lblFreeUnits;
@property (weak, nonatomic) IBOutlet UILabel *lblExitLoad;
@property (weak, nonatomic) IBOutlet UILabel *lblLTCG;
@property (weak, nonatomic) IBOutlet UILabel *lblSTCG;

@end

NS_ASSUME_NONNULL_END
