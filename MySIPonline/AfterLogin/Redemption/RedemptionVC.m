//
//  RedemptionVC.m
//  MySIPonline
//
//  Created by Ganpat on 21/02/19.
//

#import "RedemptionVC.h"
#import "RedemptionTypeVC.h"
#import "LockTimePopupVC.h"

// Cells
#import "RedemptionFundCell.h"

@interface RedemptionVC () <UITableViewDataSource> {
    RedemptionFundCell *redemptionFundCell;
}

@end

@implementation RedemptionVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblRedemption registerNib:[UINib nibWithNibName:@"RedemptionFundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"RedemptionFundCell"];
    self.tblRedemption.dataSource = self;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (redemptionFundCell == nil) {
        redemptionFundCell = [tableView dequeueReusableCellWithIdentifier:@"RedemptionFundCell"];
    }
    return redemptionFundCell;
}

#pragma mark - Button Tap Events

- (IBAction)bnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnContinue_Tapped:(id)sender {
    RedemptionTypeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RedemptionTypeVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    
    LockTimePopupVC *destVC2 = [self.storyboard instantiateViewControllerWithIdentifier:@"LockTimePopupVC"];
    [self presentViewController:destVC2 animated:YES completion:nil];
}


@end
