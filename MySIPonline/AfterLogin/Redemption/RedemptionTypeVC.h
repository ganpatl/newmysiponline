//
//  RedemptionTypeVC.h
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface RedemptionTypeVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblRedemptionType;

@end

NS_ASSUME_NONNULL_END
