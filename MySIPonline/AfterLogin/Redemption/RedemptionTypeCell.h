//
//  RedemptionTypeCell.h
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface RedemptionTypeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblUnit;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet RKTagsView *redemptionTypeTagsView;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;

@end

NS_ASSUME_NONNULL_END
