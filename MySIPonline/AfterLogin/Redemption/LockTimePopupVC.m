//
//  LockTimePopupVC.m
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import "LockTimePopupVC.h"

@interface LockTimePopupVC ()

@end

@implementation LockTimePopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.popupView.layer.cornerRadius = 4;
    self.btnGotIt.layer.cornerRadius = self.btnGotIt.frame.size.height / 2;
}

#pragma mark - Button Tap Events

- (IBAction)btnGotIt_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}


@end
