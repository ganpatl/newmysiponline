//
//  RedemptionTypeVC.m
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import "RedemptionTypeVC.h"
#import "RedemptionTypeCell.h"

@interface RedemptionTypeVC () <UITableViewDataSource, RKTagsViewDelegate> {
    RedemptionTypeCell *redemptionTypeCell;
}

@end

@implementation RedemptionTypeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblRedemptionType registerNib:[UINib nibWithNibName:@"RedemptionTypeCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"RedemptionTypeCell"];
    self.tblRedemptionType.dataSource = self;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (redemptionTypeCell == nil) {
        redemptionTypeCell = [tableView dequeueReusableCellWithIdentifier:@"RedemptionTypeCell"];
        // Setup RKTagsView
        [redemptionTypeCell.redemptionTypeTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
        redemptionTypeCell.redemptionTypeTagsView.tagButtonHeight = 30;
        redemptionTypeCell.redemptionTypeTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
        redemptionTypeCell.redemptionTypeTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
        redemptionTypeCell.redemptionTypeTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
        redemptionTypeCell.redemptionTypeTagsView.selectedTagTintColor = [UIColor whiteColor];
        redemptionTypeCell.redemptionTypeTagsView.editable = NO;
        redemptionTypeCell.redemptionTypeTagsView.allowsMultipleSelection = NO;
        redemptionTypeCell.redemptionTypeTagsView.lineSpacing = 8;
        redemptionTypeCell.redemptionTypeTagsView.interitemSpacing = 8;
        redemptionTypeCell.redemptionTypeTagsView.buttonCornerRadius = 3;
        redemptionTypeCell.redemptionTypeTagsView.delegate = self;
        redemptionTypeCell.redemptionTypeTagsView.scrollView.showsHorizontalScrollIndicator = NO;
        
        NSArray *aryType = @[@{@"name":@"Partial"}, @{@"name":@"Full"}];
        if (redemptionTypeCell.redemptionTypeTagsView.tags.count != aryType.count) {
            [redemptionTypeCell.redemptionTypeTagsView removeAllTags];
            for (NSDictionary *dic in aryType) {
                if ([AppHelper isNotNull:dic[@"name"]]) {
                    [redemptionTypeCell.redemptionTypeTagsView addTag:dic[@"name"]];
                }
            }
            // Set First item selected
            [redemptionTypeCell.redemptionTypeTagsView selectTagAtIndex:0];
        }
    }
    return redemptionTypeCell;
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (index == 0) {
        // Partial
        
    } else {
        // Full
        
    }
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)bnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendOTP_Tapped:(id)sender {
    
}
@end
