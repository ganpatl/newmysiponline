//
//  DashboardVC.m
//  MySIPonline
//
//  Created by Ganpat on 08/02/19.
//

#import "DashboardVC.h"
#import "TransactionListVC.h"
#import "FundExploreVC.h"

// Cells
#import "DashboardCell.h"
#import "InvestmentOptionCell.h"


@interface DashboardVC () <UITableViewDataSource, UITableViewDelegate> {
    DashboardCell *dashboardCell;
    //InvestmentOptionCell *investmentOptionCell;
    
    CAGradientLayer *theViewGradient;
    NSMutableArray *aryOtherCells;
}

@end

@implementation DashboardVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.backGradientView];
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"3A6ACB"] CGColor], (id)[[UIColor colorWithHexString:@"3780D1"] CGColor], (id)[[UIColor colorWithHexString:@"33A1DB"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblDashboard registerNib:[UINib nibWithNibName:@"DashboardCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DashboardCell"];
    [self.tblDashboard registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    aryOtherCells = @[
                      @{@"type":@"dashboard"},
                      @{@"type":@"inprocess", @"title":@"1 Transaction In process", @"desc":@"Dashboard will updated once transaction gets processed", @"image":@"info-error"},
                      @{@"type":@"viewtransaction", @"title":@"View Transaction", @"desc":@"6 Funds, 1 Portfilio", @"image":@"payment"},
                      @{@"type":@"upcomingsip", @"title":@"Upcoming SIPs", @"desc":@"You haven't started any SIP yet, Start SIP", @"image":@"investments"},
                      @{@"type":@"findmore", @"title":@"Find More Investment Funds", @"desc":@"dasds asdfasda asdas adasd adasd asdasd", @"image":@"personalizePortfolio"}
                      ].mutableCopy;
    self.tblDashboard.dataSource = self;
    self.tblDashboard.delegate = self;
}

-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  aryOtherCells.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (dashboardCell == nil) {
            dashboardCell = [tableView dequeueReusableCellWithIdentifier:@"DashboardCell"];
            dashboardCell.selectionStyle = UITableViewCellSelectionStyleNone;
            dashboardCell.progressView.layer.cornerRadius = 7.5;
            dashboardCell.progressView.clipsToBounds = YES;
        }
        return dashboardCell;
    } else if (indexPath.row > 0) {
        if (aryOtherCells.count > indexPath.row) {
            InvestmentOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
            NSDictionary *dict = aryOtherCells[indexPath.row];
            if ([dict[@"type"] isEqualToString:@"inprocess"]) {
                cell.shadowView.backgroundColor = [UIColor colorWithHexString:@"FFFDF1"];
            } else {
                cell.shadowView.backgroundColor = UIColor.whiteColor;
            }
            cell.lblTitle.text = dict[@"title"];
            cell.lblDesc.text = dict[@"desc"];
            cell.imgIcon.image = [UIImage imageNamed:dict[@"image"]];
            return cell;
        }
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row > 0) {
        if (aryOtherCells.count > indexPath.row) {
            NSDictionary *dict = aryOtherCells[indexPath.row];
            if ([dict[@"type"] isEqualToString:@"inprocess"]) {
                // In Process
                
            } else if ([dict[@"type"] isEqualToString:@"viewtransaction"]) {
                // View My Transaction
                TransactionListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionListVC"];
                [self.navigationController pushViewController:destVC animated:YES];
            
            } else if ([dict[@"type"] isEqualToString:@"upcomingsip"]) {
                // Upcoming Sip
                //TODO:: Add Upcoming sip page redirection
                
            } else if ([dict[@"type"] isEqualToString:@"findmore"]) {
                // Find More
                FundExploreVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundExploreVC"];
                [self.navigationController pushViewController:destVC animated:YES];
                
            }
        }
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
