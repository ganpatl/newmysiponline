//
//  DashboardCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestment;
@property (weak, nonatomic) IBOutlet UILabel *lblGainLoss;
@property (weak, nonatomic) IBOutlet UILabel *lblChangePer;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;
@property (weak, nonatomic) IBOutlet UILabel *lblEquity;
@property (weak, nonatomic) IBOutlet UILabel *lblDebt;
@property (weak, nonatomic) IBOutlet UIProgressView *progressView;

@end

NS_ASSUME_NONNULL_END
