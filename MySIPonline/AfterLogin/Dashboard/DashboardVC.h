//
//  DashboardVC.h
//  MySIPonline
//
//  Created by Ganpat on 08/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DashboardVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblDashboard;
@property (weak, nonatomic) IBOutlet UIView *backGradientView;

@end

NS_ASSUME_NONNULL_END
