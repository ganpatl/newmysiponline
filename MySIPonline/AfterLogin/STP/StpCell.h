//
//  StpCell.h
//  MySIPonline
//
//  Created by Ganpat on 23/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface StpCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet UILabel *lblFreeUnits;
@property (weak, nonatomic) IBOutlet UILabel *lblExitLoad;
@property (weak, nonatomic) IBOutlet UILabel *lblStpSchemeName;
@property (weak, nonatomic) IBOutlet UIButton *btnStpSchemeName;
@property (weak, nonatomic) IBOutlet UIButton *btnKnowMoreFund;
@property (weak, nonatomic) IBOutlet RKTagsView *dateTagsView;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet UITextField *tfNoOfInstalment;

@end

NS_ASSUME_NONNULL_END
