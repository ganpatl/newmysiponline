//
//  STPVC.m
//  MySIPonline
//
//  Created by Ganpat on 23/02/19.
//

#import "STPVC.h"
#import "RKTagsView.h"

// Cells
#import "StpCell.h"

@interface STPVC () <UITableViewDataSource, RKTagsViewDelegate> {
    StpCell *stpCell;
    NSArray *arySipDates;
}

@end

@implementation STPVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    arySipDates = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30"];
    
    [self.tblSTP registerNib:[UINib nibWithNibName:@"StpCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"StpCell"];
    self.tblSTP.dataSource = self;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (stpCell == nil) {
        stpCell = [tableView dequeueReusableCellWithIdentifier:@"StpCell"];        
        // Setup SIP Date RKTagsView
        stpCell.dateTagsView.tag = 11;
        [stpCell.dateTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
        stpCell.dateTagsView.tagButtonHeight = 30;
        stpCell.dateTagsView.tagBackgroundColor = UIColor.whiteColor;
        stpCell.dateTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
        stpCell.dateTagsView.tintColor = [UIColor getAppColorDarkGrayText];
        stpCell.dateTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
        stpCell.dateTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
        stpCell.dateTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
        stpCell.dateTagsView.editable = NO;
        stpCell.dateTagsView.allowsMultipleSelection = NO;
        stpCell.dateTagsView.lineSpacing = 8;
        stpCell.dateTagsView.interitemSpacing = 10;
        stpCell.dateTagsView.buttonCornerRadius = 4;
        stpCell.dateTagsView.delegate = self;
        stpCell.dateTagsView.scrollView.showsHorizontalScrollIndicator = NO;
        if (stpCell.dateTagsView.tags.count != self->arySipDates.count) {
            [stpCell.dateTagsView removeAllTags];
            for (NSString *tag in self->arySipDates) {
                [stpCell.dateTagsView addTag:tag];
            }
            // Set First item selected
            [stpCell.dateTagsView selectTagAtIndex:0];
        }
    }
    return stpCell;
    
    return [UITableViewCell new];
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (index == 0) {
        // Partial
        
    } else {
        // Full
        
    }
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)bnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendOTP_Tapped:(id)sender {
    
}

@end
