//
//  SupportDeskVC.h
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SupportDeskVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblSupportDesk;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *noTicketView;

@end

NS_ASSUME_NONNULL_END
