//
//  ChatDateCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatDateCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblDate;

@end

NS_ASSUME_NONNULL_END
