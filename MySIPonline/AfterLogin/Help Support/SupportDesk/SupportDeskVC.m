//
//  SupportDeskVC.m
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import "SupportDeskVC.h"
#import "TicketListCell.h"
#import "SupportChatVC.h"

@interface SupportDeskVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryTickets;
}

@end

@implementation SupportDeskVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    [self.tblSupportDesk registerNib:[UINib nibWithNibName:@"TicketListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TicketListCell"];
    self.tblSupportDesk.dataSource = self;
    self.tblSupportDesk.delegate = self;
    self.tblSupportDesk.tableFooterView = [UIView new];
    self.tblSupportDesk.estimatedRowHeight = 110;
    
    aryTickets = @[@{@"status":@"Open", @"ticket_no":@"584759", @"date":@"12 May 2018", @"query":@"Dear sir, your KYC update within 3-7 days in Lorem Ipsum is simply dummy text of the printing"},
                   @{@"status":@"Open", @"ticket_no":@"584759", @"date":@"12 May 2018", @"query":@"Dear sir, your KYC update within 3-7 days in Lorem Ipsum is simply dummy text of the printing this is a testing text added for multiline test"}];
    [self.tblSupportDesk reloadData];
    
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryTickets.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryTickets.count > indexPath.row) {
        TicketListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TicketListCell"];
        cell.lblStatus.text = aryTickets[indexPath.row][@"status"];
        cell.lblQuery.text = aryTickets[indexPath.row][@"query"];
        return cell;
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    SupportChatVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportChatVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnCreateNewTicket_Tapped:(id)sender {
    
}

@end
