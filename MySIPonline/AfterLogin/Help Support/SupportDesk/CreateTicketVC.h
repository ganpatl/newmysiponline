//
//  CreateTicketVC.h
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CreateTicketVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UITextField *tfIssueType;
@property (weak, nonatomic) IBOutlet UITextField *tfIssue;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblAttachment;

@end

NS_ASSUME_NONNULL_END
