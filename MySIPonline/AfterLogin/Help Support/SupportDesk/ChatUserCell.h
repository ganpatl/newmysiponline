//
//  ChatUserCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatUserCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bubleView;
@property (weak, nonatomic) IBOutlet SubHeaderLabel *lblMessage;

@end

NS_ASSUME_NONNULL_END
