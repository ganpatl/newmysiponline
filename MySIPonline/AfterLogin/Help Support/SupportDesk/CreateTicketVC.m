//
//  CreateTicketVC.m
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import "CreateTicketVC.h"

@interface CreateTicketVC ()

@end

@implementation CreateTicketVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAttachment_Tapped:(id)sender {
    
}

- (IBAction)btnSubmit_Tapped:(id)sender {
    
}

@end
