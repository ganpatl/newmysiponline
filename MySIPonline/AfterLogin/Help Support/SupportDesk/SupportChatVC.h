//
//  SupportChatVC.h
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SupportChatVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblTicketStatus;
@property (weak, nonatomic) IBOutlet UITableView *tblSupportChat;
@property (weak, nonatomic) IBOutlet UICollectionView *cvAttachment;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *attachmentHeightConstraint;

@end

NS_ASSUME_NONNULL_END
