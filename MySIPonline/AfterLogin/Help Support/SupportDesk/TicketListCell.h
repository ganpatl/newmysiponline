//
//  TicketListCell.h
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TicketListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTicketNumber;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblQuery;
@property (weak, nonatomic) IBOutlet UIButton *btnTicket;

@end

NS_ASSUME_NONNULL_END
