//
//  SupportChatVC.m
//  MySIPonline
//
//  Created by Ganpat on 10/12/18.
//

#import "SupportChatVC.h"
#import "ChatTeamCell.h"
#import "ChatDateCell.h"
#import "ChatUserCell.h"

@interface SupportChatVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryChat;
    NSMutableArray *aryAttachment;
}

@end

@implementation SupportChatVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.attachmentHeightConstraint.constant = 0;
    [AppHelper addTopShadowOnView:self.shadowView];
    
    [self.tblSupportChat registerNib:[UINib nibWithNibName:@"ChatTeamCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ChatTeamCell"];
    [self.tblSupportChat registerNib:[UINib nibWithNibName:@"ChatDateCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ChatDateCell"];
    [self.tblSupportChat registerNib:[UINib nibWithNibName:@"ChatUserCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ChatUserCell"];
    
    self.tblSupportChat.dataSource = self;
    self.tblSupportChat.delegate = self;
    self.tblSupportChat.tableFooterView = [UIView new];
    self.tblSupportChat.estimatedRowHeight = 110;
    
    aryChat = @[@{@"type":@"team", @"msg":@"Gastroenteritis means inflammation of stomach as well as the gastrointestinal tract."},
                @{@"type":@"date", @"msg":@"25 May 18"},
                @{@"type":@"user", @"msg":@"I think it`s COOL place!"},
                @{@"type":@"date", @"msg":@"Today"},
                @{@"type":@"user", @"msg":@"Hi man! I find wery good flat near Kotti. Let`s roll"}];
    [self.tblSupportChat reloadData];
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryChat.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryChat.count > indexPath.row) {
        if ([aryChat[indexPath.row][@"type"] isEqualToString:@"team"]) {
            ChatTeamCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatTeamCell"];
            cell.lblMessage.text = aryChat[indexPath.row][@"msg"];
            return cell;
        } else if ([aryChat[indexPath.row][@"type"] isEqualToString:@"date"]) {
            ChatDateCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatDateCell"];
            cell.lblDate.text = aryChat[indexPath.row][@"msg"];
            return cell;
        } else {
            ChatUserCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ChatUserCell"];
            cell.lblMessage.text = aryChat[indexPath.row][@"msg"];
            return cell;
        }
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAttachment_Tapped:(id)sender {
    
}

- (IBAction)btnSendMessage_Tapped:(id)sender {
    
}

@end
