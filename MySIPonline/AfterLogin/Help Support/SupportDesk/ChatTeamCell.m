//
//  ChatTeamCell.m
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import "ChatTeamCell.h"

@implementation ChatTeamCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.bubleView.layer.cornerRadius = 12;
    self.bubleView.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
