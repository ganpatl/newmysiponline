//
//  ChatTeamCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ChatTeamCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *bubleView;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblTeam;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblMessage;


@end

NS_ASSUME_NONNULL_END
