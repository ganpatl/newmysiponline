//
//  HelpQuestionCell.h
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpQuestionCell : UITableViewCell

@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblQuestion;

@end

NS_ASSUME_NONNULL_END
