//
//  HelpVC.m
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import "HelpVC.h"
#import "HelpCell.h"
#import "GetStartedTitleCell.h"
#import "HelpQuestionsVC.h"
#import "SupportDeskVC.h"
#import "CreateTicketVC.h"

@interface HelpVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryHelp;
}

@end

@implementation HelpVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    [AppHelper addShadowOnView:self.menuView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
    
    [self.tblHelp registerNib:[UINib nibWithNibName:@"GetStartedTitleCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GetStartedTitleCell"];
    [self.tblHelp registerNib:[UINib nibWithNibName:@"HelpCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"HelpCell"];
    self.tblHelp.dataSource = self;
    self.tblHelp.delegate = self;
    
    aryHelp = @[@YES,
                @{@"image":@"investments", @"title":@"Mutual Fund Investments", @"desc":@"If you have any issues while buying, selling, withdrawing mutual funds."},
                @{@"image":@"investments", @"title":@"Smart Savings", @"desc":@"issue related to Buy, withdraw, switch and Additional Purchase etc"},
                @{@"image":@"investments", @"title":@"Bank Mandate", @"desc":@"issue related to Buy, withdraw, switch and Additional Purchase etc"},
                @{@"image":@"investments", @"title":@"Payment", @"desc":@"issue related to Buy, withdraw, switch and Additional Purchase etc"},
                @{@"image":@"investments", @"title":@"Portfolio Related", @"desc":@"issue related to Buy, withdraw, switch and Additional Purchase etc"},
                @{@"image":@"investments", @"title":@"Account Statement", @"desc":@"issue related to Buy, withdraw, switch and Additional Purchase etc"}];
    [self.tblHelp reloadData];    
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryHelp.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryHelp.count > indexPath.row) {
        if ([aryHelp[indexPath.row] isKindOfClass:[NSNumber class]]) {
            GetStartedTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"GetStartedTitleCell"];
            cell.lblTitle.text = @"Please Select Your Query";
            return cell;
        } else {
            HelpCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HelpCell"];
            NSDictionary *dicHelp = aryHelp[indexPath.row];
            // Icon
            cell.imgHelp.image = [UIImage imageNamed:dicHelp[@"image"]];
            
            // Title
            cell.lblTitle.text = dicHelp[@"title"];
            
            // Desc
            cell.lblDesc.text = dicHelp[@"desc"];
            return cell;
        }
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (aryHelp.count > indexPath.row) {
        if ([aryHelp[indexPath.row] isKindOfClass:[NSNumber class]]) {
            return 36;
        } else {
            return UITableViewAutomaticDimension;
        }
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HelpQuestionsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpQuestionsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMoreOption_Tapped:(id)sender {
    self.menuView.hidden = NO;
}

- (IBAction)btnNeedMoreHelp_Tapped:(id)sender {
    
}

- (IBAction)btnViewTicket_Tapped:(id)sender {
    SupportDeskVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportDeskVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    self.menuView.hidden = YES;
}

- (IBAction)btnCreateNewTicket_Tapped:(id)sender {
    CreateTicketVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateTicketVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    self.menuView.hidden = YES;
}

#pragma mark - Touches Event

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
//    UITouch *touch = [touches allObjects].firstObject;
//    if (touch.view != self.menuView) {
//        //self.menuView.hidden = YES;
//    }
    self.menuView.hidden = YES;
}

@end
