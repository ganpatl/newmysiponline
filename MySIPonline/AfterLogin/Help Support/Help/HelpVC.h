//
//  HelpVC.h
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (strong, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UITableView *tblHelp;

@end

NS_ASSUME_NONNULL_END
