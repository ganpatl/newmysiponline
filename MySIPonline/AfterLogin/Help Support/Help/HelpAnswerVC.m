//
//  HelpAnswerVC.m
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import "HelpAnswerVC.h"
#import "HelpAnswerCell.h"
#import "SupportDeskVC.h"
#import "CreateTicketVC.h"

@interface HelpAnswerVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryHelpQuestions;
}

@end

@implementation HelpAnswerVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    [AppHelper addShadowOnView:self.menuView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
    
    [self.tblHelpAnswer registerNib:[UINib nibWithNibName:@"HelpAnswerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"HelpAnswerCell"];
    self.tblHelpAnswer.dataSource = self;
    self.tblHelpAnswer.delegate = self;
    self.tblHelpAnswer.tableFooterView = [UIView new];
    
    aryHelpQuestions = @[@{@"q":@"What is E-Mandate ?", @"answer":@"Debit Mandate registered by means of an electronic channel is known as e-mandate. It is very similar to an NACH mandate. It uses an electronic validation instead of a physical signature and are offered directly by individual banks. This Facility is Available with Axis Bank (Net Banking), ICICI Bank & HDFC Bank (Debit Card)."}];
    [self.tblHelpAnswer reloadData];
    
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryHelpQuestions.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryHelpQuestions.count > indexPath.row) {
        HelpAnswerCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HelpAnswerCell"];
        cell.lblQuestion.text = aryHelpQuestions[indexPath.row][@"q"];
        return cell;        
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}


#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMoreOption_Tapped:(id)sender {
    self.menuView.hidden = NO;
}

- (IBAction)btnViewTicket_Tapped:(id)sender {
    SupportDeskVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportDeskVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    self.menuView.hidden = YES;
}

- (IBAction)btnCreateNewTicket_Tapped:(id)sender {
    CreateTicketVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateTicketVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    self.menuView.hidden = YES;
}

#pragma mark - Touches Event

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //    UITouch *touch = [touches allObjects].firstObject;
    //    if (touch.view != self.menuView) {
    //        //self.menuView.hidden = YES;
    //    }
    self.menuView.hidden = YES;
}


@end
