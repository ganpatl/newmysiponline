//
//  HelpQuestionsVC.m
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import "HelpQuestionsVC.h"
#import "HelpQuestionTitleCell.h"
#import "HelpQuestionCell.h"
#import "HelpAnswerVC.h"
#import "SupportDeskVC.h"
#import "CreateTicketVC.h"

@interface HelpQuestionsVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryHelpQuestions;
}

@end

@implementation HelpQuestionsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.shadowView];
    [AppHelper addShadowOnView:self.menuView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:2];
    
    [self.tblHelpQuestion registerNib:[UINib nibWithNibName:@"HelpQuestionTitleCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"HelpQuestionTitleCell"];
    [self.tblHelpQuestion registerNib:[UINib nibWithNibName:@"HelpQuestionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"HelpQuestionCell"];
    self.tblHelpQuestion.dataSource = self;
    self.tblHelpQuestion.delegate = self;
    
    NSString *strImage = @"investments";
    NSString *strTitle = @"Mutual Fund Investments";
    NSString *strDesc = @"If you have any issues while buying, selling, withdrawing mutual funds.";
    if (self.isComeFromInsurance) {
        [self btnReliance_Tapped:nil];
        self.btnReliance.layer.cornerRadius = self.btnReliance.frame.size.height / 2;
        self.btnReliance.layer.masksToBounds = YES;
        self.btnIcici.layer.cornerRadius = self.btnIcici.frame.size.height / 2;
        self.btnIcici.layer.masksToBounds = YES;
        self.btnAbsl.layer.cornerRadius = self.btnAbsl.frame.size.height / 2;
        self.btnAbsl.layer.masksToBounds = YES;
        self.insuranceButtonsView.hidden = NO;
        //strImage = @"";
        strTitle = @"SIP insurance";
        strDesc = @"issue related to Buy, withdraw, switch and Additional Purchase etc";
    }
    aryHelpQuestions = @[@{@"image":strImage, @"title":strTitle, @"desc":strDesc},
                @{@"q":@"What is Amount based SIP?"},
                @{@"q":@"When can I withdraw the amount if I have invested in a locking period?"},
                @{@"q":@"How do SIP works?"},
                @{@"q":@"Can I miss an SIP?"},
                @{@"q":@"What are the benefits of SIP?"},
                @{@"q":@"In which mutual fund should I start SIP?"},
                @{@"q":@"Can I miss an SIP?"},
                @{@"q":@"Can I miss an SIP?"}];
    [self.tblHelpQuestion reloadData];
}


#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryHelpQuestions.count;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (aryHelpQuestions.count > indexPath.row) {
        if ([AppHelper isNotNull:aryHelpQuestions[indexPath.row][@"image"]]) {
            HelpQuestionTitleCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HelpQuestionTitleCell"];
            cell.imgHelp.image = [UIImage imageNamed:aryHelpQuestions[indexPath.row][@"image"]];
            cell.lblTitle.text = aryHelpQuestions[indexPath.row][@"title"];
            cell.lblDesc.text = aryHelpQuestions[indexPath.row][@"desc"];
            return cell;
        } else {
            HelpQuestionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"HelpQuestionCell"];
            cell.lblQuestion.text = aryHelpQuestions[indexPath.row][@"q"];
            return cell;
        }
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    HelpAnswerVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"HelpAnswerVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMoreOption_Tapped:(id)sender {
    self.menuView.hidden = NO;
}

- (IBAction)btnNeedMoreHelp_Tapped:(id)sender {
    
}

- (IBAction)btnViewTicket_Tapped:(id)sender {
    SupportDeskVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SupportDeskVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    self.menuView.hidden = YES;
}

- (IBAction)btnCreateNewTicket_Tapped:(id)sender {
    CreateTicketVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateTicketVC"];
    [self.navigationController pushViewController:destVC animated:YES];
    self.menuView.hidden = YES;
}

- (IBAction)btnReliance_Tapped:(id)sender {
    [self.btnReliance setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.btnIcici setTitleColor:UIColor.getAppColorTitleText forState:UIControlStateNormal];
    [self.btnAbsl setTitleColor:UIColor.getAppColorTitleText forState:UIControlStateNormal];
    [self.btnReliance setBackgroundColor:[UIColor colorWithHexString:@"4885ED"]];
    [self.btnIcici setBackgroundColor:UIColor.getAppColorBorder];
    [self.btnAbsl setBackgroundColor:UIColor.getAppColorBorder];
}

- (IBAction)btnIcici_Tapped:(id)sender {
    [self.btnReliance setTitleColor:UIColor.getAppColorTitleText forState:UIControlStateNormal];
    [self.btnIcici setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.btnAbsl setTitleColor:UIColor.getAppColorTitleText forState:UIControlStateNormal];
    [self.btnReliance setBackgroundColor:UIColor.getAppColorBorder];
    [self.btnIcici setBackgroundColor:[UIColor colorWithHexString:@"4885ED"]];
    [self.btnAbsl setBackgroundColor:UIColor.getAppColorBorder];
}

- (IBAction)btnAbsl_Tapped:(id)sender {
    [self.btnReliance setTitleColor:UIColor.getAppColorTitleText forState:UIControlStateNormal];
    [self.btnIcici setTitleColor:UIColor.getAppColorTitleText forState:UIControlStateNormal];
    [self.btnAbsl setTitleColor:UIColor.whiteColor forState:UIControlStateNormal];
    [self.btnReliance setBackgroundColor:UIColor.getAppColorBorder];
    [self.btnIcici setBackgroundColor:UIColor.getAppColorBorder];
    [self.btnAbsl setBackgroundColor:[UIColor colorWithHexString:@"4885ED"]];
}
#pragma mark - Touches Event

-(void)touchesBegan:(NSSet<UITouch *> *)touches withEvent:(UIEvent *)event {
    //    UITouch *touch = [touches allObjects].firstObject;
    //    if (touch.view != self.menuView) {
    //        //self.menuView.hidden = YES;
    //    }
    self.menuView.hidden = YES;
}


@end
