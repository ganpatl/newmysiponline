//
//  HelpAnswerVC.h
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpAnswerVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblHelpAnswer;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@end

NS_ASSUME_NONNULL_END
