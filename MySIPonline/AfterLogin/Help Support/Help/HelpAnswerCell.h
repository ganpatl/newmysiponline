//
//  HelpAnswerCell.h
//  MySIPonline
//
//  Created by Ganpat on 05/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpAnswerCell : UITableViewCell

@property (weak, nonatomic) IBOutlet Body2MenuLabel *lblQuestion;
@property (weak, nonatomic) IBOutlet Body1Label *lblAnswer;
@property (weak, nonatomic) IBOutlet UIButton *btnHelpful;
@property (weak, nonatomic) IBOutlet UIButton *btnNotHelpful;

@end

NS_ASSUME_NONNULL_END
