//
//  HelpQuestionsVC.h
//  MySIPonline
//
//  Created by Ganpat on 01/12/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface HelpQuestionsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIView *menuView;
@property (weak, nonatomic) IBOutlet UITableView *tblHelpQuestion;
@property (weak, nonatomic) IBOutlet UIView *insuranceButtonsView;
@property (weak, nonatomic) IBOutlet UIButton *btnReliance;
@property (weak, nonatomic) IBOutlet UIButton *btnIcici;
@property (weak, nonatomic) IBOutlet UIButton *btnAbsl;

@property BOOL isComeFromInsurance;

@end

NS_ASSUME_NONNULL_END
