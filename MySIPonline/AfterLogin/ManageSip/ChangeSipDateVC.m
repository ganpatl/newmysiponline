//
//  ChangeSipDateVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//

#import "ChangeSipDateVC.h"

@interface ChangeSipDateVC () <RKTagsViewDelegate> {    
    NSArray *arySipDates;
}

@end

@implementation ChangeSipDateVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    
    arySipDates = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30"];
    
    // Setup SIP Date RKTagsView
    self.sipDateTagsView.tag = 11;
    [self.sipDateTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.sipDateTagsView.tagButtonHeight = 30;
    self.sipDateTagsView.tagBackgroundColor = UIColor.whiteColor;
    self.sipDateTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
    self.sipDateTagsView.tintColor = [UIColor getAppColorDarkGrayText];
    self.sipDateTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
    self.sipDateTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
    self.sipDateTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
    self.sipDateTagsView.editable = NO;
    self.sipDateTagsView.allowsMultipleSelection = NO;
    self.sipDateTagsView.lineSpacing = 8;
    self.sipDateTagsView.interitemSpacing = 10;
    self.sipDateTagsView.buttonCornerRadius = 4;
    self.sipDateTagsView.delegate = self;
    self.sipDateTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    if (self.sipDateTagsView.tags.count != self->arySipDates.count) {
        [self.sipDateTagsView removeAllTags];
        for (NSString *tag in self->arySipDates) {
            [self.sipDateTagsView addTag:tag];
        }
        // Set First item selected
        [self.sipDateTagsView selectTagAtIndex:0];
    }
    
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendOtp_Tapped:(id)sender {
    
}

@end
