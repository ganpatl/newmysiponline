//
//  ManageSipVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ManageSipVC : UIViewController

@property (weak, nonatomic) IBOutlet RKTagsView *sipTagsView;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblManageSip;

@end

NS_ASSUME_NONNULL_END
