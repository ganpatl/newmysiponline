//
//  ChangeSipDateVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface ChangeSipDateVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UIButton *btnCurrentSipDate;
@property (weak, nonatomic) IBOutlet RKTagsView *sipDateTagsView;


@end

NS_ASSUME_NONNULL_END
