//
//  ManageSipVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//

#import "ManageSipVC.h"
#import "NIDropDown.h"
#import "ChangeSipDateVC.h"
#import "IncreaseSipVC.h"

// Cell
#import "ActiveSipCell.h"

@interface ManageSipVC () <UITableViewDataSource, UITableViewDelegate, RKTagsViewDelegate, NIDropDownDelegate> {
    NSArray *aryActiveSip;
    NSArray *aryOptions;    
    NIDropDown *dropDownOptions;
    BOOL isStopSip;
}

@end

@implementation ManageSipVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    isStopSip = NO;
    aryOptions = @[@"Increase SIP", @"Change SIP date", @"SKIP Next SIP"];
    
    // Setup RKTagsView
    [self.sipTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.sipTagsView.tagButtonHeight = 30;
    self.sipTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    self.sipTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
    self.sipTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
    self.sipTagsView.selectedTagTintColor = [UIColor whiteColor];
    self.sipTagsView.editable = NO;
    self.sipTagsView.allowsMultipleSelection = NO;
    self.sipTagsView.lineSpacing = 8;
    self.sipTagsView.interitemSpacing = 8;
    self.sipTagsView.buttonCornerRadius = 14;
    self.sipTagsView.delegate = self;
    self.sipTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    
    NSArray *arySip = @[@{@"name":@"Active SIPs"}, @{@"name":@"Stop SIP"}];
    if (self.sipTagsView.tags.count != arySip.count) {
        [self.sipTagsView removeAllTags];
        for (NSDictionary *dic in arySip) {
            if ([AppHelper isNotNull:dic[@"name"]]) {
                [self.sipTagsView addTag:dic[@"name"]];
            }
        }
        // Set First item selected
        [self.sipTagsView selectTagAtIndex:0];
    }
    
    // TableView setup
    [self.tblManageSip registerNib:[UINib nibWithNibName:@"ActiveSipCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ActiveSipCell"];
    self.tblManageSip.dataSource = self;
    
}

#pragma mark - TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (isStopSip) {
        return 5;
    } else {
        return 5;//aryActiveSip.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ActiveSipCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ActiveSipCell"];
    if (isStopSip) {
        cell.buttonRestartHeightConstraint.constant = 36;
        cell.btnRestart.hidden = NO;
        cell.btnMoreOption.hidden = YES;
    } else {
        [cell.btnMoreOption removeTarget:self action:@selector(btnMoreOption_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMoreOption addTarget:self action:@selector(btnMoreOption_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.buttonRestartHeightConstraint.constant = 0;
        cell.btnRestart.hidden = YES;
        cell.btnMoreOption.hidden = NO;
    }
    return cell;
}


#pragma mark - RK Tags View Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (index == 0) {
        // Active SIP
        isStopSip = NO;
        
    } else if (index == 1) {
        // Stop SIP
        isStopSip = YES;
    }
    [self.tblManageSip reloadData];
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}


#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnMoreOption_Tapped:(UIButton*)sender {
    if (dropDownOptions == nil) {
        CGFloat height = 90;
        [self hideAllDropDowns];
        CGRect frame = [sender convertRect:sender.bounds toView:self.view];
        UIButton *btn = [[UIButton alloc] initWithFrame:frame];
        dropDownOptions = [[NIDropDown alloc] showDropDown:btn Label:[UILabel new] Height:&height Array:aryOptions ImageArray:nil Direction:@"down" InView:self.view];
        dropDownOptions.delegate = self;
        dropDownOptions.tag = sender.tag;
        self.tblManageSip.scrollEnabled = NO;
    } else {
        [dropDownOptions hideDropDown:sender InView:self.view];
        dropDownOptions = nil;
        self.tblManageSip.scrollEnabled = YES;
    }
}

#pragma mark - Drop Down Methods

-(void)hideAllDropDowns {
    if (dropDownOptions) {
        [dropDownOptions hideDropDown];
        dropDownOptions = nil;
    }
}

-(void)niDropDownDelegateMethod:(NIDropDown *)sender {
    if (sender == dropDownOptions) {
        NSString *strOption = [NSString stringWithFormat:@"%@",sender.selectedDictionary];
        NSLog(@"%@ : %ld", sender.selectedDictionary, (long)sender.tag);
        dropDownOptions = nil;
        
        if ([strOption isEqualToString:aryOptions[0]]) {
            // Increase SIP
            IncreaseSipVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"IncreaseSipVC"];
            [self.navigationController pushViewController:destVC animated:YES];
            
        } else if ([strOption isEqualToString:aryOptions[1]]) {
            // Change SIP Date
            ChangeSipDateVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ChangeSipDateVC"];
            [self.navigationController pushViewController:destVC animated:YES];
            
        } else if ([strOption isEqualToString:aryOptions[2]]) {
            // SKIP Next SIP
            
        }
    }
    self.tblManageSip.scrollEnabled = YES;
}

@end
