//
//  ActiveSipCell.h
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ActiveSipCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreOption;
@property (weak, nonatomic) IBOutlet UIButton *btnRestart;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *buttonRestartHeightConstraint;

@end

NS_ASSUME_NONNULL_END
