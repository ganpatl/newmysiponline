//
//  IncreaseSipVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//

#import "IncreaseSipVC.h"

@interface IncreaseSipVC () <RKTagsViewDelegate> {
    NSArray *aryAmountSuggested;
}

@end

@implementation IncreaseSipVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    
    aryAmountSuggested = @[@"1000", @"1500", @"2000"];
    
    // Setup Amount RKTagsView
    self.amountTagsView.tag = 10;
    [self.amountTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.amountTagsView.tagButtonHeight = 30;
    self.amountTagsView.tagBackgroundColor = UIColor.whiteColor;
    self.amountTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
    self.amountTagsView.tintColor = [UIColor getAppColorDarkGrayText];
    self.amountTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
    self.amountTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
    self.amountTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
    self.amountTagsView.editable = NO;
    self.amountTagsView.allowsMultipleSelection = NO;
    self.amountTagsView.lineSpacing = 8;
    self.amountTagsView.interitemSpacing = 10;
    self.amountTagsView.buttonCornerRadius = 4;
    self.amountTagsView.delegate = self;
    
    self.amountTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    if (self.amountTagsView.tags.count != self->aryAmountSuggested.count) {
        [self.amountTagsView removeAllTags];
        for (NSString *tag in self->aryAmountSuggested) {
            [self.amountTagsView addTag:[NSString stringWithFormat:@"₹ %@", tag]];
        }
        // Set First item selected
        [self.amountTagsView selectTagAtIndex:0];
    }
    
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    
    self.tfAmount.text = aryAmountSuggested[index];
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendOtp_Tapped:(id)sender {
    
}

@end
