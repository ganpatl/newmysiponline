//
//  SmartSavingIntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/03/19.
//

#import "SmartSavingIntroVC.h"
#import "SmartSavingKnowMoreVC.h"

@interface SmartSavingIntroVC ()

@end

@implementation SmartSavingIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)openKnowMoreScreen {
    SmartSavingKnowMoreVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SmartSavingKnowMoreVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnKnowMore_Tapped:(id)sender {
    [self openKnowMoreScreen];
}

- (IBAction)btnContinue_Tapped:(id)sender {
    [self openKnowMoreScreen];
}

@end
