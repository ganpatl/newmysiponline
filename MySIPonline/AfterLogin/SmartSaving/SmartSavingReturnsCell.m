//
//  SmartSavingReturnsCell.m
//  MySIPonline
//
//  Created by Ganpat on 14/05/19.
//

#import "SmartSavingReturnsCell.h"

@implementation SmartSavingReturnsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    [AppHelper addShadowOnView:self.shadowView1 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 0) Opacity:1.0 Radius:2.0];
    [AppHelper addShadowOnView:self.shadowView2 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 0) Opacity:1.0 Radius:2.0];
    self.lblVs.layer.cornerRadius = self.lblVs.frame.size.height / 2;
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
