//
//  AddMoneyFundsCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddMoneyFundsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UICollectionView *cvFunds;

@end

NS_ASSUME_NONNULL_END
