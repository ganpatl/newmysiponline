//
//  AddMoneyVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import "AddMoneyVC.h"
#import "InvestmentThanksVC.h"

// Cells
#import "AddMoneyFundsCell.h"
#import "AddMoneyInFundCollectionCell.h"
#import "InvestmentCell.h"

@interface AddMoneyVC () <UITableViewDataSource, UITableViewDelegate, UICollectionViewDataSource, UICollectionViewDelegate, RKTagsViewDelegate> {
    AddMoneyFundsCell *addMoneyFundsCell;
    InvestmentCell *investmentCell;
    NSMutableArray *aryFunds;
    NSArray *aryAmountSuggested;
    NSArray *arySipDates;
    NSArray *aryInvestor;
}

@end

@implementation AddMoneyVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblAddMoney registerNib:[UINib nibWithNibName:@"AddMoneyFundsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AddMoneyFundsCell"];
    [self.tblAddMoney registerNib:[UINib nibWithNibName:@"InvestmentCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentCell"];
    
    aryFunds = @[
                 @{@"selected":@"1", @"scheme_name":@"Reliance Liquid Fund (G)", @"image":@"", @"amount":@"₹ 25,000"},
                 @{@"selected":@"0", @"scheme_name":@"Reliance Liquid Fund (D) Reliance Liquid Fund (D) Reliance Liquid Fund (D)", @"image":@"", @"amount":@"₹ 15,000"},
                 @{@"selected":@"0", @"scheme_name":@"Reliance", @"image":@"", @"amount":@"₹ 35,000"}
                ].mutableCopy;
    aryAmountSuggested = @[@"1000", @"1500", @"2000"];
    arySipDates = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30"];
    aryInvestor = @[@"Sonu", @"Praveen"];
    
    self.tblAddMoney.dataSource = self;
    self.tblAddMoney.delegate = self;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (addMoneyFundsCell == nil) {
            addMoneyFundsCell = [tableView dequeueReusableCellWithIdentifier:@"AddMoneyFundsCell"];
            addMoneyFundsCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [addMoneyFundsCell.cvFunds registerNib:[UINib nibWithNibName:@"AddMoneyInFundCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"AddMoneyInFundCollectionCell"];
            addMoneyFundsCell.cvFunds.dataSource = self;
            addMoneyFundsCell.cvFunds.delegate = self;
            [addMoneyFundsCell.cvFunds reloadData];
        }
        return addMoneyFundsCell;
        
    } else if (indexPath.row == 1) {
        if (investmentCell == nil) {
            investmentCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentCell"];
            
            [investmentCell setInvestmentModeHidden:YES];
            [investmentCell setSipDateViewHidden:YES];
            
            // Setup Amount RKTagsView
            investmentCell.amountTagsView.tag = 10;
            [investmentCell.amountTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investmentCell.amountTagsView.tagButtonHeight = 30;
            investmentCell.amountTagsView.tagBackgroundColor = UIColor.whiteColor;
            investmentCell.amountTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            investmentCell.amountTagsView.tintColor = [UIColor getAppColorDarkGrayText];
            investmentCell.amountTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            investmentCell.amountTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            investmentCell.amountTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            investmentCell.amountTagsView.editable = NO;
            investmentCell.amountTagsView.allowsMultipleSelection = NO;
            investmentCell.amountTagsView.lineSpacing = 8;
            investmentCell.amountTagsView.interitemSpacing = 10;
            investmentCell.amountTagsView.buttonCornerRadius = 4;
            investmentCell.amountTagsView.delegate = self;
            
            investmentCell.amountTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (investmentCell.amountTagsView.tags.count != self->aryAmountSuggested.count) {
                [investmentCell.amountTagsView removeAllTags];
                for (NSString *tag in self->aryAmountSuggested) {
                    [investmentCell.amountTagsView addTag:[NSString stringWithFormat:@"₹ %@", tag]];
                }
                // Set First item selected
                [investmentCell.amountTagsView selectTagAtIndex:0];
            }
            
            
            // Setup SIP Date RKTagsView
            investmentCell.sipDateTagsView.tag = 11;
            [investmentCell.sipDateTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investmentCell.sipDateTagsView.tagButtonHeight = 30;
            investmentCell.sipDateTagsView.tagBackgroundColor = UIColor.whiteColor;
            investmentCell.sipDateTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            investmentCell.sipDateTagsView.tintColor = [UIColor getAppColorDarkGrayText];
            investmentCell.sipDateTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            investmentCell.sipDateTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            investmentCell.sipDateTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            investmentCell.sipDateTagsView.editable = NO;
            investmentCell.sipDateTagsView.allowsMultipleSelection = NO;
            investmentCell.sipDateTagsView.lineSpacing = 8;
            investmentCell.sipDateTagsView.interitemSpacing = 10;
            investmentCell.sipDateTagsView.buttonCornerRadius = 4;
            investmentCell.sipDateTagsView.delegate = self;
            investmentCell.sipDateTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (investmentCell.sipDateTagsView.tags.count != self->arySipDates.count) {
                [investmentCell.sipDateTagsView removeAllTags];
                for (NSString *tag in self->arySipDates) {
                    [investmentCell.sipDateTagsView addTag:tag];
                }
                // Set First item selected
                [investmentCell.sipDateTagsView selectTagAtIndex:0];
            }
            
            
            
            // Setup Investor RKTagsView
            investmentCell.investorTagsView.tag = 12;
            [investmentCell.investorTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investmentCell.investorTagsView.tagButtonHeight = 30;
            investmentCell.investorTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
            investmentCell.investorTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
            investmentCell.investorTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
            investmentCell.investorTagsView.selectedTagTintColor = [UIColor whiteColor];
            investmentCell.investorTagsView.editable = NO;
            investmentCell.investorTagsView.allowsMultipleSelection = NO;
            investmentCell.investorTagsView.lineSpacing = 8;
            investmentCell.investorTagsView.interitemSpacing = 10;
            investmentCell.investorTagsView.buttonCornerRadius = 4;
            investmentCell.investorTagsView.delegate = self;
            investmentCell.investorTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (investmentCell.investorTagsView.tags.count != self->aryInvestor.count) {
                [investmentCell.investorTagsView removeAllTags];
                for (NSString *tag in self->aryInvestor) {
                    [investmentCell.investorTagsView addTag:tag];
                }
                // Set First item selected
                [investmentCell.investorTagsView selectTagAtIndex:0];
            }
            
        }
        return investmentCell;
    }
    return [UITableViewCell new];
}

#pragma mark - CollectionView DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return aryFunds.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    AddMoneyInFundCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"AddMoneyInFundCollectionCell" forIndexPath:indexPath];
    if (aryFunds.count > indexPath.item) {
        NSDictionary *dicItem = aryFunds[indexPath.item];
        if ([dicItem[@"selected"] isEqualToString:@"1"]) {
            cell.imgChecked.image = [UIImage imageNamed:@"done-Green"];
        } else {
            cell.imgChecked.image = [UIImage imageNamed:@"done-Gray"];
        }
        //[cell.imgFundLogo sd_setImageWithURL:[NSURL URLWithString:dicItem[@"image"]] placeholderImage:nil];
        cell.lblSchemeName.text = dicItem[@"scheme_name"];
        cell.lblCurrentAmount.text = dicItem[@"amount"];
    }
    //ImageAvatar = [NSString stringWithFormat:@"%@",[DictData objectAtIndex:indexPath.row]];
    //[cell.ImageView sd_setImageWithURL:[NSURL URLWithString:ImageAvatar] placeholderImage:nil];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(160, 95);
}

#pragma mark - RK Tags View Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 10) {
        // Amount
        investmentCell.tfAmount.text = aryAmountSuggested[index];
    } else if (tagsView.tag == 11) {
        // SIP Date
        
    } else if (tagsView.tag == 12) {
        // Investor
        
    }
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMakePayment_Tapped:(id)sender {
    InvestmentThanksVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InvestmentThanksVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
