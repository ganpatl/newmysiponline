//
//  AddMoneyVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface AddMoneyVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblAddMoney;

@end

NS_ASSUME_NONNULL_END
