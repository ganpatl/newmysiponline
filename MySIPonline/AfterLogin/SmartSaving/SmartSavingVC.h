//
//  SmartSavingVC.h
//  MySIPonline
//
//  Created by Ganpat on 11/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmartSavingVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblSmartSaving;
@property (weak, nonatomic) IBOutlet UIView *backGradientView;

@end

NS_ASSUME_NONNULL_END
