//
//  SmartSavingCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmartSavingCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentBalance;
@property (weak, nonatomic) IBOutlet UILabel *lbl1DayChange;
@property (weak, nonatomic) IBOutlet UIImageView *img1DayUpDown;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestment;
@property (weak, nonatomic) IBOutlet UILabel *lblGainLoss;
@property (weak, nonatomic) IBOutlet UILabel *lblUpDownPer;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpDown;

@end

NS_ASSUME_NONNULL_END
