//
//  SmartSavingVC.m
//  MySIPonline
//
//  Created by Ganpat on 11/02/19.
//

#import "SmartSavingVC.h"
#import "TransactionListVC.h"
#import "AddMoney/AddMoneyVC.h"
#import "InstaWithdraw/InstaWithdrawVC.h"

// Cells
#import "SmartSavingCell.h"
#import "InvestmentOptionCell.h"
#import "SmartSavingButtonsCell.h"

@interface SmartSavingVC () <UITableViewDataSource, UITableViewDelegate> {
    SmartSavingCell *smartSavingCell;
    SmartSavingButtonsCell *smartSavingButtonsCell;
    CAGradientLayer *theViewGradient;
    NSMutableArray *aryOtherCells;
}

@end

@implementation SmartSavingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"3A6ACB"] CGColor], (id)[[UIColor colorWithHexString:@"3780D1"] CGColor], (id)[[UIColor colorWithHexString:@"33A1DB"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblSmartSaving registerNib:[UINib nibWithNibName:@"SmartSavingCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SmartSavingCell"];
    [self.tblSmartSaving registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    [self.tblSmartSaving registerNib:[UINib nibWithNibName:@"SmartSavingButtonsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SmartSavingButtonsCell"];
    aryOtherCells = @[
                      @{@"type":@"smartsaving"},
                      @{@"type":@"inprocess", @"title":@"1 Transaction In process", @"desc":@"Dashboard will updated once transaction gets processed", @"image":@"info-error"},
                      @{@"type":@"viewtransaction", @"title":@"View Transaction", @"desc":@"6 Funds, 1 Portfilio", @"image":@"payment"},
                      @{@"type":@"buttons"}
                      ].mutableCopy;
    self.tblSmartSaving.dataSource = self;
    self.tblSmartSaving.delegate = self;
}


-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  aryOtherCells.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (smartSavingCell == nil) {
            smartSavingCell = [tableView dequeueReusableCellWithIdentifier:@"SmartSavingCell"];
            smartSavingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return smartSavingCell;
    } else if (indexPath.row > 0) {
        if (aryOtherCells.count > indexPath.row) {
            NSDictionary *dict = aryOtherCells[indexPath.row];
            if ([dict[@"type"] isEqualToString:@"buttons"]) {
                if (smartSavingButtonsCell == nil) {
                    smartSavingButtonsCell = [tableView dequeueReusableCellWithIdentifier:@"SmartSavingButtonsCell"];
                    smartSavingButtonsCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [smartSavingButtonsCell.btnAddAmount addTarget:self action:@selector(btnAddAmount_Tapped) forControlEvents:UIControlEventTouchUpInside];
                    [smartSavingButtonsCell.btnInstalWithdraw addTarget:self action:@selector(btnInstaWithdraw_Tapped) forControlEvents:UIControlEventTouchUpInside];
                }
                return smartSavingButtonsCell;
                
            } else {
                InvestmentOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
                cell.titleTopConstraint.constant = 14;
                cell.descBottomConstraint.constant = 13;
                if ([dict[@"type"] isEqualToString:@"inprocess"]) {
                    [cell.lblTitle setFont:[UIFont fontWithName:@"Roboto-Medium" size:14.0]];
                    cell.shadowView.backgroundColor = [UIColor colorWithHexString:@"FFFDF1"];
                    cell.shadowViewTopConstraint.constant = 0;
                    cell.shadowViewBottomConstraint.constant = 5;
                } else {
                    [cell.lblTitle setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
                    cell.shadowView.backgroundColor = UIColor.whiteColor;
                    cell.shadowViewTopConstraint.constant = 5;
                    cell.shadowViewBottomConstraint.constant = 5;
                }
                cell.lblTitle.text = dict[@"title"];
                cell.lblDesc.text = dict[@"desc"];
                cell.imgIcon.image = [UIImage imageNamed:dict[@"image"]];
                return cell;
            }
            
        }
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row > 0) {
        if (aryOtherCells.count > indexPath.row) {
            NSDictionary *dict = aryOtherCells[indexPath.row];
            if ([dict[@"type"] isEqualToString:@"inprocess"]) {
                // In Process
                
            } else if ([dict[@"type"] isEqualToString:@"viewtransaction"]) {
                // View My Transaction
                TransactionListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionListVC"];
                [self.navigationController pushViewController:destVC animated:YES];
           
            } else if ([dict[@"type"] isEqualToString:@"buttons"]) {
                // Find More
                
                
            }
        }
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnAddAmount_Tapped {
    AddMoneyVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMoneyVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnInstaWithdraw_Tapped {
    InstaWithdrawVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InstaWithdrawVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
