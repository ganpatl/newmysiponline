//
//  SmartSavingButtonsCell.h
//  MySIPonline
//
//  Created by Ganpat on 11/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmartSavingButtonsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnAddAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnInstalWithdraw;

@end

NS_ASSUME_NONNULL_END
