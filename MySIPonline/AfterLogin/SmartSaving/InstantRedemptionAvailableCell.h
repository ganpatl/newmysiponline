//
//  InstantRedemptionAvailableCell.h
//  MySIPonline
//
//  Created by Ganpat on 14/05/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstantRedemptionAvailableCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnKnowMore;

@end

NS_ASSUME_NONNULL_END
