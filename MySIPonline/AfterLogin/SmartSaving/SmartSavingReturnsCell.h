//
//  SmartSavingReturnsCell.h
//  MySIPonline
//
//  Created by Ganpat on 14/05/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface SmartSavingReturnsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UITextField *tfInvestmentAmount;
@property (weak, nonatomic) IBOutlet UIButton *btn1Y;
@property (weak, nonatomic) IBOutlet UIButton *btn3Y;
@property (weak, nonatomic) IBOutlet UIButton *btn5Y;
@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UILabel *lblVs;
@property (weak, nonatomic) IBOutlet UILabel *lblBankAccountAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblSmartSavingAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnViewFAQs;

@end

NS_ASSUME_NONNULL_END
