//
//  InstaWithdrawCollectionCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InstaWithdrawCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgChecked;
@property (weak, nonatomic) IBOutlet UIImageView *lblFundLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblUnit;

@end

NS_ASSUME_NONNULL_END
