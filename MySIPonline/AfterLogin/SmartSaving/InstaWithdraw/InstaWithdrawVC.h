//
//  InstaWithdrawVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface InstaWithdrawVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet RKTagsView *investorTagsView;
@property (weak, nonatomic) IBOutlet UITableView *tblInstaWithdraw;

@end

NS_ASSUME_NONNULL_END
