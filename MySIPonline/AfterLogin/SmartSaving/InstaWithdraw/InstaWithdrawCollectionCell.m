//
//  InstaWithdrawCollectionCell.m
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import "InstaWithdrawCollectionCell.h"

@implementation InstaWithdrawCollectionCell

- (void)awakeFromNib {
    [super awakeFromNib];
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    
}

@end
