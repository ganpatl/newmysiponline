//
//  InstaWithdrawVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import "InstaWithdrawVC.h"

// Cells
#import "AddMoneyFundsCell.h"
#import "InstaWithdrawCollectionCell.h"
#import "InstaRedeemCell.h"
#import "DisclaimerCell.h"

@interface InstaWithdrawVC () <RKTagsViewDelegate, UITableViewDataSource, UICollectionViewDataSource, UICollectionViewDelegate> {
    AddMoneyFundsCell *addMoneyFundsCell;
    InstaRedeemCell *instaRedeemCell;
    DisclaimerCell *disclaimerCell;
    NSArray *aryInvestor;
    NSMutableArray *aryFunds;
}

@end

@implementation InstaWithdrawVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    aryInvestor = @[@{@"name":@"Sonu"}, @{@"name":@"Praveen"}];
    aryFunds = @[
                 @{@"selected":@"1", @"scheme_name":@"Reliance Liquid Fund (G)", @"image":@"", @"amount":@"₹ 25,000"},
                 @{@"selected":@"0", @"scheme_name":@"Reliance Liquid Fund (D) Reliance Liquid Fund (G)", @"image":@"", @"amount":@"₹ 15,000"},
                 @{@"selected":@"0", @"scheme_name":@"Reliance Liquid Fund (G)", @"image":@"", @"amount":@"₹ 35,000"}
                 ].mutableCopy;
    
    // Setup RKTagsView
    [self.investorTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.investorTagsView.tagButtonHeight = 30;
    self.investorTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    self.investorTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
    self.investorTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
    self.investorTagsView.selectedTagTintColor = [UIColor whiteColor];
    self.investorTagsView.editable = NO;
    self.investorTagsView.allowsMultipleSelection = NO;
    self.investorTagsView.lineSpacing = 8;
    self.investorTagsView.interitemSpacing = 8;
    self.investorTagsView.buttonCornerRadius = 14;
    self.investorTagsView.delegate = self;
    self.investorTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    
    if (self.investorTagsView.tags.count != self->aryInvestor.count) {
        [self.investorTagsView removeAllTags];
        for (NSDictionary *dic in self->aryInvestor) {
            if ([AppHelper isNotNull:dic[@"name"]]) {
                [self.investorTagsView addTag:dic[@"name"]];
            }
        }
        // Set First item selected
        [self.investorTagsView selectTagAtIndex:0];
    }
    
    [self.tblInstaWithdraw registerNib:[UINib nibWithNibName:@"AddMoneyFundsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"AddMoneyFundsCell"];
    [self.tblInstaWithdraw registerNib:[UINib nibWithNibName:@"InstaRedeemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InstaRedeemCell"];
    [self.tblInstaWithdraw registerNib:[UINib nibWithNibName:@"DisclaimerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DisclaimerCell"];
    self.tblInstaWithdraw.dataSource = self;
    //self.tblInstaWithdraw.delegate = self;
}


#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (addMoneyFundsCell == nil) {
            addMoneyFundsCell = [tableView dequeueReusableCellWithIdentifier:@"AddMoneyFundsCell"];
            addMoneyFundsCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [addMoneyFundsCell.cvFunds registerNib:[UINib nibWithNibName:@"InstaWithdrawCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"InstaWithdrawCollectionCell"];
            addMoneyFundsCell.cvFunds.dataSource = self;
            addMoneyFundsCell.cvFunds.delegate = self;
            [addMoneyFundsCell.cvFunds reloadData];
        }
        return addMoneyFundsCell;
        
    } else if (indexPath.row == 1) {
        if (instaRedeemCell == nil) {
            instaRedeemCell = [tableView dequeueReusableCellWithIdentifier:@"InstaRedeemCell"];

        }
        return instaRedeemCell;
    } else if (indexPath.row == 2) {
        if (disclaimerCell == nil) {
            disclaimerCell = [tableView dequeueReusableCellWithIdentifier:@"DisclaimerCell"];
            disclaimerCell.lblText.text = @"Redemption request is processed almost instantly, and the money is credited to your bank account within 2-3 minutes (maximum time is 30 minutes).";
        }
        return disclaimerCell;
    }
    return [UITableViewCell new];
}

#pragma mark - CollectionView DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return aryFunds.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    InstaWithdrawCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"InstaWithdrawCollectionCell" forIndexPath:indexPath];
    if (aryFunds.count > indexPath.item) {
        NSDictionary *dicItem = aryFunds[indexPath.item];
        if ([dicItem[@"selected"] isEqualToString:@"1"]) {
            cell.imgChecked.image = [UIImage imageNamed:@"done-Green"];
        } else {
            cell.imgChecked.image = [UIImage imageNamed:@"done-Gray"];
        }
        //[cell.imgFundLogo sd_setImageWithURL:[NSURL URLWithString:dicItem[@"image"]] placeholderImage:nil];
        cell.lblSchemeName.text = dicItem[@"scheme_name"];
        cell.lblCurrentAmount.text = dicItem[@"amount"];
    }
    //ImageAvatar = [NSString stringWithFormat:@"%@",[DictData objectAtIndex:indexPath.row]];
    //[cell.ImageView sd_setImageWithURL:[NSURL URLWithString:ImageAvatar] placeholderImage:nil];
    
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    return CGSizeMake(230, 95);
}

#pragma mark - RK Tags View Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 10) {
        
    } else if (tagsView.tag == 11) {
        // SIP Date
        
    } else if (tagsView.tag == 12) {
        // Investor
        
    }
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendOtp_Tapped:(id)sender {
    
}


@end
