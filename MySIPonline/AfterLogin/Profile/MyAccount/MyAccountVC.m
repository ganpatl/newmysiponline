//
//  MyAccountVC.m
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import "MyAccountVC.h"
#import "BankMandateInvestorListVC.h"
#import "ProfileDisplayVC.h"
#import "InvestorListVC.h"

#import "InvestmentOptionCell.h"
#import "SubCategoryCell.h"

@interface MyAccountVC () <UITableViewDataSource, UITableViewDelegate> {
    
}

@end

@implementation MyAccountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.backView];
    self.imgUser.layer.cornerRadius = self.imgUser.frame.size.height / 2;
    self.imgUser.layer.masksToBounds = YES;
    self.viewKycStatus.layer.cornerRadius = self.viewKycStatus.frame.size.height / 2;
    self.viewKycStatus.layer.masksToBounds = YES;
    
    NSDictionary *dicLoginData = [NSUserData GetLoginData];
    self.lblUserName.text = [dicLoginData[@"name"] capitalizedString];
    NSURL *imageUrl = [NSURL URLWithString:dicLoginData[@"image"]];
    if (imageUrl != nil) {
        [self.imgUser sd_setImageWithURL:imageUrl placeholderImage:nil];
    }
    
    [self.tblMyAccount registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    [self.tblMyAccount registerNib:[UINib nibWithNibName:@"SubCategoryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SubCategoryCell"];
    
    self.tblMyAccount.dataSource = self;
    self.tblMyAccount.delegate = self;
}


#pragma mark - TableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 4;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        InvestmentOptionCell *investmentOptionCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
        investmentOptionCell.imgIcon.image = [UIImage imageNamed:@"bankmandate"];
        investmentOptionCell.lblTitle.text = @"Start Auto Pay for Investments";
        investmentOptionCell.lblDesc.text = @"Set Up Auto-Pay Mandate with your Bank";
        investmentOptionCell.contentView.backgroundColor = UIColor.clearColor;
        investmentOptionCell.backgroundColor = UIColor.clearColor;
        
        return investmentOptionCell;
        
    } else if (indexPath.section == 1) {
        SubCategoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SubCategoryCell"];
        cell.shadowView.layer.shadowOpacity = 0;
        cell.topMarginConstraint.constant = 0;
        cell.bottomMarginConstraint.constant = 1;
        if (indexPath.row == 0) {
            cell.lblTitle.text = @"Profile Details";
            cell.lblDesc.text = @"Personal, Bank, Nominee details";
        } else if (indexPath.row == 1) {
            cell.lblTitle.text = @"Investor List";
            cell.lblDesc.text = @"No Family Member - Add Now";
        } else if (indexPath.row == 2) {
            cell.lblTitle.text = @"Password Change";
            cell.lblDesc.text = @"change password";
        } else if (indexPath.row == 3) {
            cell.shadowView.layer.shadowOpacity = 0.5;
            cell.lblTitle.text = @"Debit Mandate";
            cell.lblDesc.text = @"1 Pending";
        }
        return cell;
    }
    return [UITableViewCell new];
}

-(UIView *)tableView:(UITableView *)tableView viewForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        UIView *vHeader = [[UIView alloc] initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, 16)];
        return vHeader;
    } else {
        return nil;
    }
}

-(CGFloat)tableView:(UITableView *)tableView heightForHeaderInSection:(NSInteger)section {
    if (section == 1) {
        return 16;
    } else {
        return 0;
    }
}

#pragma mark - TableView Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return 81;
    } else if (indexPath.section == 1) {
        return UITableViewAutomaticDimension;
    }
    return 0;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        if (indexPath.row == 0) {
            // Profile Details
            ProfileDisplayVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileDisplayVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        
        } else if (indexPath.row == 1) {
            // Investor List
            InvestorListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InvestorListVC"];
            [self.navigationController pushViewController:destVC animated:YES];
                
        } else if (indexPath.row == 3) {
            // Bank Mandate
            BankMandateInvestorListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BankMandateInvestorListVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
