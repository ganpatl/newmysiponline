//
//  ProfileDisplayVC.m
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import "ProfileDisplayVC.h"
#import "ProfileDisplayCell.h"

@interface ProfileDisplayVC () <UITableViewDataSource> {
    NSArray *aryProfileData;
}

@end

@implementation ProfileDisplayVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    aryProfileData = @[
                       @{@"title":@"Name", @"desc":@"Sonu Kumar Sharma"},
                       @{@"title":@"Mobile No", @"desc":@"+91 9484221048"},
                       @{@"title":@"Email", @"desc":@"dpatidar1990@gmail.com"},
                       @{@"title":@"Address", @"desc":@"VPO Jogpur, Sagwara, Dungarpur Rajasthan - 314035"},
                       @{@"title":@"Bank", @"desc":@"XXXX95 (ICICI Bank)"},
                       @{@"title":@"Nominee Name", @"desc":@"Rudra Patidar"},
                       @{@"title":@"Debit Mandate", @"desc":@"Registered"}
                       ];
    [self.tblProfileDisplay registerNib:[UINib nibWithNibName:@"ProfileDisplayCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ProfileDisplayCell"];
    self.tblProfileDisplay.dataSource = self;
    
}

#pragma mark - TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 7;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    ProfileDisplayCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileDisplayCell"];
    NSDictionary *dictProfile = aryProfileData[indexPath.row];
    cell.lblTitle.text = dictProfile[@"title"];
    cell.lblDesc.text = dictProfile[@"desc"];
    return cell;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

@end
