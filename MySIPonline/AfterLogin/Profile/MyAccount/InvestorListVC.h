//
//  InvestorListVC.h
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InvestorListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblInvestorList;

@end

NS_ASSUME_NONNULL_END
