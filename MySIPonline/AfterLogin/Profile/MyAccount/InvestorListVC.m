//
//  InvestorListVC.m
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import "InvestorListVC.h"
#import "InvestorListCell.h"

@interface InvestorListVC () <UITableViewDataSource> {
    NSArray *aryInvestor;
}

@end

@implementation InvestorListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    aryInvestor = @[
                    @{@"name":@"Praveen Tiwari", @"status":@"Profile Not Complete"}
                   ];
    [self.tblInvestorList registerNib:[UINib nibWithNibName:@"InvestorListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestorListCell"];
    self.tblInvestorList.dataSource = self;
}

#pragma mark - TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryInvestor.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    InvestorListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestorListCell"];
    NSDictionary *dictInvestor = aryInvestor[indexPath.row];
    cell.lblName.text = dictInvestor[@"name"];
    cell.lblStatus.text = dictInvestor[@"status"];
    return cell;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnAddNewFamilyMember_Tapped:(id)sender {
    
}

@end
