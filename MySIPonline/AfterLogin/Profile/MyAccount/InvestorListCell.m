//
//  InvestorListCell.m
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import "InvestorListCell.h"

@implementation InvestorListCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:3];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
