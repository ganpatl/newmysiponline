//
//  ProfileDisplayCell.h
//  MySIPonline
//
//  Created by Ganpat on 02/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileDisplayCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end

NS_ASSUME_NONNULL_END
