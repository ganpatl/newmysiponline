//
//  MyAccountVC.h
//  MySIPonline
//
//  Created by Ganpat on 02/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface MyAccountVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgUser;
@property (weak, nonatomic) IBOutlet UILabel *lblUserName;
@property (weak, nonatomic) IBOutlet UILabel *lblPanCard;
@property (weak, nonatomic) IBOutlet UILabel *lblKycStatus;
@property (weak, nonatomic) IBOutlet UITableView *tblMyAccount;
@property (weak, nonatomic) IBOutlet UIView *viewKycStatus;
@property (weak, nonatomic) IBOutlet UIView *backView;

@end

NS_ASSUME_NONNULL_END
