//
//  ProfileDocsVC.h
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileDocsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblProfileDocs;


@end

NS_ASSUME_NONNULL_END
