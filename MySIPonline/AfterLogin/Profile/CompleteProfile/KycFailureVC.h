//
//  KycFailureVC.h
//  MySIPonline
//
//  Created by Ganpat on 05/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KycFailureVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UILabel *lblDetailMsg;
@property (weak, nonatomic) IBOutlet UIButton *btnGotIt;

@property NSString *strMessage;

@end

NS_ASSUME_NONNULL_END
