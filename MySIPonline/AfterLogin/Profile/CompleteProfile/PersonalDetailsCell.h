//
//  PersonalDetailsCell.h
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import <UIKit/UIKit.h>
@import SkyFloatingLabelTextField;

NS_ASSUME_NONNULL_BEGIN

@interface PersonalDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfFullName;
@property (weak, nonatomic) IBOutlet UILabel *lblErrFullName;
@property (weak, nonatomic) IBOutlet UIButton *btnIndian;
@property (weak, nonatomic) IBOutlet UIButton *btnNri;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfDateOfBirth;
@property (weak, nonatomic) IBOutlet UILabel *lblErrDateOfBirth;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfCountryCode;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfMobile;
@property (weak, nonatomic) IBOutlet UILabel *lblErrMobile;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfMotherName;
@property (weak, nonatomic) IBOutlet UILabel *lblErrMotherName;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfFatherName;
@property (weak, nonatomic) IBOutlet UILabel *lblErrFatherName;
@property (weak, nonatomic) IBOutlet UIButton *btnSingle;
@property (weak, nonatomic) IBOutlet UIButton *btnMarried;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfPincode;
@property (weak, nonatomic) IBOutlet UILabel *lblErrPincode;

@property (weak, nonatomic) IBOutlet UIView *motherNameView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *motherNameViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *motherNameViewTopConstraint;

@property (weak, nonatomic) IBOutlet UIView *fatherNameView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fatherNameViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *fatherNameViewTopConstraint;

@property (weak, nonatomic) IBOutlet UIView *maritalStatusView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maritalStatusViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *maritalStatusViewTopConstraint;

-(void)setMotherNameHidden:(BOOL)doHide;
-(void)setFatherNameHidden:(BOOL)doHide;
-(void)setMaritalStatusHidden:(BOOL)doHide;

@end

NS_ASSUME_NONNULL_END
