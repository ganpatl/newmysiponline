//
//  ProfileDocsCell.h
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileDocsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgDoc;
@property (weak, nonatomic) IBOutlet UILabel *lblDoc;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@end

NS_ASSUME_NONNULL_END
