//
//  ProfileDocsVC.m
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import "ProfileDocsVC.h"
#import "PanVerificationVC.h"

// Cells
#import "DisclaimerCell.h"
#import "ProfileDocsCell.h"

@interface ProfileDocsVC () <UITableViewDataSource> {
    DisclaimerCell *disclaimerCell;
    NSArray *aryDocs;
}
@end

@implementation ProfileDocsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    aryDocs = @[
                @{@"image":@"pan card", @"title":@"Pan Card", @"desc":@"it is required to check your KYC status and also help us to submit fresh KYC requests on your behalf."},
                @{@"image":@"aadhar_back", @"title":@"Aadhaar Card", @"desc":@"it is required to check your KYC status and also help us to submit fresh KYC requests on your behalf."},
                @{@"image":@"cancelledcheque", @"title":@"Cancelled Cheque", @"desc":@"it is required to check your KYC status and also help us to submit fresh KYC requests on your behalf."},
                @{@"image":@"signature", @"title":@"Signature", @"desc":@"it is required to check your KYC status and also help us to submit fresh KYC requests on your behalf."}
                ];
    
    [self.tblProfileDocs registerNib:[UINib nibWithNibName:@"DisclaimerCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"DisclaimerCell"];
    [self.tblProfileDocs registerNib:[UINib nibWithNibName:@"ProfileDocsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ProfileDocsCell"];
    self.tblProfileDocs.dataSource = self;
    
    
}

#pragma mark - TableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else {
        return aryDocs.count;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (disclaimerCell == nil) {
            disclaimerCell = [tableView dequeueReusableCellWithIdentifier:@"DisclaimerCell"];
            disclaimerCell.selectionStyle = UITableViewCellSelectionStyleNone;
            disclaimerCell.lblText.text = @"Keep below listed document ready In soft format, you can capture picture using mobile camera";
        }
        return disclaimerCell;
    } else {
        ProfileDocsCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileDocsCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        if (aryDocs.count > indexPath.row) {
            NSDictionary *dictDoc = aryDocs[indexPath.row];
            cell.imgDoc.image = [UIImage imageNamed:dictDoc[@"image"]];
            cell.lblDoc.text = dictDoc[@"title"];
            cell.lblDesc.text = dictDoc[@"desc"];
        }
        return cell;
    }
    
    return [UITableViewCell new];
}

#pragma mark - Button Tap Event

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnLetsStart_Tapped:(id)sender {
    PanVerificationVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PanVerificationVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
