//
//  ProfileCompletedVC.h
//  MySIPonline
//
//  Created by Ganpat on 02/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileCompletedVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblComplete;
@property (weak, nonatomic) IBOutlet UIView *viewStatusBar;

@property NSString *strMessage;
@property NSString *strInvestorId;
@property BOOL isNRI;

@end

NS_ASSUME_NONNULL_END
