//
//  PersonalDetailsCell.m
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import "PersonalDetailsCell.h"

@implementation PersonalDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setMotherNameHidden:(BOOL)doHide {
    if (doHide) {
        self.motherNameViewHeightConstraint.constant = 0;
        self.motherNameView.hidden = YES;
        self.motherNameViewTopConstraint.constant = 0;
    } else {
        self.motherNameViewHeightConstraint.constant = 60;
        self.motherNameView.hidden = NO;
        self.motherNameViewTopConstraint.constant = 8;
    }
}

-(void)setFatherNameHidden:(BOOL)doHide {
    if (doHide) {
        self.fatherNameViewHeightConstraint.constant = 0;
        self.fatherNameView.hidden = YES;
        self.fatherNameViewTopConstraint.constant = 0;
        
    } else {
        self.fatherNameViewHeightConstraint.constant = 60;
        self.fatherNameView.hidden = NO;
        self.fatherNameViewTopConstraint.constant = 8;
    }
}

-(void)setMaritalStatusHidden:(BOOL)doHide {
    if (doHide) {
        self.maritalStatusViewHeightConstraint.constant = 0;
        self.maritalStatusView.hidden = YES;
        self.maritalStatusViewTopConstraint.constant = 0;
    } else {
        self.maritalStatusViewHeightConstraint.constant = 60;
        self.maritalStatusView.hidden = NO;
        self.maritalStatusViewTopConstraint.constant = 8;
    }
}

@end
