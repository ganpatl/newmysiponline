//
//  BankDetailsCell.h
//  MySIPonline
//
//  Created by Ganpat on 27/02/19.
//

#import <UIKit/UIKit.h>
@import SkyFloatingLabelTextField;

NS_ASSUME_NONNULL_BEGIN

@interface BankDetailsCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UIView *primaryBank1View;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryBank1;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfIfsc1;
@property (weak, nonatomic) IBOutlet UILabel *lblErrIfsc1;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfAccountNo1;
@property (weak, nonatomic) IBOutlet UILabel *lblErrAccountNo1;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfConfirmAccount1;
@property (weak, nonatomic) IBOutlet UILabel *lblErrConfirmAccount1;
@property (weak, nonatomic) IBOutlet UIButton *btnSaving1;
@property (weak, nonatomic) IBOutlet UIButton *btnNri1;
@property (weak, nonatomic) IBOutlet UIButton *btnNro1;
@property (weak, nonatomic) IBOutlet UIView *addNewBankAccountView;
@property (weak, nonatomic) IBOutlet UIButton *btnAddNewBank;
@property (weak, nonatomic) IBOutlet UIView *shadowView1;

@property (weak, nonatomic) IBOutlet UIView *primaryBank2View;
@property (weak, nonatomic) IBOutlet UIButton *btnPrimaryBank2;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfIfsc2;
@property (weak, nonatomic) IBOutlet UILabel *lblErrIfsc2;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfAccountNo2;
@property (weak, nonatomic) IBOutlet UILabel *lblErrAccountNo2;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfConfirmAccount2;
@property (weak, nonatomic) IBOutlet UILabel *lblErrConfirmAccount2;
@property (weak, nonatomic) IBOutlet UIButton *btnSaving2;
@property (weak, nonatomic) IBOutlet UIButton *btnNri2;
@property (weak, nonatomic) IBOutlet UIButton *btnNro2;

@property (weak, nonatomic) IBOutlet UIView *ifscCode2View;
@property (weak, nonatomic) IBOutlet UIView *bankAccount2View;
@property (weak, nonatomic) IBOutlet UIView *confirmBankAccount2View;
@property (weak, nonatomic) IBOutlet UIView *accountType2View;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *primaryBank1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *addNewBankAccountHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *primaryBank2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ifscCode2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ifscCode2TopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bankAccount2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *bankAccount2TopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmBankAccount2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *confirmBankAccount2TopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *accountType2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *accountTypeTopConstraint;


-(void)setPrimaryBank1ViewHidden:(BOOL)doHide;
-(void)setAddNewBankAccountViewHidden:(BOOL)doHide;
-(void)setPrimaryBank2ViewHidden:(BOOL)doHide;
-(void)setSecondBankHidden:(BOOL)doHide;

@end

NS_ASSUME_NONNULL_END
