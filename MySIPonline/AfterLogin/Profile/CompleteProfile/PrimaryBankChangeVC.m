//
//  PrimaryBankChangeVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/12/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "PrimaryBankChangeVC.h"

@interface PrimaryBankChangeVC ()

@end

@implementation PrimaryBankChangeVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.popupView.layer.cornerRadius = 4;
    self.popupView.clipsToBounds = YES;
    [AppHelper addShadowOnView:self.bottomShadowView Color:UIColor.lightGrayColor OffetSize:CGSizeMake(0, -2) Opacity:0.5 Radius:1.0];
}

-(void)viewDidAppear:(BOOL)animated {
    [super viewDidAppear:animated];
    self.presentingViewController.tabBarController.tabBar.hidden = YES;
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    self.presentingViewController.tabBarController.tabBar.hidden = NO;
}

#pragma mark - Button Tap Events

- (IBAction)btnYes_Tapped:(id)sender {
    [self.delegate primaryBankAcChanged:self.bankTag];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnNo_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

@end
