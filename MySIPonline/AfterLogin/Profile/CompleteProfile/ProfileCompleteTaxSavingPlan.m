//
//  ProfileCompleteTaxSavingPlan.m
//  MySIPonline
//
//  Created by Ganpat on 02/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "ProfileCompleteTaxSavingPlan.h"

@implementation ProfileCompleteTaxSavingPlan {
    //CAGradientLayer *theViewGradient;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.gradientView.layer.cornerRadius = 4;
    self.gradientView.layer.masksToBounds = YES;
    [AppHelper addShadowOnView:self.gradientView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 3) Opacity:0.5 Radius:3];
    
    [self.gradientView setBackgroundColor:[UIColor clearColor]];
//    theViewGradient = [CAGradientLayer layer];
//    CGRect mFrame = self.gradientView.bounds;
//    mFrame.size.width = [UIScreen mainScreen].bounds.size.width;
//    theViewGradient.frame = mFrame;
//    [theViewGradient setNeedsDisplayOnBoundsChange:YES];
//    theViewGradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithHexString:@"FCB350"], (id)[UIColor colorWithHexString:@"FC7E44"], nil];
//    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
//    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
//    [self.gradientView.layer insertSublayer:theViewGradient atIndex:0];
    
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

//-(void)layoutSubviews {
//    [super layoutSubviews];
//
//    [theViewGradient removeFromSuperlayer];
//    theViewGradient = nil;
//
//    self.gradientView.layer.cornerRadius = 4;
//    self.gradientView.layer.masksToBounds = YES;
//
//    [self.gradientView setBackgroundColor:[UIColor clearColor]];
    //theViewGradient = [CAGradientLayer layer];
    //CGRect mFrame = self.gradientView.bounds;
    //mFrame.size.width = [UIScreen mainScreen].bounds.size.width;
    //theViewGradient.frame = mFrame;
//
//    theViewGradient.colors = [NSArray arrayWithObjects:(id)[UIColor colorWithHexString:@"FCB350"], (id)[UIColor colorWithHexString:@"FC7E44"], nil];
//    theViewGradient.startPoint = CGPointMake(0.0, 0.5);
//    theViewGradient.endPoint = CGPointMake(1.0, 0.5);
//    [self.gradientView.layer insertSublayer:theViewGradient atIndex:0];
//}

//-(void)layoutSublayersOfLayer:(CALayer *)layer {
//    if (layer == theViewGradient) {
//        theViewGradient = [CAGradientLayer layer];
//        CGRect mFrame = self.gradientView.bounds;
//        mFrame.size.width = [UIScreen mainScreen].bounds.size.width;
//        theViewGradient.frame = mFrame;
//    }
//}

@end
