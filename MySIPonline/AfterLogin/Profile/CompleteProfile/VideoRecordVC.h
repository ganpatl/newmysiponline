//
//  VideoRecordVC.h
//  MySIPonline
//
//  Created by Ganpat on 15/02/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VideoRecordVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgDocument;
@property (weak, nonatomic) IBOutlet UIImageView *iconDocument;
@property (weak, nonatomic) IBOutlet UILabel *lblDocument;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDocument;
@property (weak, nonatomic) IBOutlet UILabel *lblVideoTopText;
@property (weak, nonatomic) IBOutlet UIView *randomNumberBorderView;
@property (weak, nonatomic) IBOutlet UILabel *lblRandomNumber;

@property NSString *strProfileStatus;
@property NSString *strDocType;
//@property NSString *strUrl;
@property NSString *strBankId;
@property NSString *strUserId;
@property NSString *strSignature;
@property BOOL isDocUploaded;
@property BOOL isKycVerified;

@end

NS_ASSUME_NONNULL_END
