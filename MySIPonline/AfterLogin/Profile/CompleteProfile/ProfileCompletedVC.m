//
//  ProfileCompletedVC.m
//  MySIPonline
//
//  Created by Ganpat on 02/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "ProfileCompletedVC.h"
//#import "FundList.h"
//#import "TaxSavingView.h"
//#import "NewPhyscialKYC.h"
//#import "BestSIPFundList.h"

// Tableview Cell
#import "ProfileCompleteTopCell.h"
#import "InvestmentOptionCell.h"
#import "ProfileCompleteTaxSavingPlan.h"

@interface ProfileCompletedVC () <UITableViewDataSource, UITableViewDelegate> {
    
}

@end

@implementation ProfileCompletedVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.tblComplete registerNib:[UINib nibWithNibName:@"ProfileCompleteTopCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ProfileCompleteTopCell"];
    [self.tblComplete registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    [self.tblComplete registerNib:[UINib nibWithNibName:@"ProfileCompleteTaxSavingPlan" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ProfileCompleteTaxSavingPlan"];
    self.tblComplete.dataSource = self;
    self.tblComplete.delegate = self;
    
//    if (@available(iOS 11.0, *)) {
//        self.viewStatusBar.frame = CGRectMake(0, 0, self.viewStatusBar.frame.size.width, UIApplication.sharedApplication.keyWindow.safeAreaInsets.top);
//        self.tblComplete.frame = [AppHelper getSafeFrameWithUpdatedYIncrease:self.tblComplete.frame];
//    }
}

#pragma mark - TableView DataSource

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 3;
}

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return 1;
    } else if (section == 1) {
        return 1;
    } else if (section == 2) {
        return 1;
    } else {
        return 0;
    }
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        ProfileCompleteTopCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCompleteTopCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.lblDesc.text = self.strMessage;
        cell.lblInvestoCode.text = self.strInvestorId;
        if (self.isNRI) {
            //[cell.btnDesc removeTarget:self action:@selector(btnDesc_Tapped) forControlEvents:UIControlEventTouchUpInside];
            //[cell.btnDesc addTarget:self action:@selector(btnDesc_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return cell;
        
    } else if (indexPath.section == 1) {
        InvestmentOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        cell.imgIcon.image = [UIImage imageNamed:@"personalizePortfolio"];
        cell.lblTitle.text = @"Explore Recommended Funds";
        cell.lblDesc.text = @"Wide range of funds providing SIP with Insurance, Liquid funds etc adadas";
        return cell;
        
    } else {
        ProfileCompleteTaxSavingPlan *cell = [tableView dequeueReusableCellWithIdentifier:@"ProfileCompleteTaxSavingPlan"];
        cell.selectionStyle = UITableViewCellSelectionStyleNone;
        return cell;
    }
}
-(void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 2) {
        ProfileCompleteTaxSavingPlan *myCell = (ProfileCompleteTaxSavingPlan*)cell;
        if (myCell.gradientLayer == nil) {
            [myCell.gradientView setBackgroundColor:[UIColor clearColor]];
            myCell.gradientLayer = [CAGradientLayer layer];
            CGRect mFrame = myCell.gradientView.bounds;
            mFrame.size.width = myCell.frame.size.width;
            myCell.gradientLayer.frame = mFrame;
            myCell.gradientLayer.colors = [NSArray arrayWithObjects:(id)UIColor.getAppTaxGradientColor1.CGColor, (id)UIColor.getAppTaxGradientColor2.CGColor, nil];
            myCell.gradientLayer.startPoint = CGPointMake(0.0, 0.5);
            myCell.gradientLayer.endPoint = CGPointMake(1.0, 0.5);
            [myCell.gradientView.layer insertSublayer:myCell.gradientLayer atIndex:0];
        }
    }
}

#pragma mark - TableView Delegate Methods

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        return UITableViewAutomaticDimension;
    } else if (indexPath.section == 1) {
        return 81;
    } else {
        return UITableViewAutomaticDimension;
    }
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    if (indexPath.section == 1) {
        // Go to Recommended Fund
//        FundList *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"FundList"];
//        svc.navTitle = @"Recommended Fund";
//        svc.FundlistString = @"1";
//        UINavigationController *nav = ((UINavigationController*)self.presentingViewController).viewControllers.firstObject;
//        if ([self.presentingViewController isKindOfClass:[UITabBarController class]]) {
//            ((UITabBarController*)self.presentingViewController).selectedIndex = 0;
//        }
//        [self dismissViewControllerAnimated:YES completion:nil];
//        [nav pushViewController:svc animated:YES];
        
    } else if (indexPath.section == 2) {
        // Go to Tax Saving Plan
//        BestSIPFundList *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BestSIPFundList"];
//        destVC.SavingStringID = @"0";
//        destVC.FinalAgeID = @"0";
//        destVC.TenureID = @"0";
//        destVC.RiskID = @"0";
        
//        UINavigationController *nav = ((UINavigationController*)self.presentingViewController).viewControllers.firstObject;
//        if ([self.presentingViewController isKindOfClass:[UITabBarController class]]) {
//            ((UITabBarController*)self.presentingViewController).selectedIndex = 0;
//        }
//        [self dismissViewControllerAnimated:YES completion:nil];
//        [nav popToRootViewControllerAnimated:YES];
//        [nav pushViewController:destVC animated:YES];
    }
}

-(void)btnDesc_Tapped {
    // NRI case
//    NewPhyscialKYC *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPhyscialKYC"];
//    svc.kycBackStatus = @"0";
//    svc.strUserId = self.strInvestorId;
//    UINavigationController *nav = ((UINavigationController*)self.presentingViewController).viewControllers.firstObject;
//    [self dismissViewControllerAnimated:YES completion:nil];
//    [nav pushViewController:svc animated:YES];
}

@end
