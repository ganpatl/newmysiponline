//
//  UploadCollectionCell.h
//  MySIPonline
//
//  Created by Ganpat on 07/12/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UploadCollectionCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgDocument;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblDocument;
@property (weak, nonatomic) IBOutlet UIButton *btnDocument;

@end

NS_ASSUME_NONNULL_END
