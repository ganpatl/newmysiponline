//
//  CreateSignatureVC.h
//  MySIPonline
//
//  Created by Ganpat on 26/07/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "GSSignatureView.h"

// Deletage Defination
@protocol CreateSignatureDelegate <NSObject>
@required
- (void)signatureSaved:(UIImage*)imgSignature;
@end

// Class Declaration
@interface CreateSignatureVC : UIViewController
@property id<CreateSignatureDelegate> delegate;
@property (weak, nonatomic) IBOutlet GSSignatureView *signatureView;
@property (weak, nonatomic) IBOutlet UIButton *btnSave;
@property (weak, nonatomic) IBOutlet UIButton *btnCancel;
@property (weak, nonatomic) IBOutlet UIButton *btnRubber;

@end
