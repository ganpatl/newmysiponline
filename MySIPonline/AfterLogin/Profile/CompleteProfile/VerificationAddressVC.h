//
//  VerificationAddressVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface VerificationAddressVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITextView *tvAddress;

@property NSString *strUserId;
@property NSString *strVideoUrl;

@end

NS_ASSUME_NONNULL_END
