//
//  DocsGridVC.m
//  MySIPonline
//
//  Created by Ganpat on 28/02/19.
//

#import "DocsGridVC.h"
#import "UploadSingleDocVC.h"
#import "VideoRecordVC.h"
#import "ContactUsVC.h"
#import "KycFailureVC.h"
#import "UploadCollectionCell.h"
#import "ProfileCompletedVC.h"
//#import "NewPhyscialKYC.h"

//check_video_kyc
#define RedirectPhysicalKyc @"1"
#define RedirectVideoKyc @"2"
#define RedirectProfileComplete @"3"


@interface DocsGridVC () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout> {
    NSMutableDictionary *Profiledetails;
    NSMutableArray *aryUIDocs;
    NSString *bankId;
    NSString *strRedirectionStatus;
    NSArray *aryDocKey;
    NSDictionary *detailDic;
    BOOL checkVideoKYC;
    BOOL redirectToNext;
}
@end

@implementation DocsGridVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    checkVideoKYC = NO;
    redirectToNext = NO;
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    //Profiledetails = [NSUserData GetProfileDetails];
    
    [self.cvUploadDocument registerNib:[UINib nibWithNibName:@"UploadCollectionCell" bundle:[NSBundle mainBundle]] forCellWithReuseIdentifier:@"UploadCollectionCell"];
    self.cvUploadDocument.dataSource = self;
    self.cvUploadDocument.delegate = self;
    
    if (self.isNRI) {
        self.lblDesc.text = @"Please confirm all the documents have been successfully uploaded. Click on Next button.";
        //@"Check that all the documents have been successfully uploaded and then click on Next button.";
    }
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [AppHelper sendToScreenTracker:NSStringFromClass([self class])];
    
    // Clear Cache
    //[[SDImageCache sharedImageCache]clearMemory];
    //[[SDImageCache sharedImageCache]clearDisk];
    
    NSDictionary *bankdetails = [[Profiledetails valueForKey:@"Bankdetail"] objectAtIndex:0];
    if ([_familyDetailsId isEqualToString:@"yes"]) {
        if ([_familymembID length]) {
            bankId = [bankdetails valueForKey:@"id"];
            if ([bankId isEqualToString:@"0"]) {
                bankId = _bankFamilyId;
            }
            [self checkDocumentsData:bankId];
        } else {
            bankId = _bankFamilyId;
            [self checkDocumentsData:bankId];
        }
    } else {
        bankId = [bankdetails valueForKey:@"id"];
        if ([bankId length]) {
            [self checkDocumentsData:bankId];
        } else {
            bankId = _bankFamilyId;
            [self checkDocumentsData:_bankFamilyId];
        }
    }
}

-(void)dismissMyView {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - CollectionView DataSource

-(NSInteger)collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section {
    return aryDocKey.count;
}

-(UICollectionViewCell *)collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath {
    UploadCollectionCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"UploadCollectionCell" forIndexPath:indexPath];
    
    //cell.SrNoTxt.text = [NSString stringWithFormat:@"%ld", (long)indexPath.row + 1];
    NSString *documentType = [NSString stringWithFormat:@"%@", [aryDocKey objectAtIndex:indexPath.item]];
    cell.lblDocument.text = documentType;
    
    NSString *newString = [detailDic valueForKey:documentType];
    NSArray *arySplit = [newString componentsSeparatedByString:@"="];
    
    cell.imgIcon.hidden = NO;
    //cell.imgDocument.image = [UIImage imageNamed:@""];
    cell.imgDocument.image = nil;
    
    if ([arySplit.lastObject isEqualToString:@""]) {
        NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@"=" withString:@""];
        //cell.DetailsTxt.text = newString1;
        //cell.uploadTxt.text = @"Browse";
        //cell.UploadImg.image = [UIImage imageNamed:@"upload-document.png"];
    } else {
        // NSLog(@"There is no = in the string");
        NSRange equalRange1 = [newString rangeOfString:@"=" options:NSBackwardsSearch];
        if (equalRange1.location != NSNotFound) {
            // Has Image URL
            NSString *result2 = [newString substringFromIndex:equalRange1.location + equalRange1.length];
            [cell.imgDocument sd_setImageWithURL:[NSURL URLWithString:result2] placeholderImage:nil];
            cell.imgIcon.hidden = YES;
            // NSLog(@"The result = %@", result2);
            //NSString *strr = [NSString stringWithFormat:@"=%@", result2];
            //NSString *newString2 = [newString stringByReplacingOccurrencesOfString:strr withString:@""];
            //cell.DetailsTxt.text = newString2;
        } else {
            // NSLog(@"There is no = in the string");
        }
        //cell.uploadTxt.text = @"Uploaded";
        //cell.UploadImg.image = [UIImage imageNamed:@"UpCancel.png"];
    }
    
    cell.btnDocument.tag = indexPath.item;
    [cell.btnDocument addTarget:self action:@selector (btnDocument_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    //int investStatus = [[[Profiledetails valueForKey:@"personal"] valueForKey:@"invest_status"] intValue];
    //if (investStatus == 1 && [cell.uploadTxt.text isEqualToString:@"Uploaded"]) {
    //    cell.UploadBtn.tag = indexPath.row;
    //    cell.UploadBtn.userInteractionEnabled = NO;
    //    cell.UploadImg.hidden = YES;
    //    cell.uploadTxt.textColor = [UIColor colorWithHexString:@"72B91D"];
    //}
    return cell;
}

-(CGSize)collectionView:(UICollectionView *)collectionView layout:(UICollectionViewLayout *)collectionViewLayout sizeForItemAtIndexPath:(NSIndexPath *)indexPath {
    CGSize cellSize = CGSizeMake((collectionView.frame.size.width - 50) / 3, 93);
    return cellSize;
}

#pragma mark - Web Api Call

-(void) checkDocumentsData : (NSString *) bankIdNo {
    aryDocKey = @[@"Selfie Image", @"Pan Card", @"Aadhaar Front", @"Aadhaar Back", @"Signature", @"Bank Statement"];
    detailDic = @{
                  aryDocKey[0]:[NSString stringWithFormat:@"%@=", aryDocKey[0]],
                  aryDocKey[1]:[NSString stringWithFormat:@"%@=", aryDocKey[1]],
                  aryDocKey[2]:[NSString stringWithFormat:@"%@=", aryDocKey[2]],
                  aryDocKey[3]:[NSString stringWithFormat:@"%@=", aryDocKey[3]],
                  aryDocKey[4]:[NSString stringWithFormat:@"%@=", aryDocKey[4]],
                  aryDocKey[5]:[NSString stringWithFormat:@"%@=", aryDocKey[5]]
                };
//    NSDictionary *params = [NSUserData GetLoginDataAPI];
//    NSString *UserID = [NSString stringWithFormat:@"%@",[params valueForKey:@"id"]];
//    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *params;
//        if ([_familyDetailsId isEqualToString:@"yes"]) {
//            if ([_familymembID length]) {
//                params = @{@"user_id":_familymembID,@"bank_id":bankIdNo};
//            } else {
//                params = @{@"user_id":_FamilyIdMem,@"bank_id":bankIdNo};
//            }
//        } else {
//            params = @{@"user_id":UserID,@"bank_id":bankIdNo};
//        }
//        NSLog(@"%@", params);
//        [LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_checkDocumentsData:params AndCompletionHandler:^(id response, bool isError) {
//            NSLog(@"%@", response);
//            if (!isError) {
//                self->strRedirectionStatus = [NSString stringWithFormat:@"%@", response[@"check_video_kyc"]];
//                NSString *status = [NSString stringWithFormat:@"%@", response[@"status"]];
//                if ([status isEqualToString:@"1"]) {
//                    self->checkVideoKYC = [response[@"check_video_kyc"] boolValue];
//                    self->detailDic = [response valueForKey:@"msg"];
//                    self->aryDocKey = [[response valueForKey:@"msg"] allKeys];
//
//                    //NSSortDescriptor *sd = [[NSSortDescriptor alloc] initWithKey:nil ascending:YES];
//                    //self->arrUpload = [[self->arrUpload sortedArrayUsingDescriptors:@[sd]] mutableCopy];
//                    self->aryDocKey = [self->aryDocKey sortedArrayUsingSelector:@selector(localizedCaseInsensitiveCompare:)];
//
//                    BOOL allNotUploaded = NO;
//                    NSMutableArray *aryTemp = [NSMutableArray new];
//                    for (NSString *documentType in self->aryDocKey) {
//                        if (![documentType isEqualToString:@"check_video_kyc"]) {
//                            [aryTemp addObject:documentType];
//                        }
//
//                        // get document url
//                        NSString *strDocUrl = @"";
//                        NSString *newString = [self->detailDic valueForKey:documentType];
//                        NSArray *arySplit = [newString componentsSeparatedByString:@"="];
//                        if ([arySplit.lastObject isEqualToString:@""]) {
//                            NSString *newString1 = [newString stringByReplacingOccurrencesOfString:@"=" withString:@""];
//                            //cell.DetailsTxt.text = newString1;
//                            //cell.uploadTxt.text = @"Browse";
//                            //cell.UploadImg.image = [UIImage imageNamed:@"upload-document.png"];
//                        } else {
//                            // NSLog(@"There is no = in the string");
//                            NSRange equalRange1 = [newString rangeOfString:@"=" options:NSBackwardsSearch];
//                            if (equalRange1.location != NSNotFound) {
//                                // Has Image URL
//                                strDocUrl = [newString substringFromIndex:equalRange1.location + equalRange1.length];
//                            }
//                        }
//
//                        // Save to local dictionary
//                        NSMutableDictionary *dic = [[NSUserData GetVideoKycDocUrls] mutableCopy];
//                        if (dic == nil) {
//                            dic = [NSMutableDictionary new];
//                            dic[documentType] = strDocUrl;
//                        } else {
//                            dic[documentType] = strDocUrl;
//                        }
//                        if ([strDocUrl isEqualToString:@""]) {
//                            allNotUploaded = YES;
//                        }
//                        [NSUserData SaveVideoKycDocUrls:dic];
//
//                    }
//                    self->aryDocKey = aryTemp;
//
//                }
//                self.btnNext.hidden = NO;
//                if ([self->strRedirectionStatus isEqualToString:@"0"]) {
//                    self.btnNext.hidden = YES;
//                    self.lblDesc.text = @"Please confirm all the documents have been successfully uploaded.";
//                } else if ([self->strRedirectionStatus isEqualToString:RedirectPhysicalKyc]) {
//                    //
//                } else if ([self->strRedirectionStatus isEqualToString:RedirectVideoKyc]) {
//                    self.lblDesc.text = @"Please confirm all the documents have been successfully uploaded. Click on Next button.";
//                } else if ([self->strRedirectionStatus isEqualToString:RedirectProfileComplete]) {
//
//                }
//                [self.cvUploadDocument reloadData];
//            } else {
//                // NSError *err = (NSError*)response;
//                // NSLog(@"Error %@",err.localizedDescription);
//            }
//            [LoaderVC dismiss];
//        }];
//    } else {
//        NoInternetConnection *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetConnection"];
//        [self presentViewController:svc animated:NO completion:nil];
//    }
}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnContactUs_Tapped:(id)sender {
    ContactUsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnDocument_Tapped:(UIButton*)sender {
    NSString *documentType = [NSString stringWithFormat:@"%@", [aryDocKey objectAtIndex:sender.tag]];
    
    UploadSingleDocVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UploadSingleDocVC"];
    destVC.strDocType = documentType;
    destVC.strBankId = bankId;
    if ([_familyDetailsId isEqualToString:@"yes"]) {
        if ([_familymembID length]) {
            destVC.strUserId = _familymembID;
        } else {
            destVC.strUserId = _FamilyIdMem;
        }
    } else {
        NSDictionary *params = [NSUserData GetLoginData];
        destVC.strUserId = [NSString stringWithFormat:@"%@",[params valueForKey:@"id"]];
    }
    destVC.strProfileStatus = self.strProfileStatus;
    //destVC.strUrl = @"";
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnNext_Tapped:(id)sender {
//    if ([strRedirectionStatus isEqualToString:RedirectPhysicalKyc]) {
//        // Proceed For Physical KYC
//        if (checkVideoKYC) {
//            NewPhyscialKYC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NewPhyscialKYC"];
//            if ([_familyDetailsId isEqualToString:@"yes"]) {
//                if ([_familymembID length]) {
//                    destVC.strUserId = _familymembID;
//                } else {
//                    destVC.strUserId = _FamilyIdMem;
//                }
//            } else {
//                NSDictionary *params = [NSUserData GetLoginDataAPI];
//                destVC.strUserId = [NSString stringWithFormat:@"%@",[params valueForKey:@"id"]];
//            }
//            [self.navigationController pushViewController:destVC animated:YES];
//        }
//    } else if ([strRedirectionStatus isEqualToString:RedirectVideoKyc]) {
//        // Proceed For Video KYC
//        if (checkVideoKYC) {
//            [self openVideoKycScreen];
//        }
//
//    } else if ([strRedirectionStatus isEqualToString:RedirectProfileComplete]) {
//        // Profile Complete
//        ProfileCompletedVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileCompletedVC"];
//        destVC.isNRI = self.isNRI;
//        NSDictionary *params = [NSUserData GetLoginDataAPI];
//        NSString *UserID = [NSString stringWithFormat:@"%@",[params valueForKey:@"id"]];
//        if ([_familyDetailsId isEqualToString:@"yes"]) {
//            if ([_familymembID length]) {
//                destVC.strInvestorId = _familymembID;
//            } else {
//                destVC.strInvestorId =_FamilyIdMem;
//            }
//        } else {
//            destVC.strInvestorId = UserID;
//        }
//        destVC.strMessage = @"Now, you are just a few steps away from creating wealth for your future with mutual fund investments. With the best robotic solutions, we ensure to make your investment journey simplified.";
//        [self presentViewController:destVC animated:YES completion:nil];
//    }
    
    ProfileCompletedVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileCompletedVC"];
    destVC.isNRI = self.isNRI;
    //NSDictionary *params = [NSUserData GetLoginDataAPI];
    //NSString *UserID = [NSString stringWithFormat:@"%@",[params valueForKey:@"id"]];
    //if ([_familyDetailsId isEqualToString:@"yes"]) {
    //    if ([_familymembID length]) {
    //        destVC.strInvestorId = _familymembID;
    //    } else {
    //        destVC.strInvestorId =_FamilyIdMem;
    //    }
    //} else {
    //    destVC.strInvestorId = UserID;
    //}
    destVC.strMessage = @"Now, you are just a few steps away from creating wealth for your future with mutual fund investments. With the best robotic solutions, we ensure to make your investment journey simplified.";
    [self presentViewController:destVC animated:YES completion:nil];
    //[self openVideoKycScreen];
}

-(void)openVideoKycScreen {
    NSString *UserID = @"";
    if ([self->_familyDetailsId isEqualToString:@"yes"]) {
        if ([self->_familymembID length]) {
            UserID = self->_familymembID;
        } else {
            UserID = self->_FamilyIdMem;
        }
    } else {
        NSDictionary *params = [NSUserData GetLoginData];
        UserID = [NSString stringWithFormat:@"%@", [params valueForKey:@"id"]];
    }
    VideoRecordVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VideoRecordVC"];
    destVC.strUserId = UserID;
    destVC.strDocType = @"Selfie Video";
    destVC.strProfileStatus = @"95";
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
