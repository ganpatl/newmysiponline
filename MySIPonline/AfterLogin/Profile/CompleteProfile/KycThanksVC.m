//
//  KycThanksVC.m
//  MySIPonline
//
//  Created by Ganpat on 05/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "KycThanksVC.h"
//#import "FundList.h"

@interface KycThanksVC ()

@end

@implementation KycThanksVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addShadowOnView:self.shadowView1 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:0.5 Radius:0];
    [AppHelper addShadowOnView:self.shadowView2 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:0.5 Radius:0];
    [AppHelper addShadowOnView:self.shadowView3 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:0.5 Radius:0];
    
    if (![self.strUserName isEqualToString:@""]) {
        //self.lblUserName.text = [NSString stringWithFormat:@"Dear %@", self.strUserName];
    } else {
        //self.lblUserName.text = @"";
    }
    if (![self.strMessage isEqualToString:@""]) {
        self.lblMessage.text = self.strMessage;
    } else {
        self.lblMessage.text = @"";
    }
    
}

- (void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [self.navigationController setNavigationBarHidden:YES animated:YES];   //it hides
    self.tabBarController.tabBar.hidden = YES;
}

- (void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [self.navigationController setNavigationBarHidden:NO animated:YES]; // it shows
    self.tabBarController.tabBar.hidden = NO;
}

-(void)dismissMyView {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnEmergencyFund_Tapped:(id)sender {
//    LiquidFundList *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"LiquidFundList"];
//    [self.navigationController pushViewController:destVC animated:YES];
    //self.navigationController.navigationBar.hidden = NO;
}

- (IBAction)btnTaxSavingFunds_Tapped:(id)sender {
//    BestSIPFundList *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"BestSIPFundList"];
//    svc.SavingStringID = @"0";
//    svc.FinalAgeID = @"0";
//    svc.TenureID = @"0";
//    svc.RiskID = @"0";
//    [self.navigationController pushViewController:svc animated:YES];
}

- (IBAction)btnWealthCreation_Tapped:(id)sender {
//    FundList *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"FundList"];
//    svc.navTitle = @"Recommended Fund";
//    [self.navigationController pushViewController:svc animated:YES];
}


@end
