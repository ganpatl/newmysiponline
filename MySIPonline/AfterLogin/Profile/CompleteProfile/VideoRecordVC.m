//
//  VideoRecordVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/02/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "VideoRecordVC.h"
#import "KycFailureVC.h"
//#import "ProfileCompleted/ProfileCompletedVC.h"
#import "VerificationAddressVC.h"
@import  MobileCoreServices;

@interface VideoRecordVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate> {
    UIButton *btnBack;
    NSString *strInvestorName;
    CAShapeLayer *dashedBorder;
}

@end

@implementation VideoRecordVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dashedBorder = [CAShapeLayer layer];
    dashedBorder.strokeColor = UIColor.blackColor.CGColor;
    dashedBorder.fillColor = nil;
    dashedBorder.lineDashPattern = @[@3, @5];
    dashedBorder.path = [UIBezierPath bezierPathWithRect:self.imgDocument.bounds].CGPath;
    [self.imgDocument.layer addSublayer:dashedBorder];
    
    
    [AppHelper addDashedBorderOnView:self.randomNumberBorderView withColor:[UIColor colorWithHexString:@"49D981"]];
    
    self.iconDocument.hidden = NO;
    self.imgDocument.image = nil;
    btnBack.hidden = YES;
    
    [self call_VerifyCaptchaAPI];
}

-(void)viewWillLayoutSubviews {
    dashedBorder.path = [UIBezierPath bezierPathWithRect:self.imgDocument.bounds].CGPath;
}

-(void)showCameraGalleryOptionWithFrontCamera:(BOOL)frontCamera {
    UIAlertController *alertController_Browse = [UIAlertController alertControllerWithTitle:@"" message:@"Choose Image" preferredStyle:UIAlertControllerStyleActionSheet];
    // Gallery
    [alertController_Browse addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    // Camera
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
        [alertController_Browse addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            if (frontCamera) {
                picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            [self presentViewController:picker animated:YES completion:NULL];
        }]];
    }
    [alertController_Browse addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
    }]];
    [self presentViewController:alertController_Browse animated:TRUE completion:nil];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// Button Record Video
- (IBAction)btnTakePicture_Tapped:(UIButton *)sender {
//    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
//        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
//        picker.delegate = self;
//        picker.allowsEditing = YES;
//        NSArray *mediaTypes = @[(NSString*)kUTTypeMovie];
//        picker.mediaTypes = mediaTypes;
//        picker.videoMaximumDuration = 15;
//        picker.sourceType = UIImagePickerControllerSourceTypeCamera;
//        picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
//        [self presentViewController:picker animated:YES completion:NULL];
//    }
    // Open Verification Address Screen
    VerificationAddressVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationAddressVC"];
    destVC.strUserId = self.strUserId;
    //destVC.strVideoUrl = response[@"file"][@"directURL"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Image Picker Controller Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        
        NSURL *localUrl = [info objectForKey:UIImagePickerControllerMediaURL];
        NSString *strurl   = [localUrl path];
        NSData *videoData  = [NSData dataWithContentsOfFile:strurl];
        if ([videoData length] > 5000000) {
            [AppHelper ShowAlert:@"File size greater than 5MB." Title:@"Error" FromVC:self Completion:nil];
        } else {
            //NSURL *localUrl = (NSURL *)[info valueForKey:UIImagePickerControllerMediaURL];
            
            NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
            NSString* videoPath = [NSString stringWithFormat:@"%@/xyz.mp4", [paths objectAtIndex:0]];
            NSURL *outputURL = [NSURL fileURLWithPath:videoPath];
            
            [self convertVideoToLowQuailtyWithInputURL:localUrl outputURL:outputURL handler:^(AVAssetExportSession *exportSession)
             {
                 if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                     NSLog(@"Capture video complete");
                     NSString *stringPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                     NSString *fileName = [stringPath stringByAppendingFormat:@"/xyz.mp4"];
                     NSData *mp4Data = [NSData dataWithContentsOfFile:fileName];
                     NSLog(@"MP4: %@", [[NSByteCountFormatter new] stringFromByteCount:mp4Data.length]);
                     dispatch_async(dispatch_get_main_queue(), ^{
                         [self UploadDocument:mp4Data];
                     });
                     //[self performSelectorOnMainThread:@selector(doneCompressing) withObject:nil waitUntilDone:YES];
                 }
             }];
            
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL outputURL:(NSURL*)outputURL handler:(void (^)(AVAssetExportSession*))handler {
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
        handler(exportSession);
    }];
}

#pragma mark - Web Api Call

-(void)call_VerifyCaptchaAPI {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *dictionary = @{@"client_id":self.strUserId};
//
//        [LoaderVC showWithStatus:@"Please wait as this may take a few minutes..."];
//        [WebServices api_verifyCaptcha:dictionary AndCompletionHandler:^(id response, bool isError) {
//            [LoaderVC dismiss];
//            NSLog(@"%@", response);
//            if (!isError) {
//                int statusVa = [[response valueForKey:@"status"] intValue];
//                NSString *msg = [response valueForKey:@"msg"];
//                if (statusVa == 1) {
//                    [NSUserData SaveCaptchaVerificationData:response];
//                    // display Random Number from Response
//                    self.lblRandomNumber.text = [NSString stringWithFormat:@"%@", response[@"randNum"]];
//                    self.randomNumberBorderView.hidden = NO;
//                    self.lblVideoTopText.hidden = NO;
//
//                } else {
//                    [AppHelper ShowAlert:msg Title:@"" FromVC:self Completion:nil];
//                }
//            } else {
//                [LoaderVC dismiss];
//                NSError *err = (NSError*)response;
//                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
//            }
//        }];
    } else {
        //   [Helper PopUpAlertController:sender :InternetConnectionMessage :@""];
    }
}

-(void)UploadDocument:(id)selectedDoc {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        if (![[NSUserData GetThirdPartyUploadUrl] isEqualToString:@""]) {
//            // Upload to Third-Party Server
//            [LoaderVC showWithStatus:@"Please wait as this may take a few seconds..."];
//            [WebServices api_UploadFile:selectedDoc Parameters:@{@"ttl":@"3 years"} AndCompletionHandler:^(id response, bool isError) {
//                if (isError == NO) {
//                    NSLog(@"%@", response);
//
//                    // Open Verification Address Screen
//                    VerificationAddressVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationAddressVC"];
//                    destVC.strUserId = self.strUserId;
//                    destVC.strVideoUrl = response[@"file"][@"directURL"];
//                    [self.navigationController pushViewController:destVC animated:YES];
//                    [LoaderVC dismiss];
//                } else {
//                    NSLog(@"Failure");
//                    [LoaderVC dismiss];
//                    [Helper ShowAlert:@"Please upload again." FromVC:self Completion:nil];
//                }
//            }];
//        } else {
//            // Upload to MySip Server
//            // [self call_UploadDocumentAPI:selectedDoc ThirdPartyUrl:@""];
//            [AppHelper ShowAlert:@"Video upload URL not found!" Title:@"" FromVC:self Completion:nil];
//        }
    } else {
        // No Network
    }
}

@end
