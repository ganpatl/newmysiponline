//
//  PrimaryBankChangeVC.h
//  MySIPonline
//
//  Created by Ganpat on 15/12/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@protocol PrimaryBankDelegate<NSObject>
@required
-(void)primaryBankAcChanged:(NSInteger)bankTag;
@end

@interface PrimaryBankChangeVC : UIViewController
@property (weak, nonatomic) IBOutlet UIView *popupView;
@property (weak, nonatomic) IBOutlet UIView *bottomShadowView;
@property id<PrimaryBankDelegate> delegate;
@property NSInteger bankTag;
@end

NS_ASSUME_NONNULL_END
