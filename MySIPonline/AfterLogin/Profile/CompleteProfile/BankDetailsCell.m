//
//  BankDetailsCell.m
//  MySIPonline
//
//  Created by Ganpat on 27/02/19.
//

#import "BankDetailsCell.h"

@implementation BankDetailsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    
    [AppHelper addShadowOnView:self.shadowView1 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:0.7 Radius:1];
    self.btnSaving1.layer.cornerRadius = 3;
    self.btnNri1.layer.cornerRadius = 3;
    self.btnNro1.layer.cornerRadius = 3;
    self.btnSaving2.layer.cornerRadius = 3;
    self.btnNri2.layer.cornerRadius = 3;
    self.btnNro2.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setPrimaryBank1ViewHidden:(BOOL)doHide {
    if (doHide) {
        self.primaryBank1HeightConstraint.constant = 0;
        self.primaryBank1View.hidden = YES;
    } else {
        self.primaryBank1HeightConstraint.constant = 60;
        self.primaryBank1View.hidden = NO;
    }
}

-(void)setAddNewBankAccountViewHidden:(BOOL)doHide {
    if (doHide) {
        self.addNewBankAccountHeightConstraint.constant = 0;
        self.addNewBankAccountView.hidden = YES;
    } else {
        self.addNewBankAccountHeightConstraint.constant = 43;
        self.addNewBankAccountView.hidden = NO;
    }
}

-(void)setPrimaryBank2ViewHidden:(BOOL)doHide {
    if (doHide) {
        self.primaryBank2HeightConstraint.constant = 0;
        self.primaryBank2View.hidden = YES;
    } else {
        self.primaryBank2HeightConstraint.constant = 60;
        self.primaryBank2View.hidden = NO;
    }
}

-(void)setIfscCode2ViewHidden:(BOOL)doHide {
    if (doHide) {
        self.ifscCode2HeightConstraint.constant = 0;
        self.ifscCode2TopConstraint.constant = 0;
        self.ifscCode2View.hidden = YES;
    } else {
        self.ifscCode2HeightConstraint.constant = 60;
        self.ifscCode2TopConstraint.constant = 8;
        self.ifscCode2View.hidden = NO;
    }
}

-(void)setBankAccount2ViewHidden:(BOOL)doHide {
    if (doHide) {
        self.bankAccount2HeightConstraint.constant = 0;
        self.bankAccount2TopConstraint.constant = 0;
        self.bankAccount2View.hidden = YES;
    } else {
        self.bankAccount2HeightConstraint.constant = 60;
        self.bankAccount2TopConstraint.constant = 8;
        self.bankAccount2View.hidden = NO;
    }
}

-(void)setConfirmBankAccount2ViewHidden:(BOOL)doHide {
    if (doHide) {
        self.confirmBankAccount2HeightConstraint.constant = 0;
        self.confirmBankAccount2TopConstraint.constant = 0;
        self.confirmBankAccount2View.hidden = YES;
    } else {
        self.confirmBankAccount2HeightConstraint.constant = 60;
        self.confirmBankAccount2TopConstraint.constant = 8;
        self.confirmBankAccount2View.hidden = NO;
    }
}

-(void)setAccountType2ViewHidden:(BOOL)doHide {
    if (doHide) {
        self.accountType2HeightConstraint.constant = 0;
        self.accountTypeTopConstraint.constant = 0;
        self.accountType2View.hidden = YES;
    } else {
        self.accountType2HeightConstraint.constant = 60;
        self.accountTypeTopConstraint.constant = 8;
        self.accountType2View.hidden = NO;
    }
}

-(void)setSecondBankHidden:(BOOL)doHide {
    [self setIfscCode2ViewHidden:doHide];
    [self setBankAccount2ViewHidden:doHide];
    [self setConfirmBankAccount2ViewHidden:doHide];
    [self setAccountType2ViewHidden:doHide];
}

@end
