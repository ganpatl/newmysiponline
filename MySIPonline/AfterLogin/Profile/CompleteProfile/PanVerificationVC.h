//
//  PanVerificationVC.h
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import <UIKit/UIKit.h>
@import SkyFloatingLabelTextField;

NS_ASSUME_NONNULL_BEGIN

@interface PanVerificationVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet SkyFloatingLabelTextField *tfPanCard;
@property (weak, nonatomic) IBOutlet UILabel *lblErrPanCard;

@end

NS_ASSUME_NONNULL_END
