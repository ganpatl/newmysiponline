//
//  DocsGridVC.h
//  MySIPonline
//
//  Created by Ganpat on 28/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DocsGridVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UICollectionView *cvUploadDocument;
@property (weak, nonatomic) IBOutlet UIButton *btnNext;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@property (strong, nonatomic) NSString *holdingNature;
@property (strong,nonatomic) NSString *FamilyIdMem;
@property (strong,nonatomic) NSString *familyDetailsId;
@property (strong, nonatomic) NSString *bankFamilyId;
@property (strong, nonatomic) NSString *familymembID;

@property NSString *strInvestorName;
@property NSString *strProfileStatus;
@property BOOL isNRI;

@end

NS_ASSUME_NONNULL_END
