//
//  BankDetailsVC.h
//  MySIPonline
//
//  Created by Ganpat on 27/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BankDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblBankDetails;

@end

NS_ASSUME_NONNULL_END
