//
//  UploadSingleDocVC.h
//  MySIPonline
//
//  Created by Ganpat on 28/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface UploadSingleDocVC : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgDocument;
@property (weak, nonatomic) IBOutlet UIImageView *iconDocument;
@property (weak, nonatomic) IBOutlet UILabel *lblDocument;
@property (weak, nonatomic) IBOutlet UIButton *btnRemoveDocument;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDocument;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectDocument2;
@property (weak, nonatomic) IBOutlet UIView *signButtonView;

@property NSString *strProfileStatus;
@property NSString *strDocType;
//@property NSString *strUrl;
@property NSString *strBankId;
@property NSString *strUserId;
@property NSString *strSignature;
@property BOOL isDocUploaded;
@property BOOL isKycVerified;


@end

NS_ASSUME_NONNULL_END
