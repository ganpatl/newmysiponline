//
//  ProfileCompleteTopCell.h
//  MySIPonline
//
//  Created by Ganpat on 02/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileCompleteTopCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgStatus;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestoCode;
@property (weak, nonatomic) IBOutlet UIButton *btnDesc;

@end

NS_ASSUME_NONNULL_END
