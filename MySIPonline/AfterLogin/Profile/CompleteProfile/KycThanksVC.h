//
//  KycThanksVC.h
//  MySIPonline
//
//  Created by Ganpat on 05/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface KycThanksVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property NSString *strUserName;
@property NSString *strMessage;
@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UIView *shadowView3;

@end

NS_ASSUME_NONNULL_END
