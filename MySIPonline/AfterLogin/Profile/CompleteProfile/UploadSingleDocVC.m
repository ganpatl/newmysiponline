//
//  UploadSingleDocVC.m
//  MySIPonline
//
//  Created by Ganpat on 28/02/19.
//

#import "UploadSingleDocVC.h"
#import "DocsGridVC.h"
#import "PECropViewController.h"
#import "CreateSignatureVC.h"
#import "KycFailureVC.h"
//#import "ProfileCompleted/ProfileCompletedVC.h"
//#import "VerificationAddressVC.h"
@import  MobileCoreServices;
@import WebKit;

@interface UploadSingleDocVC () <UINavigationControllerDelegate, UIImagePickerControllerDelegate, PECropViewControllerDelegate, CreateSignatureDelegate, WKScriptMessageHandler> {
    UIButton *btnBack;
    CAShapeLayer *dashedBorder;
    NSString *strInvestorName;
    NSString *strRandNumber;
}
@end

@implementation UploadSingleDocVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    dashedBorder = [CAShapeLayer layer];
    dashedBorder.strokeColor = UIColor.blackColor.CGColor;
    dashedBorder.fillColor = nil;
    dashedBorder.lineDashPattern = @[@3, @5];
    dashedBorder.path = [UIBezierPath bezierPathWithRect:self.imgDocument.bounds].CGPath;
    [self.imgDocument.layer addSublayer:dashedBorder];
    
    self.btnRemoveDocument.hidden = !self.isDocUploaded;
    
    //NSDictionary *dicVerifyCaptcha = [NSUserData GetCaptchaVerificationData];
    //strRandNumber = dicVerifyCaptcha[@"randNum"];
    
    self.iconDocument.hidden = NO;
    self.imgDocument.image = nil;
    
    [self.btnSelectDocument setTitle:@"Take a Picture" forState:UIControlStateNormal];
    if ([self.strDocType.lowercaseString containsString:@"selfie image"]) {
        self.title = @"Photo";
        [self.btnSelectDocument setTitle:@"Take a Picture" forState:UIControlStateNormal];
        if (self.isDocUploaded) {
            self.lblDocument.text = @"Upload your most recent passport size photograph.";
        } else {
            self.lblDocument.text = @"Make sure that the image you Upload have a good visibility.";
        }
        self.iconDocument.image = [UIImage imageNamed:@"photo_capture"];
        
    } else if ([self.strDocType.lowercaseString containsString:@"selfie video"]) {
        btnBack.hidden = YES;
        self.title = @"Video";
        self.lblDocument.text = @"Tap on the Record Video button & record a 15 seconds video with your face clearly visible";
        self.iconDocument.image = [UIImage imageNamed:@"video_record"];
        
        [self.btnSelectDocument setTitle:@"Record Video" forState:UIControlStateNormal];
        [self call_VerifyCaptchaAPI];
        
    } else if ([self.strDocType.lowercaseString containsString:@"pan card"]) {
        self.title = @"Pan Card";
        self.lblDocument.text = @"All the details on Pan Card should be clearly visible.";
        self.iconDocument.image = [UIImage imageNamed:@"pan card"];
        
    } else if ([self.strDocType.lowercaseString containsString:@"aadhaar back"]) {
        self.title = @"Aadhaar Back";
        self.lblDocument.text = @"Upload a clear image of back side of your Aadhaar Card.";
        self.iconDocument.image = [UIImage imageNamed:@"aadhar_back"];
        
    } else if ([self.strDocType.lowercaseString containsString:@"aadhaar front"]) {
        self.title = @"Aadhaar Front";
        self.lblDocument.text = @"Upload a clear image of front side of your Aadhaar Card.";
        self.iconDocument.image = [UIImage imageNamed:@"aadhhar_front"];
        
    } else if ([self.strDocType.lowercaseString containsString:@"bank statement"]) {
        self.title = @"Cancelled Cheque";
        self.lblDocument.text = @"Upload a photo of a canceled cheque and make sure all the details are clear.";
        self.iconDocument.image = [UIImage imageNamed:@"cancelledcheque"];
        
    } else if ([self.strDocType.lowercaseString containsString:@"signature"]) {
        self.title = @"Signature";
        self.lblDocument.text = @"Put your signature on a plane white paper and upload a picture of the same in portrait mode only.";
        self.iconDocument.image = [UIImage imageNamed:@"signature"];
        self.signButtonView.hidden = NO;
        if (self.strSignature != nil && self.strSignature.length > 0) {
            NSURL *imageUrl = [NSURL URLWithString:self.strSignature];
            [self.imgDocument sd_setImageWithURL:imageUrl placeholderImage:[UIImage imageNamed:@"default_banner_ic.png"]];
            self.iconDocument.hidden = YES;
        }
        
    } else if ([self.strDocType.lowercaseString containsString:@"birth"]) {
        self.title = @"Birth Certificate";
        self.lblDocument.text = @"Upload a clear image of back side of Birth Certificate.";
        self.iconDocument.image = [UIImage imageNamed:@"winner"];
        
    } else {
        // Any Other Document
        self.title = self.strDocType.capitalizedString;
    }
}

-(void)viewWillLayoutSubviews {
    dashedBorder.path = [UIBezierPath bezierPathWithRect:self.imgDocument.bounds].CGPath;
}

-(void)showCameraGalleryOptionWithFrontCamera:(BOOL)frontCamera {
    UIAlertController *alertController_Browse = [UIAlertController alertControllerWithTitle:@"" message:@"Choose Image" preferredStyle:UIAlertControllerStyleActionSheet];
    // Gallery
    [alertController_Browse addAction:[UIAlertAction actionWithTitle:@"Gallery" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
        UIImagePickerController *picker = [[UIImagePickerController alloc] init];
        picker.delegate = self;
        //picker.allowsEditing = YES;
        picker.sourceType = UIImagePickerControllerSourceTypePhotoLibrary;
        [self presentViewController:picker animated:YES completion:NULL];
    }]];
    // Camera
    if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
        [alertController_Browse addAction:[UIAlertAction actionWithTitle:@"Camera" style:UIAlertActionStyleDefault handler:^(UIAlertAction * _Nonnull action){
            UIImagePickerController *picker = [[UIImagePickerController alloc] init];
            picker.delegate = self;
            //picker.allowsEditing = YES;
            picker.sourceType = UIImagePickerControllerSourceTypeCamera;
            if (frontCamera) {
                picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
            }
            [self presentViewController:picker animated:YES completion:NULL];
        }]];
    }
    [alertController_Browse addAction:[UIAlertAction actionWithTitle:@"Cancel" style:UIAlertActionStyleCancel handler:^(UIAlertAction * _Nonnull action){
    }]];
    [self presentViewController:alertController_Browse animated:TRUE completion:nil];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self CompleteProfileData];
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRemoveDocument_Tapped:(UIButton *)sender {
    self.btnRemoveDocument.hidden = YES;
    self.imgDocument.image = nil;
    self.iconDocument.hidden = NO;
    [self.btnSelectDocument setTitle:@"Take a Picture" forState:UIControlStateNormal];
    [self.btnSelectDocument2 setTitle:@"Take a Picture" forState:UIControlStateNormal];
    if ([self.strDocType.lowercaseString containsString:@"signature"]) {
        self.signButtonView.hidden = NO;
    }
}

// Button Select Document
- (IBAction)btnTakePicture_Tapped:(UIButton *)sender {
    if ([sender.titleLabel.text.lowercaseString containsString:@"confirm"]) {
        [self UploadDocument:self.imgDocument.image];
        //TODO: temp redirect to Grid screen
        DocsGridVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DocsGridVC"];
        [self.navigationController pushViewController:destVC animated:YES];
        
        
    } else {
        if ([self.strDocType.lowercaseString containsString:@"image"]) {
            [self showCameraGalleryOptionWithFrontCamera:YES];
            
        } else if ([self.strDocType.lowercaseString containsString:@"video"]) {
            if ([UIImagePickerController isCameraDeviceAvailable:UIImagePickerControllerCameraDeviceRear]) {
                UIImagePickerController *picker = [[UIImagePickerController alloc] init];
                picker.delegate = self;
                picker.allowsEditing = YES;
                NSArray *mediaTypes = @[(NSString*)kUTTypeMovie];
                picker.mediaTypes = mediaTypes;
                picker.videoMaximumDuration = 15;
                picker.sourceType = UIImagePickerControllerSourceTypeCamera;
                picker.cameraDevice = UIImagePickerControllerCameraDeviceFront;
                [self presentViewController:picker animated:YES completion:NULL];
            }
            
        } else if ([self.strDocType.lowercaseString containsString:@"pan card"]) {
            [self showCameraGalleryOptionWithFrontCamera:NO];
            
        } else if ([self.strDocType.lowercaseString containsString:@"aadhaar"]) {
            [self showCameraGalleryOptionWithFrontCamera:NO];
            
        } else if ([self.strDocType.lowercaseString containsString:@"bank statement"]) {
            [self showCameraGalleryOptionWithFrontCamera:NO];
            
        } else if ([self.strDocType.lowercaseString containsString:@"signature"]) {
            [self showCameraGalleryOptionWithFrontCamera:NO];
            
        } else  {
            // Any Other Docs
            [self showCameraGalleryOptionWithFrontCamera:NO];
        }
    }
}

- (IBAction)btnSignHere_Tapped:(id)sender {
    CreateSignatureVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CreateSignatureVC"];
    destVC.delegate = self;
    [self presentViewController:destVC animated:YES completion:nil];
}

#pragma mark - Create Signature Delegate
- (void)signatureSaved:(UIImage *)imgSignature {
    [self openEditor:imgSignature];
}

#pragma mark - Image Picker Controller Delegate

- (void)imagePickerController:(UIImagePickerController *)picker didFinishPickingMediaWithInfo:(NSDictionary *)info {
    [picker dismissViewControllerAnimated:YES completion:^{
        if ([self.strDocType.lowercaseString containsString:@"video"]) {
            NSURL *localUrl = [info objectForKey:UIImagePickerControllerMediaURL];
            NSString *strurl   = [localUrl path];
            NSData *videoData  = [NSData dataWithContentsOfFile:strurl];
            if ([videoData length] > 5000000) {
                [AppHelper ShowAlert:@"File size greater than 5MB." Title:@"Error" FromVC:self Completion:nil];
            } else {
                NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
                NSString* videoPath = [NSString stringWithFormat:@"%@/xyz.mp4", [paths objectAtIndex:0]];
                NSURL *outputURL = [NSURL fileURLWithPath:videoPath];
                
                [self convertVideoToLowQuailtyWithInputURL:localUrl outputURL:outputURL handler:^(AVAssetExportSession *exportSession)
                 {
                     if (exportSession.status == AVAssetExportSessionStatusCompleted) {
                         NSLog(@"Capture video complete");
                         NSString *stringPath = [NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES) objectAtIndex:0];
                         NSString *fileName = [stringPath stringByAppendingFormat:@"/xyz.mp4"];
                         NSData *mp4Data = [NSData dataWithContentsOfFile:fileName];
                         NSLog(@"MP4: %@", [[NSByteCountFormatter new] stringFromByteCount:mp4Data.length]);
                         dispatch_async(dispatch_get_main_queue(), ^{
                             [self UploadDocument:mp4Data];
                         });
                     }
                 }];
                
            }
            
        } else {
            [self openEditor:info[UIImagePickerControllerOriginalImage]];
        }
    }];
}

- (void)imagePickerControllerDidCancel:(UIImagePickerController *)picker {
    [picker dismissViewControllerAnimated:YES completion:NULL];
}

- (void)convertVideoToLowQuailtyWithInputURL:(NSURL*)inputURL outputURL:(NSURL*)outputURL handler:(void (^)(AVAssetExportSession*))handler {
    [[NSFileManager defaultManager] removeItemAtURL:outputURL error:nil];
    AVURLAsset *asset = [AVURLAsset URLAssetWithURL:inputURL options:nil];
    AVAssetExportSession *exportSession = [[AVAssetExportSession alloc] initWithAsset:asset presetName:AVAssetExportPresetPassthrough];
    exportSession.outputURL = outputURL;
    exportSession.outputFileType = AVFileTypeMPEG4;
    [exportSession exportAsynchronouslyWithCompletionHandler:^(void) {
        handler(exportSession);
    }];
}

#pragma mark CropView

- (void)cropViewControllerDidCancel:(PECropViewController *)controller {
    [controller dismissViewControllerAnimated:YES completion:NULL];
}

- (void)cropViewController:(PECropViewController *)controller didFinishCroppingImage:(UIImage *)croppedImage transform:(CGAffineTransform)transform cropRect:(CGRect)cropRect {
    [controller dismissViewControllerAnimated:YES completion:NULL];
    UIImage *chosenCustomerImage = croppedImage;
    chosenCustomerImage = [AppHelper scaleAndRotateImage:chosenCustomerImage];
    CGFloat compression = 1.0f;
    NSData *imageData = UIImageJPEGRepresentation(chosenCustomerImage, compression);
    //NSLog(@"%lu",(unsigned long)[imageData length]);
    
    self.iconDocument.hidden = YES;
    self.btnRemoveDocument.hidden = NO;
    self.imgDocument.image = [UIImage imageWithData:imageData];
    [self.btnSelectDocument setTitle:@"Confirm" forState:UIControlStateNormal];
    [self.btnSelectDocument2 setTitle:@"Confirm" forState:UIControlStateNormal];
    self.signButtonView.hidden = YES;
}

-(void)openEditor:(UIImage*)temImage; {
    PECropViewController *controller = [[PECropViewController alloc] init];
    controller.delegate = self;
    controller.image = temImage;
    UIImage *image = temImage;
    CGFloat width = image.size.width;
    CGFloat height = image.size.height;
    CGFloat length = MIN(width, height);
    controller.imageCropRect = CGRectMake((width - length) / 2, (height - length) / 2, length, length);
    
    UINavigationController *navigationController = [[UINavigationController alloc] initWithRootViewController:controller];
    [self presentViewController:navigationController animated:YES completion:NULL];
}

#pragma mark - Web Api Call

-(void)call_VerifyCaptchaAPI {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *dictionary = @{@"client_id":self.strUserId};
//        [LoaderVC showWithStatus:@"Please wait as this may take a few minutes..."];
//        [WebServices api_verifyCaptcha:dictionary AndCompletionHandler:^(id response, bool isError) {
//            [LoaderVC dismiss];
//            NSLog(@"%@", response);
//            if (!isError) {
//                int statusVa = [[response valueForKey:@"status"] intValue];
//                NSString *msg = [response valueForKey:@"message"];
//                if (statusVa == 1) {
//                    [NSUserData SaveCaptchaVerificationData:response];
//                    // display Random Number from Response
//                    self->strRandNumber = response[@"randNum"];
//                    NSString *strFullMsg = [NSString stringWithFormat:@"Upload a video with 15 seconds of self recording with speaking number %@. Size should not exceed 5 Mb.", self->strRandNumber];
//                    if (self->strRandNumber != nil) {
//                        NSRange range = [strFullMsg rangeOfString:self->strRandNumber];
//                        NSMutableAttributedString *attributedString = [[NSMutableAttributedString alloc] initWithString:strFullMsg];
//                        [attributedString addAttribute:NSForegroundColorAttributeName value:[UIColor colorWithHexString:@"745BCC"] range:range];
//                        self.lblDocument.attributedText = attributedString;
//                    }
//
//                } else {
//                    [AppHelper ShowAlert:msg Title:@"" FromVC:self Completion:nil];
//                }
//            } else {
//                [LoaderVC dismiss];
//                NSError *err = (NSError*)response;
//                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
//            }
//        }];
    } else {
        //   [Helper PopUpAlertController:sender :InternetConnectionMessage :@""];
    }
}

-(void)UploadDocument:(id)selectedDoc {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        if (![[NSUserData GetThirdPartyUploadUrl] isEqualToString:@""]) {
//            // Upload to Third-Party Server
//            [LoaderVC showWithStatus:@"Please wait as this may take a few seconds..."];
//            [WebServices api_UploadFile:selectedDoc Parameters:@{@"ttl":@"3 years"} AndCompletionHandler:^(id response, bool isError) {
//                if (isError == NO) {
//                    NSLog(@"%@", response);
//
//                    if ([self.strDocType.lowercaseString containsString:@"video"]) {
//                        VerificationAddressVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"VerificationAddressVC"];
//                        destVC.strUserId = self.strUserId;
//                        destVC.strVideoUrl = response[@"file"][@"directURL"];
//                        [self.navigationController pushViewController:destVC animated:YES];
//                        [LoaderVC dismiss];
//                    } else {
//                        // Upload to MySip Server
//                        [self call_UploadDocumentAPI:selectedDoc ThirdPartyUrl:response[@"file"][@"directURL"]];
//                    }
//                } else {
//                    NSLog(@"Failure");
//                    [LoaderVC dismiss];
//                    [AppHelper ShowAlert:@"Please upload again." Title:@"" FromVC:self Completion:nil];
//                }
//            }];
//        } else {
//            // Upload to MySip Server
//            [self call_UploadDocumentAPI:selectedDoc ThirdPartyUrl:@""];
//        }
    } else {
        // No Network
    }
}

-(void)call_UploadDocumentAPI:(id)selectedDoc ThirdPartyUrl:(NSString*)strThirdPartyUrl {
    NSString *base64StringImage;
    if ([selectedDoc isKindOfClass:[UIImage class]]) {
        base64StringImage = [UIImagePNGRepresentation(selectedDoc)
                             base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    } else {
        base64StringImage = [selectedDoc base64EncodedStringWithOptions:NSDataBase64Encoding64CharacterLineLength];
    }
    
//    NSMutableDictionary *newDict = [[NSMutableDictionary alloc] init];
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *dictionary;
//        if ([self.strDocType.lowercaseString containsString:@"bank statement"] || [self.strDocType.lowercaseString containsString:@"signature"]) {
//            dictionary = @{@"user_id":self.strUserId, @"document_type":self.strDocType, @"bank_id":self.strBankId, @"document":base64StringImage, @"profile_status":self.strProfileStatus, @"directURL":strThirdPartyUrl};
//        } else {
//            dictionary = @{@"user_id":self.strUserId, @"document_type":self.strDocType, @"document":base64StringImage, @"profile_status":self.strProfileStatus, @"directURL":strThirdPartyUrl};
//        }
//
//        [LoaderVC showWithStatus:@"Please wait as this may take a few seconds..."];
//        [WebServices api_uploadDocumentsData:dictionary AndCompletionHandler:^(id response, bool isError) {
//            if (!isError) {
//                int statusVa = [[response valueForKey:@"status"] intValue];
//                NSString *msg = [response valueForKey:@"msg"];
//                self->strInvestorName = response[@"name"];
//                if (statusVa == 1) {
//                    // Save return url of document
//                    if ([AppHelper isNotNull:response[@"returnurl"]]) {
//                        NSMutableDictionary *dic = [[NSUserData GetVideoKycDocUrls] mutableCopy];
//                        if (dic == nil) {
//                            dic = [NSMutableDictionary new];
//                            dic[self.strDocType] = response[@"returnurl"];
//                        } else {
//                            dic[self.strDocType] = response[@"returnurl"];
//                        }
//                        [NSUserData SaveVideoKycDocUrls:dic];
//                    }
//
//                    if ([self.strDocType.lowercaseString containsString:@"image"]) {
//                        NSDictionary *params = [NSUserData GetLoginDataAPI];
//                        NSString *loginUserId = [NSString stringWithFormat:@"%@", [params valueForKey:@"id"]];
//                        if ([loginUserId isEqualToString:self.strUserId]) {
//                            NSString *urlLink = [response valueForKey:@"returnurl"];
//                            self.lblDocument.text = @"Upload your most recent passport size photograph.";
//                            [self.btnSelectDocument setTitle:@"Confirm" forState:UIControlStateNormal];
//                            [self.btnSelectDocument2 setTitle:@"Confirm" forState:UIControlStateNormal];
//
//                            [newDict addEntriesFromDictionary:params];
//                            [newDict setObject:urlLink forKey:@"image"];
//                            [NSUserData SaveLoginDataAPI:newDict];
//                        }
//                    }
//
//                    if ([self.strDocType.lowercaseString containsString:@"signature"]) {
//                        // Open Hidden WebView
//
//                        // Create Web View
//                        CGRect wFrame = self.view.frame;
//                        wFrame = [AppHelper getSafeFrameWithUpdatedHeightTop:wFrame];
//                        wFrame = [AppHelper getSafeFrameWithUpdatedHeightBottom:wFrame];
//                        wFrame.size.height -= self.tabBarController.tabBar.frame.size.height;
//                        WKWebViewConfiguration *configuration = [[WKWebViewConfiguration alloc] init];
//                        [configuration.userContentController addScriptMessageHandler:self name:@"MySipOnline"];
//                        WKWebView *webView = [[WKWebView alloc] initWithFrame:wFrame configuration:configuration];
//                        [self.view addSubview:webView];
//                        [webView setHidden:YES];
//                        NSString *strUrl = [NSString stringWithFormat:@"%@aof.php?id=%@", API_BLOGURL, self.strUserId];
//                        NSURL *url = [NSURL URLWithString:strUrl];
//                        NSURLRequest *requestObj = [NSURLRequest requestWithURL:url];
//                        [webView loadRequest:requestObj];
//
//                    } else {
//                        [LoaderVC dismiss];
//                        [AppHelper ShowAlert:msg Title:@"" FromVC:self Completion:^(UIAlertAction *action) {
//                            if (self.isKycVerified) {
//                                [self.navigationController popToRootViewControllerAnimated:YES];
//                            } else {
//                                [self dismissMyView];
//                            }
//                        }];
//                    }
//
//                } else {
//                    [LoaderVC dismiss];
//                    [AppHelper ShowAlert:msg Title:@"" FromVC:self Completion:nil];
//                }
//            } else {
//                [LoaderVC dismiss];
//                NSError *err = (NSError*)response;
//                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
//            }
//            //[LoaderVC dismiss];
//        }];
    } else {
        //   [Helper PopUpAlertController:sender :InternetConnectionMessage :@""];
    }
}

-(void)CompleteProfileData {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        NSDictionary *params = @{@"client_id":self.strUserId};
//        [LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_CompleteProfileData:params AndCompletionHandler:^(id response, bool isError) {
//            if (!isError) {
//                NSDictionary *personalDetails = [response valueForKey:@"personal"];
//                if ([Helper isNotNull:personalDetails]) {
//                    [NSUserData SaveProfileDetails:response];
//                    [self.navigationController popViewControllerAnimated:YES];
//                }
//            } else {
//            }
//            [LoaderVC dismiss];
//        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma Mark - WebView Script Message Receive Method

-(void)userContentController:(WKUserContentController *)userContentController didReceiveScriptMessage:(WKScriptMessage *)message {
    NSDictionary *sentData = (NSDictionary *)message.body;
    // window.webkit.messageHandlers.MySipOnline.postMessage({"message":"Button Tapped"});
    //NSLog(@"\n\nMessage received: %@", sentData);
    
    if ([AppHelper isNotNull:sentData[@"message"]]) {
        if ([sentData[@"message"] isEqualToString:@"SignatureUploadSuccess"]) {
            [LoaderVC dismiss];
            [AppHelper ShowAlert:@"File has been uploaded" Title:@"" FromVC:self Completion:^(UIAlertAction *action) {
                if (self.isKycVerified) {
//                    ProfileCompletedVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ProfileCompletedVC"];
//                    destVC.strInvestorId = self.strUserId;
//                    destVC.strMessage = @"Now, you are just a few steps away from creating wealth for your future with mutual fund investments. With the best robotic solutions, we ensure to make your investment journey simplified.";
//                    [self presentViewController:destVC animated:YES completion:nil];
                } else {
                    [self btnBack_Tapped:nil];
                }
            }];
        }
    }
}

@end
