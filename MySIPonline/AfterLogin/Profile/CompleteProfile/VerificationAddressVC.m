//
//  VerificationAddressVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/02/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "VerificationAddressVC.h"
#import "KycThanksVC.h"
#import "KycFailureVC.h"

@interface VerificationAddressVC () {
    UIToolbar *ViewForDoneButtonOnKeyboard;
    NSDictionary *dicCaptchaVerification;
}
@end

@implementation VerificationAddressVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    ViewForDoneButtonOnKeyboard = [[UIToolbar alloc] init];
    [ViewForDoneButtonOnKeyboard sizeToFit];
    UIBarButtonItem *btnDoneOnKeyboard = [[UIBarButtonItem alloc] initWithTitle:@"Done"
                                                                          style:UIBarButtonItemStylePlain
                                                                         target:self
                                                                         action:@selector(doneBtnFromKeyboardClicked:)];
    [ViewForDoneButtonOnKeyboard setItems:[NSArray arrayWithObjects:btnDoneOnKeyboard, nil]];
    self.tvAddress.inputAccessoryView = ViewForDoneButtonOnKeyboard;
    
    //dicCaptchaVerification = [NSUserData GetCaptchaVerificationData];
    //self.tvAddress.text = dicCaptchaVerification[@"updatedata1"][@"address"];
}

-(void)dismissMyView {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Web api call

-(void)call_UpdateFormAPI {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        [LoaderVC showWithStatus:@"Please wait as this may take a few seconds..."];
//
//        NSError * err;
//        NSDictionary *dictUpdateData = dicCaptchaVerification[@"updatedata1"];
//        NSData * jsonData = [NSJSONSerialization dataWithJSONObject:dictUpdateData options:0 error:&err];
//        NSString * myString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
//        //NSLog(@"%@", myString);
//
//        NSDictionary *params = @{@"client_id":self.strUserId, @"merchantId":dicCaptchaVerification[@"merchantId"], @"accesstoken":dicCaptchaVerification[@"accesstoken"], @"transactionId":dicCaptchaVerification[@"transactionId"], @"videoUrl":self.strVideoUrl, @"signatureUrl":dicCaptchaVerification[@"signatureUrl"], @"photoUrl":dicCaptchaVerification[@"photoUrl"], @"updatedata1":myString, @"confirmaddress":[Helper trimString:self.tvAddress.text]};
//        NSLog(@"%@", params);
//
//        [WebServices api_updateForm:params AndCompletionHandler:^(id response, bool isError) {
//            [LoaderVC dismiss];
//            NSLog(@"%@", response);
//
//            if (!isError) {
//                int statusVa = [[response valueForKey:@"status"] intValue];
//                NSString *msg = [response valueForKey:@"msg"];
//                if (statusVa == 1) {
//                    KycThanksVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"KycThanksVC"];
//                    destVC.strMessage = msg;
//                    destVC.strUserName = self->dicCaptchaVerification[@"updatedata1"][@"pan"][@"name"];
//                    [self.navigationController pushViewController:destVC animated:YES];
//
//                } else {
//                    KycFailureVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"KycFailureVC"];
//                    destVC.strMessage = @"The details you provided are incorrect.";
//                    [self.navigationController pushViewController:destVC animated:YES];
//                }
//            } else {
//                [AppHelper ShowAlert:@"Something went wrong." Title:@"" FromVC:self Completion:nil];
//            }
//        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - Button Tap Evens

-(void)doneBtnFromKeyboardClicked:(id)sender {
    [self.view endEditing:YES];
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnVerifyAndProceed_Tapped:(id)sender {
//    if (self.tvAddress.text.length > 0) {
//        [self call_UpdateFormAPI];
//    } else {
//        [AppHelper ShowAlert:@"Address is compulsory!" Title:@"" FromVC:self Completion:nil];
//    }
    
    BOOL isSuccess = NO;
    if (isSuccess) {
        KycThanksVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"KycThanksVC"];
        destVC.strMessage = @"KYC registration process will be completed in 3-4 working days. Meanwhile, you can start investing in mutual funds. However, in case of KYC rejection, your transaction(s) will also be rejected.";
        //destVC.strUserName = self->dicCaptchaVerification[@"updatedata1"][@"pan"][@"name"];
        [self.navigationController pushViewController:destVC animated:YES];
    } else {
        KycFailureVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"KycFailureVC"];
        destVC.strMessage = @"The details you provided are incorrect.";
        [self.navigationController pushViewController:destVC animated:YES];
    }
}

@end
