//
//  KycFailureVC.m
//  MySIPonline
//
//  Created by Ganpat on 05/11/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "KycFailureVC.h"

@interface KycFailureVC ()

@end

@implementation KycFailureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.lblMessage.text = self.strMessage;
    
    self.btnGotIt.layer.cornerRadius = self.btnGotIt.frame.size.height / 2;
}

-(void)dismissMyView {
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Button Tap Events

- (IBAction)btnGotIt_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


@end
