//
//  CreateSignatureVC.m
//  MySIPonline
//
//  Created by Ganpat on 26/07/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "CreateSignatureVC.h"

@interface CreateSignatureVC ()

@end

@implementation CreateSignatureVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationLandscapeLeft) forKey:@"orientation"];
    ((AppDelegate *)[UIApplication sharedApplication].delegate).orientationMask = UIInterfaceOrientationMaskLandscape;
    [UIViewController attemptRotationToDeviceOrientation];
}

-(void)viewWillTransitionToSize:(CGSize)size withTransitionCoordinator:(id<UIViewControllerTransitionCoordinator>)coordinator {
    [coordinator animateAlongsideTransition:^(id<UIViewControllerTransitionCoordinatorContext> context) {
         //UIInterfaceOrientation orientation = [[UIApplication sharedApplication] statusBarOrientation];
         // do whatever
         [AppHelper addDashedBorderOnView:self.signatureView withColor:UIColor.blackColor];
     } completion:^(id<UIViewControllerTransitionCoordinatorContext> context) {
         
     }];
    
    [super viewWillTransitionToSize:size withTransitionCoordinator:coordinator];
}

-(void)viewWillDisappear:(BOOL)animated {
    [super viewWillDisappear:animated];
    [[UIDevice currentDevice] setValue:@(UIInterfaceOrientationPortrait) forKey:@"orientation"];
    ((AppDelegate *)[UIApplication sharedApplication].delegate).orientationMask = UIInterfaceOrientationMaskPortrait;
    [UIViewController attemptRotationToDeviceOrientation];
}

#pragma mark - Button Tap Events

- (IBAction)btnRubber_Tapped:(id)sender {
    [_signatureView erase];
}

- (IBAction)btnSave_Tapped:(id)sender {
    _btnSave.hidden = YES;
    _btnCancel.hidden = YES;
    _btnRubber.hidden = YES;
    UIImage *imgTemp = [_signatureView getSignatureImage];
    [self dismissViewControllerAnimated:YES completion:nil];
    [_delegate signatureSaved:imgTemp];
}

- (IBAction)btnCancel_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
