//
//  PersonalDetailsVC.m
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import "PersonalDetailsVC.h"
#import "ContactUsVC.h"
#import "BankDetailsVC.h"
#import "PersonalDetailsCell.h"

@interface PersonalDetailsVC () <UITableViewDataSource, UIPickerViewDelegate, UIPickerViewDataSource> {
    PersonalDetailsCell *personalDetailsCell;
    NSMutableArray *aryCountry;
}
@end

@implementation PersonalDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    [self getCountryCodeList];
    
    [self.tblPersonalDetails registerNib:[UINib nibWithNibName:@"PersonalDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PersonalDetailsCell"];
    self.tblPersonalDetails.dataSource = self;
}

#pragma mark - Web Api call

-(void)getCountryCodeList {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        [WebServices api_getCountryCodeList:nil AndCompletionHandler:^(id response, bool isError) {
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->aryCountry = [response[@"result"] mutableCopy];
                }
            } else {
                //NSError *err = (NSError*)response;
                //[AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

-(int)getIndexOfCounty:(NSString *)country {
    int idx = 0;
    for (NSDictionary *dic in aryCountry) {
        if ([dic[@"name"] isEqualToString:country]) {
            return idx;
        }
        idx++;
    }
    return -1;
}

#pragma mark - TableView Data Source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (personalDetailsCell == nil) {
        personalDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"PersonalDetailsCell"];
        //[personalDetailsCell setMotherNameHidden:YES];
        //[personalDetailsCell setFatherNameHidden:YES];
        //[personalDetailsCell setMaritalStatusHidden:YES];
        
        UIPickerView *pickerView = [UIPickerView new];
        pickerView.delegate = self;
        pickerView.dataSource = self;
        personalDetailsCell.tfCountryCode.inputView = pickerView;
        
        if (self->aryCountry != nil) {
            int idx = [self getIndexOfCounty:personalDetailsCell.tfCountryCode.text];
            if (idx >= 0) {
                personalDetailsCell.tfCountryCode.tag = idx;
                personalDetailsCell.tfCountryCode.text = [NSString stringWithFormat:@"%@ +%@", self->aryCountry[idx][@"code"], self->aryCountry[idx][@"phone_code"]];
            } else {
                personalDetailsCell.tfCountryCode.text = @"India";
                int idx2 = [self getIndexOfCounty:personalDetailsCell.tfCountryCode.text];
                if (idx2 >= 0) {
                    personalDetailsCell.tfCountryCode.tag = idx2;
                    personalDetailsCell.tfCountryCode.text = [NSString stringWithFormat:@"%@ +%@", self->aryCountry[idx2][@"code"], self->aryCountry[idx2][@"phone_code"]];
                }
            }
            [pickerView reloadAllComponents];
        }
    }
    return personalDetailsCell;
}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnContactUs_Tapped:(id)sender {
    ContactUsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNext_Tapped:(id)sender {
    BankDetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BankDetailsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Country Picker View Delegates

-(NSInteger)numberOfComponentsInPickerView:(UIPickerView *)pickerView {
    return 1;
}

-(NSInteger)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component {
    return aryCountry.count;
}

-(NSString *)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component {
    if (row < aryCountry.count) {
        return aryCountry[row][@"name"];
    } else {
        return @"";
    }
}

-(void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component {
    if (aryCountry.count > row) {
        personalDetailsCell.tfCountryCode.text = [NSString stringWithFormat:@"+%@", aryCountry[row][@"phone_code"]];
        personalDetailsCell.tfCountryCode.tag = row;
        //if (self.tfMobileNumber.text.length == 10) {
        //    self.transparentView.hidden = YES;
        //} else {
        //    self.transparentView.hidden = NO;
        //}
    }
}

@end
