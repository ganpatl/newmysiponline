//
//  BankDetailsVC.m
//  MySIPonline
//
//  Created by Ganpat on 27/02/19.
//

#import "BankDetailsVC.h"
#import "ContactUsVC.h"
#import "PrimaryBankChangeVC.h"
#import "UploadSingleDocVC.h"

// Cells
#import "BankDetailsCell.h"
#import "NomineeDetailsCell.h"

@interface BankDetailsVC () <UITableViewDataSource, PrimaryBankDelegate> {
    BankDetailsCell *bankDetailsCell;
    NomineeDetailsCell *nomineeDetailsCell;
    
    NSString *defaultBank;
}
@end

@implementation BankDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    defaultBank = @"";
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblBankDetails registerNib:[UINib nibWithNibName:@"BankDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BankDetailsCell"];
    [self.tblBankDetails registerNib:[UINib nibWithNibName:@"NomineeDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"NomineeDetailsCell"];
    self.tblBankDetails.dataSource = self;
}

#pragma mark - TableView Data Source
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (bankDetailsCell == nil) {
            bankDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"BankDetailsCell"];
            //[bankDetailsCell setPrimaryBank1ViewHidden:YES];
            //[bankDetailsCell setPrimaryBank2ViewHidden:YES];
            [bankDetailsCell setAddNewBankAccountViewHidden:YES];
            //[bankDetailsCell setSecondBankHidden:YES];
            [bankDetailsCell.btnPrimaryBank1 addTarget:self action:@selector(btnPrimaryBankFirst_Tapped) forControlEvents:UIControlEventTouchUpInside];
            [bankDetailsCell.btnPrimaryBank2 addTarget:self action:@selector(btnPrimaryBankSecond_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return bankDetailsCell;
    } else {
        if (nomineeDetailsCell == nil) {
            nomineeDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"NomineeDetailsCell"];
        }
        return nomineeDetailsCell;
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnContactUs_Tapped:(id)sender {
    ContactUsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnNext_Tapped:(id)sender {
    UploadSingleDocVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"UploadSingleDocVC"];
    destVC.strDocType = @"selfie image";
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnPrimaryBankFirst_Tapped {
    if ([defaultBank isEqualToString:@"2"]) {
        PrimaryBankChangeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrimaryBankChangeVC"];
        destVC.bankTag = 1;
        destVC.delegate = self;
        [self presentViewController:destVC animated:YES completion:nil];
    } else if ([defaultBank isEqualToString:@""]) {
        defaultBank = @"1";
        //bankDetailCell1.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_checked.png"];
        //bankDetailCell2.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_unchecked.png"];
    }
}

-(void)btnPrimaryBankSecond_Tapped {
    if ([defaultBank isEqualToString:@"1"]) {
        PrimaryBankChangeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PrimaryBankChangeVC"];
        destVC.bankTag = 2;
        destVC.delegate = self;
        [self presentViewController:destVC animated:YES completion:nil];
    } else if ([defaultBank isEqualToString:@""]) {
        defaultBank = @"1";
        //bankDetailCell1.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_checked.png"];
        //bankDetailCell2.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_unchecked.png"];
    }
}

#pragma mark - Primary Bank Change Delegate

-(void)primaryBankAcChanged:(NSInteger)bankTag {
    if (bankTag == 1) {
        //defaultBank = @"1";
        //bankDetailCell1.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_checked.png"];
        //bankDetailCell2.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_unchecked.png"];
        //bankDetailCell1.lblMandateMsg.text = DebitMandateDefaultBankMsg;
        //bankDetailCell2.lblMandateMsg.text = @"";
    } else {
        //defaultBank = @"2";
        //bankDetailCell1.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_unchecked.png"];
        //bankDetailCell2.imgDefaultBank.image = [UIImage imageNamed:@"ic_check_box_checked.png"];
        //bankDetailCell1.lblMandateMsg.text = @"";
        //bankDetailCell2.lblMandateMsg.text = DebitMandateDefaultBankMsg;
    }
    [self.tblBankDetails reloadData];
}

@end
