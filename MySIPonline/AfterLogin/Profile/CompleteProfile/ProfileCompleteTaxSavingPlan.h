//
//  ProfileCompleteTaxSavingPlan.h
//  MySIPonline
//
//  Created by Ganpat on 02/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ProfileCompleteTaxSavingPlan : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *gradientView;
@property (weak, nonatomic) IBOutlet UIImageView *imgIcon;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;

@property CAGradientLayer *gradientLayer;

@end

NS_ASSUME_NONNULL_END
