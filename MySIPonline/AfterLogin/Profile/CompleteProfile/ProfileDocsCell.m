//
//  ProfileDocsCell.m
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import "ProfileDocsCell.h"

@implementation ProfileDocsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
