//
//  PanVerificationVC.m
//  MySIPonline
//
//  Created by Ganpat on 25/02/19.
//

#import "PanVerificationVC.h"
#import "ContactUsVC.h"
#import "PersonalDetailsVC.h"

@interface PanVerificationVC ()

@end

@implementation PanVerificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    
}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnContactUs_Tapped:(id)sender {
    ContactUsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ContactUsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnContinue_Tapped:(id)sender {
    PersonalDetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PersonalDetailsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
