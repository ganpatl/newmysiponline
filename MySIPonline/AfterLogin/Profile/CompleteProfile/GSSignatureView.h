//
//  GSSignatureView.h
//  GSSignature
//
//  Created by Ganpat on 18/07/18.
//  Copyright © 2018 Ganpat Lakhara. All rights reserved.
//

#import <UIKit/UIKit.h>

// Protocol definition starts here
@protocol GSSignatureViewDelegate <NSObject>
@required
- (void)shakeCompleted;
@end

@interface GSSignatureView : UIView {
    CGPoint previousPoint;
    UIBezierPath *signPath;
    NSArray *backgroundLines;
}

@property (nonatomic, strong, nonnull) NSMutableArray *pathArray;
@property (nonatomic, strong, nullable) UIColor *lineColor;
@property (nonatomic) CGFloat lineWidth;
@property (nonatomic, readonly) BOOL signatureExists;
- (void)captureSignature;
- (UIImage*_Nullable)signatureImage:(CGPoint)position text:(NSString*_Nullable)text;
- (UIImage*_Nullable)getSignatureImage;
- (void)erase;

@end
