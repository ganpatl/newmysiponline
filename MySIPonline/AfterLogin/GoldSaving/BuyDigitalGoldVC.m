//
//  BuyDigitalGoldVC.m
//  MySIPonline
//
//  Created by Ganpat on 06/06/19.
//

#import "BuyDigitalGoldVC.h"
#import "GoldPlanCalculationVC.h"

@interface BuyDigitalGoldVC ()

@end

@implementation BuyDigitalGoldVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRefresh_Tapped:(id)sender {
    
}

- (IBAction)btnInAmount_Tapped:(id)sender {

}

- (IBAction)btnInGram_Tapped:(id)sender {
    
}

- (IBAction)btnPlus_Tapped:(id)sender {
    
}

- (IBAction)btnMinus_Tapped:(id)sender {
    
}

- (IBAction)btnHowCalculate_Tapped:(id)sender {
    GoldPlanCalculationVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"GoldPlanCalculationVC"];
    //destVC.fltSelectedGram = ;
    //destVC.fltCurrentRate = ;
    [self presentViewController:destVC animated:YES completion:nil];
}

- (IBAction)btnBuyNow_Tapped:(id)sender {
    
}

@end
