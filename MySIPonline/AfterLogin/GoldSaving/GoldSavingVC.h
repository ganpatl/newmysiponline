//
//  GoldSavingVC.h
//  MySIPonline
//
//  Created by Ganpat on 06/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoldSavingVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblGoldSaving;
@property (weak, nonatomic) IBOutlet UIView *backGradientView;

@end

NS_ASSUME_NONNULL_END
