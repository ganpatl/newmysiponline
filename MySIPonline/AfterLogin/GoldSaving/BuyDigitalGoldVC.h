//
//  BuyDigitalGoldVC.h
//  MySIPonline
//
//  Created by Ganpat on 06/06/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BuyDigitalGoldVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblTodayGoldPrice;
@property (weak, nonatomic) IBOutlet UITextField *tfGoldUnit;
@property (weak, nonatomic) IBOutlet UILabel *lblGoldUnit;

@end

NS_ASSUME_NONNULL_END
