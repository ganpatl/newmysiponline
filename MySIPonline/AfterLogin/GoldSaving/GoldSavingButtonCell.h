//
//  GoldSavingButtonCell.h
//  MySIPonline
//
//  Created by Ganpat on 06/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoldSavingButtonCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnBuyMoreGold;

@end

NS_ASSUME_NONNULL_END
