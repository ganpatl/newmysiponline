//
//  GoldSavingFundVC.m
//  MySIPonline
//
//  Created by Ganpat on 06/06/19.
//

#import "GoldSavingFundVC.h"
#import "BuyDigitalGoldVC.h"

// Cell
#import "TrustedByCell.h"
#import "GoldSavingFundCell.h"

@interface GoldSavingFundVC () <UITableViewDataSource> {
    TrustedByCell *trustedByCell;
    GoldSavingFundCell *goldSavingFundCell;
}

@end

@implementation GoldSavingFundVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.tblGoldSaving registerNib:[UINib nibWithNibName:@"TrustedByCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TrustedByCell"];
    [self.tblGoldSaving registerNib:[UINib nibWithNibName:@"GoldSavingFundCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GoldSavingFundCell"];
    
    self.tblGoldSaving.dataSource = self;
    //self.tblGoldSaving.delegate = self;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (trustedByCell == nil) {
            trustedByCell = [tableView dequeueReusableCellWithIdentifier:@"TrustedByCell"];
            trustedByCell.lblRightTop.text = @"50 Lakhs";
            trustedByCell.lblRightBottom.text = @"Insurance Cover";
        }
        return trustedByCell;
        
    } else if (indexPath.row == 1) {
        if (goldSavingFundCell == nil) {
            goldSavingFundCell = [tableView dequeueReusableCellWithIdentifier:@"GoldSavingFundCell"];
            goldSavingFundCell.selectionStyle = UITableViewCellSelectionStyleNone;
            [goldSavingFundCell.btnKnowMore addTarget:self action:@selector(btnKnowMore_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return goldSavingFundCell;
        
    }
    return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btnBuyGold_Tapped:(id)sender {
    BuyDigitalGoldVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BuyDigitalGoldVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnKnowMore_Tapped {
    
}

@end
