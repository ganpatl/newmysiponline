//
//  GoldSavingVC.m
//  MySIPonline
//
//  Created by Ganpat on 06/03/19.
//

#import "GoldSavingVC.h"

// Cells
#import "GoldSavingCell.h"
#import "InvestmentOptionCell.h"
#import "GoldSavingButtonCell.h"

@interface GoldSavingVC () <UITableViewDataSource, UITableViewDelegate> {
    GoldSavingCell *goldSavingCell;
    GoldSavingButtonCell *goldSavingButtonCell;
    NSMutableArray *aryOtherCells;
    CAGradientLayer *theViewGradient;
}

@end

@implementation GoldSavingVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.backGradientView];
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"FCB350"] CGColor], (id)[[UIColor colorWithHexString:@"FC964A"] CGColor], (id)[[UIColor colorWithHexString:@"FC7E44"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblGoldSaving registerNib:[UINib nibWithNibName:@"GoldSavingCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GoldSavingCell"];
    [self.tblGoldSaving registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    [self.tblGoldSaving registerNib:[UINib nibWithNibName:@"GoldSavingButtonCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"GoldSavingButtonCell"];
    aryOtherCells = @[
                      @{@"type":@"goldsaving"},
                      @{@"type":@"inprocess", @"title":@"1 Transaction In process", @"desc":@"Dashboard will updated once transaction gets processed", @"image":@"info-error"},
                      @{@"type":@"viewtransaction", @"title":@"View Transaction", @"desc":@"6 Funds, 1 Portfilio", @"image":@"payment"},
                      @{@"type":@"buttons"}
                      ].mutableCopy;
    self.tblGoldSaving.dataSource = self;
    self.tblGoldSaving.delegate = self;
}


-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return  aryOtherCells.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (goldSavingCell == nil) {
            goldSavingCell = [tableView dequeueReusableCellWithIdentifier:@"GoldSavingCell"];
            goldSavingCell.selectionStyle = UITableViewCellSelectionStyleNone;
        }
        return goldSavingCell;
    } else if (indexPath.row > 0) {
        if (aryOtherCells.count > indexPath.row) {
            NSDictionary *dict = aryOtherCells[indexPath.row];
            if ([dict[@"type"] isEqualToString:@"buttons"]) {
                if (goldSavingButtonCell == nil) {
                    goldSavingButtonCell = [tableView dequeueReusableCellWithIdentifier:@"GoldSavingButtonCell"];
                    goldSavingButtonCell.selectionStyle = UITableViewCellSelectionStyleNone;
                    [goldSavingButtonCell.btnBuyMoreGold addTarget:self action:@selector(btnBuyMoreGold_Tapped) forControlEvents:UIControlEventTouchUpInside];
                }
                return goldSavingButtonCell;

            } else {
                InvestmentOptionCell *cell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
                cell.titleTopConstraint.constant = 14;
                cell.descBottomConstraint.constant = 13;
                if ([dict[@"type"] isEqualToString:@"inprocess"]) {
                    [cell.lblTitle setFont:[UIFont fontWithName:@"Roboto-Medium" size:14.0]];
                    cell.shadowView.backgroundColor = [UIColor colorWithHexString:@"FFFDF1"];
                    cell.shadowViewTopConstraint.constant = 0;
                    cell.shadowViewBottomConstraint.constant = 5;
                } else {
                    [cell.lblTitle setFont:[UIFont fontWithName:@"Roboto-Regular" size:14.0]];
                    cell.shadowView.backgroundColor = UIColor.whiteColor;
                    cell.shadowViewTopConstraint.constant = 5;
                    cell.shadowViewBottomConstraint.constant = 5;
                }
                cell.lblTitle.text = dict[@"title"];
                cell.lblDesc.text = dict[@"desc"];
                cell.imgIcon.image = [UIImage imageNamed:dict[@"image"]];
                return cell;
            }

        }
    }
    return [UITableViewCell new];
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    if (indexPath.row > 0) {
        if (aryOtherCells.count > indexPath.row) {
            NSDictionary *dict = aryOtherCells[indexPath.row];
            if ([dict[@"type"] isEqualToString:@"inprocess"]) {
                // In Process
                
            } else if ([dict[@"type"] isEqualToString:@"viewtransaction"]) {
                // View My Transaction
                //TransactionListVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionListVC"];
                //[self.navigationController pushViewController:destVC animated:YES];
                
            } else if ([dict[@"type"] isEqualToString:@"buttons"]) {
                // Find More
                
                
            }
        }
    }
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnBuyMoreGold_Tapped {
    //AddMoneyVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AddMoneyVC"];
    //[self.navigationController pushViewController:destVC animated:YES];
}

@end
