//
//  GoldSavingFundVC.h
//  MySIPonline
//
//  Created by Ganpat on 06/06/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GoldSavingFundVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblGoldSaving;

@end

NS_ASSUME_NONNULL_END
