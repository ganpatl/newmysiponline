//
//  InvestmentThanksVC.h
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InvestmentThanksVC : UIViewController

@property (weak, nonatomic) IBOutlet CaptionLabel *lblDateTime;
@property (weak, nonatomic) IBOutlet TitleLabel *lblAmount;
@property (weak, nonatomic) IBOutlet UIView *shadowView1;
@property (weak, nonatomic) IBOutlet UIView *shadowView2;
@property (weak, nonatomic) IBOutlet UIView *ratingView;

@property (weak, nonatomic) IBOutlet UIButton *btnStar1;
@property (weak, nonatomic) IBOutlet UIButton *btnStar2;
@property (weak, nonatomic) IBOutlet UIButton *btnStar3;
@property (weak, nonatomic) IBOutlet UIButton *btnStar4;
@property (weak, nonatomic) IBOutlet UIButton *btnStar5;

@end

NS_ASSUME_NONNULL_END
