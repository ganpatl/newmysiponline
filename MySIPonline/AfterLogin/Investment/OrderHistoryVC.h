//
//  OrderHistoryVC.h
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderHistoryVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblOrderHistory;

@end

NS_ASSUME_NONNULL_END
