//
//  OrderHistoryCell.h
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface OrderHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgAmcLogo;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblDateTime;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblOrderNumber;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblAmount;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblTransType;
@property (weak, nonatomic) IBOutlet UIImageView *imgPaymentOk;
@property (weak, nonatomic) IBOutlet UIImageView *imgSentToAmcOk;
@property (weak, nonatomic) IBOutlet UIImageView *imgUpdateDashboardOk;
@property (weak, nonatomic) IBOutlet UIView *viewSentToAmc;
@property (weak, nonatomic) IBOutlet UIView *viewUpdateDashboard;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblPaymentStatus;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblSentToAmcStatus;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblUpdateDashboardStatus;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblAmcExpectedDate;
@property (weak, nonatomic) IBOutlet CaptionLabel *lblDashboardExpectedDate;
@property (weak, nonatomic) IBOutlet UIButton *btnWhyTakeTime;

-(void)setSentToAmc:(BOOL)sentToAmc UpdateDashboard:(BOOL)updateDashboard;

@end

NS_ASSUME_NONNULL_END
