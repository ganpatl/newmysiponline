//
//  InvestmentVC.m
//  MySIPonline
//
//  Created by Ganpat on 28/01/19.
//

#import "InvestmentVC.h"
#import "InvestmentCell.h"
#import "InvestmentInfoCell.h"
#import "InvestmentThanksVC.h"

@interface InvestmentVC () <UITableViewDataSource, UITableViewDelegate, RKTagsViewDelegate> {
    InvestmentCell *investmentCell;
    InvestmentInfoCell *investmentInfoCell;
    
    NSArray *aryAmountSuggested;
    NSArray *arySipDates;
    NSArray *aryInvestor;
    
    NSDictionary *dicInvestmenFormData;
}

@end

@implementation InvestmentVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    self.lblTitle.text = self.strSchemeName;
    
    aryAmountSuggested = @[@"1000", @"1500", @"2000"];
    arySipDates = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30"];
    aryInvestor = @[@"Sonu", @"Praveen"];
    
    [self.tblInvestment registerNib:[UINib nibWithNibName:@"InvestmentCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentCell"];
    [self.tblInvestment registerNib:[UINib nibWithNibName:@"InvestmentInfoCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentInfoCell"];
    
    self.tblInvestment.dataSource = self;
    self.tblInvestment.delegate = self;
    
    [self getInvestmentFormData];
}

#pragma mark - Web Api Call

-(void)getInvestmentFormData {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
        NSDictionary *dicParam = @{@"scheme_id":self.strSchemeId};
        [LoaderVC showWithStatus:@"Please Wait"];
        [WebServices api_getInvestmentFormData:dicParam AndCompletionHandler:^(id response, bool isError) {
            [LoaderVC dismiss];
            if (!isError) {
                if ([response[@"status"] intValue] == 1) {
                    self->dicInvestmenFormData = response[@"result"];
                    NSLog(@"%@", self->dicInvestmenFormData);
                    if (self->dicInvestmenFormData[@"sip_date_include"] != nil) {
                        self->arySipDates = [self->dicInvestmenFormData[@"sip_date_include"] componentsSeparatedByString:@","];
                    }
                    
                    
                    [self.tblInvestment reloadData];
                }
            } else {
                NSError *err = (NSError*)response;
                [AppHelper ShowAlert:err.localizedDescription Title:@"Error" FromVC:self Completion:nil];
            }
        }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 2;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (investmentCell == nil) {
            investmentCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentCell"];
            // Setup Amount RKTagsView
            investmentCell.amountTagsView.tag = 10;
            [investmentCell.amountTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investmentCell.amountTagsView.tagButtonHeight = 30;
            investmentCell.amountTagsView.tagBackgroundColor = UIColor.whiteColor;
            investmentCell.amountTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            investmentCell.amountTagsView.tintColor = [UIColor getAppColorDarkGrayText];
            investmentCell.amountTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            investmentCell.amountTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            investmentCell.amountTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            investmentCell.amountTagsView.editable = NO;
            investmentCell.amountTagsView.allowsMultipleSelection = NO;
            investmentCell.amountTagsView.lineSpacing = 8;
            investmentCell.amountTagsView.interitemSpacing = 10;
            investmentCell.amountTagsView.buttonCornerRadius = 4;
            investmentCell.amountTagsView.delegate = self;
            
            investmentCell.amountTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (investmentCell.amountTagsView.tags.count != self->aryAmountSuggested.count) {
                [investmentCell.amountTagsView removeAllTags];
                for (NSString *tag in self->aryAmountSuggested) {
                    [investmentCell.amountTagsView addTag:[NSString stringWithFormat:@"₹ %@", tag]];
                }
                // Set First item selected
                [investmentCell.amountTagsView selectTagAtIndex:0];
            }
            
            
            // Setup SIP Date RKTagsView
            investmentCell.sipDateTagsView.tag = 11;
            [investmentCell.sipDateTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investmentCell.sipDateTagsView.tagButtonHeight = 30;
            investmentCell.sipDateTagsView.tagBackgroundColor = UIColor.whiteColor;
            investmentCell.sipDateTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            investmentCell.sipDateTagsView.tintColor = [UIColor getAppColorDarkGrayText];
            investmentCell.sipDateTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            investmentCell.sipDateTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            investmentCell.sipDateTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            investmentCell.sipDateTagsView.editable = NO;
            investmentCell.sipDateTagsView.allowsMultipleSelection = NO;
            investmentCell.sipDateTagsView.lineSpacing = 8;
            investmentCell.sipDateTagsView.interitemSpacing = 10;
            investmentCell.sipDateTagsView.buttonCornerRadius = 4;
            investmentCell.sipDateTagsView.delegate = self;
            investmentCell.sipDateTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (investmentCell.sipDateTagsView.tags.count != self->arySipDates.count) {
                [investmentCell.sipDateTagsView removeAllTags];
                for (NSString *tag in self->arySipDates) {
                    [investmentCell.sipDateTagsView addTag:tag];
                }
                // Set First item selected
                [investmentCell.sipDateTagsView selectTagAtIndex:0];
            }
            
            
            
            // Setup Investor RKTagsView
            investmentCell.investorTagsView.tag = 12;
            [investmentCell.investorTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investmentCell.investorTagsView.tagButtonHeight = 30;
            investmentCell.investorTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
            investmentCell.investorTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
            investmentCell.investorTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
            investmentCell.investorTagsView.selectedTagTintColor = [UIColor whiteColor];
            investmentCell.investorTagsView.editable = NO;
            investmentCell.investorTagsView.allowsMultipleSelection = NO;
            investmentCell.investorTagsView.lineSpacing = 8;
            investmentCell.investorTagsView.interitemSpacing = 10;
            investmentCell.investorTagsView.buttonCornerRadius = 4;
            investmentCell.investorTagsView.delegate = self;
            investmentCell.investorTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (investmentCell.investorTagsView.tags.count != self->aryInvestor.count) {
                [investmentCell.investorTagsView removeAllTags];
                for (NSString *tag in self->aryInvestor) {
                    [investmentCell.investorTagsView addTag:tag];
                }
                // Set First item selected
                [investmentCell.investorTagsView selectTagAtIndex:0];
            }
        }
        return investmentCell;
        
    } else if (indexPath.row == 1) {
        if (investmentInfoCell == nil) {
            investmentInfoCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentInfoCell"];
        }
        return investmentInfoCell;
    }
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 10) {
        // Amount
        investmentCell.tfAmount.text = aryAmountSuggested[index];
    } else if (tagsView.tag == 11) {
        // SIP Date
        
    } else if (tagsView.tag == 12) {
        // Investor
        
    }
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMakePayment_Tapped:(id)sender {
    InvestmentThanksVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InvestmentThanksVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
