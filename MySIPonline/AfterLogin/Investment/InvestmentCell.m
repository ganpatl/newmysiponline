//
//  InvestmentCell.m
//  MySIPonline
//
//  Created by Ganpat on 28/01/19.
//

#import "InvestmentCell.h"

@implementation InvestmentCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setInvestmentModeHidden:(BOOL)hide {
    if (hide) {
        //self.investmentModeTopConstraint.constant = 0;
        self.investmentModeHeightConstraint.constant = 0;
        self.investmentModeBottomConstraint.constant = 0;
        self.sipButtonHeightConstraint.constant = 0;
        self.sipButtonBottomConstraint.constant = 0;
    } else {
        //self.investmentModeTopConstraint.constant = 16;
        self.investmentModeHeightConstraint.constant = 19;
        self.investmentModeBottomConstraint.constant = 8;
        self.sipButtonHeightConstraint.constant = 32;
        self.sipButtonBottomConstraint.constant = 25;
    }
}

-(void)setSipDateViewHidden:(BOOL)hide {
    if (hide) {
        self.sipDateViewHeightConstraint.constant = 0;
        self.sipDateViewBottomConstraint.constant = 0;
    } else {
        self.sipDateViewHeightConstraint.constant = 60;
        self.sipDateViewBottomConstraint.constant = 18;
    }
}

-(void)hideOnlySipButton:(BOOL)hide {
    if (hide) {
        self.sipButtonWidthConstraint.constant = 0;
        self.sipButtonTrailingConstraint.constant = 0;
    } else {
        self.sipButtonWidthConstraint.constant = 54;
        self.sipButtonTrailingConstraint.constant = 16;
    }
}

@end
