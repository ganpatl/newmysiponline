//
//  WhyTakeTimeVC.h
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface WhyTakeTimeVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblMessage;
@property (weak, nonatomic) IBOutlet UIButton *btnGotIt;

@end

NS_ASSUME_NONNULL_END
