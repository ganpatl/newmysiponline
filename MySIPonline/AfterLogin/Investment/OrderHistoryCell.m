//
//  OrderHistoryCell.m
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import "OrderHistoryCell.h"

@implementation OrderHistoryCell {
    CAShapeLayer *lineLayer1;
    CAShapeLayer *lineLayer2;
}

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    self.imgAmcLogo.layer.cornerRadius = self.imgAmcLogo.frame.size.height / 2;
    self.imgPaymentOk.layer.cornerRadius = self.imgPaymentOk.frame.size.height / 2;
    self.imgSentToAmcOk.layer.cornerRadius = self.imgSentToAmcOk.frame.size.height / 2;
    self.imgUpdateDashboardOk.layer.cornerRadius = self.imgUpdateDashboardOk.frame.size.height / 2;
    
//    CAShapeLayer *lineLayer = [CAShapeLayer new];
//    lineLayer.strokeColor = [UIColor colorWithHexString:@"D1D1D1"].CGColor;
//    lineLayer.lineWidth = 1;
//    lineLayer.lineDashPattern = @[@1, @2];
//    CGMutablePathRef path = CGPathCreateMutable();
//    CGPathMoveToPoint(path, NULL, 6.5, 0);
//    CGPathAddLineToPoint(path, NULL, 6.5, self.viewSentToAmc.frame.size.height);
//    lineLayer.path = path;
//    [self.viewSentToAmc.layer addSublayer:lineLayer];
    
    self.imgPaymentOk.image = [UIImage imageNamed:@"done-Green"];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

-(void)setSentToAmc:(BOOL)sentToAmc UpdateDashboard:(BOOL)updateDashboard {
    
    // Sent To AMC
    if (lineLayer1 == nil) {
        lineLayer1 = [CAShapeLayer new];
        lineLayer1.lineWidth = 1;
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 6.5, 0);
        CGPathAddLineToPoint(path, NULL, 6.5, self.viewSentToAmc.frame.size.height);
        lineLayer1.path = path;
        [self.viewSentToAmc.layer addSublayer:lineLayer1];
    }
    if (sentToAmc) {
        lineLayer1.strokeColor = UIColor.greenColor.CGColor;
        lineLayer1.lineDashPattern = nil;
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 6.5, 0);
        CGPathAddLineToPoint(path, NULL, 6.5, self.viewSentToAmc.frame.size.height);
        lineLayer1.path = path;
    } else {
        lineLayer1.strokeColor = [UIColor colorWithHexString:@"D1D1D1"].CGColor;
        lineLayer1.lineDashPattern = @[@1, @2];
    }
    
    // Update Dashboard
    // Sent To AMC
    if (lineLayer2 == nil) {
        lineLayer2 = [CAShapeLayer new];
        lineLayer2.lineWidth = 1;
        [self.viewUpdateDashboard.layer addSublayer:lineLayer2];
    }
    if (updateDashboard) {
        lineLayer2.strokeColor = UIColor.greenColor.CGColor;
        lineLayer2.lineDashPattern = nil;
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 6.5, 0);
        CGPathAddLineToPoint(path, NULL, 6.5, self.viewUpdateDashboard.frame.size.height);
        lineLayer2.path = path;
    } else {
        lineLayer2.strokeColor = [UIColor colorWithHexString:@"D1D1D1"].CGColor;
        lineLayer2.lineDashPattern = @[@1, @2];
        CGMutablePathRef path = CGPathCreateMutable();
        CGPathMoveToPoint(path, NULL, 6.5, 0);
        CGPathAddLineToPoint(path, NULL, 6.5, self.viewUpdateDashboard.frame.size.height);
        lineLayer2.path = path;
    }
}

@end
