//
//  OrderHistoryVC.m
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import "OrderHistoryVC.h"
#import "OrderHistoryCell.h"
#import "WhyTakeTimeVC.h"

@interface OrderHistoryVC () <UITableViewDataSource, UITableViewDelegate>

@end

@implementation OrderHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblOrderHistory registerNib:[UINib nibWithNibName:@"OrderHistoryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"OrderHistoryCell"];
    
    self.tblOrderHistory.dataSource = self;
    self.tblOrderHistory.delegate = self;
}

#pragma mark - TableView Datasource

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 8;
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    OrderHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"OrderHistoryCell"];
    [cell.btnWhyTakeTime removeTarget:self action:@selector(btnWhyTakeTime_Tapped) forControlEvents:UIControlEventTouchUpInside];
    [cell.btnWhyTakeTime addTarget:self action:@selector(btnWhyTakeTime_Tapped) forControlEvents:UIControlEventTouchUpInside];
    
    if (indexPath.row == 0) {
        [cell setSentToAmc:NO UpdateDashboard:NO];
    } else if (indexPath.row == 1) {
        [cell setSentToAmc:NO UpdateDashboard:YES];
    } else {
        [cell setSentToAmc:YES UpdateDashboard:NO];
    }
    return cell;
    
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnWhyTakeTime_Tapped {
    WhyTakeTimeVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"WhyTakeTimeVC"];
    [self presentViewController:destVC animated:YES completion:nil];
}


@end
