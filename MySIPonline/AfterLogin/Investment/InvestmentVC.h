//
//  InvestmentVC.h
//  MySIPonline
//
//  Created by Ganpat on 28/01/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InvestmentVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblInvestment;
@property (weak, nonatomic) IBOutlet TitleLabel *lblTitle;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;

@property NSString *strSchemeId;
@property NSString *strSchemeName;

@end

NS_ASSUME_NONNULL_END
