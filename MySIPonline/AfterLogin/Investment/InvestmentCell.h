//
//  InvestmentCell.h
//  MySIPonline
//
//  Created by Ganpat on 28/01/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface InvestmentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnSip;
@property (weak, nonatomic) IBOutlet UIButton *btnLumpsum;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet RKTagsView *amountTagsView;
@property (weak, nonatomic) IBOutlet RKTagsView *sipDateTagsView;
@property (weak, nonatomic) IBOutlet RKTagsView *investorTagsView;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *investmentModeHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *investmentModeTopConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *investmentModeBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipButtonWidthConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipButtonTrailingConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipButtonHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipButtonBottomConstraint;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipDateViewHeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *sipDateViewBottomConstraint;

-(void)setInvestmentModeHidden:(BOOL)hide;
-(void)setSipDateViewHidden:(BOOL)hide;

@end

NS_ASSUME_NONNULL_END
