//
//  InvestmentThanksVC.m
//  MySIPonline
//
//  Created by Ganpat on 30/01/19.
//

#import "InvestmentThanksVC.h"
#import "OrderHistoryVC.h"

@interface InvestmentThanksVC ()

@end

@implementation InvestmentThanksVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [AppHelper addShadowOnView:self.shadowView1 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    [AppHelper addShadowOnView:self.shadowView2 Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    [AppHelper addShadowOnView:self.ratingView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
}


#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self.navigationController popToRootViewControllerAnimated:YES];
}

- (IBAction)btnTrackYourTransaction_Tapped:(id)sender {
    OrderHistoryVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"OrderHistoryVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

- (IBAction)btnDebitMandate_Tapped:(id)sender {
    
}

// Star Tapped

- (IBAction)btnStar1_Tapped:(id)sender {
    
}

- (IBAction)btnStar2_Tapped:(id)sender {
    
}

- (IBAction)btnStar3_Tapped:(id)sender {
    
}

- (IBAction)btnStar4_Tapped:(id)sender {
    
}

- (IBAction)btnStar5_Tapped:(id)sender {
    
}


@end
