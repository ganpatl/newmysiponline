//
//  CartEditVC.m
//  MySIPonline
//
//  Created by Ganpat on 21/02/19.
//

#import "CartEditVC.h"

@interface CartEditVC () <RKTagsViewDelegate> {
    NSArray *aryAmountSuggested;
    NSArray *arySipDates;
    BOOL insuranceChecked;
}
@end

@implementation CartEditVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.btnInsuranceTick.layer.borderColor = [UIColor colorWithHexString:@"4885ED"].CGColor;
    self.btnInsuranceTick.layer.borderWidth = 1;
    self.btnInsuranceTick.imageView.contentMode = UIViewContentModeScaleAspectFit;
    [self.btnInsuranceTick setImage:nil forState:UIControlStateNormal];
    
    aryAmountSuggested = @[@"1000", @"1500", @"2000"];
    arySipDates = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30"];
    
    // Setup Amount RKTagsView
    self.amountTagsView.tag = 10;
    [self.amountTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.amountTagsView.tagButtonHeight = 30;
    self.amountTagsView.tagBackgroundColor = UIColor.whiteColor;
    self.amountTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
    self.amountTagsView.tintColor = [UIColor getAppColorDarkGrayText];
    self.amountTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
    self.amountTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
    self.amountTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
    self.amountTagsView.editable = NO;
    self.amountTagsView.allowsMultipleSelection = NO;
    self.amountTagsView.lineSpacing = 8;
    self.amountTagsView.interitemSpacing = 10;
    self.amountTagsView.buttonCornerRadius = 4;
    self.amountTagsView.delegate = self;
    
    self.amountTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    if (self.amountTagsView.tags.count != self->aryAmountSuggested.count) {
        [self.amountTagsView removeAllTags];
        for (NSString *tag in self->aryAmountSuggested) {
            [self.amountTagsView addTag:[NSString stringWithFormat:@"₹ %@", tag]];
        }
        // Set First item selected
        [self.amountTagsView selectTagAtIndex:0];
    }
    
    // Setup SIP Date RKTagsView
    self.sipDateTagsView.tag = 11;
    [self.sipDateTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.sipDateTagsView.tagButtonHeight = 30;
    self.sipDateTagsView.tagBackgroundColor = UIColor.whiteColor;
    self.sipDateTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
    self.sipDateTagsView.tintColor = [UIColor getAppColorDarkGrayText];
    self.sipDateTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
    self.sipDateTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
    self.sipDateTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
    self.sipDateTagsView.editable = NO;
    self.sipDateTagsView.allowsMultipleSelection = NO;
    self.sipDateTagsView.lineSpacing = 8;
    self.sipDateTagsView.interitemSpacing = 10;
    self.sipDateTagsView.buttonCornerRadius = 4;
    self.sipDateTagsView.delegate = self;
    self.sipDateTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    if (self.sipDateTagsView.tags.count != self->arySipDates.count) {
        [self.sipDateTagsView removeAllTags];
        for (NSString *tag in self->arySipDates) {
            [self.sipDateTagsView addTag:tag];
        }
        // Set First item selected
        [self.sipDateTagsView selectTagAtIndex:0];
    }

}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 10) {
        // Amount
        self.tfAmount.text = aryAmountSuggested[index];
    } else if (tagsView.tag == 11) {
        // SIP Date
        
    } else if (tagsView.tag == 12) {
        // Investor
        
    }
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}


#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)btnInsuranceTick_Tapped:(id)sender {
    if (insuranceChecked) {
        [self.btnInsuranceTick setImage:nil forState:UIControlStateNormal];
    } else {
        [self.btnInsuranceTick setImage:[UIImage imageNamed:@"check_tick"] forState:UIControlStateNormal];
    }
    insuranceChecked = !insuranceChecked;
}

- (IBAction)btnDone_Tapped:(id)sender {
    
}


@end
