//
//  GenerateOrderVC.m
//  MySIPonline
//
//  Created by Ganpat on 23/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "GenerateOrderVC.h"
//#import "PaymentMethodVC.h"
#import "ConfirmCartItemCell.h"

@interface GenerateOrderVC () <UITableViewDataSource, UITableViewDelegate> {
    NSString *strRequestUrl;
    NSString *strMandateId;
    NSString *strMandateMessage;
    BOOL isMandateRegistered;
}

@end

@implementation GenerateOrderVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    
    [self.tblGenerateOrder registerNib:[UINib nibWithNibName:@"ConfirmCartItemCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ConfirmCartItemCell"];
    self.tblGenerateOrder.dataSource = self;
    self.tblGenerateOrder.delegate = self;
    
    [self SaveBseStarDataAPI];
}

-(NSString *)getStatusFromSchemeId:(NSString*)strSchemeId ResultArray:(NSArray *)aryResult {
    for (NSDictionary *dic in aryResult) {
        if ([dic[@"sch_id"] isEqualToString:strSchemeId]) {
            return dic[@"orderstatus"];
        }
    }
    return @"";
}

#pragma mark - Web Api Call

-(void)SaveBseStarDataAPI {
    if ([AppDelegate getAppDelegate].checkNetworkReachability) {
//        int number = arc4random_uniform(1000000);
//        [NSUserData SaveRandomNo:number];
//        NSDictionary *paramSave = @{@"user_id":self.strUserId, @"parent_id":self.strParentId};
//        //[LoaderVC showWithStatus:@"Please Wait"];
//        [WebServices api_SaveBseOrderIdData:paramSave AndCompletionHandler:^(id response, bool isError)
//         {
//             if (!isError) {
//                 int status = [[response valueForKey:@"status"] intValue];
//                 if (status == 1) {
//                     [NSUserData SaveOrderAmount:[NSString stringWithFormat:@"%@", self.strOrderTotalAmount]];
//                     self->strRequestUrl = response[@"request_url"];
//                     NSArray *aryOrderId = response[@"orderno"];
//                     if (aryOrderId != nil && aryOrderId.count > 0) {
//                         [NSUserData SaveOrderIdArray:aryOrderId];
//                     }
//                     NSArray *aryStatusList = response[@"statusList"];
//                     int intCount = 0;
//                     for (NSDictionary *dic in self.aryCartItems) {
//                         NSString *strResult = [self getStatusFromSchemeId:dic[@"sch_id"] ResultArray:aryStatusList];
//                         if ([strResult.lowercaseString isEqualToString:@"success"]) {
//                             NSMutableDictionary *mdic = [dic mutableCopy];
//                             mdic[@"status"] = @"1";
//                             [self.aryCartItems replaceObjectAtIndex:intCount withObject:mdic];
//                         }
//                         intCount++;
//                     }
//                     [self.tblGenerateOrder reloadData];
//                     if ([response[@"payment_status"] boolValue]) {
//                         self.buttonDisableView.hidden = YES;
//                     }
//
//                     // Debit Mandate
//                     self->isMandateRegistered = [response[@"mandate_flag"] boolValue];
//                     if (self->isMandateRegistered) {
//                         self->strMandateId = response[@"mandate_id"];
//                         self->strMandateMessage = response[@"thanks_content"];
//                     }
//
//                 } else {
//                     NSString *strMsg = response[@"msg"];
//                     if (strMsg != nil) {
//                         [Helper ShowAlert:strMsg FromVC:self Completion:nil];
//                     } else {
//                         [Helper ShowAlert:@"Something went wrong." FromVC:self Completion:nil];
//                     }
//                 }
//             } else {
//                 NSError *err = (NSError*)response;
//                 NSLog(@"Error %@",err.localizedDescription);
//                 [Helper ShowAlert:err.localizedDescription FromVC:self Completion:nil];
//             }
//         }];
    } else {
        NoInternetVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"NoInternetVC"];
        [self presentViewController:destVC animated:NO completion:nil];
    }
}

#pragma mark - TableView Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.aryCartItems.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (self.aryCartItems.count > indexPath.row) {
        ConfirmCartItemCell *cell = [tableView dequeueReusableCellWithIdentifier:@"ConfirmCartItemCell"];
        if ([AppHelper isNotNull:self.aryCartItems[indexPath.row][@"scheme_name"]]) {
            cell.lblSchemeName.text = [NSString stringWithFormat:@"%@", self.aryCartItems[indexPath.row][@"scheme_name"]];
        }
        if ([AppHelper isNotNull:self.aryCartItems[indexPath.row][@"amount"]]) {
            cell.lblAmount.text = [NSString stringWithFormat:@"%@", self.aryCartItems[indexPath.row][@"amount"]];
        }
        if ([AppHelper isNotNull:self.aryCartItems[indexPath.row][@"inv_type"]]) {
            cell.lblType.text = [NSString stringWithFormat:@"%@", self.aryCartItems[indexPath.row][@"inv_type"]].uppercaseString;
        }
        if ([AppHelper isNotNull:self.aryCartItems[indexPath.row][@"sip_date"]]) {
            cell.lblSipDate.text = [NSString stringWithFormat:@"%@", self.aryCartItems[indexPath.row][@"sip_date"]];
        } else {
            cell.lblSipDate.text = @"-";
        }
        if ([self.aryCartItems[indexPath.row][@"status"] isEqualToString:@"1"]) {
            cell.imgLoader.hidden = YES;
            cell.imgConfirmed.hidden = NO;
            cell.lblStatus.text = @"Confirmed";
            cell.lblStatus.textColor = [UIColor colorWithHexString:@"34A853"];
        }
        return cell;
    }
    return [UITableViewCell new];
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}


- (IBAction)btnPayAmount_Tapped:(id)sender {
//    UIViewController *destVC = [Helper createWebViewControllerWithTitle:@"Payment" URLString:strRequestUrl];
//    [self.navigationController pushViewController:destVC animated:YES];
}


@end
