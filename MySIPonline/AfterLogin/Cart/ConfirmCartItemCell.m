//
//  ConfirmCartItemCell.m
//  MySIPonline
//
//  Created by Ganpat on 23/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import "ConfirmCartItemCell.h"
#import "UIImage+animatedGIF.h"

@implementation ConfirmCartItemCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(1, 3) Opacity:1 Radius:3];
    
    // Set Gif Image
    NSString *filePath = [[NSBundle mainBundle] pathForResource: @"loader2" ofType: @"gif"];
    NSData *gifData = [NSData dataWithContentsOfFile: filePath];
    self.imgLoader.image = [UIImage animatedImageWithAnimatedGIFData:gifData];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
