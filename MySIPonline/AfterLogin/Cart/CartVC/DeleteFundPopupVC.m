//
//  DeleteFundPopupVC.m
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import "DeleteFundPopupVC.h"

@interface DeleteFundPopupVC ()

@end

@implementation DeleteFundPopupVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addShadowOnView:self.botomShadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, -2) Opacity:0.5 Radius:2.0];
    self.popupView.layer.cornerRadius = 4;
}

#pragma mark - Button Tap Events

- (IBAction)btnCancel_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

 - (IBAction)btnDelete_Tapped:(id)sender {
     [self dismissViewControllerAnimated:YES completion:nil];
 }

@end
