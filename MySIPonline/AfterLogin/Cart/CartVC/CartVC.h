//
//  CartVC.h
//  MySIPonline
//
//  Created by Ganpat on 20/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CartVC : UIViewController

@property (weak, nonatomic) IBOutlet TitleLabel *lblTopTitle;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblCart;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnContinue;
@property (weak, nonatomic) IBOutlet UIView *bottomShadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnContinueInvestment;

@property BOOL isComeFromLiquid;

@end

NS_ASSUME_NONNULL_END
