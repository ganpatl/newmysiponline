//
//  InvestorCartCell.h
//  MySIPonline
//
//  Created by Ganpat on 20/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface InvestorCartCell : UITableViewCell

@property (weak, nonatomic) IBOutlet RKTagsView *investorTagsView;

@end

NS_ASSUME_NONNULL_END
