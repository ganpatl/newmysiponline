//
//  CartVC.m
//  MySIPonline
//
//  Created by Ganpat on 20/02/19.
//

#import "CartVC.h"
#import "CartEditVC.h"
#import "DeleteFundPopupVC.h"

// Cells
#import "LumpsumCartCell.h"
#import "SipCartCell.h"
#import "InvestorCartCell.h"
#import "LiquidFundCartCell.h"

@interface CartVC () <UITableViewDataSource, RKTagsViewDelegate> {
    InvestorCartCell *investorCartCell;
    NSMutableArray *aryCartItem;
    NSArray *aryAmountSuggested;
}

@end

@implementation CartVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.

    [AppHelper addTopShadowOnView:self.topTitleView];
    [AppHelper addShadowOnView:self.bottomShadowView Color:[UIColor darkGrayColor] OffetSize:CGSizeMake(0, -1) Opacity:0.3 Radius:2];
    self.btnContinue.layer.cornerRadius = 4;
    self.btnContinueInvestment.layer.cornerRadius = 4;
    
    aryAmountSuggested = @[@"1000", @"1500", @"2000"];
    
    aryCartItem = [NSMutableArray arrayWithArray:@[
                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"Lumpsum", @"amount":@"5000"},
//                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"sip", @"amount":@"5000"},
//                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"sip", @"amount":@"500"},
//                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"sip", @"amount":@"5000"},
//                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"sip", @"amount":@"1000"},
                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"sip", @"amount":@"3000", @"insurance":@"1"},
                    @{@"scheme_name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"type":@"sip", @"amount":@"2500"}
                   ]];
    
    [self.tblCart registerNib:[UINib nibWithNibName:@"LumpsumCartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LumpsumCartCell"];
    [self.tblCart registerNib:[UINib nibWithNibName:@"SipCartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SipCartCell"];
    [self.tblCart registerNib:[UINib nibWithNibName:@"InvestorCartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestorCartCell"];
    [self.tblCart registerNib:[UINib nibWithNibName:@"LiquidFundCartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LiquidFundCartCell"];
    
    self.tblCart.dataSource = self;
    //self.tblCart.delegate = self;
    
    //[self displayEmptyCartMessage];
    
    if (self.isComeFromLiquid) {
        self.btnContinue.backgroundColor = [UIColor getAppAlertSuccessColor];
    }
}

-(void)displayEmptyCartMessage {
    self.lblTopTitle.text = @"Cart";
    self.tblCart.hidden = YES;
    self.bottomShadowView.hidden = YES;
}

#pragma mark - TableView Data Source

-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 2;
}

- (NSInteger)tableView:(nonnull UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    if (section == 0) {
        return aryCartItem.count;
    } else {
        return 1;
    }
}

- (nonnull UITableViewCell *)tableView:(nonnull UITableView *)tableView cellForRowAtIndexPath:(nonnull NSIndexPath *)indexPath {
    if (indexPath.section == 0) {
        if (self.isComeFromLiquid) {
            LiquidFundCartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LiquidFundCartCell"];
            cell.tfAmount.tag = indexPath.row;
            [cell.tfAmount addTarget:self action:@selector(amountTextField_Changed:) forControlEvents:UIControlEventEditingChanged];
            NSDictionary *dictFund = aryCartItem[indexPath.row];
            cell.tfAmount.text = dictFund[@"amount"];
            
            // Setup Amount RKTagsView
            cell.tagsView.tag = indexPath.row;
            [cell.tagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            cell.tagsView.tagButtonHeight = 30;
            cell.tagsView.tagBackgroundColor = UIColor.whiteColor;
            cell.tagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            cell.tagsView.tintColor = [UIColor getAppColorDarkGrayText];
            cell.tagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            cell.tagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            cell.tagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            cell.tagsView.editable = NO;
            cell.tagsView.allowsMultipleSelection = NO;
            cell.tagsView.lineSpacing = 8;
            cell.tagsView.interitemSpacing = 10;
            cell.tagsView.buttonCornerRadius = 4;
            cell.tagsView.delegate = self;
            
            cell.tagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (cell.tagsView.tags.count != aryAmountSuggested.count) {
                [cell.tagsView removeAllTags];
                for (NSString *tag in aryAmountSuggested) {
                    [cell.tagsView addTag:[NSString stringWithFormat:@"₹ %@", tag]];
                }
                // Set First item selected
                //[cell.tagsView selectTagAtIndex:0];
            }
            return cell;
        } else {
            if (aryCartItem.count > indexPath.row) {
                NSDictionary *dictFund = aryCartItem[indexPath.row];
                if ([dictFund[@"type"] isEqualToString:@"sip"]) {
                    SipCartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"SipCartCell"];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.btnEdit.tag = indexPath.row;
                    [cell.btnEdit removeTarget:self action:@selector(btnEdit_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnEdit_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnDelete.tag = indexPath.row;
                    [cell.btnDelete removeTarget:self action:@selector(btnRemove_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnDelete addTarget:self action:@selector(btnRemove_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    if ([dictFund[@"scheme_name"] isKindOfClass:[NSString class]]) {
                        cell.lblSchemeName.text = dictFund[@"scheme_name"];
                    }
                    if ([dictFund[@"amount"] isKindOfClass:[NSString class]]) {
                        cell.lblAmount.text = [NSString stringWithFormat:@"₹ %@", dictFund[@"amount"]];
                    }
                    if ([dictFund[@"insurance"] isKindOfClass:[NSString class]] && [dictFund[@"insurance"] isEqualToString:@"1"]) {
                        cell.insuranceViewHeightConstraint.constant = 45.5;
                        cell.insuranceView.hidden = NO;
                    } else {
                        cell.insuranceViewHeightConstraint.constant = 0;
                        cell.insuranceView.hidden = YES;
                    }
                    return cell;
                    
                } else {
                    LumpsumCartCell *cell = [tableView dequeueReusableCellWithIdentifier:@"LumpsumCartCell"];
                    cell.selectionStyle = UITableViewCellSelectionStyleNone;
                    cell.btnEdit.tag = indexPath.row;
                    [cell.btnEdit removeTarget:self action:@selector(btnEdit_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnEdit addTarget:self action:@selector(btnEdit_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    cell.btnDelete.tag = indexPath.row;
                    [cell.btnDelete removeTarget:self action:@selector(btnRemove_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    [cell.btnDelete addTarget:self action:@selector(btnRemove_Tapped:) forControlEvents:UIControlEventTouchUpInside];
                    if ([dictFund[@"scheme_name"] isKindOfClass:[NSString class]]) {
                        cell.lblSchemeName.text = dictFund[@"scheme_name"];
                    }
                    if ([dictFund[@"amount"] isKindOfClass:[NSString class]]) {
                        cell.lblAmount.text = [NSString stringWithFormat:@"₹ %@", dictFund[@"amount"]];
                    }
                    return cell;
                    
                }
            }
        }
        
    } else {
        // Investor Cell
        if (investorCartCell == nil) {
            investorCartCell = [tableView dequeueReusableCellWithIdentifier:@"InvestorCartCell"];
            investorCartCell.selectionStyle = UITableViewCellSelectionStyleNone;
            // Setup RKTagsView
            [investorCartCell.investorTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            investorCartCell.investorTagsView.tagButtonHeight = 30;
            investorCartCell.investorTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
            investorCartCell.investorTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
            investorCartCell.investorTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
            investorCartCell.investorTagsView.selectedTagTintColor = [UIColor whiteColor];
            investorCartCell.investorTagsView.editable = NO;
            investorCartCell.investorTagsView.allowsMultipleSelection = NO;
            investorCartCell.investorTagsView.lineSpacing = 8;
            investorCartCell.investorTagsView.interitemSpacing = 8;
            investorCartCell.investorTagsView.buttonCornerRadius = 3;
            investorCartCell.investorTagsView.delegate = self;
            investorCartCell.investorTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            investorCartCell.investorTagsView.tag = 100;
            
            NSArray *aryInvestor = @[@{@"name":@"Sonu"}, @{@"name":@"Praveen"}];
            if (investorCartCell.investorTagsView.tags.count != aryInvestor.count) {
                [investorCartCell.investorTagsView removeAllTags];
                for (NSDictionary *dic in aryInvestor) {
                    if ([AppHelper isNotNull:dic[@"name"]]) {
                        [investorCartCell.investorTagsView addTag:dic[@"name"]];
                    }
                }
                // Set First item selected
                [investorCartCell.investorTagsView selectTagAtIndex:0];
            }
        }
        return investorCartCell;
    }
    
    return [UITableViewCell new];
}

#pragma TableView Delegate

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 100) {
        GSLog(@"Index %ld selected", (long)index);
        if (index == 0) {
            // SIP
            
        } else {
            // Lumpsum
            
        }
    } else {
        // Amount
        LiquidFundCartCell *cell = (LiquidFundCartCell*)tagsView.superview.superview.superview;
        cell.tfAmount.text = aryAmountSuggested[index];
        NSMutableDictionary *mdictFund = [aryCartItem[tagsView.tag] mutableCopy];
        mdictFund[@"amount"] = aryAmountSuggested[index];
        [aryCartItem replaceObjectAtIndex:tagsView.tag withObject:mdictFund];
    }
    return YES;
}
-(BOOL)tagsView:(RKTagsView *)tagsView shouldDeselectTagAtIndex:(NSInteger)index {
    return NO;
}

#pragma mark - TextField Method

-(void)amountTextField_Changed:(UITextField*)sender {
    NSMutableDictionary *mdictFund = [aryCartItem[sender.tag] mutableCopy];
    mdictFund[@"amount"] = sender.text;
    [aryCartItem replaceObjectAtIndex:sender.tag withObject:mdictFund];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnContinue_Tapped:(id)sender {
    
}

- (IBAction)btnContinueInvestment_Tapped:(id)sender {
    // Go to Investment
}

-(void)btnEdit_Tapped:(UIButton*)sender {
    CartEditVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"CartEditVC"];    
    [self presentViewController:destVC animated:YES completion:nil];
}

-(void)btnRemove_Tapped:(UIButton*)sender {
    DeleteFundPopupVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"DeleteFundPopupVC"];
    [self presentViewController:destVC animated:YES completion:nil];
}

@end
