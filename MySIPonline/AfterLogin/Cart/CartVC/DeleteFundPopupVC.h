//
//  DeleteFundPopupVC.h
//  MySIPonline
//
//  Created by Ganpat on 22/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface DeleteFundPopupVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *botomShadowView;

@property (weak, nonatomic) IBOutlet UIView *popupView;

@end

NS_ASSUME_NONNULL_END
