//
//  CartEditVC.h
//  MySIPonline
//
//  Created by Ganpat on 21/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface CartEditVC : UIViewController

@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet RKTagsView *amountTagsView;
@property (weak, nonatomic) IBOutlet RKTagsView *sipDateTagsView;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet UIButton *btnSip;
@property (weak, nonatomic) IBOutlet UIButton *btnLumpsum;
@property (weak, nonatomic) IBOutlet UIButton *btnInsuranceTick;

@end

NS_ASSUME_NONNULL_END
