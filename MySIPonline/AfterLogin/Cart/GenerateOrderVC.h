//
//  GenerateOrderVC.h
//  MySIPonline
//
//  Created by Ganpat on 23/01/19.
//  Copyright © 2019 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface GenerateOrderVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblGenerateOrder;
@property (weak, nonatomic) IBOutlet UIButton *btnPayAmount;
@property (weak, nonatomic) IBOutlet UIView *buttonDisableView;

@property NSString *strUserId;
@property NSString *strParentId;
@property NSString *strOrderTotalAmount;
@property NSDictionary *dicProfileCheckData;
@property NSMutableArray *aryCartItems;

@end

NS_ASSUME_NONNULL_END
