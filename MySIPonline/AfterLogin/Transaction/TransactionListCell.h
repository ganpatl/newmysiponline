//
//  TransactionListCell.h
//  MySIPonline
//
//  Created by Ganpat on 07/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionListCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreOption;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblInvestment;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet SubHeaderDarkLabel *lblGainLoss;

@end

NS_ASSUME_NONNULL_END
