//
//  TransactionUserView.h
//  MySIPonline
//
//  Created by Ganpat on 06/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionUserView : UIView

@property (weak, nonatomic) IBOutlet UILabel *lblInvestorName;
@property (weak, nonatomic) IBOutlet UILabel *lblInvestment;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet UILabel *lblReturnPer;

@end

NS_ASSUME_NONNULL_END
