//
//  TransactionHistoryVC.m
//  MySIPonline
//
//  Created by Ganpat on 08/02/19.
//

#import "TransactionHistoryVC.h"
#import "TransactionHistoryCell.h"
#import "AccountStatementVC.h"

@interface TransactionHistoryVC () <UITableViewDataSource, UITableViewDelegate> {
    NSArray *aryTransaction;
}

@end

@implementation TransactionHistoryVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    aryTransaction = @[
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Purchase", @"transaction_type":@"SIP", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Purchase", @"transaction_type":@"Lumpsum", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Withdraw", @"transaction_type":@"", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Purchase", @"transaction_type":@"Lumpsum", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Purchase", @"transaction_type":@"SIP", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Withdraw", @"transaction_type":@"", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Purchase", @"transaction_type":@"Lumpsum", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Purchase", @"transaction_type":@"SIP", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"},
                       @{@"date":@"14 Sep, 2018", @"transaction":@"Withdraw", @"transaction_type":@"", @"amount":@"₹ 5,000", @"units":@"24.45", @"nav":@"14.24"}
                       ];
    
    [self.tblTransactionHistory registerNib:[UINib nibWithNibName:@"TransactionHistoryCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TransactionHistoryCell"];
    self.tblTransactionHistory.dataSource = self;
    self.tblTransactionHistory.delegate = self;
    
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryTransaction.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TransactionHistoryCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionHistoryCell"];
    if (aryTransaction.count > indexPath.row) {
        NSDictionary *dicTransaction = aryTransaction[indexPath.row];
        cell.lblDate.text = dicTransaction[@"date"];
        cell.lblTransaction.text = dicTransaction[@"transaction"];
        cell.lblTransType.text = dicTransaction[@"transaction_type"];
        cell.lblAmount.text = dicTransaction[@"amount"];
        cell.lblUnit.text = dicTransaction[@"units"];
        cell.lblNAV.text = dicTransaction[@"nav"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnRequestTransactionReport_Tapped:(id)sender {
    AccountStatementVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AccountStatementVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
