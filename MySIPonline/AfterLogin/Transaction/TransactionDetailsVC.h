//
//  TransactionDetailsVC.h
//  MySIPonline
//
//  Created by Ganpat on 07/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionDetailsVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblTransactionDetails;

@end

NS_ASSUME_NONNULL_END
