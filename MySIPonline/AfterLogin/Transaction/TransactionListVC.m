//
//  TransactionListVC.m
//  MySIPonline
//
//  Created by Ganpat on 05/02/19.
//

#import "TransactionListVC.h"
#import "TransactionUserView.h"
#import "TransactionListCell.h"
#import "NIDropDown.h"
#import "TransactionDetailsVC.h"
#import "RedemptionVC.h"
#import "SwitchVC.h"
#import "STPVC.h"

@interface TransactionListVC () <iCarouselDataSource, iCarouselDelegate, UITableViewDataSource, UITableViewDelegate, NIDropDownDelegate> {
    NSMutableArray *arySlides;
    NSArray *aryTransactions;
    NSArray *aryOptions;
    int currentSlideIndex;
    NIDropDown *dropDownOptions;
}

@end

@implementation TransactionListVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
//    _scrollView.delegate = self;
    
    _carousel.dataSource = self;
    _carousel.delegate = self;
    _carousel.type = iCarouselTypeCoverFlow;//iCarouselTypeLinear; //iCarouselTypeRotary;
    _carousel.vertical = NO;
    _carousel.decelerationRate = 0;
    _carousel.bounces = NO;
    _carousel.scrollSpeed = 0.5;
    
    aryOptions = @[@"Additional Purchase", @"Redemption", @"STP", @"Switch"];
    aryTransactions = @[
                        @{@"name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"investment":@"₹ 55,000", @"current_value":@"₹ 60,000", @"gain_loss":@"₹ 5,000"},
                        @{@"name":@"Tax Saving Portfolio", @"investment":@"₹ 55,000", @"current_value":@"₹ 60,000", @"gain_loss":@"₹ 5,000"},
                        @{@"name":@"ICICI Prudential US Bluechip Equity Fund (G)", @"investment":@"₹ 55,000", @"current_value":@"₹ 60,000", @"gain_loss":@"₹ 5,000"},
                        @{@"name":@"Aggressive Portfolio", @"investment":@"₹ 55,000", @"current_value":@"₹ 60,000", @"gain_loss":@"₹ 5,000"},
                        @{@"name":@"Tata Retirement Savings Fund - Progressive Plan (G)", @"investment":@"₹ 55,000", @"current_value":@"₹ 60,000", @"gain_loss":@"₹ 5,000"}
                        ];
    
    [self.tblTransactionList registerNib:[UINib nibWithNibName:@"TransactionListCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TransactionListCell"];
    self.tblTransactionList.allowsSelection = YES;
    self.tblTransactionList.dataSource = self;
    self.tblTransactionList.delegate = self;
    
    currentSlideIndex = 0;
    [self prepareSliderForUser];
}

-(void)prepareSliderForUser {
    arySlides = [NSMutableArray new];
    
    NSArray *aryTemp = @[
                         @{@"name":@"Sonu Sharma", @"investment":@"50000", @"current_value":@"56000", @"color1":@"6AADFD", @"color2":@"487DFA"},
                         @{@"name":@"Shailendra Naruka", @"investment":@"50000", @"current_value":@"56000", @"color1":@"FCB350", @"color2":@"FC7E44"},
                         @{@"name":@"Deepak Vaishnav", @"investment":@"50000", @"current_value":@"56000", @"color1":@"AB7AEE", @"color2":@"753CDB"},
                         @{@"name":@"Ganpat Lakhara", @"investment":@"50000", @"current_value":@"56000", @"color1":@"FCB350", @"color2":@"FC7E44"}
                         ];
    for (NSDictionary *dic in aryTemp) {
        NSMutableDictionary *mutDic = [dic mutableCopy];
        //NSString *strColors = dic[@"color"];
        //NSArray *aryColor = [strColors.TrimSpace componentsSeparatedByString:@","];
        //if (aryColor.count > 0) {
            NSString *hex1 = [dic[@"color1"] stringByReplacingOccurrencesOfString:@"0xFF" withString:@""];
            NSString *hex2 = [dic[@"color2"] stringByReplacingOccurrencesOfString:@"0xFF" withString:@""];
            mutDic[@"color1"] = [UIColor colorWithHexString:hex1.TrimSpace];
            mutDic[@"color2"] = [UIColor colorWithHexString:hex2.TrimSpace];
        //}
        
        TransactionUserView *slide = [[NSBundle mainBundle] loadNibNamed:@"TransactionUserView" owner:self options:nil].firstObject;
        slide.layer.cornerRadius = 5;
        slide.layer.masksToBounds = YES;
        slide.lblInvestorName.text = dic[@"name"];
        
        [AppHelper setBackgroundGradient:slide Color1:mutDic[@"color1"] Color2:mutDic[@"color2"]];
        [AppHelper addShadowOnView:slide Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
        [arySlides addObject:slide];
    }
    //[self setupSlideScrollView];
    // Page Control
    _pageControl.numberOfPages = arySlides.count;
    _pageControl.currentPage = 0;
    _pageControl.currentPageIndicatorTintColor = [UIColor getAppColorBlue];
    [self.view bringSubviewToFront:_pageControl];
    [_carousel reloadData];
}

//-(void)setupSlideScrollView {
//    _scrollView.frame = CGRectMake(0, 0, self.view.frame.size.width, _scrollView.frame.size.height);
//    //_scrollView.contentSize = CGSizeMake(self.view.frame.size.width * arySlides.count, _scrollView.frame.size.height);
//    //_scrollView.pagingEnabled = YES;
//
//    int xPos = 17;
//    int margin = xPos;
//    for (UIView *view in arySlides) {
//        [_scrollView addSubview:view];
//        view.frame = CGRectMake(xPos, 18, view.frame.size.width, view.frame.size.height);
//        xPos += view.frame.size.width + margin;
//    }
//    _scrollView.contentSize = CGSizeMake(xPos, _scrollView.frame.size.height);
//}

//-(void)scrollViewDidEndDecelerating:(UIScrollView *)scrollView {
//    [self.pageControl scrollViewDidEndDecelerating:scrollView];
//}
//
//-(void)scrollViewDidEndScrollingAnimation:(UIScrollView *)scrollView {
//    [self.pageControl scrollViewDidEndScrollingAnimation:scrollView];
//}
//
//-(void)scrollViewDidScroll:(UIScrollView *)scrollView {
//    [self.pageControl scrollViewDidScroll:scrollView];
//
//    int pageIndex = round(_scrollView.contentOffset.x / 302);
//    NSLog(@"%d", pageIndex);
//    if (pageIndex != currentSlideIndex) {
//        currentSlideIndex = pageIndex;
//        scrollView.contentOffset = CGPointMake(302 * pageIndex, scrollView.contentOffset.y);
//    }
//}

#pragma mark iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel {
    return arySlides.count;
}

- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view {
    return arySlides[index];
}

- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel {
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 0;
}

- (UIView *)carousel:(__unused iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view {
    UILabel *label = nil;
    
    //create new view if no view is available for recycling
    if (view == nil) {
        //don't do anything specific to the index within
        //this `if (view == nil) {...}` statement because the view will be
        //recycled and used with other index values later
        view = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 200.0, 200.0)];
        ((UIImageView *)view).image = [UIImage imageNamed:@"page.png"];
        view.contentMode = UIViewContentModeCenter;
        
        label = [[UILabel alloc] initWithFrame:view.bounds];
        label.backgroundColor = [UIColor clearColor];
        label.textAlignment = NSTextAlignmentCenter;
        label.font = [label.font fontWithSize:50.0];
        label.tag = 1;
        [view addSubview:label];
    } else {
        //get a reference to the label in the recycled view
        label = (UILabel *)[view viewWithTag:1];
    }
    
    //set item label
    //remember to always set any properties of your carousel item
    //views outside of the `if (view == nil) {...}` check otherwise
    //you'll get weird issues with carousel item content appearing
    //in the wrong place in the carousel
    label.text = (index == 0)? @"[": @"]";
    
    return view;
}

- (CATransform3D)carousel:(__unused iCarousel *)carousel itemTransformForOffset:(CGFloat)offset baseTransform:(CATransform3D)transform {
    //implement 'flip3D' style carousel
    transform = CATransform3DRotate(transform, M_PI / 8.0, 0.0, 1.0, 0.0);
    return CATransform3DTranslate(transform, 0.0, 0.0, offset * self.carousel.itemWidth);
}

- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value {
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return NO;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.05;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark - iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index {
    NSLog(@"Tapped view number: %ld", (long)index);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousel {
    NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
}

-(void)carousel:(iCarousel *)carousel myDidSelectItemAtIndex:(NSInteger)index {
    NSLog(@"My Index: %ld", (long)index);
    [_pageControl setCurrentPage:index animated:NO];
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryTransactions.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    TransactionListCell *cell = [tableView dequeueReusableCellWithIdentifier:@"TransactionListCell"];
    if (indexPath.row < aryTransactions.count) {
        NSDictionary *dictTransaction = aryTransactions[indexPath.row];
        cell.btnMoreOption.tag = indexPath.row;
        [cell.btnMoreOption removeTarget:self action:@selector(btnMoreOption_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [cell.btnMoreOption addTarget:self action:@selector(btnMoreOption_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        cell.lblSchemeName.text = dictTransaction[@"name"];
        cell.lblInvestment.text = dictTransaction[@"investment"];
        cell.lblCurrentValue.text = dictTransaction[@"current_value"];
        cell.lblGainLoss.text = dictTransaction[@"gain_loss"];
    }
    return cell;
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [tableView deselectRowAtIndexPath:indexPath animated:YES];
    
    TransactionDetailsVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionDetailsVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnMoreOption_Tapped:(UIButton*)sender {
    if (dropDownOptions == nil) {
        CGFloat height = 120;
        [self hideAllDropDowns];
        CGRect frame = [sender convertRect:sender.bounds toView:self.view];
        UIButton *btn = [[UIButton alloc] initWithFrame:frame];
        dropDownOptions = [[NIDropDown alloc] showDropDown:btn Label:[UILabel new] Height:&height Array:aryOptions ImageArray:nil Direction:@"down" InView:self.view];
        dropDownOptions.delegate = self;
        dropDownOptions.tag = sender.tag;
        self.tblTransactionList.scrollEnabled = NO;
        self.carousel.userInteractionEnabled = NO;
    } else {
        [dropDownOptions hideDropDown:sender InView:self.view];
        dropDownOptions = nil;
        self.tblTransactionList.scrollEnabled = YES;
        self.carousel.userInteractionEnabled = YES;
    }
}

#pragma mark - Drop Down Methods

-(void)hideAllDropDowns {
    if (dropDownOptions) {
        [dropDownOptions hideDropDown];
        dropDownOptions = nil;
    }
}

-(void)niDropDownDelegateMethod:(NIDropDown *)sender {
    if (sender == dropDownOptions) {
        NSString *strSelectedString = [NSString stringWithFormat:@"%@",sender.selectedDictionary];
        NSLog(@"%@ : %ld", strSelectedString, (long)sender.tag);
        dropDownOptions = nil;
        if ([strSelectedString isEqualToString:@"Additional Purchase"]) {
            
        } else if ([strSelectedString isEqualToString:@"Redemption"]) {
            RedemptionVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"RedemptionVC"];
            [self.navigationController pushViewController:destVC animated:YES];
            
        } else if ([strSelectedString isEqualToString:@"STP"]) {
            STPVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"STPVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        
        } else if ([strSelectedString isEqualToString:@"Switch"]) {
            SwitchVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SwitchVC"];
            [self.navigationController pushViewController:destVC animated:YES];
        }
    }
    self.tblTransactionList.scrollEnabled = YES;
    self.carousel.userInteractionEnabled = YES;
}

@end
