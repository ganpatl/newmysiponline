//
//  TransactionHistoryCell.h
//  MySIPonline
//
//  Created by Ganpat on 08/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionHistoryCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblDate;
@property (weak, nonatomic) IBOutlet UILabel *lblTransaction;
@property (weak, nonatomic) IBOutlet UILabel *lblTransType;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount;
@property (weak, nonatomic) IBOutlet UILabel *lblUnit;
@property (weak, nonatomic) IBOutlet UILabel *lblNAV;

@end

NS_ASSUME_NONNULL_END
