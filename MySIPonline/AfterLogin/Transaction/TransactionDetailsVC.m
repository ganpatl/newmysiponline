//
//  TransactionDetailsVC.m
//  MySIPonline
//
//  Created by Ganpat on 07/02/19.
//

#import "TransactionDetailsVC.h"
#import "TransactionHistoryVC.h"

// Cell
#import "TransactionDetailsCell.h"
#import "TransactionNoteCell.h"
#import "InvestmentOptionCell.h"

@interface TransactionDetailsVC () <UITableViewDataSource> {
    TransactionDetailsCell *transactionDetailsCell;
    TransactionNoteCell *transactionNoteCell;
    InvestmentOptionCell *investmentOptionCell;
    NSMutableArray *aryDisplayRows;
}

@end

@implementation TransactionDetailsVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    aryDisplayRows = [NSMutableArray new];
    [AppHelper addTopShadowOnView:self.topTitleView];
    [self.tblTransactionDetails registerNib:[UINib nibWithNibName:@"TransactionDetailsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TransactionDetailsCell"];
    [self.tblTransactionDetails registerNib:[UINib nibWithNibName:@"TransactionNoteCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TransactionNoteCell"];
    [self.tblTransactionDetails registerNib:[UINib nibWithNibName:@"InvestmentOptionCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentOptionCell"];
    
    self.tblTransactionDetails.dataSource = self;
    
    [aryDisplayRows addObject:@"details"];
    [aryDisplayRows addObject:@"note"];
    [aryDisplayRows addObject:@"inprocess"];
    
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return aryDisplayRows.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    NSString *strRow = aryDisplayRows[indexPath.row];
    if ([strRow isEqualToString:@"details"]) {
        if (transactionDetailsCell == nil) {
            transactionDetailsCell = [tableView dequeueReusableCellWithIdentifier:@"TransactionDetailsCell"];
            [transactionDetailsCell.btnTransactionHistory addTarget:self action:@selector(btnTransactionHistory_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return transactionDetailsCell;
    
    } else if ([strRow isEqualToString:@"note"]) {
        if (transactionNoteCell == nil) {
            transactionNoteCell = [tableView dequeueReusableCellWithIdentifier:@"TransactionNoteCell"];
            
        }
        return transactionNoteCell;
        
    } else if ([strRow isEqualToString:@"inprocess"]) {
        if (investmentOptionCell == nil) {
            investmentOptionCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentOptionCell"];
            investmentOptionCell.shadowView.backgroundColor = [UIColor colorWithHexString:@"FFFDF1"];
            investmentOptionCell.lblTitle.text = @"1 Transaction In process";
            investmentOptionCell.lblDesc.text = @"Dashboard will updated once transaction gets processed";
            investmentOptionCell.imgIcon.image = [UIImage imageNamed:@"info-error"];
        }
        return investmentOptionCell;
    }
    return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

-(void)btnTransactionHistory_Tapped {
    TransactionHistoryVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TransactionHistoryVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}


@end
