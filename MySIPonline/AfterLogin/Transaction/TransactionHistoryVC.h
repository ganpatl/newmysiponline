//
//  TransactionHistoryVC.h
//  MySIPonline
//
//  Created by Ganpat on 08/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionHistoryVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UITableView *tblTransactionHistory;

@end

NS_ASSUME_NONNULL_END
