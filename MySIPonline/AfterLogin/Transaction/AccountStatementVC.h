//
//  AccountStatementVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface AccountStatementVC : UIViewController

@property (weak, nonatomic) IBOutlet RKTagsView *investorTagsView;
@property (weak, nonatomic) IBOutlet UIView *topTitleView;

@property (weak, nonatomic) IBOutlet UIImageView *imgPortfolioStatement;
@property (weak, nonatomic) IBOutlet UIImageView *imgTransactionStatement;
@property (weak, nonatomic) IBOutlet UIImageView *imgCapitalGainReport;

@property (weak, nonatomic) IBOutlet UIImageView *imgCurrentFinancialYear;
@property (weak, nonatomic) IBOutlet UIImageView *imgFinancialYear;
@property (weak, nonatomic) IBOutlet UILabel *lblCurrentFinancialYear;
@property (weak, nonatomic) IBOutlet UILabel *lblFinancialYear;

@end

NS_ASSUME_NONNULL_END
