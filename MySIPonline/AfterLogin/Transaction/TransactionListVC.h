//
//  TransactionListVC.h
//  MySIPonline
//
//  Created by Ganpat on 05/02/19.
//

#import <UIKit/UIKit.h>
#import "MDCPageControl.h"
#import "iCarousel.h"

NS_ASSUME_NONNULL_BEGIN

@interface TransactionListVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
//@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (weak, nonatomic) IBOutlet UITableView *tblTransactionList;
@property (weak, nonatomic) IBOutlet MDCPageControl *pageControl;
@property (weak, nonatomic) IBOutlet iCarousel *carousel;

@end

NS_ASSUME_NONNULL_END
