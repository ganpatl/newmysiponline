//
//  AccountStatementVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/02/19.
//

#import "AccountStatementVC.h"

@interface AccountStatementVC () <RKTagsViewDelegate> {
    NSArray *aryInvestor;
    UIImage *imgTypeSelect;
    UIImage *imgDurationSelect;
    UIImage *imgDeselect;
}
@end

@implementation AccountStatementVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    imgTypeSelect = [UIImage imageNamed:@"done-Green"];
    imgDurationSelect = [UIImage imageNamed:@"checked"];
    imgDeselect = [UIImage imageNamed:@"done-Gray"];
    
    
    aryInvestor = @[@{@"name":@"Sonu"}, @{@"name":@"Praveen"}];
    
    // Setup RKTagsView
    [self.investorTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
    self.investorTagsView.tagButtonHeight = 30;
    self.investorTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
    self.investorTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
    self.investorTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
    self.investorTagsView.selectedTagTintColor = [UIColor whiteColor];
    self.investorTagsView.editable = NO;
    self.investorTagsView.allowsMultipleSelection = NO;
    self.investorTagsView.lineSpacing = 8;
    self.investorTagsView.interitemSpacing = 8;
    self.investorTagsView.buttonCornerRadius = 14;
    self.investorTagsView.delegate = self;
    self.investorTagsView.scrollView.showsHorizontalScrollIndicator = NO;
    
    if (self.investorTagsView.tags.count != self->aryInvestor.count) {
        [self.investorTagsView removeAllTags];
        for (NSDictionary *dic in self->aryInvestor) {
            if ([AppHelper isNotNull:dic[@"name"]]) {
                [self.investorTagsView addTag:dic[@"name"]];
            }
        }
        // Set First item selected
        [self.investorTagsView selectTagAtIndex:0];
    }
    
}

#pragma mark - RK Tags View Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 10) {
        
    } else if (tagsView.tag == 11) {
        // SIP Date
        
    } else if (tagsView.tag == 12) {
        // Investor
        
    }
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}



#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

// Statement Type
- (IBAction)btnPortfolioStatement_Tapped:(id)sender {
    self.imgPortfolioStatement.image = imgTypeSelect;
    self.imgTransactionStatement.image = imgDeselect;
    self.imgCapitalGainReport.image = imgDeselect;
}

- (IBAction)btnTransactionStatement_Tapped:(id)sender {
    self.imgPortfolioStatement.image = imgDeselect;
    self.imgTransactionStatement.image = imgTypeSelect;
    self.imgCapitalGainReport.image = imgDeselect;
}

- (IBAction)btnCapitalGainReport_Tapped:(id)sender {
    self.imgPortfolioStatement.image = imgDeselect;
    self.imgTransactionStatement.image = imgDeselect;
    self.imgCapitalGainReport.image = imgTypeSelect;
}

// Duration for Statement

- (IBAction)btnCurrentFinancialYear_Tapped:(id)sender {
    self.imgCurrentFinancialYear.image = imgDurationSelect;
    self.imgFinancialYear.image = imgDeselect;
}

- (IBAction)btnFinancialYear_Tapped:(id)sender {
    self.imgCurrentFinancialYear.image = imgDeselect;
    self.imgFinancialYear.image = imgDurationSelect;
}



- (IBAction)btnRequestStatement_Tapped:(id)sender {
    [AppHelper showToast:@"Statement Sent On Registered Email Address"];
}


@end
