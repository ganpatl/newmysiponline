//
//  TransactionDetailsCell.h
//  MySIPonline
//
//  Created by Ganpat on 07/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionDetailsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreOption;
@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnMoreInvest;
@property (weak, nonatomic) IBOutlet UIButton *btnRedemtion;
@property (weak, nonatomic) IBOutlet UIButton *btnTransactionHistory;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblInvestmentAmount;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblCurrentValue;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblGainLoss;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblTotalUnit;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblFolioNo;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblAnnualReturn;
@property (weak, nonatomic) IBOutlet BodyDarkLabel *lblCgrReturn;

@end

NS_ASSUME_NONNULL_END
