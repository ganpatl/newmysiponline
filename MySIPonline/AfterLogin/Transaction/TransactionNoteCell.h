//
//  TransactionNoteCell.h
//  MySIPonline
//
//  Created by Ganpat on 08/02/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TransactionNoteCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblNote;

@end

NS_ASSUME_NONNULL_END
