//
//  SwitchVC.m
//  MySIPonline
//
//  Created by Ganpat on 23/02/19.
//

#import "SwitchVC.h"
#import "RKTagsView.h"

// Cells
#import "SwitchCell.h"

@interface SwitchVC () <UITableViewDataSource, RKTagsViewDelegate> {
    SwitchCell *switchCell;
}

@end

@implementation SwitchVC

- (void)viewDidLoad {
    [super viewDidLoad];

    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblSwitch registerNib:[UINib nibWithNibName:@"SwitchCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"SwitchCell"];
    self.tblSwitch.dataSource = self;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (switchCell == nil) {
        switchCell = [tableView dequeueReusableCellWithIdentifier:@"SwitchCell"];
        // Setup RKTagsView
        [switchCell.switchTypeTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
        switchCell.switchTypeTagsView.tagButtonHeight = 30;
        switchCell.switchTypeTagsView.tagBackgroundColor = [UIColor colorWithHexString:@"EEEEEE"];
        switchCell.switchTypeTagsView.tagSelectedBackgroundColor = [UIColor getAppColorBlue];
        switchCell.switchTypeTagsView.tintColor = [UIColor colorWithHexString:@"2D2D2D"];
        switchCell.switchTypeTagsView.selectedTagTintColor = [UIColor whiteColor];
        switchCell.switchTypeTagsView.editable = NO;
        switchCell.switchTypeTagsView.allowsMultipleSelection = NO;
        switchCell.switchTypeTagsView.lineSpacing = 8;
        switchCell.switchTypeTagsView.interitemSpacing = 8;
        switchCell.switchTypeTagsView.buttonCornerRadius = 3;
        switchCell.switchTypeTagsView.delegate = self;
        switchCell.switchTypeTagsView.scrollView.showsHorizontalScrollIndicator = NO;

        NSArray *aryType = @[@{@"name":@"Partial"}, @{@"name":@"Full"}];
        if (switchCell.switchTypeTagsView.tags.count != aryType.count) {
            [switchCell.switchTypeTagsView removeAllTags];
            for (NSDictionary *dic in aryType) {
                if ([AppHelper isNotNull:dic[@"name"]]) {
                    [switchCell.switchTypeTagsView addTag:dic[@"name"]];
                }
            }
            // Set First item selected
            [switchCell.switchTypeTagsView selectTagAtIndex:0];
        }
    }
    return switchCell;
    
    return [UITableViewCell new];
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (index == 0) {
        // Partial
        
    } else {
        // Full
        
    }
    return YES;
}

#pragma mark - Button Tap Events

- (IBAction)bnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnSendOTP_Tapped:(id)sender {
    
}

@end
