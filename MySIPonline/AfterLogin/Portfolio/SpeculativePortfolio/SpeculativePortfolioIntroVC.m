//
//  SpeculativePortfolioIntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/03/19.
//

#import "SpeculativePortfolioIntroVC.h"
#import "SpeculativePortfolioVC.h"

// Cells
#import "TaxSavingIntroCell.h"

@interface SpeculativePortfolioIntroVC () <UITableViewDataSource> {
    TaxSavingIntroCell *taxSavingIntroCell;
    CAGradientLayer *theViewGradient;
}
@end

@implementation SpeculativePortfolioIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"FFAABD"] CGColor], (id)[[UIColor colorWithHexString:@"F34F75"] CGColor], (id)[[UIColor colorWithHexString:@"E7405B"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TaxSavingIntroCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TaxSavingIntroCell"];
    
    self.tblPortfolio.dataSource = self;
}

-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (taxSavingIntroCell == nil) {
        taxSavingIntroCell = [tableView dequeueReusableCellWithIdentifier:@"TaxSavingIntroCell"];
        taxSavingIntroCell.selectionStyle = UITableViewCellSelectionStyleNone;
        taxSavingIntroCell.lblMainTitle.text = @"Endless Growth, Highest Risk";
        taxSavingIntroCell.lblMainDesc.text = @"Invest in speculative opportunities, so risk is very high but so is the growth.";
        taxSavingIntroCell.imgMainIcon.image = [UIImage imageNamed:@"Dynamic"];
        taxSavingIntroCell.lblWhyInvest.text = @"Why to Invest in Speculative Portfolio?";
        taxSavingIntroCell.img1.image = [UIImage imageNamed:@"chart"];
        taxSavingIntroCell.lblTitle1.text = @"Opportunity Grabber of Market";
        taxSavingIntroCell.lblDesc1.text = @"Tracks the funds that are undervalued or have gone through huge fluctuations, but hold great potential for growth.";
        taxSavingIntroCell.img2.image = [UIImage imageNamed:@"investments"];
        taxSavingIntroCell.lblTitle2.text = @"Unpredictable Growth Possibilities";
        taxSavingIntroCell.lblDesc2.text = @"As the growth of speculative investment options can’t be predicted, so is the growth provided by this portfolio.";
        taxSavingIntroCell.img3.image = [UIImage imageNamed:@"wealth-creation"];
        taxSavingIntroCell.lblTitle3.text = @"Boon in Market Upside";
        taxSavingIntroCell.lblDesc3.text = @"This portfolio has a track record of providing growth better than others during the bull market phase.";
    }
    return taxSavingIntroCell;
    
    //return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnViewPortfolio_Tapped:(id)sender {
    SpeculativePortfolioVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"SpeculativePortfolioVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}


@end
