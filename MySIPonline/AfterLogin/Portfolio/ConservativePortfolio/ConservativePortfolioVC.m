//
//  ConservativePortfolioVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/03/19.
//

#import "ConservativePortfolioVC.h"
#import "XYDoughnutChart.h"
#import "PortfolioAmountVC.h"

// Cells
#import "TrustedByCell.h"
#import "InvestmentModeCell.h"
#import "PieChartCell.h"
#import "LineChartCompareCell.h"

@interface ConservativePortfolioVC () <UITableViewDataSource, XYDoughnutChartDataSource, XYDoughnutChartDelegate> {
    TrustedByCell *trustedByCell;
    InvestmentModeCell *investmentModeCell;
    PieChartCell *pieChartCell;
    LineChartCompareCell *lineChartCompareCell;
    CAGradientLayer *theViewGradient;
    NSMutableArray *aryPieSlice;
    NSArray *aryColor;
}
@end

@implementation ConservativePortfolioVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"31C691"] CGColor], (id)[[UIColor colorWithHexString:@"2BCE9D"] CGColor], (id)[[UIColor colorWithHexString:@"1CAA8F"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    aryColor = @[
                 [UIColor colorWithHexString:@"49D981"],
                 [UIColor colorWithHexString:@"FB8005"],
                 [UIColor colorWithHexString:@"4285F4"]
                 ];
    aryPieSlice = [NSMutableArray new];
    [aryPieSlice addObject:[NSNumber numberWithFloat:33.33]];
    [aryPieSlice addObject:[NSNumber numberWithFloat:33.33]];
    [aryPieSlice addObject:[NSNumber numberWithFloat:33.33]];
    
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TrustedByCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TrustedByCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"InvestmentModeCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"InvestmentModeCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"PieChartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PieChartCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"LineChartCompareCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LineChartCompareCell"];
    
    self.tblPortfolio.dataSource = self;
}


-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (trustedByCell == nil) {
            trustedByCell = [tableView dequeueReusableCellWithIdentifier:@"TrustedByCell"];            
        }
        return trustedByCell;
        
    } else if (indexPath.row == 1) {
        if (investmentModeCell == nil) {
            investmentModeCell = [tableView dequeueReusableCellWithIdentifier:@"InvestmentModeCell"];
            investmentModeCell.selectionStyle = UITableViewCellSelectionStyleNone;
            investmentModeCell.lblInvestmentMode.text = @"Select Investment Mode";
        }
        return investmentModeCell;
        
    } else if (indexPath.row == 2) {
        if (pieChartCell == nil) {
            pieChartCell = [tableView dequeueReusableCellWithIdentifier:@"PieChartCell"];
            pieChartCell.chartView.radiusOffset = 0.2;
            pieChartCell.chartView.labelFont = [UIFont systemFontOfSize:11];
            [pieChartCell.chartView setShowPercentage:NO];
            [pieChartCell.chartView setLabelColor:[UIColor whiteColor]];
            pieChartCell.chartView.dataSource = self;
            pieChartCell.chartView.delegate = self;
            [pieChartCell.chartView reloadData];
        }
        return pieChartCell;
        
    } else if (indexPath.row == 3) {
        if (lineChartCompareCell == nil) {
            lineChartCompareCell = [tableView dequeueReusableCellWithIdentifier:@"LineChartCompareCell"];
            //lineChartCompareCell.chartView
            lineChartCompareCell.chartView.dragEnabled = YES;
            [lineChartCompareCell.chartView setScaleEnabled:YES];
            lineChartCompareCell.chartView.pinchZoomEnabled = NO;
            
            // Hide all axis data & horizontal grid line
            lineChartCompareCell.chartView.rightAxis.enabled = NO;
            lineChartCompareCell.chartView.leftAxis.enabled = YES;
            lineChartCompareCell.chartView.xAxis.enabled = NO;
            
            //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
            //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
            
            // Hide legend
            lineChartCompareCell.chartView.legend.form = ChartLegendFormNone;
            
            lineChartCompareCell.chartView.highlightPerTapEnabled = YES;
            lineChartCompareCell.chartView.drawMarkers = YES;
        }
        
        //        BalloonMarker *marker = [[BalloonMarker alloc]
        //                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
        //                                 font: [UIFont systemFontOfSize:12.0]
        //                                 textColor: UIColor.whiteColor
        //                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
        //        marker.chartView = _chartView;
        //        marker.minimumSize = CGSizeMake(80.f, 40.f);
        //        lineChartCell.chartView.marker = marker;
        
        //         In Use
        //        BalloonMarker *marker = [[BalloonMarker alloc]
        //                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
        //                                 font: [UIFont systemFontOfSize:12.0]
        //                                 textColor: UIColor.whiteColor
        //                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
        //        marker.chartView = lineChartCompareCell.chartView;;
        //        marker.minimumSize = CGSizeMake(80.f, 40.f);
        //        lineChartCompareCell.chartView.marker = marker;
        
        
        
        //_sliderX.value = 45.0;
        //_sliderY.value = 100.0;
        //[self slidersValueChanged:nil];
        [self setDataCount:60 range:60 ChartView:lineChartCompareCell.chartView];
        
        [lineChartCompareCell.chartView animateWithXAxisDuration:2.5];
        
        return lineChartCompareCell;
    }
    return [UITableViewCell new];
}

- (void)setDataCount:(int)count range:(double)range ChartView:(ChartViewBase *)chartView {
    // Create DataSet 1
    double dynamicRange = 1;
    NSMutableArray *values1 = [[NSMutableArray alloc] init];
    for (int i = 0; i < count; i++) {
        double val = arc4random_uniform (dynamicRange) + 3;
        [values1 addObject:[[ChartDataEntry alloc] initWithX:i y:val]];
        dynamicRange += 1;
    }
    // Create DataSet 2
    dynamicRange = 1;
    NSMutableArray *values2 = [[NSMutableArray alloc] init];
    for (int i = 0; i < count; i++) {
        double val = arc4random_uniform (dynamicRange) + 3;
        [values2 addObject:[[ChartDataEntry alloc] initWithX:i y:val]];
        dynamicRange += 1;
    }
    
    // Verticle Height
    ChartYAxis *leftAxis = lineChartCompareCell.chartView.leftAxis;
    leftAxis.axisMaximum = dynamicRange;
    leftAxis.axisMinimum = -60;
    
    LineChartDataSet *set1 = nil;
    if (chartView.data.dataSetCount > 0) {
        set1 = (LineChartDataSet *)chartView.data.dataSets[0];
        set1.values = values1;
        [chartView.data notifyDataChanged];
        [chartView notifyDataSetChanged];
    } else {
        set1 = [[LineChartDataSet alloc] initWithValues:values1 label:@""]; // DataSet 1
        set1.drawIconsEnabled = NO;
        [set1 setColor:[UIColor colorWithHexString:@"49D981"]];
        set1.lineWidth = 1.0;
        //[set1 setCircleColor:UIColor.blackColor];
        //set1.circleRadius = 3.0;
        set1.drawCirclesEnabled = NO;
        //set1.drawCircleHoleEnabled = YES;
        set1.valueFont = [UIFont systemFontOfSize:9.f];
        set1.formLineDashLengths = @[ @5.f, @2.5f ];
        set1.formSize = 15.0;
        
        
        //[set1 setHighlightEnabled:YES]; // handle cross hair
        // Hide Top Values
        set1.drawValuesEnabled = NO;
        set1.drawFilledEnabled = NO;
    }
    
    LineChartDataSet *set2 = nil;
    if (chartView.data.dataSetCount > 1) {
        set2 = (LineChartDataSet *)chartView.data.dataSets[1];
        set2.values = values2;
        [chartView.data notifyDataChanged];
        [chartView notifyDataSetChanged];
    } else {
        set2 = [[LineChartDataSet alloc] initWithValues:values2 label:@""]; // DataSet 2
        set2.drawIconsEnabled = NO;
        [set2 setColor:[UIColor colorWithHexString:@"FF7141"]];
        set2.lineWidth = 1.0;
        //[set1 setCircleColor:UIColor.blackColor];
        //set1.circleRadius = 3.0;
        set2.drawCirclesEnabled = NO;
        //set1.drawCircleHoleEnabled = YES;
        set2.valueFont = [UIFont systemFontOfSize:9.f];
        set2.formLineDashLengths = @[ @5.f, @2.5f ];
        set2.formSize = 15.0;
        
        //[set1 setHighlightEnabled:YES]; // handle cross hair
        // Hide Top Values
        set2.drawValuesEnabled = NO;
        set2.drawFilledEnabled = NO;
    }
    
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];
    LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
    chartView.data = data;
}

#pragma mark - Pie Chart Data Source

-(NSInteger)numberOfSlicesInDoughnutChart:(XYDoughnutChart *)doughnutChart {
    return aryPieSlice.count;
}

-(CGFloat)doughnutChart:(XYDoughnutChart *)doughnutChart valueForSliceAtIndexPath:(XYDoughnutIndexPath *)indexPath {
    return [aryPieSlice[indexPath.slice] intValue];
}

-(NSString *)doughnutChart:(XYDoughnutChart *)doughnutChart textForSliceAtIndexPath:(XYDoughnutIndexPath *)indexPath {
    return [NSString stringWithFormat:@"%@%%", aryPieSlice[indexPath.slice]];
}

#pragma mark - Pie Chart Delegate

-(UIColor *)doughnutChart:(XYDoughnutChart *)doughnutChart colorForSliceAtIndexPath:(XYDoughnutIndexPath *)indexPath {
    return aryColor[indexPath.slice];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnInvestNow_Tapped:(id)sender {
    PortfolioAmountVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PortfolioAmountVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
