//
//  ConservativePortfolioIntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 15/03/19.
//

#import "ConservativePortfolioIntroVC.h"
#import "ConservativePortfolioVC.h"

// Cells
#import "TaxSavingIntroCell.h"

@interface ConservativePortfolioIntroVC () <UITableViewDataSource> {
    TaxSavingIntroCell *taxSavingIntroCell;
    CAGradientLayer *theViewGradient;
}
@end

@implementation ConservativePortfolioIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"31C691"] CGColor], (id)[[UIColor colorWithHexString:@"2BCE9D"] CGColor], (id)[[UIColor colorWithHexString:@"1CAA8F"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TaxSavingIntroCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TaxSavingIntroCell"];
    
    self.tblPortfolio.dataSource = self;
}

-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (taxSavingIntroCell == nil) {
        taxSavingIntroCell = [tableView dequeueReusableCellWithIdentifier:@"TaxSavingIntroCell"];
        taxSavingIntroCell.selectionStyle = UITableViewCellSelectionStyleNone;
        taxSavingIntroCell.lblMainTitle.text = @"Moderate Growth with Minimal Risk";
        taxSavingIntroCell.lblMainDesc.text = @"Enjoy a moderate growth of your investments with lower level of risks.";
        taxSavingIntroCell.imgMainIcon.image = [UIImage imageNamed:@"safe"];
        taxSavingIntroCell.lblWhyInvest.text = @"Why to Invest in Conservative Portfolio?";
        taxSavingIntroCell.img1.image = [UIImage imageNamed:@"wealth-creation"];
        taxSavingIntroCell.lblTitle1.text = @"Less Affected By Volatility";
        taxSavingIntroCell.lblDesc1.text = @"Due to high investments in debt and money market instruments, it shows less fluctuations during the volatility in the equity market.";
        taxSavingIntroCell.img2.image = [UIImage imageNamed:@"investments"];
        taxSavingIntroCell.lblTitle2.text = @"Perfect to Start Savings";
        taxSavingIntroCell.lblDesc2.text = @"As it shows less fluctuations during the market volatility, it is the best alternative for your conventional saving instruments.";
        taxSavingIntroCell.img3.image = [UIImage imageNamed:@"chart"];
        taxSavingIntroCell.lblTitle3.text = @"Least Risky Investment";
        taxSavingIntroCell.lblDesc3.text = @"The investment in debt space is the highest, thus is the least risky investment among all the options.";
    }
    return taxSavingIntroCell;
    
    //return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnViewPortfolio_Tapped:(id)sender {
    ConservativePortfolioVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ConservativePortfolioVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
