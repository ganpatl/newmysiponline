//
//  BalancedPortfolioIntroVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface BalancedPortfolioIntroVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblPortfolio;
@property (weak, nonatomic) IBOutlet UIView *backGradientView;

@end

NS_ASSUME_NONNULL_END
