//
//  BalancedPortfolioVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/03/19.
//

#import "BalancedPortfolioVC.h"
#import "MySIPonline-Swift.h"
#import "XYDoughnutChart.h"
#import "PortfolioAmountVC.h"

// Cells
#import "TrustedByCell.h"
#import "PieChartCell.h"
#import "LineChartCell.h"
#import "BalancePortfolioNoteCell.h"

@interface BalancedPortfolioVC () <UITableViewDataSource, XYDoughnutChartDataSource, XYDoughnutChartDelegate> {
    TrustedByCell *trustedByCell;
    PieChartCell *pieChartCell;
    LineChartCell *lineChartCell;
    BalancePortfolioNoteCell *balancePortfolioNoteCell;
    
    CAGradientLayer *theViewGradient;
    NSMutableArray *aryPieSlice;
    NSArray *aryColor;
}
@end

@implementation BalancedPortfolioVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"47C8DE"] CGColor], (id)[[UIColor colorWithHexString:@"1DBBD6"] CGColor], (id)[[UIColor colorWithHexString:@"0AA7A1"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    aryColor = @[
                 [UIColor colorWithHexString:@"49D981"],
                 [UIColor colorWithHexString:@"FB8005"],
                 [UIColor colorWithHexString:@"4285F4"]
                 ];
    aryPieSlice = [NSMutableArray new];
    [aryPieSlice addObject:[NSNumber numberWithFloat:33.33]];
    [aryPieSlice addObject:[NSNumber numberWithFloat:33.33]];
    [aryPieSlice addObject:[NSNumber numberWithFloat:33.33]];
    
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TrustedByCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TrustedByCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"PieChartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PieChartCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"LineChartCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"LineChartCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"BalancePortfolioNoteCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"BalancePortfolioNoteCell"];
    
    self.tblPortfolio.dataSource = self;
}


-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 4;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (trustedByCell == nil) {
            trustedByCell = [tableView dequeueReusableCellWithIdentifier:@"TrustedByCell"];        
        }
        return trustedByCell;
        
    } else if (indexPath.row == 1) {
        if (pieChartCell == nil) {
            pieChartCell = [tableView dequeueReusableCellWithIdentifier:@"PieChartCell"];
            pieChartCell.chartView.radiusOffset = 0.2;
            pieChartCell.chartView.labelFont = [UIFont systemFontOfSize:11];
            [pieChartCell.chartView setShowPercentage:NO];
            [pieChartCell.chartView setLabelColor:[UIColor whiteColor]];
            pieChartCell.chartView.dataSource = self;
            pieChartCell.chartView.delegate = self;
            [pieChartCell.chartView reloadData];
        }
        return pieChartCell;
        
    } else if (indexPath.row == 2) {
        if (lineChartCell == nil) {
            lineChartCell = [tableView dequeueReusableCellWithIdentifier:@"LineChartCell"];
            lineChartCell.shadowView.hidden = YES;
            lineChartCell.lblShowingPerformanceSince.hidden = NO;
            lineChartCell.leftMarginConstraint.constant = 8;
            lineChartCell.rightMarginConstraint.constant = 8;
            
            lineChartCell.chartView.dragEnabled = YES;
            [lineChartCell.chartView setScaleEnabled:YES];
            lineChartCell.chartView.pinchZoomEnabled = YES;
            
            // Hide all axis data & horizontal grid line
            lineChartCell.chartView.rightAxis.enabled = NO;
            lineChartCell.chartView.leftAxis.enabled = NO;
            lineChartCell.chartView.xAxis.enabled = NO;
            
            //[_chartView.viewPortHandler setMaximumScaleY: 2.f];
            //[_chartView.viewPortHandler setMaximumScaleX: 2.f];
            
            // Hide legend
            lineChartCell.chartView.legend.form = ChartLegendFormNone;
            
            lineChartCell.chartView.highlightPerTapEnabled = YES;
            lineChartCell.chartView.drawMarkers = YES;
        }
        ChartYAxis *leftAxis = lineChartCell.chartView.leftAxis;
        leftAxis.axisMaximum = 1000.0;
        leftAxis.axisMinimum = 0;
        
        
        //        BalloonMarker *marker = [[BalloonMarker alloc]
        //                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
        //                                 font: [UIFont systemFontOfSize:12.0]
        //                                 textColor: UIColor.whiteColor
        //                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
        //        marker.chartView = _chartView;
        //        marker.minimumSize = CGSizeMake(80.f, 40.f);
        //        lineChartCell.chartView.marker = marker;
        
        BalloonMarker *marker = [[BalloonMarker alloc]
                                 initWithColor: [UIColor colorWithWhite:180/255. alpha:1.0]
                                 font: [UIFont systemFontOfSize:12.0]
                                 textColor: UIColor.whiteColor
                                 insets: UIEdgeInsetsMake(8.0, 8.0, 20.0, 8.0)];
        marker.chartView = lineChartCell.chartView;;
        marker.minimumSize = CGSizeMake(80.f, 40.f);
        lineChartCell.chartView.marker = marker;
        
        
        
        //_sliderX.value = 45.0;
        //_sliderY.value = 100.0;
        //[self slidersValueChanged:nil];
        [self setDataCount:30 range:1000 ChartView:lineChartCell.chartView];
        
        [lineChartCell.chartView animateWithXAxisDuration:2.5];
        
        return lineChartCell;
     
    } else if (indexPath.row == 3) {
        if (balancePortfolioNoteCell == nil) {
            balancePortfolioNoteCell = [tableView dequeueReusableCellWithIdentifier:@"BalancePortfolioNoteCell"];
        }
        return balancePortfolioNoteCell;
    }
    return [UITableViewCell new];
}

- (void)setDataCount:(int)count range:(double)range ChartView:(ChartViewBase *)chartView {
    NSMutableArray *values = [[NSMutableArray alloc] init];
    
    for (int i = 0; i < count; i++) {
        double val = arc4random_uniform (range) + 3;
        [values addObject:[[ChartDataEntry alloc] initWithX:i y:val icon:[UIImage imageNamed:@"icon"]]];
    }
    
    LineChartDataSet *set1 = nil;
    if (chartView.data.dataSetCount > 0) {
        set1 = (LineChartDataSet *)chartView.data.dataSets[0];
        set1.values = values;
        [chartView.data notifyDataChanged];
        [chartView notifyDataSetChanged];
    } else {
        set1 = [[LineChartDataSet alloc] initWithValues:values label:@""]; // DataSet 1
        set1.drawIconsEnabled = NO;
        [set1 setColor:[UIColor colorWithHexString:@"49D981"]];
        set1.lineWidth = 1.0;
        //[set1 setCircleColor:UIColor.blackColor];
        //set1.circleRadius = 3.0;
        set1.drawCirclesEnabled = NO;
        //set1.drawCircleHoleEnabled = YES;
        set1.valueFont = [UIFont systemFontOfSize:9.f];
        set1.formLineDashLengths = @[ @5.f, @2.5f ];
        set1.formSize = 15.0;
        
        
        //[set1 setHighlightEnabled:YES]; // handle cross hair
        // Hide Top Values
        set1.drawValuesEnabled = NO;
        
        // Set gradient fill
        NSArray *gradientColors = @[
                                    //(id)[ChartColorTemplates colorFromString:@"#00ff0000"].CGColor,
                                    //(id)[ChartColorTemplates colorFromString:@"#ffff0000"].CGColor
                                    (id)[UIColor whiteColor].CGColor,
                                    (id)[UIColor colorWithHexString:@"21CE99"].CGColor
                                    ];
        CGGradientRef gradient = CGGradientCreateWithColors (nil, (CFArrayRef)gradientColors, nil);
        set1.fillAlpha = 1.f;
        set1.fill = [ChartFill fillWithLinearGradient:gradient angle:90.f];
        set1.drawFilledEnabled = YES;
        CGGradientRelease (gradient);
        
        
        NSMutableArray *dataSets = [[NSMutableArray alloc] init];
        [dataSets addObject:set1];
        
        LineChartData *data = [[LineChartData alloc] initWithDataSets:dataSets];
        
        chartView.data = data;
    }
}

#pragma mark - Pie Chart Data Source

-(NSInteger)numberOfSlicesInDoughnutChart:(XYDoughnutChart *)doughnutChart {
    return aryPieSlice.count;
}

-(CGFloat)doughnutChart:(XYDoughnutChart *)doughnutChart valueForSliceAtIndexPath:(XYDoughnutIndexPath *)indexPath {
    return [aryPieSlice[indexPath.slice] intValue];
}

-(NSString *)doughnutChart:(XYDoughnutChart *)doughnutChart textForSliceAtIndexPath:(XYDoughnutIndexPath *)indexPath {
    return [NSString stringWithFormat:@"%@%%", aryPieSlice[indexPath.slice]];
}

#pragma mark - Pie Chart Delegate

-(UIColor *)doughnutChart:(XYDoughnutChart *)doughnutChart colorForSliceAtIndexPath:(XYDoughnutIndexPath *)indexPath {
    return aryColor[indexPath.slice];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnInvestNow_Tapped:(id)sender {
    PortfolioAmountVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"PortfolioAmountVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
