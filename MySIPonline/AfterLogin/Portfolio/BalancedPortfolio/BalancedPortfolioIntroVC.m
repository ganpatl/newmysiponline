//
//  BalancedPortfolioIntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/03/19.
//

#import "BalancedPortfolioIntroVC.h"
#import "BalancedPortfolioVC.h"

// Cells
#import "TaxSavingIntroCell.h"

@interface BalancedPortfolioIntroVC () <UITableViewDataSource> {
    TaxSavingIntroCell *taxSavingIntroCell;
    CAGradientLayer *theViewGradient;
}
@end

@implementation BalancedPortfolioIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"47C8DE"] CGColor], (id)[[UIColor colorWithHexString:@"1DBBD6"] CGColor], (id)[[UIColor colorWithHexString:@"0AA7A1"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TaxSavingIntroCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TaxSavingIntroCell"];
    
    self.tblPortfolio.dataSource = self;
}

-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (taxSavingIntroCell == nil) {
        taxSavingIntroCell = [tableView dequeueReusableCellWithIdentifier:@"TaxSavingIntroCell"];
        taxSavingIntroCell.selectionStyle = UITableViewCellSelectionStyleNone;
        taxSavingIntroCell.lblMainTitle.text = @"Stable Growth with Moderate Risk";
        taxSavingIntroCell.lblMainDesc.text = @"Invest equally across asset classes to provide a smooth investment experience.";
        taxSavingIntroCell.imgMainIcon.image = [UIImage imageNamed:@"weight-balance"];
        taxSavingIntroCell.lblWhyInvest.text = @"Why to Invest in Balanced Portfolio?";
        taxSavingIntroCell.img1.image = [UIImage imageNamed:@"wealth-creation"];
        taxSavingIntroCell.lblTitle1.text = @"A Perfect Diversity";
        taxSavingIntroCell.lblDesc1.text = @"Invest 50% in stocks and 50% in bonds providing a perfect diversification to your investments.";
        taxSavingIntroCell.img2.image = [UIImage imageNamed:@"chart"];
        taxSavingIntroCell.lblTitle2.text = @"Provide Risk-Adjusted Returns";
        taxSavingIntroCell.lblDesc2.text = @"Follow fairly tested strategies to provide generous returns for the risk associated with your investments.";
        taxSavingIntroCell.img3.image = [UIImage imageNamed:@"long-term-tenure"];
        taxSavingIntroCell.lblTitle3.text = @"Best for All Seasons";
        taxSavingIntroCell.lblDesc3.text = @"Equal investments in both asset classes, so show stability even during the fall in equity market.";
    }
    return taxSavingIntroCell;
    
    //return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnViewPortfolio_Tapped:(id)sender {
    BalancedPortfolioVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"BalancedPortfolioVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
