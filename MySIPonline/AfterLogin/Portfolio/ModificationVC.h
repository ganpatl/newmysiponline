//
//  ModificationVC.h
//  MySIPonline
//
//  Created by Ganpat on 13/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ModificationVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet UITableView *tblModification;

@end

NS_ASSUME_NONNULL_END
