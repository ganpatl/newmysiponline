//
//  TermOfUseCell.h
//  MySIPonline
//
//  Created by Ganpat on 13/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TermOfUseCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnTermOfUse;

@end

NS_ASSUME_NONNULL_END
