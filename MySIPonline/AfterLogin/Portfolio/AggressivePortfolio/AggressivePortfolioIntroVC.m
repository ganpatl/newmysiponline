//
//  AggressivePortfolioIntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/03/19.
//

#import "AggressivePortfolioIntroVC.h"
#import "AggressivePortfolioVC.h"

// Cells
#import "TaxSavingIntroCell.h"

@interface AggressivePortfolioIntroVC () <UITableViewDataSource> {
    TaxSavingIntroCell *taxSavingIntroCell;
    CAGradientLayer *theViewGradient;
}
@end

@implementation AggressivePortfolioIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"6AADFD"] CGColor], (id)[[UIColor colorWithHexString:@"4B85F2"] CGColor], (id)[[UIColor colorWithHexString:@"487DFA"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TaxSavingIntroCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TaxSavingIntroCell"];
    
    self.tblPortfolio.dataSource = self;
}

-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (taxSavingIntroCell == nil) {
        taxSavingIntroCell = [tableView dequeueReusableCellWithIdentifier:@"TaxSavingIntroCell"];
        taxSavingIntroCell.selectionStyle = UITableViewCellSelectionStyleNone;
        taxSavingIntroCell.lblMainTitle.text = @"High Risk Higher Profit";
        taxSavingIntroCell.lblMainDesc.text = @"Experience wealth growth at a much higher rate by taking some extra risk.";
        taxSavingIntroCell.imgMainIcon.image = [UIImage imageNamed:@"Aggressive-white"];
        taxSavingIntroCell.lblWhyInvest.text = @"Why to Invest in Aggressive Portfolio?";
        taxSavingIntroCell.lblTitle1.text = @"Perfect for Wealth Creation";
        taxSavingIntroCell.lblDesc1.text = @"Have a track of providing annual average returns of 15% and more, making it perfect for wealth creation.";
        taxSavingIntroCell.lblTitle2.text = @"Best for Long Term Tenure";
        taxSavingIntroCell.lblDesc2.text = @"Growth is really high, and as the investment tenure increases, the risk associated with the investment decreases.";
        taxSavingIntroCell.lblTitle3.text = @"Active Management Strategy";
        taxSavingIntroCell.lblDesc3.text = @"An active management of stocks helps in taking the full advantage of the equity market.";
    }
    return taxSavingIntroCell;
    
    //return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnViewPortfolio_Tapped:(id)sender {
    AggressivePortfolioVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"AggressivePortfolioVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
