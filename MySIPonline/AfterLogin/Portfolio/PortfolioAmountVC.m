//
//  PortfolioAmountVC.m
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import "PortfolioAmountVC.h"
#import "ModificationVC.h"
#import "InvestmentThanksVC.h"

// Cells
#import "PortfolioAmountCell.h"
#import "PortfolioFundsCell.h"
#import "TermOfUseCell.h"

@interface PortfolioAmountVC () <UITableViewDataSource, UITableViewDelegate, RKTagsViewDelegate> {
    PortfolioAmountCell *portfolioAmountCell;
    PortfolioFundsCell *portfolioFundsCell;
    TermOfUseCell *termOfUseCell;
    NSArray *aryAmountSuggested;
    NSArray *arySipDates;
}

@end

@implementation PortfolioAmountVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    aryAmountSuggested = @[@"1000", @"1500", @"2000"];
    arySipDates = @[@"1", @"2", @"3", @"4", @"5", @"6", @"7", @"8", @"9", @"10", @"11", @"12", @"13", @"14", @"15", @"16", @"17", @"18", @"19", @"20", @"21", @"22", @"23", @"24", @"25", @"26", @"27", @"28", @"29", @"30"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"PortfolioAmountCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PortfolioAmountCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"PortfolioFundsCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"PortfolioFundsCell"];
    [self.tblPortfolio registerNib:[UINib nibWithNibName:@"TermOfUseCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TermOfUseCell"];
    
    self.tblPortfolio.rowHeight = UITableViewAutomaticDimension;
    self.tblPortfolio.estimatedRowHeight = 2;
    self.tblPortfolio.dataSource = self;
    self.tblPortfolio.delegate = self;
}

#pragma mark - Tableview Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 3;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (indexPath.row == 0) {
        if (portfolioAmountCell == nil) {
            portfolioAmountCell = [tableView dequeueReusableCellWithIdentifier:@"PortfolioAmountCell"];
            
            // Setup Amount RKTagsView
            portfolioAmountCell.amountTagsView.tag = 10;
            [portfolioAmountCell.amountTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            portfolioAmountCell.amountTagsView.tagButtonHeight = 30;
            portfolioAmountCell.amountTagsView.tagBackgroundColor = UIColor.whiteColor;
            portfolioAmountCell.amountTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            portfolioAmountCell.amountTagsView.tintColor = [UIColor getAppColorDarkGrayText];
            portfolioAmountCell.amountTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            portfolioAmountCell.amountTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            portfolioAmountCell.amountTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            portfolioAmountCell.amountTagsView.editable = NO;
            portfolioAmountCell.amountTagsView.allowsMultipleSelection = NO;
            portfolioAmountCell.amountTagsView.lineSpacing = 8;
            portfolioAmountCell.amountTagsView.interitemSpacing = 10;
            portfolioAmountCell.amountTagsView.buttonCornerRadius = 4;
            portfolioAmountCell.amountTagsView.delegate = self;
            
            portfolioAmountCell.amountTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (portfolioAmountCell.amountTagsView.tags.count != self->aryAmountSuggested.count) {
                [portfolioAmountCell.amountTagsView removeAllTags];
                for (NSString *tag in self->aryAmountSuggested) {
                    [portfolioAmountCell.amountTagsView addTag:[NSString stringWithFormat:@"₹ %@", tag]];
                }
                // Set First item selected
                [portfolioAmountCell.amountTagsView selectTagAtIndex:0];
            }
            
            // Setup SIP Date RKTagsView
            portfolioAmountCell.sipDateTagsView.tag = 11;
            [portfolioAmountCell.sipDateTagsView setFont:[UIFont fontWithName:@"Roboto-Regular" size:14]];
            portfolioAmountCell.sipDateTagsView.tagButtonHeight = 30;
            portfolioAmountCell.sipDateTagsView.tagBackgroundColor = UIColor.whiteColor;
            portfolioAmountCell.sipDateTagsView.tagSelectedBackgroundColor = UIColor.whiteColor;
            portfolioAmountCell.sipDateTagsView.tintColor = [UIColor getAppColorDarkGrayText];
            portfolioAmountCell.sipDateTagsView.selectedTagTintColor = [UIColor getAppColorBlue_2];
            portfolioAmountCell.sipDateTagsView.tagBorderColor = [UIColor colorWithHexString:@"CFCFCF"];
            portfolioAmountCell.sipDateTagsView.tagSelectedBorderColor = [UIColor getAppColorBlue_2];
            portfolioAmountCell.sipDateTagsView.editable = NO;
            portfolioAmountCell.sipDateTagsView.allowsMultipleSelection = NO;
            portfolioAmountCell.sipDateTagsView.lineSpacing = 8;
            portfolioAmountCell.sipDateTagsView.interitemSpacing = 10;
            portfolioAmountCell.sipDateTagsView.buttonCornerRadius = 4;
            portfolioAmountCell.sipDateTagsView.delegate = self;
            portfolioAmountCell.sipDateTagsView.scrollView.showsHorizontalScrollIndicator = NO;
            if (portfolioAmountCell.sipDateTagsView.tags.count != self->arySipDates.count) {
                [portfolioAmountCell.sipDateTagsView removeAllTags];
                for (NSString *tag in self->arySipDates) {
                    [portfolioAmountCell.sipDateTagsView addTag:tag];
                }
                // Set First item selected
                [portfolioAmountCell.sipDateTagsView selectTagAtIndex:0];
            }
        }
        return portfolioAmountCell;
    
    } else if (indexPath.row == 1) {
        if (portfolioFundsCell == nil) {
            portfolioFundsCell = [tableView dequeueReusableCellWithIdentifier:@"PortfolioFundsCell"];
            [portfolioFundsCell.btnEdit addTarget:self action:@selector(btnEdit_Tapped) forControlEvents:UIControlEventTouchUpInside];
        }
        return portfolioFundsCell;
        
    } else if (indexPath.row == 2) {
        if (termOfUseCell == nil) {
            termOfUseCell = [tableView dequeueReusableCellWithIdentifier:@"TermOfUseCell"];
        }
        return termOfUseCell;
    }    
    
    return [UITableViewCell new];
}
#pragma mark - Tableview Delegate
-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return UITableViewAutomaticDimension;
}

#pragma mark - RKTagsView Delegate

-(BOOL)tagsView:(RKTagsView *)tagsView shouldSelectTagAtIndex:(NSInteger)index {
    if (tagsView.tag == 10) {
        // Amount
        portfolioAmountCell.tfAmount.text = aryAmountSuggested[index];
    } else if (tagsView.tag == 11) {
        // SIP Date
        
    }
    // NSDictionary *dic = aryCategory[index];
    //strInvesterId = dic[@"id"]; //name
    
    //NSMutableArray *dataparams = [[aryPopularSearches objectAtIndex:index] valueForKey:@"category_id"];
    //[NSUserData SaveCategoryIdAPI:dataparams];
    
    //SearchTableDataPage *svc = [self.storyboard instantiateViewControllerWithIdentifier:@"SearchTableDataPage"];
    //svc.SearchDataElement = cellText;
    //[self.navigationController pushViewController:svc animated:YES];
    return YES;
}

#pragma mark - Button Tap Event

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnMakePayment_Tapped:(id)sender {
    InvestmentThanksVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"InvestmentThanksVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

-(void)btnEdit_Tapped {
    ModificationVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"ModificationVC"];
    [self presentViewController:destVC animated:YES completion:nil];
}

@end
