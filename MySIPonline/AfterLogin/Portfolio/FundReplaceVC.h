//
//  FundReplaceVC.h
//  MySIPonline
//
//  Created by Ganpat on 14/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface FundReplaceVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *fund1View;
@property (weak, nonatomic) IBOutlet UIView *fund2View;
@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UIImageView *img2;


@end

NS_ASSUME_NONNULL_END
