//
//  PortfolioAmountCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import <UIKit/UIKit.h>
#import "RKTagsView.h"

NS_ASSUME_NONNULL_BEGIN

@interface PortfolioAmountCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UITextField *tfAmount;
@property (weak, nonatomic) IBOutlet RKTagsView *amountTagsView;
@property (weak, nonatomic) IBOutlet RKTagsView *sipDateTagsView;

@end

NS_ASSUME_NONNULL_END
