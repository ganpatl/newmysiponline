//
//  ModificationCell.h
//  MySIPonline
//
//  Created by Ganpat on 13/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface ModificationCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UIButton *btnUpdate;

@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName1;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount1;
@property (weak, nonatomic) IBOutlet UILabel *lblPer1;
@property (weak, nonatomic) IBOutlet UIButton *btnReplace1;

@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName2;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount2;
@property (weak, nonatomic) IBOutlet UILabel *lblPer2;
@property (weak, nonatomic) IBOutlet UIButton *btnReplace2;

@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (weak, nonatomic) IBOutlet UILabel *lblSchemeName3;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount3;
@property (weak, nonatomic) IBOutlet UILabel *lblPer3;
@property (weak, nonatomic) IBOutlet UIButton *btnReplace3;

@end

NS_ASSUME_NONNULL_END
