//
//  PortfolioFundsCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PortfolioFundsCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIButton *btnEdit;
@property (weak, nonatomic) IBOutlet UIView *shadowView;

@property (weak, nonatomic) IBOutlet UIImageView *img1;
@property (weak, nonatomic) IBOutlet UILabel *lblScheme1;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount1;
@property (weak, nonatomic) IBOutlet UILabel *lblPer1;
@property (weak, nonatomic) IBOutlet UILabel *lblReturn1;
@property (weak, nonatomic) IBOutlet UILabel *lblRisk1;
@property (weak, nonatomic) IBOutlet UILabel *lblFundSize1;
@property (weak, nonatomic) IBOutlet UIView *returnRiskView1;
@property (weak, nonatomic) IBOutlet UILabel *lblShowMore1;

@property (weak, nonatomic) IBOutlet UIImageView *img2;
@property (weak, nonatomic) IBOutlet UILabel *lblScheme2;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount2;
@property (weak, nonatomic) IBOutlet UILabel *lblPer2;
@property (weak, nonatomic) IBOutlet UILabel *lblReturn2;
@property (weak, nonatomic) IBOutlet UILabel *lblRisk2;
@property (weak, nonatomic) IBOutlet UILabel *lblFundSize2;
@property (weak, nonatomic) IBOutlet UIView *returnRiskView2;
@property (weak, nonatomic) IBOutlet UILabel *lblShowMore2;

@property (weak, nonatomic) IBOutlet UIImageView *img3;
@property (weak, nonatomic) IBOutlet UILabel *lblScheme3;
@property (weak, nonatomic) IBOutlet UILabel *lblAmount3;
@property (weak, nonatomic) IBOutlet UILabel *lblPer3;
@property (weak, nonatomic) IBOutlet UILabel *lblReturn3;
@property (weak, nonatomic) IBOutlet UILabel *lblRisk3;
@property (weak, nonatomic) IBOutlet UILabel *lblFundSize3;
@property (weak, nonatomic) IBOutlet UIView *returnRiskView3;
@property (weak, nonatomic) IBOutlet UILabel *lblShowMore3;

// Constraints

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnRisk1HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnRisk2HeightConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *returnRisk3HeightConstraint;

@end

NS_ASSUME_NONNULL_END
