//
//  ModificationVC.m
//  MySIPonline
//
//  Created by Ganpat on 13/03/19.
//

#import "ModificationVC.h"
#import "FundReplaceVC.h"

// Cell
#import "ModificationCell.h"

@interface ModificationVC () <UITableViewDataSource> {
    ModificationCell *modificationCell;
}

@end

@implementation ModificationVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [AppHelper addTopShadowOnView:self.topTitleView];
    
    [self.tblModification registerNib:[UINib nibWithNibName:@"ModificationCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"ModificationCell"];
    self.tblModification.dataSource = self;
}

#pragma mark - Tableview Data Source

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (modificationCell == nil) {
        modificationCell = [tableView dequeueReusableCellWithIdentifier:@"ModificationCell"];
        modificationCell.btnReplace1.tag = 1;
        modificationCell.btnReplace2.tag = 2;
        modificationCell.btnReplace3.tag = 3;
        [modificationCell.btnReplace1 addTarget:self action:@selector(btnReplace_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [modificationCell.btnReplace2 addTarget:self action:@selector(btnReplace_Tapped:) forControlEvents:UIControlEventTouchUpInside];
        [modificationCell.btnReplace3 addTarget:self action:@selector(btnReplace_Tapped:) forControlEvents:UIControlEventTouchUpInside];
    }
    return modificationCell;
    
    return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnClose_Tapped:(id)sender {
    [self dismissViewControllerAnimated:YES completion:nil];
}

-(void)btnReplace_Tapped:(UIButton*)sender {
    FundReplaceVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"FundReplaceVC"];
    [self presentViewController:destVC animated:YES completion:nil];
}

@end
