//
//  ModificationCell.m
//  MySIPonline
//
//  Created by Ganpat on 13/03/19.
//

#import "ModificationCell.h"

@implementation ModificationCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    _btnUpdate.layer.cornerRadius = 2;
    _btnUpdate.layer.masksToBounds = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
