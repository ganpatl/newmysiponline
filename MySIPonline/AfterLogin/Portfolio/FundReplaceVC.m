//
//  FundReplaceVC.m
//  MySIPonline
//
//  Created by Ganpat on 14/03/19.
//

#import "FundReplaceVC.h"

@interface FundReplaceVC ()

@end

@implementation FundReplaceVC

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    self.fund1View.layer.borderWidth = 1;
    self.fund1View.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.fund2View.layer.borderWidth = 1;
    self.fund2View.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.img1.layer.borderWidth = 1;
    self.img1.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.img2.layer.borderWidth = 1;
    self.img2.layer.borderColor = [UIColor getAppColorBorder].CGColor;
    self.img1.layer.cornerRadius = self.img1.frame.size.height / 2;
    self.img2.layer.cornerRadius = self.img2.frame.size.height / 2;
    
}

-(void)dismissViewController {
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - Button Tap Event


- (IBAction)btnClose_Tapped:(id)sender {
    [self dismissViewController];
}

- (IBAction)btnFund1_Tapped:(id)sender {

    [self dismissViewController];
}

- (IBAction)btnFund2_Tapped:(id)sender {

    [self dismissViewController];
}

@end
