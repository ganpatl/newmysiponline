//
//  TaxSaverIntroVC.m
//  MySIPonline
//
//  Created by Ganpat on 09/03/19.
//

#import "TaxSaverIntroVC.h"
#import "TaxSaverPortfolioVC.h"

#import "TaxSavingIntroCell.h"

@interface TaxSaverIntroVC () <UITableViewDataSource> {
    TaxSavingIntroCell *taxSavingIntroCell;
    CAGradientLayer *theViewGradient;
}

@end

@implementation TaxSaverIntroVC

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self.backGradientView setBackgroundColor:[UIColor clearColor]];
    theViewGradient = [CAGradientLayer layer];
    theViewGradient.frame = self.backGradientView.bounds;
    theViewGradient.colors = [NSArray arrayWithObjects:(id)[[UIColor colorWithHexString:@"FCB350"] CGColor], (id)[[UIColor colorWithHexString:@"FC964A"] CGColor], (id)[[UIColor colorWithHexString:@"FC7E44"] CGColor], nil];
    [self.backGradientView.layer insertSublayer:theViewGradient atIndex:0];
    
    [self.tblTaxSaving registerNib:[UINib nibWithNibName:@"TaxSavingIntroCell" bundle:[NSBundle mainBundle]] forCellReuseIdentifier:@"TaxSavingIntroCell"];
    
    self.tblTaxSaving.dataSource = self;
}


-(void)viewDidLayoutSubviews {
    theViewGradient.frame = self.backGradientView.bounds;
}

#pragma mark - TableView DataSource

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return 1;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    if (taxSavingIntroCell == nil) {
        taxSavingIntroCell = [tableView dequeueReusableCellWithIdentifier:@"TaxSavingIntroCell"];
        taxSavingIntroCell.selectionStyle = UITableViewCellSelectionStyleNone;
    }
    return taxSavingIntroCell;
    
    return [UITableViewCell new];
}

#pragma mark - Button Tap Events

- (IBAction)btnBack_Tapped:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)btnViewPortfolio_Tapped:(id)sender {
    TaxSaverPortfolioVC *destVC = [self.storyboard instantiateViewControllerWithIdentifier:@"TaxSaverPortfolioVC"];
    [self.navigationController pushViewController:destVC animated:YES];
}

@end
