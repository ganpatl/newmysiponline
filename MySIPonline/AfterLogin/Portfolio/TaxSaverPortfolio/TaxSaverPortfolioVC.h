//
//  TaxSaverPortfolioVC.h
//  MySIPonline
//
//  Created by Ganpat on 09/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TaxSaverPortfolioVC : UIViewController

@property (weak, nonatomic) IBOutlet UITableView *tblTaxSaving;
@property (weak, nonatomic) IBOutlet UIView *backGradientView;

@end

NS_ASSUME_NONNULL_END
