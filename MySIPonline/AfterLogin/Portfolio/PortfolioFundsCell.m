//
//  PortfolioFundsCell.m
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import "PortfolioFundsCell.h"

@implementation PortfolioFundsCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    [AppHelper addShadowOnView:self.shadowView Color:[UIColor lightGrayColor] OffetSize:CGSizeMake(0, 1) Opacity:1.0 Radius:1.0];
    
    //[self btnShowMore1_Tapped:nil];
    //[self btnShowMore2_Tapped:nil];
    //[self btnShowMore3_Tapped:nil];
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnShowMore1_Tapped:(id)sender {
    if (_returnRiskView1.hidden) {
        _lblShowMore1.text = @"Show Less";
        _returnRiskView1.hidden = NO;
        _returnRisk1HeightConstraint.constant = 42;
    } else {
        _lblShowMore1.text = @"Show More";
        _returnRisk1HeightConstraint.constant = 0;
        _returnRiskView1.hidden = YES;
    }
}

- (IBAction)btnShowMore2_Tapped:(id)sender {
    if (_returnRiskView2.hidden) {
        _lblShowMore2.text = @"Show Less";
        _returnRiskView2.hidden = NO;
        _returnRisk2HeightConstraint.constant = 42;
    } else {
        _lblShowMore2.text = @"Show More";
        _returnRisk2HeightConstraint.constant = 0;
        _returnRiskView2.hidden = YES;
    }
}

- (IBAction)btnShowMore3_Tapped:(id)sender {
    if (_returnRiskView3.hidden) {
        _lblShowMore3.text = @"Show Less";
        _returnRiskView3.hidden = NO;
        _returnRisk3HeightConstraint.constant = 42;
    } else {
        _lblShowMore3.text = @"Show More";
        _returnRisk3HeightConstraint.constant = 0;
        _returnRiskView3.hidden = YES;
    }
}

@end
