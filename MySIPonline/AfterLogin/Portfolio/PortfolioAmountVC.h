//
//  PortfolioAmountVC.h
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface PortfolioAmountVC : UIViewController

@property (weak, nonatomic) IBOutlet UIView *topTitleView;
@property (weak, nonatomic) IBOutlet TitleLabel *lblTitle;
@property (weak, nonatomic) IBOutlet UITableView *tblPortfolio;

@end

NS_ASSUME_NONNULL_END
