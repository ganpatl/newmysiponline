//
//  InvestmentModeCell.m
//  MySIPonline
//
//  Created by Ganpat on 09/03/19.
//

#import "InvestmentModeCell.h"

@implementation InvestmentModeCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.btnSip.layer.cornerRadius = 3;
    self.btnLumpsum.layer.cornerRadius = 3;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

- (IBAction)btnSip_Tapped:(id)sender {
    if (self.isLumpsumSelected) {
        
        self.isLumpsumSelected = NO;
    }
}

- (IBAction)btnLumpsum_Tapped:(id)sender {
    if (self.isLumpsumSelected == NO) {
        
        self.isLumpsumSelected = YES;
    }
}

@end
