//
//  LineChartCompareCell.h
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import <UIKit/UIKit.h>
@import Charts;

NS_ASSUME_NONNULL_BEGIN

@interface LineChartCompareCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblComparisonWith;
@property (weak, nonatomic) IBOutlet UILabel *lblThisPlanReturn;
@property (weak, nonatomic) IBOutlet UILabel *lblOtherPlanReturn;
@property (weak, nonatomic) IBOutlet UIButton *btnSelectOtherPlan;
@property (weak, nonatomic) IBOutlet UILabel *lblVs;
@property (weak, nonatomic) IBOutlet LineChartView *chartView;
@property (weak, nonatomic) IBOutlet UILabel *lblShowingPerformance;
@property (weak, nonatomic) IBOutlet UIView *thisPlanView;
@property (weak, nonatomic) IBOutlet UIView *otherPlanView;

@end

NS_ASSUME_NONNULL_END
