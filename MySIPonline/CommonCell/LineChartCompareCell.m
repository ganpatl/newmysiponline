//
//  LineChartCompareCell.m
//  MySIPonline
//
//  Created by Ganpat on 12/03/19.
//

#import "LineChartCompareCell.h"

@implementation LineChartCompareCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
    self.lblVs.layer.cornerRadius = self.lblVs.frame.size.height / 2;
    self.lblVs.layer.masksToBounds = YES;
    
    self.thisPlanView.layer.borderWidth = 1;
    self.thisPlanView.layer.borderColor = [UIColor colorWithHexString:@"CFCFCF"].CGColor;
    self.otherPlanView.layer.borderWidth = 1;
    self.otherPlanView.layer.borderColor = [UIColor colorWithHexString:@"CFCFCF"].CGColor;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
