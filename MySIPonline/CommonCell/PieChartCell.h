//
//  PieChartCell.h
//  MySIPonline
//
//  Created by Ganpat on 09/03/19.
//

#import <UIKit/UIKit.h>
#import "XYDoughnutChart.h"

NS_ASSUME_NONNULL_BEGIN

@interface PieChartCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet XYDoughnutChart *chartView;
@property (weak, nonatomic) IBOutlet UILabel *lblSlice1;
@property (weak, nonatomic) IBOutlet UILabel *lblSlice2;
@property (weak, nonatomic) IBOutlet UILabel *lblSlice3;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory1;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory2;
@property (weak, nonatomic) IBOutlet UILabel *lblCategory3;

@end

NS_ASSUME_NONNULL_END
