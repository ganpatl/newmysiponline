//
//  TrustedByCell.h
//  MySIPonline
//
//  Created by Ganpat on 09/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface TrustedByCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UIView *shadowView;
@property (weak, nonatomic) IBOutlet UILabel *lblTotalInvestor;
@property (weak, nonatomic) IBOutlet UILabel *lblRightTop;
@property (weak, nonatomic) IBOutlet UILabel *lblRightBottom;

@end

NS_ASSUME_NONNULL_END
