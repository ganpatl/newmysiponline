//
//  InvestmentModeCell.h
//  MySIPonline
//
//  Created by Ganpat on 09/03/19.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface InvestmentModeCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lblInvestmentMode;
@property (weak, nonatomic) IBOutlet UIButton *btnSip;
@property (weak, nonatomic) IBOutlet UIButton *btnLumpsum;

@property BOOL isLumpsumSelected;

@end

NS_ASSUME_NONNULL_END
