//
//  NSString+Extension.h
//  WoodenStreet
//
//  Created by User on 04/01/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSString (Utils)

-(NSString *)TrimSpace;
-(NSString *)PercentEscapesUsingUTF8;
-(NSString *)stringByDecodeHTMLCharacters;

@end
