//
//  NSString+Extension.m
//  WoodenStreet
//
//  Created by User on 04/01/18.
//  Copyright © 2018 Rahul Saini. All rights reserved.
//

#import "NSString+Extension.h"

@implementation NSString (Utils)

-(NSString *)TrimSpace {
    return [self stringByTrimmingCharactersInSet: [NSCharacterSet whitespaceAndNewlineCharacterSet]];
}

-(NSString *)PercentEscapesUsingUTF8 {
    return [self stringByAddingPercentEncodingWithAllowedCharacters:[NSCharacterSet URLFragmentAllowedCharacterSet]];
    //return [self stringByAddingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
}

-(NSString *)stringByDecodeHTMLCharacters {
    //NSString *htmlString = @"&#63743; &amp; &#38; &lt; &gt; &trade; &copy; &hearts; &clubs; &spades; &diams;";
    NSData *stringData = [self dataUsingEncoding:NSUTF8StringEncoding];
    NSDictionary *options = @{NSDocumentTypeDocumentAttribute:NSHTMLTextDocumentType};
    NSAttributedString *decodedString;
    decodedString = [[NSAttributedString alloc] initWithData: stringData
                                                     options: options
                                          documentAttributes: NULL
                                                       error: NULL];
    return [decodedString string];
}

@end
