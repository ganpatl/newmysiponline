//
//  HexColor.m
//
//  Created by Marius Landwehr on 02.12.12.
//  The MIT License (MIT)
//  Copyright (c) 2013 Marius Landwehr marius.landwehr@gmail.com
//
//  Permission is hereby granted, free of charge, to any person obtaining a copy of this software and associated documentation files (the "Software"), to deal in the Software without restriction, including without limitation the rights to use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software, and to permit persons to whom the Software is furnished to do so, subject to the following conditions:
//
//  The above copyright notice and this permission notice shall be included in all copies or substantial portions of the Software.
//
//  THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
//

#import "HexColor.h"

@implementation HXColor (HexColorAddition)

+ (HXColor *)colorWithHexString:(NSString *)hexString {
    return [[self class] colorWithHexString:hexString alpha:1.0];
}

+ (HXColor *)colorWithHexString:(NSString *)hexString alpha:(CGFloat)alpha {
    if (hexString.length == 0) {
        return nil;
    }

    // Check for hash and add the missing hash
    if ('#' != [hexString characterAtIndex:0]) {
        hexString = [NSString stringWithFormat:@"#%@", hexString];
    }

    // check for string length
    assert (7 == hexString.length || 4 == hexString.length);

    // check for 3 character HexStrings
    hexString = [[self class] hexStringTransformFromThreeCharacters:hexString];

    NSString *redHex = [NSString stringWithFormat:@"0x%@", [hexString substringWithRange:NSMakeRange (1, 2)]];
    unsigned redInt = [[self class] hexValueToUnsigned:redHex];

    NSString *greenHex = [NSString stringWithFormat:@"0x%@", [hexString substringWithRange:NSMakeRange (3, 2)]];
    unsigned greenInt = [[self class] hexValueToUnsigned:greenHex];

    NSString *blueHex = [NSString stringWithFormat:@"0x%@", [hexString substringWithRange:NSMakeRange (5, 2)]];
    unsigned blueInt = [[self class] hexValueToUnsigned:blueHex];

    HXColor *color = [HXColor colorWith8BitRed:redInt green:greenInt blue:blueInt alpha:alpha];

    return color;
}

+ (HXColor *)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue {
    return [[self class] colorWith8BitRed:red green:green blue:blue alpha:1.0];
}

+ (HXColor *)colorWith8BitRed:(NSInteger)red green:(NSInteger)green blue:(NSInteger)blue alpha:(CGFloat)alpha {
    HXColor *color = nil;
#if (TARGET_IPHONE_SIMULATOR || TARGET_OS_IPHONE)
    color = [HXColor colorWithRed:(float)red / 255 green:(float)green / 255 blue:(float)blue / 255 alpha:alpha];
#else
    color = [HXColor colorWithCalibratedRed:(float)red / 255 green:(float)green / 255 blue:(float)blue / 255 alpha:alpha];
#endif

    return color;
}

+ (NSString *)hexStringTransformFromThreeCharacters:(NSString *)hexString {
    if (hexString.length == 4) {
        hexString = [NSString stringWithFormat:@"#%@%@%@%@%@%@",
                   [hexString substringWithRange:NSMakeRange (1, 1)], [hexString substringWithRange:NSMakeRange (1, 1)],
                   [hexString substringWithRange:NSMakeRange (2, 1)], [hexString substringWithRange:NSMakeRange (2, 1)],
                   [hexString substringWithRange:NSMakeRange (3, 1)], [hexString substringWithRange:NSMakeRange (3, 1)]];
    }
    return hexString;
}

+ (unsigned)hexValueToUnsigned:(NSString *)hexValue {
    unsigned value = 0;
    NSScanner *hexValueScanner = [NSScanner scannerWithString:hexValue];
    [hexValueScanner scanHexInt:&value];
    return value;
}

#pragma mark - Custom Color Used in APP

+ (HXColor *)getAppColorBlue {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"0D6FF4" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorBlue_2 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"4285F4" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorBackground {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"EEEEEE" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorLightBlue {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"F1F9FF" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorBorder {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"E5E5E7" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorGrayText {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"959595" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorDarkGrayText {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"454545" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppColorTitleText {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"2D2D2D" alpha:1.0];
    }
    return color;
}

#pragma mark - Gradient Colors

+ (HXColor *)getAppTaxGradientColor1 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"FCB350" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppTaxGradientColor2 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"FC7E44" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppAggressiveGradientColor1 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"6AADFD" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppAggressiveGradientColor2 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"487DFA" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppBalanceGradientColor1 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"47C8DE" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppBalanceGradientColor2 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"0AA7A1" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppSafeGradientColor1 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"AB7AEE" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppSafeGradientColor2 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"753CDB" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppDynamicGradientColor1 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"FFAABD" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppDynamicGradientColor2 {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"E7405B" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppAlertSuccessColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"34A853" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppAlertDangerColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"EB4335" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppRiskHighColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"D44C4C" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppRiskLowColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"83C605" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppRiskModerateColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"ECB004" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppRiskModerateLowColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"BCD103" alpha:1.0];
    }
    return color;
}

+ (HXColor *)getAppRiskModerateHighColor {
    static UIColor *color = nil;
    if (color == nil) {
        color = [[self class] colorWithHexString:@"DF873A" alpha:1.0];
    }
    return color;
}
@end
