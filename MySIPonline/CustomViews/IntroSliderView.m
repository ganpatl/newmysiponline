//
//  IntroSliderView.m
//  MySIPonline
//
//  Created by Ganpat on 13/11/18.
//

#import "IntroSliderView.h"

@implementation IntroSliderView

-(void)awakeFromNib {
    [super awakeFromNib];
    self.lblTitle.textColor = [UIColor getAppColorTitleText];
    self.lblDesc.textColor  = [UIColor getAppColorGrayText];
}

@end
