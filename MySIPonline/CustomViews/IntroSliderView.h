//
//  IntroSliderView.h
//  MySIPonline
//
//  Created by Ganpat on 13/11/18.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface IntroSliderView : UIView

@property (weak, nonatomic) IBOutlet UIImageView *imgMain;
@property (weak, nonatomic) IBOutlet UILabel *lblTitle;
@property (weak, nonatomic) IBOutlet UILabel *lblDesc;


@end

NS_ASSUME_NONNULL_END
