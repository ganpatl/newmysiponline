//
//  NIDropDownDict.h
//  MySIPOnline
//
//  Created by Rahul Saini on 18/02/17.
//  Copyright © 2017 Rahul Saini. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIDropDownDict;
@protocol NIDropDownDictDelegate
- (void) niDropDownDictDelegateMethod: (NIDropDownDict *) sender;
@end

@interface NIDropDownDict : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
    UIView *superView;
}
@property (nonatomic, retain) id <NIDropDownDictDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
@property (nonatomic, strong) NSMutableDictionary *selectedDictionary;
-(void)hideDropDown:(UIButton *)b InView:(UIView*) inView;
-(void)hideDropDown;
- (id)showDropDown:(UIButton *)b Label:(UILabel*)label Height:(CGFloat *)height Array:(NSArray *)arr ImageArray:(NSArray *)imgArr Direction:(NSString *)direction InView:(UIView*)inView;


@end
