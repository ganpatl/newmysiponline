//
//  NIDropDownDict.m
//  MySIPOnline
//
//  Created by Rahul Saini on 18/02/17.
//  Copyright © 2017 Rahul Saini. All rights reserved.
//

#import "NIDropDownDict.h"
#import "QuartzCore/QuartzCore.h"

@interface NIDropDownDict ()
{
    NSString *keyValue;
}

@property(nonatomic, strong) UITableView *table;
@property(nonatomic, strong) UIButton *btnSender;
@property(nonatomic, strong) UILabel *labelSender;
@property(nonatomic, retain) NSArray *list;
@property(nonatomic, retain) NSArray *imageList;
@end

@implementation NIDropDownDict
@synthesize table;
@synthesize btnSender;
@synthesize labelSender;
@synthesize list;
@synthesize imageList;
@synthesize delegate;
@synthesize animationDirection;



- (id)showDropDown:(UIButton *)b Label:(UILabel *)label Height:(CGFloat *)height Array:(NSArray *)arr ImageArray:(NSArray *)imgArr Direction:(NSString *)direction InView:(UIView *)inView{
    
    
    keyValue = [NSUserData GetKeyValue];
    
    btnSender = b;
    labelSender = label;
    animationDirection = direction;
    self.table = (UITableView *)[super init];
    if (self) {
        // Initialization code
        //        CGRect btn = b.frame;
        superView = inView;
        self.list = [NSArray arrayWithArray:arr];
        self.imageList = [NSArray arrayWithArray:imgArr];
        
        CGRect frame = [b convertRect:b.bounds toView:inView];
        
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, -5);
        }else if ([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(frame.origin.x, frame.origin.y+frame.size.height, frame.size.width, 0);
            self.layer.shadowOffset = CGSizeMake(-5, 5);
        }
        
        self.layer.masksToBounds = NO;
        self.layer.cornerRadius = 8;
        self.layer.shadowRadius = 5;
        self.layer.shadowOpacity = 0.5;
        
        table = [[UITableView alloc] initWithFrame:CGRectMake(0, 0, frame.size.width, 0)];
        table.delegate = self;
        table.dataSource = self;
        table.layer.cornerRadius = 5;
        // table.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
        table.backgroundColor = [UIColor whiteColor];
        table.separatorStyle = UITableViewCellSeparatorStyleSingleLine;
        table.separatorColor = [UIColor lightGrayColor];
        
        [UIView beginAnimations:nil context:nil];
        [UIView setAnimationDuration:0.5];
        
        if ([direction isEqualToString:@"up"]) {
            self.frame = CGRectMake(frame.origin.x, frame.origin.y-*height, frame.size.width, *height);
        } else if([direction isEqualToString:@"down"]) {
            self.frame = CGRectMake(frame.origin.x, frame.origin.y+frame.size.height, frame.size.width, *height);
        }
        table.frame = CGRectMake(0, 0, frame.size.width, *height);
        [UIView commitAnimations];
        [inView addSubview:self];
        [self addSubview:table];
    }
    return self;
}

-(void)hideDropDown:(UIButton *)b InView:(UIView *)inView {
    
    CGRect frame = [b convertRect:b.bounds toView:inView];
    
    [UIView beginAnimations:nil context:nil];
    [UIView setAnimationDuration:0.5];
    if ([animationDirection isEqualToString:@"up"]) {
        self.frame = CGRectMake(frame.origin.x, frame.origin.y, frame.size.width, 0);
    }else if ([animationDirection isEqualToString:@"down"]) {
        self.frame = CGRectMake(frame.origin.x, frame.origin.y+frame.size.height, frame.size.width, 0);
    }
    table.frame = CGRectMake(0, 0, frame.size.width, 0);
    [UIView commitAnimations];
}

-(void)hideDropDown{
    self.frame = CGRectMake(0, 0, 0, 0);
    table.frame = CGRectMake(0, 0, 0, 0);
    [UIView commitAnimations];
    [self myDelegate];
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
    return 1;
}

-(CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
    return 30;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return [self.list count];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *CellIdentifier = @"Cell";
    
    UITableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        cell = [[UITableViewCell alloc] initWithStyle:UITableViewCellStyleDefault reuseIdentifier:CellIdentifier];
        cell.textLabel.font = [UIFont systemFontOfSize:11];
        cell.textLabel.textAlignment = NSTextAlignmentLeft;
    }
    if ([self.imageList count] == [self.list count]) {
        cell.textLabel.text =[[list objectAtIndex:indexPath.row] objectForKey:keyValue];
        cell.imageView.image = [imageList objectAtIndex:indexPath.row];
    } else if ([self.imageList count] > [self.list count]) {
        cell.textLabel.text =[[list objectAtIndex:indexPath.row] objectForKey:keyValue];
        if (indexPath.row < [imageList count]) {
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        }
    } else if ([self.imageList count] < [self.list count]) {
        cell.textLabel.text =[[list objectAtIndex:indexPath.row] objectForKey:keyValue];
        if (indexPath.row < [imageList count]) {
            cell.imageView.image = [imageList objectAtIndex:indexPath.row];
        }
    }
    
    cell.textLabel.textColor = [UIColor grayColor];
    
    UIView * v = [[UIView alloc] init];
    v.backgroundColor = [UIColor whiteColor];
    cell.selectedBackgroundView = v;
    // cell.backgroundColor = [UIColor colorWithRed:0.9 green:0.9 blue:0.9 alpha:1];
    cell.backgroundColor = [UIColor whiteColor];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self hideDropDown:btnSender InView:superView];
    
    UITableViewCell *c = [tableView cellForRowAtIndexPath:indexPath];
    //    [btnSender setTitle:c.textLabel.text forState:UIControlStateNormal];
    //labelSender.text = c.textLabel.text;
    labelSender.text = [[list objectAtIndex:indexPath.row] objectForKey:keyValue];
    for (UIView *subview in btnSender.subviews) {
        if ([subview isKindOfClass:[UIImageView class]]) {
            [subview removeFromSuperview];
        }
    }
    imgView.image = c.imageView.image;
    imgView = [[UIImageView alloc] initWithImage:c.imageView.image];
    imgView.frame = CGRectMake(5, 5, 25, 25);
    [btnSender addSubview:imgView];
    _selectedDictionary = [[NSMutableDictionary alloc] initWithDictionary:[self.list objectAtIndex:indexPath.row]];
    [self myDelegate];
}

- (void) myDelegate {
    [self.delegate niDropDownDictDelegateMethod:self];
}

-(void)dealloc {
    //    [super dealloc];
    //    [table release];
    //    [self release];
}

@end
