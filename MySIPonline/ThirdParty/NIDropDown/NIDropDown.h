//
//  NIDropDown.h
//  NIDropDown
//
//  Created by Bijesh N on 12/28/12.
//  Copyright (c) 2012 Nitor Infotech. All rights reserved.
//

#import <UIKit/UIKit.h>

@class NIDropDown;
@protocol NIDropDownDelegate
- (void) niDropDownDelegateMethod: (NIDropDown *) sender;
@end

@interface NIDropDown : UIView <UITableViewDelegate, UITableViewDataSource>
{
    NSString *animationDirection;
    UIImageView *imgView;
    UIView *superView;
}
@property (nonatomic, retain) id <NIDropDownDelegate> delegate;
@property (nonatomic, retain) NSString *animationDirection;
@property (nonatomic, strong) NSMutableDictionary *selectedDictionary;
@property (nonatomic) NSInteger selectedIndex;
- (void)hideDropDown:(UIButton *)b InView:(UIView*) inView;
- (void)hideDropDown;
- (id)showDropDown:(UIButton *)b Label:(UILabel*)label Height:(CGFloat *)height Array:(NSArray *)arr ImageArray:(NSArray *)imgArr Direction:(NSString *)direction InView:(UIView*)inView;
@end
